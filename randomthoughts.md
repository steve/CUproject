## Thoughts on being an imprint, in no particular order

Encourage authors to publish under CC-BY-SA. Anyone can reproduce and/or remix the material, and even charge for it, but cannot prevent others from doing the same.

Sell digital copies under a pay-what-you-want arragement. Experiment with public ways of thanking buyers (within publications?). Consider a subscription/sponsor model (roughly what NPR does, except without the coporate sponsors). Consider selling books long before they're completed. Consider a serialized or condensed version of long works. Consider compilations of short works.

Encourage and even support printers printing our books. Make it easy for them to pay us -- it's not required, but we'll promote those who do. Help those who want to convert them to different formats, like podcasts, or translate them into other languages. Accept FairCoin.

Promote the use of free software. Develop git for prose (and rework its terminology).

Standardize on "A" sizing (A3, A4, etc.), at least as a default.

Partner with local and/or co-op bookstores to market the books. Build connections between local printers and local sellers. Partner with organizations connected with the content. Consider printed material beyond books (booklets, fliers, posters, postcards, business cards, greeting cards, stickers, magazines, tabloids, etc.)

Focus on quality. Write well, proofread well. Create paper material people won't want to recycle.

Actively encourage remixes. Look around for other BY-SA material to integrate, especially photos.

Develop alternative (paying) uses of the content, like conference appearances. 