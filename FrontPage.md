---
format: Markdown
toc: no
...

# The Common Understanding Project

Welcome to the Common Understanding Project. This is a source of books on the solidarity economy, especially worker cooperatives. Many are works in progress, but we are making them available even before they are complete.

This a pay-what-you-like bookstore. The text is under a [Creative Commons Attribution-ShareAlike](https://creativecommons.org/licenses/by-sa/4.0/) license. You can repurpose it and even resell it, but you can't prevent others from doing the same with your work, and you can't take credit for the work of others.

[BAUEN: The Workers' Hotel](bauen)

[The Cooperative Man: Arizmendiarrieta's Thought](CoopMan)

