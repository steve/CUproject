## The closure

“We were very happy about the World Cup, without knowing what was happening around us. We only saw the personalities who paraded by the hotel, while outside, people were being killed,” remembers Arminda Palacios, one of the members of the cooperative who worked at the hotel since its founding, or more precisely, a month prior to its opening.

Arminda's memory and that of other hotel workers takes on another dimension in the present. Currently, the BAUEN hotel (the name of the cooperative means *Buenos Aires, A National Business*[^26]) is a symbol of social struggles, the place where organizations in conflict meet, where mobilizations are proposed and discussed, congresses and union meetings are held, and political parties and academic groups get together. The recovered hotel is the place where the representatives of recovered businesses from different parts of the country converge when they come to Buenos Aires. The BAUEN is the place to stay for workers from the former Zanon (current FASINPAT), from Textiles Pigüé, from recovered businesses in Rosario, Cordoba, or Mendoza. It is were the Metrodelegados of the Buenos Aires Subway organized when they confronted the UTA union bureaucracy, and where so many political debates have been held that it would be difficult to list them.

[^26]: To distinguish the cooperative of private business of the Iurcoviches, we use "Bauen" for the company and "BAUEN" for the cooperative.

But in those years, the BAUEN was something very different, a symbol of the policy of the dominant classes, a hotel where the powerful, rich, and famous met. Being close to the theaters and entertainment venues on Corrientes avenue made it an optimum site for them to frequent the farándula, as well as characters from the Buenos Aires elite, businesspeople, functionaries of the governments from Videla to Menem all mixed with the tourists with the greatest buying power of any who came to the Argentine capital. Olmedo and Porcel filmed several of their movies there, and it was also be the headquarters of the official launch of different candidacies, especially during Menem's early years, when the hotel was in its splendor.

At the beginning of the '90s, the hotel started to see changes: the business “outfitted the fourth floor of the hotel with offices for rent, to attract Chilean executives coming to do business. And the auditoriums of the hotel were transformed into the site of political meetings of every stripe.”[^27] The Iurcoviches' Bauen was, according to researcher Alberto Bonnet,[^28] one of the places where Menem materialized his frivilous and consumerist ideology: “The nocturnal scenes of meetings in the fashionable nightclubs and restaurants—New York City, Trump's, El Cielo, Hippopotamus, Fechoría —in the most exclusive five-star suites of the Alvear or Bauen hotels,” are among the elements that he points out were part of the Menemist cultural climate during his time in power in the '90s.

Arminda left the hotel in 1983, but returned ten years later, still under the Iurcoviches as bosses. As was to be expected, those ten years outside of the establishment caused her to lose seniority. But, her co-workers who had stayed at the BAUEN would share the same fate. The business soon would begin to take evasive maneuvers against its social security obligations, which is a standard part of asset stripping. María Eva Lossada, hotel worker and, as of the date of this publication, chairwoman of the BAUEN cooperative, tells how since the middle of the '90s, “there were changes to the registered name to evade taxes and avoid paying seniority, social expenses, and vacations for the staff.” Her arrival at the hotel in 1994 was marked by a fact that impacted her on her first workday: “there was a group of people that had been fired, around twenty, and they were gathered in the doorway of the hotel.” As a punitive warning, making the replacements enter with the fired workers in full view was no small thing.

In María Eva, however, the result was not what the bosses expected.

[^27]: “Chilean businessmen bought the hotel Bauen,” in daily *Clarín*, 13 of March of 1997. Recovered from http://edant.clarin.com/daily/1997/03/13/or-02901d.htm
[^28]: Alberto Bonnet, The hegemony Menemist: neo-conservatism in Argentina, 1989-2001. Bs. As.: Prometheus books, 1997.

The pressure on workers becomes more and more intense. As with the entire labor market at the time, conditions in the hotel and food industries were ripe for deteriorating standards and labor precarity. Workers had to accept whatever came their way, María Eva continues, “because, like he said, ‘there are 50 people outside waiting to replace you.’ They also started to outsource activities.” The number of workers was reduced to “a handful of 70 people who belonged to the hotel,” and the rest of the employees started to be assigned to outsourced areas, like “the laundry, the public areas, the bar -- the majority belonged to other management.” Iurcovich applied the manual on creating precarity to the letter, expelling workers, taking on new ones in worse conditions, changing the registered name to avoid the accumulation of seniority, and outsourcing divisions to move employees from the hotel payroll to working for contractors who were really subsidiaries of their own business group (see box).

### The sale to Solari

Frequent changes to the registered name were a lead-up to the sale of the hotel to a Chilean businessman named Solari. It was the end of an ever-more obvious decline, and coincided with the collapse of Buenos Aires as hotel showcase, as another product of the economic policy that was sinking into recession and making the city very expensive in dollars for tourists. Competition from big international chains that were starting to show up in the country, with more modern and luxurious facilities, began to push the Bauen out of the center of the hotel scene.

At the beginning of 1997[^29], the tower at Callao 360 finally wound up in the hands of the “Chilean Solari group,” while the Iurcoviches held on to the BAUEN Suite and its other businesses. The sale, carried out February 24, 1997, was for twelve million peso-dollars, although in the end, Solari would only pay four million. Like everything related to the group's business, there is a certain lack of clarity in this operation.

At first glimpse, it looks like a skillful move by Iurcovich, who was getting out from under a hotel that, as he himself would admit to *Clarín*, needed updates and was losing market, which was due –in a curious self-forgiveness– “to wear from the passage of time and the lack of investments.”[^30] According to the same article, the BAUEN had already been relegated to the third category in the Argentine hospitality ranking by that time, working “with contingents of Brazilian and Chilean tourists who buy packages for well below the 145 pesos for double occupancy at the official rate.” Still, it is debatable whether this a matter of business acumen: the “Chilean Solari group” that Clarín mentions was not a group, but an individual, Félix Santiago Solari Morello, who was coming off several restaurant failures on the other side of the [*Andean*] mountain range. The only thing he shared with the real Solari group, which owns the famous Falabella chain, was his last name (see box). The question that remains is whether Félix Solari really could have closed a deal of such magnitude.

[^29]: “Chilean businessmen bought the hotel Bauen,” ob. cit.
[^30]: Id.

For workers, the change in boss was a wake-up call that marked the beginning of the end. In María Eva's words:

> In '97, it was reported to us that this part was going to be sold, that the Bauen Tower was going to be divided from the Bauen Suite.[^31] They offered us severance money that was far less than what we were due. I said that I was going to think about it, and they answered me that I had until tomorrow, and that if I wanted to file suit against them, I might win after five or six years, and 'if you were going to be paid, you would be paid in installments.' I consulted with my husband (who was working at Polytechnics, which also belonged to Iurcovich) and we decided to sign up to file suit. But they made me sign before a scribe where I accepted all the proposed terms. Others made arrangements through the union. This served to give me more knowledge of our rights. I began to realize that doing my job in the best way was no guarantee of keeping it, that we could end up on the street at any time, that we were just numbers to them, we weren't people.

In the midst of the economic recession the country was going through, things got harder for the business in Solari's hands, and the prices of the rooms dropped from 120 dollars to around 70.[^32] Things were not going as expected, and Menem and Cavallo's convertibility model was winding down.

[^31] The Bauen hotel we refer to in this book was also known as the Bauen Tower, a marketing name the Iurkoviches used to distinguish it from the Bauen Suite, the fantasy name for the hotel they still maintain on Callao avenue.

[^32]: Santiago Or’Donnell, ob. cit.

December 7, 1998, the BAUEN was selected by the Alliance between Radical Civic Unity and FREPASO to proclaim the presidential slate of Fernando de la Rúa and Carlos “Chacho” Álvarez, which would ultimately win the election in 1999. In those days, the hotel was also the site of meetings between a number of hierarchs in the *Justicialista* Party of Buenos Aires, who frequented its facilities so often they ended up becoming known as the Bauen Group.[^33] Meanwhile, Solari contributed a few worker dismissals from the hotel to the unemployment statistics, and also reduced the hotel to the category of four stars. As if this was not enough, he began to accumulate debt for ABL to the GCBA, which, in 2005, was estimated at five million pesos. The decline was conspicuous.

For the business owners in the hotel sector, the summer of 1999 was the worst in ten years. The devaluation of the *real* stopped the flow of Brazilian tourists to Buenos Aires, and even factoring in that January and February are considered the low season in the city, it was still expected to be an unusually difficult year.[^34]

### The Bauen closes its doors

The Bauen managed by Solari reached a crisis, and Iurcovich himself forced him to present a call of creditors (the first step in the mechanism in the Bankruptcy Law to resolve business insolvencies). A lien was placed by judge Rodolfo Herrera of the National Court on Civil and Commercial Affairs, no. 3, Sec. 5, which took 20% of the hotel's earnings. That put a financial chokehold on Solari, depriving him of the cash he needed to comply with his obligations to his creditors. That is how Solari SA's legal agent, Gabriel Jaijel, explained it: “It was an emergency measure caused by a financial chokehold. Now, my client's wish is to come to an agreement with his creditors for liabilities of around eight million pesos.”

The political and economic situation of the country was deteriorating rapidly with the government of the Alliance, whose crisis was accelerated when the Vice-President, Carlos Chacho Álvarez, resigned in October of 2000 in the fallout from the bribery scandal in the National Senate known as the “Banelco Law,” a law that made work precarious and caused the destruction of labor rights. With bankruptcy decreed on February 22, 2001, management of the BAUEN passed into the hands of a trusteeship. “A trustee started to manage us. We made 50 pesos per week,” María Eva recalls. “By February of 2001, a trustee took charge of the hotel,” relates Horacio Lalli, another worker. “Two trustees really—one was an accountant, and the other I don't remember very well now, but I think he was a lawyer, and they carried out all the the managerial part of the hotel.”

[^33]: In the group Bauen there was leaders like Felipe Solá, Alberto Pierri and Luis Barrionuevo. For example, see: “The duhaldismo stays to to put together their list and the ‘Grupo Bauen’ endurece positions,” in daily The Day, 1 of February of 1999. Recovered from http://past.eldia.com/editions/19990201/laciudad0.html
[^34]: Silvina Heguy, “Hotels of the Capital: the worst summer in 10 years,” in daily *Clarín*, 21 of February of 1999. Recovered from http://edant.clarin.com/daily/1999/02/21/or-02501d.htm

Another worker, Gladys Alegre, maintains that the trusteeship did nothing to stop asset-stripping: among other less obvious things, the furniture of the tower became part of the assets of the Bauen Suite. As Horacio points out, the fate of the Bauen business was, “as of that point, a chronicle of an announced death.” And Marcelo Ruarte, another storied cooperative member and its first president, reinforces the idea: “we were expecting the final blow, because, to tell the truth, there was no longer any movement. I mean, we knew that the hotel would be closing at any moment.”

The decline of the Bauen paralleled the decline of the country. The year 2001 was full of disastrous measures, starting with the ruinous business of the “Megaswap” of external debt, negotiated by (among others) Federico Sturzenegger, president of the Central Bank fifteen years later with Mauricio Macri. There was also the brutal attempt at adjustment in education that marked the brief tenure of Ricardo López Murphy in the Ministry of Economics, which was repudiated by enormous mobilizations of students and teachers. And then there was the resulting return to the economic guidance of the father of the whole crumbling mess, the creator of convertibility, Domingo Cavallo. In July of 2001, the newspapers reported that unemployment had reached 16.4%, a figured only exceeded by the 17.3% registered in 1996. Together with the announcement of “zero deficit” as an economic goal came a 13% cut to pensions and salaries of state employees, and a series of measures that were more and more unpopular, while things were getting worse: the pickets of the unemployed, mobilizations by different social and political sectors, and the widespread sense, soon corroborated by facts, that the country was picking up pace as it walked towards the abyss.

De la Rua resigned in December of 2001, when the economic measures imposed by Cavallo—especially “the *corralito*” that confiscated deposits and mainly affected the middle class, and, in a domino effect, the poorest sectors—were not able stop the crisis that led to the massive protests of December of 2001, for which the state of emergency decreed by the President himself served as the detonator. The social uprising and the unprecedented institutional crisis combined an enormous wave of looting by hungry crowds in suburban neighborhoods and in many cities with a massive mobilization of the middle and working classes in the capital shouting “Out with all of them!” It also forced the resignation, first of Domingo Cavallo (the night of December 19) and, several hours hours and 35 deaths later, of Fernando de la Rúa, who fled the Pink House [*Presidential residence*] in a helicopter flight that remains engraved in the tragic memory of the country.

Rodríguez Saá (the Peronista governor of the province of San Luis, who also learned how to weave alliances in the BAUEN around the year 2000), momentarily in charge of the presidency, declared that the country had entered into default with international credit organizations the 23rd of December. A short time later, he, in turn, left the government, in the rapid and chaotic succession of five presidents, culminating in the inauguration of Eduardo Duhalde, who had lost the elections of 1999 to the same De la Rua.

In the middle of this traumatic and unprecedented situation, the BAUEN closed its doors the 28th of December, like a bitter April Fools' Day joke. “I told a *companero*, ‘at least we have work,’" María Eva says. "How naive I was! Because, on the 28th of December of 2001, they threw us all out without severance and without paying our wages, or vacations, or anything.”

In spite of all the indications of the general state of the country and of the accelerated decline of the business, the workers were left in a state of profound helplessness and a certain bewilderment by the loss of their jobs. “They were taking out comforters, curtains, beds, etc., a bunch of things, and we were saying, "what are we going to do?" We can't do anything. We can only go to the Trustee and tell him not to let them take any else, because they were stripping the place,” is how María Eva recounts it.

The 28th of December, workers were clear they were unemployed as of that day. The climate was one of dismay, a sadness that seemed to have no possible solution, either inside or outside the doors of the hotel. Gladys recalls that, “on the 28th, we already knew about closure of the hotel. So, I left my house at six in the morning, because we started at seven. We arrived. The head of housekeeping made us make up the rooms that had to be made up. Some had already been dismantled.” This is not very different from what Marcelo lived through, having gone to work like every day, but that “each time we finished our shift, we went by the linens department to turn in our clothes.”

The final image comes from María Eva: “We greeted the 28th as if it was just another day, and then we were gripped by sadness. I didn't want to leave. I was on the eighth floor. I didn't want to leave.”

The scam was consumated, the BAUEN had been stripped, its workers fired without severance or any semblance of their rights being respected. Everything was disarray and hopelessness for them, and they walked out the door of the hotel convinced they would never walk back in, and that they were beginning a very difficult stage of their lives.

### Solari

Félix Santiago Solari Morello doesn't have anything to do with the Solari family (except perhaps his anti-communism and his sympathy for the dictatorship of Augusto Pinochet),[^35] prominent members of the bourgeoisie of Chile. Together with the Cúneo and Del Río families[^36], they control the ownership of the chain of Falabella businesses, which arrived in Argentina in 1993. It is probable that this happy coincidence of last name and nationality did not awaken suspicions in anyone who learned that, four years later, the BAUEN hotel passed into the hands of “the Chilean Solari group,” as the newspaper *Clarín*[^37] reported. Félix, who was fleeing lawsuits in his country, also came from another economic sector, food. Even after his failed hotel management, he continues in the same activity through the use of the Niña Bonita grill, which is located around the corner from the BAUEN, on Corrientes avenue[^38]. Another version, published in *Página/12*, maintains that Solari is not the one who exploits the grill but is, rather, exploited in it, which is to say, is employed.[^39] In any case, Solari's solvency when he signed the contracts with the Iurcoviches does not seem to have been guaranteed by anything more than his coincidental last name and national origin. Still, the Iurcoviches do not appear be very worried by these financial aspects as they close multi-million-peso agreements, if the dubious deal they made with Mercoteles is any indication.

[^35]: See: https://www.Facebook.com/felixsolarimorello
[^36]: “Carlo Solari assumes presidency of Falabella in the replacement of Juan Cúneo,” in daily The Third, 29 April 2014. Recovered from http://www.latercera.com/news/businesses/2014/04/655-576073-9-carlo-solari-assumes-presidency-of-falabella-in-replacement-of-juan-cuneo.shtml
[^37]: “Chilean businessmen bought the BAUEN hotel,” ob. cit.
[^38]: In the portal FineRestaurantFinder figures as general manager. See: http://www.finerestaurantfinder.com/restaurant/226498734057018/Parrilla+Neither%C3%B1a+Bonita, (consulted the 18/2/17).
[^39]: Santiago Or’Donnell, ob. cit.

### Iurchovich's companies

According to an investigation by journalist Guillermo Berasategui, the Iurcovich group is composed of almost 30 firms, several of which are simply name changes (changes to legal names) of existing firms, to facilitate certain evasive or fraudulent maneuvers. The flagship company is the Bauen hotel.

Original group businesses:

- Polytechnics (hospitality supplies)
- Industrial Maintenance and Services Company (new name of Polytechnics)

Hotel Bauen and derived or connected businesses:

- Bauen SA
- Bauen SACIC
- Bauen SRL (which operated the hotel through 1992)
- Finvercon (the legal advisers of this enterprise were sons of Martínez de Hoz and Mariano Grondona)
- Wells Fargo (bank that gave loans; bought Finvercon in 1997)
- Colomba Viajes (received a credit of U$S1,100,000 from Finvercon)
- Divelux (works in the Polytechnics office; is a subsidiary of Indaltec)
- Harrington Financial Overseas
- Arvic SA (held events in the Bauen's rooms)
- Feast SA (in charge of the Bauen's food service)
- Tourism Hotels (partner in the loan from BANADE)
- Sol Jet SA (used the hotel in Misiones)
- Ramsay SA (tourist hotels)
- Grower (shareholder in the Bauen)
- Indaltec (subsidiary of Polytechnics)
- Mercoteles SA (bought the BAUEN after Solari's bankruptcy)
- Bauen Suites
- Ghesa
- Rentatur
- Corman Corporation Virtual of Tourism
- Vacation Systems
- Buzios Megaresorts
- Lodgings and Temporary Shelters
- Solo Aparts
- Crafton Inc., SA
- Investment Bauen Do Brazil
- Radwell (off-shore located in Uruguay)
- Darwell (off-shore located in Uruguay)
- Bolton Group (off-shore located in Uruguay; this enterprise had judicial problems with Juan Carlos Hernández of Mercoteles)
- Deporcor (belongs to Hugo Iurcovich)
- Event Organization (Hugo Iurcovich)
- Sedaguma (Hugo Iurcovich is on the board)
- Suma (Hugo Iurcovich is on the board)
- Sycic (subsidiary of Polytechnics; focused on real-estate investments in Puerto Madero)
- JM Aragon Construction (Sycic is a shareholder)
- Dique 1 (Sycic is a shareholder)
- Puerto Santo (Sycic is a shareholder)
- Consultex (Raquel Kaliman, wife of Marcelo Iurcovich, is the owner; Carlos Sterin and Alejandro Granillo Ocampo are
  on the board).