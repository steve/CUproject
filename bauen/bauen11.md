Resistance and self-management

The eviction ordered by judge Paula Hualde unleashed a wave of solidarity with the BAUEN workers that grew endlessly from then on. If there is one thing the recovery of the downtown hotel had, it was solidarity. It received all kinds of groups of popular struggle in its rooms and its hospitality, which led to that open-door policy coming back toward them in the difficult times that followed.

The news of the eviction order dropped like a bomb that, instead of destroying, built a barrier of solidarity that crossed very diverse political and social sectors, even at the international level. The workers' BAUEN had already turned into a global point of reference for self-management. Immediately, a press conference was organized that overflowed the Utopia bar with were union representatives, political parties, recovered businesses, human-rights organizations, university students, and intellectuals.

The hotel workers felt that this was not merely about political relationships, especially those who had worked for the Iurcoviches, like Arminda, who declared that (she had worked) “with that business for twenty years, and I know those people very well. Right now, we don't have anything to negotiate with them, because the BAUEN is ours, it’s going to be ours, even if they don't like it.” And the answer was a large mobilization, close to 5,000 people, which, logically, far exceeded the members of the BAUEN cooperative, that loudly addressed Commercial Court n.° 9, a few blocks from the hotel. This visit would be repeated several times over the years.

The day the workers were meant to leave the building, August 21, 2007, a massive concert was organized in the doorway, on Callao avenue, where various musical groups played, hundreds of supporters turned out, and at least one side street was closed off. There, an assembly was improvised that was extended the whole length of the avenue, in which Fabio Resino asked the crowd if were ready to support the BAUEN:

> *Compañeros*: The BAUEN workers, like so many workers, where we're able, make decisions in assembly. So, we're going to transform this act into a big assembly, and we want to ask you if you're ready to stand together with the workers to the end. Raise your hands if you agree, *compañeros*! Long live the struggle of the BAUEN! Long live the businesses managed by their workers!

Of course, the answer was unanimous. The eviction order was not carried out, and the cooperative appealed the measure. That gave the workers a certain peace, while the dispute was still in the judicial sphere.

## The formation of FACTA

While these disputes were going on in the judicial and legislative spheres, the old mother organization of the recovered businesses, the National Movement of Recovered Businesses, was thrown into crisis. Political differences and divirging organizational practices had reached a point of no return that brought on a new division in the MNER. A significant part of the movement opted to create a federation of cooperatives in place of the lax, top-down structure they had used until then. The Argentine Federation of Cooperatives of Self-Managed Workers (FACTA) came to be managed as a federated structure that gathered recovered businesses and other self-managed worker cooperatives, which over time became diversified and ever more distant from the imprint of the original MNER. The BAUEN has always had a central place in the Federation, and Fabio Resino, first, and Federico Tonarelli, since, have occupied leadership roles. Since then, FACTA is inextricably linked to the history of the BAUEN.

The Federation was founded on December 9, 2006. Fabio Resino, in the official presentation of the Federation (which was carried out months later in the hotel), summarized it this way:

 > FACTA emerges as a necessary tool, for unity, for cooperatives and for recovered businesses. Not only as a tool to struggle, but yes, it is very necessary be united in the struggle for expropriations, for the businesses still to come, for a national expropriation law, for a fiduciary fund, to be able to get credit from banking entities, for all struggles that are legitimate and positive. And also as a tool of unity to be able compete in a capitalist market where, individually, cooperatives are condemned to failure. So if we are setting up production and capitalization networks, we have many more chances to survive in that market, which is so difficult for a worker cooperative that doesn't have capitalization or State support and of course, everything is much more difficult.67 

But in contrast to MNER, which was made up exclusively of recovered businesses, the Federation intended from its beginning to expand the connection with other collectives of self-managed workers. 

Resino continues:

> We are worker cooperatives that are self-managed businesses, because some are recovered businesses and others are cooperatives, which, while they are not recovered businesses in the broad sense of the word, they are cooperatives that have made it possible to recover work and the dignity of the compañeros that make them up and and also self-manage their destiny, so that's why we've named it the Argentine Federation of Cooperatives of Self-Managed Workers. Any kind of cooperative can be here, it can be a worker cooperative, service cooperative, or housing cooperative. The condition that we put is that they are self-managed and that that horizontal relationship exists within the cooperative.
67 http://www.revolutionvideo.org/agoratv/programs/empresasrecuperadas/factapresentacion.html

The role of the BAUEN was important for the social and political fabric that made up the Federation. Resino also highlights:

> (...) which means the BAUEN, its relationship and its geographic location, comes to be a nucleus of relationships with another kind of workers, for example the Casino[^68]. During their whole conflict,  the Casino workers held assemblies held in the BAUEN, as did the the mail workers, and the subway workers. In a great many conflicts that happen, decisions are made inside the BAUEN, and we believe that is necessary to take all those matters to the heart of the Federation.
 
With this, FACTA  announced clearly that it belongs to the working class, and in that sense, “that each struggle, of each worker, is also their struggle, and that doesn't matter if the person is an employee or part of a cooperative.”

As the years went by, FACTA became consolidated as an organization with a national reach, with affiliated cooperatives in different regions, like the provinces of Jujuy, Salta, Santa Fe, Entre Ríos, Cordoba, Mendoza, San Juan, La Rioja, Catamarca, La Pampa, Formosa, Chaco, and Buenos Aires and the City of Buenos Aires, growing to include some 60 organizations. The cooperatives of FACTA belong to different industries and areas: textiles, printing, audiovisual arts, metallurgy, tourism, gastronomy, food production, shoemaking, construction, paintings, glass, among others[^69]. During its first year, it intervened actively in conflicts at the Incob meat-packing plant (Bahía Blanca), the Huesitos Wilde pet food factory, and a tambera located in Punta Alta[^70]. Recently, the Federation played a leading role in cases like that of the chain of recovered restaurants, including Los Chanchitos and Alé Alé, in the La Casona pizzeria, and in La Litoraleña, a factory of tapas of empanadas and cakes. In every case, the BAUEN served as a point of reference and, in many cases, as a place for meeting and organizing.

Years later, Federico Tonarelli, current president of FACTA, gave an account of the first decade of FACTA on the basis of the objectives listed by Resino in 2007:

68 The conflict of the workers of the Casino of Buenos Aires was developed during the year 2007, the time of the above-mentioned declarations of Fabio Resino al alternative half outlet Ágora TV.

69 http://www.facta.org.ar/who-we are/

70 http://www.revolutionvideo.org/agoratv/programs/empresasrecuperadas/factapresentacion.html

> What said Fabio then is absolutely in effect. The organization by branches of worker cooperatives is most advanced. The rest is harder, because it depends on public policies that in some cases, with its limitations, were there, and in other cases, not. Ideologically, we're clear that it’s necessary to argue for funds that the State has, product of everyone's efforts, the collections, the taxes that people pay. We're very clear on that. What we need to do is grow to such an extent that, technically, legally, administratively, and politically, our businesses are strong enough to resist political changes, and for the businesses to work, and to work very well (…) We have the obligation to make them function, and better than the businesses of capital.

In 2009, FACTA also participated in the foundation of an even larger entity, a third-tier organization, the National Confederation of Worker Cooperatives (CNCT), which brought together an important number of federations of worker cooperatives across the whole country, and today is one of the most important organizations of the cooperative sector. The formation of a confederation of this scope, that currently includes close to hundred thousand worker-cooperative members through 36 federations in almost all provinces[^71], was one more step towards try to overcome the historical dispersion that self-managed work suffers in Argentina. The CNCT worked for several years on the 19th floor of the BAUEN, before moving on to a headquarters of its own in the Buenos Aires neighborhood of Balvanera.

Resistance and solidarity continue

Throughout the following years, the legal case in which the original judge ruled that the cooperative had to leave the hotel, continued to advance through different levels of the judiciary, always with unfavorable result for workers. The appeal by the cooperative's lawyers was granted by the judge and then rejected in appelate court on May 20th, 2008, by the National Chamber of Appeals for Commerce, Room C. In that presentation, the Secretariat of Human Rights of the Nation, whose head was Eduardo Luis Duhalde, had presented himself as amicus curiae of the cooperative, but none the less, the chamber confirmed the original judgment.

71 http://www.cnct.org.ar/sections/federations

This resolution of the Chamber was also appealed, but the next court rejected it on August 16th of the same year. Diego Carbone, the cooperative's lawyer, explains that then “what we did is appeal for extraordinary remedy before the Supreme Court, but the Court of Appeals denied it, saying that there's no Constitutional matter to appeal before the Court.” From then on, it remained a complaint before the Supreme Court of the Nation. The Court allowed some time to pass before it also rejected the petition, and thus validated the judgment of Paula Hualde, on July 5, 2011. “The Court says that it was all right that the petition was not granted, that will not take it up as a Constitutional matter,” explains Carbone. “In that sense, it obviously was a fatal blow, because we we understand that, yes, there are clashing constitutional rights: the right to property against the right to work.” The lawyer adds that, all that time that the file was in the Court, Hualde had stopped it. She did not advance it, even though she could have.

From that moment on, the cooperative was left with only one way out, which was expropriation.

Of course, it did not stop building its collective management every day. Nor did it stop building support and social strength for its demands, even as it pushed for new expropriation bills in Congress. One significant milestone in that search was the event in which it honored the Grandmothers of the Plaza de Mayo by naming the main auditorium of the hotel after them, on April 17th, 2012. Human-rights organizations had constantly supported the workers by participating in their demonstrations, showing solidarity, and committing themselves in various ways. The cooperative recognized that support and added the significant presence of the Grandmothers to its history.

The act of renaming the hall known until then simply as the Auditorium, a  first-floor theater with 350 seats, was attended by Estela de Carlotto and other members of the group. Meanwhile, the old BANADE case was under review in the Ministry of Economy, and the workers were hoping for a intervention from the nation-State that would alleviate the pressure on them.

The date of the event with the Grandmothers was not chosen at random. Two days later, April 19, 2012, another demonstration was held outside Paula Hualde's court, where a hearing had been convened to bring Mercoteles and the cooperative face to face, trying to reach an impossible agreement.[^72] With all other options exhausted, right up the highest court, the judge again found the file in her hands. All this, as might be expected, added tension to the daily work in the hotel. Assemblies were held, and calls were made for solidarity from self-managed businesses, labor unions, and politicians, and the result was a big rally outside the court, at the corner of Marcelo T. de Alvear and Callao Avenue, a few blocks from the BAUEN.

72 “A appointment with the judge about the case,” Página/12, 8 April 2012. Recovered from https://www.pagina12.com.ar/daily/elpais/1-191385-2012-04-08.html

As the representatives of the cooperative, its lawyers, and various legislators and social and political leaders that accompanied them went into the hearing, the protesters listened to a series of speakers. One of them was Omar Villablanca, leader of FASINPAT, Factory Without Bosses, the former Zanon Ceramics, another great example of the recovered businesses in the country.

> To come to these spaces, which are not our spaces, the courts (…) produces indignation (…). In these 10 years, we haven't seen and we haven't heard any businessperson come through these courts -- not the Solaris, not the Iurcoviches, not the Zanons, not the people from Aurora Grundig or from Textiles… No businessperson comes through these courts like us.

After a long meeting, Federico Tonarelli announced the meager results:

> An extensive hearing, a long hearing, a hard hearing, but it has been made clear, both to Judge Hualde and to the old employer that we're not ready to give up continuity of the cooperative, we're not ready to leave behind self-management, and we're not going to abandon the facilities of the hotel.

In the meeting, no progress was made, because all Hugo Iurcovich had to offer was the surrender of the cooperative in exchange for an uncertain promise of work, something that, with their background, the former workers knew well was highly unlikely and even undesirable for those who had experienced self-management.

The judge did not decide to attempt another call for eviction at the time, being conscious of the widespread support that the cooperative had. The question was (and is): who would commit an injustice like the eviction of a cooperative that gave work to 160 people, to benefit a businessperson of highly dubious suitability, and face the cost of making that decision?

The presentation of the criminal complaint by the adjunct general defender Roberto Gallardo against the Iurcovich group for fraud again BANADE and other irregularities in this case temporarily relieved the judge of her imbroglio. Gallardo requested the nullification of the action and, consequently, of the eviction, requesting that federal justice be expedited on the topic and that, meanwhile, commercial justice abstain from making any decisions.

That complaint was shelved, a year later, by Federal Judge Casanello. Once again, the case returned to Hualde's hands, and started all over. Meanwhile, the expropriation bills were accumulating in Congress, but not managing to advance.

The BAUEN workers pay homage to the Grandmothers of the Plaza de Mayo

Speech by Federico Tonarelli April 18, 2012*

> “We simply think that it was the best opportunity to remind all of you, and the grandmothers, obviously, of the story of this place. It is a place that is intimately linked to the story of Estela (de Carlotto), and the Grandmothers, from its beginning with the military dictatorship -- when there's talk of the civilian members of the military dictatorship, this is perhaps, the most visible case for all of us.

> The hotel was built with public funds from the former BANADE, in the times of the military dictatorship, through a close relationship with the Armada, and from that very moment, in the middle of World Cup of 1978, the hotel became *the* hotel of sectors of power of the dominant class, hosting each and every one of the activities of the most powerful sectors. Even as late as the democracy of the '90s, they finished raffling off Argentina [here]. And what makes us so proud is that we have turned this place into the place complete opposite of the place that it was for more than 25 years. And we intend to accompany them to the end, until each one of the identities of each one of the grandchildren who are missing is recovered. We think that the best way to honor them is to call this lovely auditorium “Grandmothers of the Plaza de Mayo,” and build, together with you, from here on, an infinte, endless path, and thank you very much!”

Extracted from the documentary BAUEN. Struggle, culture and work, of Fabián Pierucci.

“No businessperson comes through these courts like us” Intervention of Omar Villablanca, representative of FASINPAT and of the Union of Ceramistas of Neuquén, in the doors of the court n.° 9 the 19 April 2012*

> We prefer to call ourselves Zanon, because we've exproporiated everything from the boss, even his name. We continue to be Zanon under worker control, together with the *compañeros* of Cerámica Stefani and other factories of ceramic bricks that are about to celebrate two years of worker self-management. It's an apppoinment of honor to be here today with the *compañeros* of the BAUEN. The truth is that we've walked these 10 years of struggle hand in hand, with our similarities, with our differences, but understanding that what we've been defending is work. Real work, the work that we have created. The jobs that we have created and defended, every one of the *compañeros*, the length and breadth of the country. After ten years, *compañeros*, it seems to us that there's no longer anything left to prove.

> Worker management, cooperatives, are a concrete fact. They are a real thing, a fact that has prospered, and we workers have demonstrated and proved that not only can we defend our jobs, but we've created jobs. We've created an economy that today feeds more than 25 thousand *compañeros* throughout the country. The truth, *compañeros*, is that to come to these spaces, who are not our spaces, the courts, that don't have political independence, the truth is that it produces indignation, and that has to do with the political positions not only of the Government of the City, but of all governments, the length and breadth of the country. In these ten years, we haven't seen, and we haven't heard, any businessperson come through these courts, not the Solaris, not the Iurcoviches, not the Zanons, not the people from Aurora Grundig or from Textiles… no businessperson has come through these courts. No businessperson has been burdened with a legal case like those that we workers have been burdened with. Back then, more than ten years ago, when we were walking the streets with the *compañeros* from Brukman, with the *compañeros* from the BAUEN, with the *compañeros* of Renacimiento, with the *compañeros* of the textile factories, the word “expropriation” seemed like a bad word in this country. It seemed like it didn't exist, and we looked like crazy people who traveled 1,200 km to say the word here in the Federal Capital. Today, it's front-page news, they just expropriated YPF, which means it's not something that can't be done. We don't take over a factory to exploit ourselves. We don't take over a factory or a businesses for for people to judge us. We take them over to work! To be dignified workers!

BAUEN belongs to the workers, and whoever doesn't like it, fuck 'em! Fuck them!

Extracted from the documentary BAUEN. Struggle, culture and work, of Fabián Pierucci.
