# BAUEN

The workers' hotel

BAUEN

The workers' hotel

Andrés Ruggeri    
Desiderio Alfonso    
Emiliano Balaguer    

BAUEN.  The workers' hotel    
Andrés Ruggeri, Desiderio Alfonso, and Emiliano Balaguer    
Cover design :    
Interior design: Worker Cooperative Project Wow Ltd.    

Publisher: Callao cultural cooperative. Worker Economy Library    
Editing: Karina Luchetti    
Printing:    
Director Worker Economy Library: Andrés Ruggeri    
Deposit made as required by Law 11723    
All rights reserved.

This publication can be reproduced graphically up to 1000 words, quoting the source. It cannot be reproduced, either in whole or in part, registered in or transmitted by a system of information recovery, in any way or by any medium, whether mechanical, photochemical, electronic, by photocopy or any other, without prior written permission of the publisher and/or author, authors, or rights-holders, as the case may be.

In memory of Arminda Palacios and Luisa Casanova, workers of the BAUEN Cooperative.

### Introduction

This is a story that does not have an end yet. The BAUEN hotel is possibly one of the best-known experiences of labor self-management in the world, a symbol of the ability of the workers to not only manage a company, but to take their fate into their own hands. It does not have an end, because the story is not over. As this book went to press, a new eviction order against the cooperative, issued by judge Paula María Hualde, was pending on the hotel. Earlier, December 26, 2016, President Mauricio Macri had vetoed the expropriation law that, after fourteen years of struggle, workers had finally gotten approved by both houses of Congress, in two anguishing sessions separated by almost a year of waiting. That Presidential veto set up the situation in which this book came to be.

The immediate result of this highly complex situation, perhaps the most difficult one the members of the cooperative have been through in a history that has had no shortage of difficult moments, is not going to substantially change what is told here, which is the long struggle of a group of workers who have written several pages in the rich history of the Argentine workers' movement. They have become a point of reference for self-management, for our country, for France, Greece, and Mexico. The story of the BAUEN has similarities to that of other recovered businesses, but also several differences that need to be highlighted. Like most of the protagonists of the approximately 370 businesses recovered by workers in Argentina, they have been through anguishing moments, times of struggle and resistance, but also of happiness, collective building, creativity, and solidarity. But the difference is an obsession with putting an end to this cooperative, especially on the part economic and judicial powers, who join with the unmediated political power of the *Cambiemos* Alliance in the current government. So, the crux of this book is not so much the experience of management within the cooperative, but the stubborn struggle that its members have maintained from the very beginning against this immense power.

The story of the Bauen begins in the most tragic period in our recent history, the civilian-military dictatorship that was inflicted on our people between 1976 and 1983. A dark web of interests and maneuvers defined the profile of businesses in the "contractor homeland" and the economic groups that grew and profitted from being cozy with the dictatorship. The Iurcovich group, which founded the Bauen SACIC, is one of them, a little cousin of larger ones, like the Macri group. The Bauen was, until its closure in December of 2001, the hotel of power.

And now, since its recovery by the cooperative, it is the workers' hotel, the people's hotel. This is its story.

### About this book

This book is a collective creation. Its authors, those of us who drafted it, did interviews, and collected material, are members of the Open Faculty Program of the University of Buenos Aires, a university extension team that has studied businesses recovered by workers since 2002. Emiliano Balaguer and Desiderio Alfonso worked on the documentary part and on putting together the body of information that established the core of the text, but the rest of the team collaborated in different ways. The undersigned, Andrés Ruggeri, is the author of the final draft.

But it would be misleading to say that the three of us who sign are the sole authors. The BAUEN cooperative workers provided their experiences and an enormous amount of documentation that form the skeleton of data and information that give shape to the final product. Federico Tonarelli, especially, was the great organizer of information, telling us where to search, and who to ask. The testimony of María Eva Lossada, Marcelo Ruarte, Horacio Lalli, Arminda Palacios (who, unfortunately, passed away a few days before we finished the first draft), Gladys Alegre, and Federico himself, are the nerves and the muscles of this story. Fabián Pierucci, who recorded the whole history of the BAUEN from the beginning with his camera, and who made the great documentary "BAUEN: Struggle, Culture and, Work," provided the script of his movie, from which the remaining personal stories were extracted.

Diego Carbone, the cooperative's lawyer, contributed to a better understanding of some of the intricate legal matters that surround this story. We are also beneficiaries of journalistic research published by Santiago O’Donnell and Guillermo Berasategui, who have contributed greatly to the knowledge of the hidden web of asset-stripping and business fraud, together with judge Roberto Gallardo and his criminal complaint, which added important data to be able to reconstruct that framework.

Finally, this book is also part of a cooperative effort, edited, designed, and printed by worker cooperatives. It is also, in that sense, an integral part of the self-management movement.

Whatever happens after the arbitrary end of this book, which is required by circumstances and our time, which is limited by publishing and writing needs, we are convinced that the ending remains open, even if the correlation of forces seems indicate something else. It remains so because of the workers' struggle and, since that struggle is going to continue, we know the next edition will have a happier and less uncertain ending.

Andrés Ruggeri
