BAUEN: The Workers' Hotel

[Introduction](bauen/bauen00)  
[The Cinderella of expropriation](bauen/bauen01)  
[The dark web of Bauen SACIC](bauen/bauen02)  
[How to scam the State](bauen/bauen03)  
[The closure](bauen/bauen04)  
[The takeover](bauen/bauen05)  
[Holding on](bauen/bauen06)  
[The recovered hotel](bauen/bauen07)  
[The return of those who never left: the Iurcoviches reappear](bauen/bauen08)  
[The macrismo enters on stage: the “Morando Law”](bauen/bauen09)  
[Eviction order](bauen/bauen10)  
[Resistance and self-management](bauen/bauen11)  
[The long road to the expropriation](bauen/bauen12)  
