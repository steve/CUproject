CHAPTER III

EDUCATION AND WORK

Arizmendiarrieta's writings relative to the topic of education, which
are quite numerous, invariably have a character that we could call
propagandistic. Arizmendiarrieta makes an effort to convince the people
of Mondragon of the urgency of promoting professional teaching. The
reasons range from their need both for social peace and progress, to the
promotion of the working class or simply humanity. Slogans occupy a
favored place: "to know is to be able," "we must socialize knowledge to
democratize power," "man is not so much born as made through education"
(CLP, III, 248); "is easier educate a young person than reform a man,"
"give a man a fish, and he will eat one day; teach him to fish, and he
will eat the rest of his life" (EP, II, 22); "to live is to see,"
"better to light a candle than curse the darkness" (Ib. 181)...

His considerations on the importance and meaning of education are always
in close relationship with the process of reflection on work. There is
an impression that the nucleus of Arizmendiarrieta's thought on man was
formed in direct contact with the world of labor; as he enriched and
expanded his concept of work, so too, his reflections on education
evolved.

We believe, therefore, that Arizmendiarrieta's concept of education is,
more precisely, a concept of "education and work."

1. An urgent task

"Teaching and education are the first task of a people" (EP, I, 269), if
they want to avoid other tasks being stalled or half-developed. The
stagnation of the number of schools is an index of social and industrial
sclerosis, and as such, the interruption of the process of creation and
well-being.  To show the urgency of this task, Arizmendiarrieta will use
practical and utilitarian reasons. In the USA, over the last 50 years,
the number of wage-earners has increased by 60%, and leaders by 600%.
Today, half the active population in that country is called
"starched-collar" [*white-collar*], the other half being industry and
services (Ib.).

So that there can be entrepreneurial people, which are indispensable for
communities to progress, measures must taken in time "so that everyone
has the facilities to cultivate their faculties in a climate of work and
improvement with meaning and social extension" (FC, I, 87). We are
already far behind: "the formation of a man starts a hundred years
before his appearance" (EP, I, 64). There are few things that lend
themselves to improvisation, but perhaps none is as incompatible with it
as education. To reform this society, we first need to reform ideas and
mentalities. But feelings and ideas represent something, as long as they
have roots in the souls of peoples and in the consciousness of men.
"Plants take time to deepen their roots in the earth; we may say the
same thing about feelings and ideas in the spirit of men and peoples,
with the only difference that they need more time than the former,
because, while the life of plants is measured by decades or centuries,
the story of the latter is regularly told in millenia" (Ib.).  It is
urgent, then, that we all concentrate on the task of education.

"A people that suffers, a people that hopes for a better tomorrow, must
prepare for it. Its needs to be instructed and educated" (SS, II, 94).
Only better people, more trained and educated, can build a better
tomorrow.

"There is nothing as urgent," he insists, "for those who are not
resigned to allow themselves to be overwhelmed by circumstances, as this
cultural, professional, and social training of the new generations. The
first redistribution of goods incumbent upon us is that which is
necessary to make education and culture a common heritage" (EP, I, 127).
We already know that is not the same thing make motorcycles or
televisions affordable to the masses as a high level of culture: the
former can obtained with less and less effort. On the other hand, the
greater remuneration demanded by human services in teaching or education
cannot by reduced by increases parallel performance, because this
activity is not susceptible to mechanization or mass production. We need
to face its demands without immediate satisfaction. "Men and people that
really are conscious of their responsibilities, and want to act with the
minimum foresight required by an activity like cultural and professional
training, whose process is irreducible, must renew and intensify their
efforts, even at the cost of sacrificing other, less indispensable
attentions and satisfactions" (Ib. 128).

"In this regard, the investments that are called to be most fertile and
interesting for all are the ones that we can and must make for more
resolute action for cultural development of new generations" (FC, I,
87). The creation of new jobs, the evolution and transformation of new
industrial and commercial activities in tune with the circumstances,
will be problems of minimal complexity, in Arizmendiarrieta's judgment,
if the new generations can be given due preparation. On the other hand,
the conquests made so far and our whole order will sink if the new
generations that burst into life do not arrive with adequate preparation
and a broad social outlook (Ib. 88). "Education is economy, because
without education, scarce goods or services cannot be produced or
distributed" (CLP, III, 269).

"To train our youth professionally is to sow at the right time. This
expense is transformed into seed that produces a hundred to one" (EP, I,
197).

The argument of the cost-effectiveness of investments made in education
appears a multitude of times in Arizmediarrieta's writings (EP, I, 127,
169, 197, 201, 257, 270, 274), and it is not necessary for us to insist
any more on this aspect. The topic has given rise, nevertheless, to
interesting reflections by Arizmendiarrieta on the issue of inheritance,
which we wouldn't want pass over.

"Man," says Arizmendiarrieta, "has possibilities of transmitting to
others something more interesting than wealth or money. The interesting
things that can be transmitted by way of education is his experience,
his science. We have described this as the most interesting factor in
development (...) it is transmited fully by educational activity" (EP,
II, 336).

A review is needed of the concept and sense of application that we have
of inheritance (EP, I, 313 ss.). "The deep changes registered by modern
society," writes Arizmendiarrieta, "have given way to a new arrangement
of values" (Ib. 316). In past centuries, fortunes lasted whole
generations, with little or nothing necessary to do for their renewal; a
settled industry was considered almost invulnerable, provided it had
normal management.

In our day, in contrast, a buoyant company may well no longer be so five
or ten years later, because rapid technical evolution and profound
changes in market situations force continuous efforts in advancement and
readjustment, within a strong demand for constant and
ever-more-important investments. In the same way, fortunes are less
enduring, and situations that previously presented guarantees of
continuity for 50 years may not offer solid perspectives for more than
10 years, since everything evolves at ever-greater speeds.

In other order of ideas, inheritance, in past times, represented an
important factor for successors. However, and it is not difficult to
prove it, it has less and less repercussion on the social life of
developed countries, on the one hand, due to more and more intense
intervention of tax authorities and, on the other hand, because it hs
become much more important, in our era, have a good flow of knowledge
than to have an inherited purse.

Therefore, the best inheritance that can be given to children, says
Arizmendiarrieta, is to help them achieve the best preparation for their
development in life: the concept of posthumous inheritance must be
replaced by that of inheritance to children during life (Ib. 317).

Arizmendiarrieta takes advantage of the occasion to recall that the
preparation and training to be given to children cannot be solely
technical, but must encompass, "with a character of even more pressing
need than professional or technical initiation, the formative and
educational aspects of ethics and moral behavior"; these must build the
spirit of consciousness and social duty (Ib.).

"Even looking at the aspect of the selfish interest of the parents,
there is not the slightest doubt that the help of good children, who
have benefitted from support that later allowed them scale up into jobs
of responsibility, represents a much greater guarantee for their old age
than the hoarding of goods that would represent the equivalent cost of
studies or professional preparation" (Ib.).

"The life and future of those of us congregated in this place," he
reminded parents in June of 1961, "men who have combed grey hair for
some time, or are respectably bald, depend more on what our children
will be than on what each of us do in our professional activity (...).
The deepest transformation of our society and more intense development
will be objectives that we will have to entrust to our children.
Naturally, this transformation and development, if they occur, are bound
to affect our lives more profoundly than they could be affected by our
own efforts directed at the safeguarding of our personal and
professional interests. Therefore, we dare to affirm that our future is
going to depend more on which we do today with our children. They are
the base or foundation on which must lift up our people. After ten or
fifteen years, we will enjoy what they are capable of doing, if we do
not waste time today, and we preferrentially dedicate our attention to
their training" (Ib. 275).

2. A community task

"The most fruitful inheritance is not that which is transmitted to the
children 'nominatim,'" Arizmendiarrieta observes, "but that which is
granted to youth through the creation of an infrastructure that allows
them to rise to the level of their capacity and willpower" (Ib. 314).
That is, it is not enough to update the concept of inheritance; it is
necessary to correct it too, in the sense that inheritance, conceived
thus far like at the individual, private level, is now understood at the
community level. It will not be enough, therefore, to seek a career for
the children: they must be oriented above all to the creation of centers
in which these careers can have a course. It must contribute to the
formation of the children with a common effort.

Arizmendiarrieta argues his position as a dichotomy: either capital,
which inheritance represents, is so large that it must, rather, be
considered a social evil and, in any case, would require the revision of
such concepts of inheritance, or is of a relative volume, and has no
importance in backing initiatives, as its owners do not apply and employ
it jointly.  In either case, we arrive at a concept of anticipated and
communal inheritance.

The most complex question posed both at the scale of the community and
level of families is that of knowing or determining what to do about
children. A child is a treasured product, but, at the same time, a
pitiless judge of the behavior of adults. "Children are our glory and
our ruin, and whether they will be one or the other depends on what our
action educational gave of itself" (EP, II, 202). This is not an issue
that is incumbent only on teachers and teachers, it is a right and an
inalienable duty of parents, and as such, their greatest weight and
responsibility (Ib.; cf. SS, II, 100 ss.); but is also imcumbent on all
of society, both as citizens and as social and economic entities.  "This
need is all the more urgent the less we are willing to settle for what
governments or ministries may have at a broad, peninsular scale, given
that our standard of living is not adequate for such limits" (EP, II,
203). "The plans and services adopted and imposed on a widespread basis,
on a national scale, will be unlikely to satisfy the needs and
aspirations of those want live in the vanguard or correspond to their
current situation" (EP, I, 117).

Arizmendiarrieta's insistence on community responsibility for education
seems to have two roots. One is, without a doubt, his personal
experience of the insufficiency of the State. Another, no less
important, is his general idea that society should tend towards
self-management in all its forms and resolve its problems on its own.
How, Arizmendiarrieta wonders, can our children reach the degree of
cultural development they are due as a result of their willpower and
capacity?  "To be practical, we're going to forego, as of this moment,
the possibility of attributing to the community—to the State—the full
burden of education.  That solution, while perhaps the most correct, is
not viable at this time, and therefore, we are not going to consider it"
(FC, III, 40).

Let us look at the solution that Arizmendiarrieta proposes (1967): so
far, the posture generally adopted in political society that surrounds
us was that each man, theoretically quite free to follow his destiny, is
confronted with himself and resolves himself, if he can, to the extent
his possibilities allow. We can all confirm the results of that
position: only those men who have significant economic means have
managed to satisfy that need, all others being frustrated.  The
individualist approach to the struggles of life has large drawbacks;
only the especially powerful attain interesting successes.

Let us suppose a parent that has, for example, three children: for their
education at the intermediate level, he must spend an approximate
quantity of 1,500 pesetas a month. That level of expenses is so high
that, in practice, it results in the abandonment of those studies in
many cases.  Many of us are not going to be able to solve that problem
if we try it individually. Instead, if we address it communally, the
problem is reduced and, above all, is diluted.

It is an indisputable fact that in a wide community, needs, even general
ones, are not felt by all at the same time. For this reason, with a
small amount paid by each member of the group, the needs felt at any
moment within that community can be satisified. Definitively, the cost
of the service must be paid in every case, but in a fractional and
divided way, over time, dealt with communally.

Continuing the prior example, we could confirm that the total cost—some
hundred twenty thousand pesetas if the period of studies lasts eight
years—could diluted over twenty-five or more years, and in the first
case, the monthly payment would be 480 pesetas, which would be reduced
to the extent that the period of time was increased.

It can easily be seen that it is not the same, regarding immediate
difficulties, to pay 1,500 pesetas a month as 480 pesetas. The
concentrated effort in a short period of time is not bearable for most;
however, it is so in an ongoing but gentle effort.

Today, there is no socialization that must be demanded as urgently and
rigorously as that for options of education and culture: neither can
children henceforth remain entirely dependent on the prospects of their
parents, nor can parents be abandoned to the exclusive prosects for the
training and advancement of their children (EP, II, 155).

On participation and community responsibility in the educational task

Arizmendiarrieta deduces two conclusions. The first is pedagogical: if
the community as a whole commits itself in this way to the education of
youth, it will be able to "demand something so elemental that no
privately tutored person would try—on the basis that education is good
and the socialization of culture desirable, proceed to their
assimilation with no other considerations than [the community's]
exclusive willpower, without weighing the direct or indirect costs.
From the moment we stop making educational options a matter for citizens
of closed paradises, it is natural that their beneficiaries will have no
hesitation in combining work with study to the extent desirable for all"
(Ib.).

The other conclusion refers to public life. Whose hands has our present
and future luck been in, or does it tend to be in? In the past,
Arizmendiarrieta responds, in the development of our communities of all
kinds, it has been minorities who stand out because of their fortune,
caste, or imposed power.  But to the extent that the means of cultural
or educational development are democratized and socialized, we can hope
that it will be in the hands of those among us who reach assorted levels
of knowledge or become due the trust that we grant them.

"Educational responsibilities are inalienable by men and communities
that are aware of the evolution of the times and interested in the
future itself. Executive instruments may be those be demanded of us by
circumstances of competition and efficiency, but educational policy is
something about which workers and men of the day cannot be inhibited"
(Ib.).

Permit us a brief note on private teaching before concluding this
section. The subterfuge of private teaching, says Arizmendiarrieta, is
of no use except when we understand it as somehow serving a small
minority or elite.  "Here, among us, we can wonder and we should, in
fact, wonder how much our process of development, which leads us toward
being a flourishing community, has been promoted or served by the
isolated educational advancements of “daddy's boys” or of those whose
studies have been motivated by individualist views, both on the part of
the parents and of the students themselves." (Ib. 204).

It cannot be universally affirmed that all those who have attended
private schools or academies were "daddy's boys," or that all of them
have followed the path of individual advancement, playing for the
highest bidders or angling for the most desirable position from a
narrowly individualist point of view.  But it can be affirmed that this
was the general tendency.  "How many towns are there, not far from us,
in which they have attained middle and high-ranking careers, without
either them or the community where they started from being aware of
their common interest, and after notable efforts to advance, and not
lacking for trained particiapnts, such collectives or towns keep waiting
for their time to come, the time when someone would try to do something
more in the common interest and benefit?" (Ib.).

3. Study and work

In the first writings, the combination of study and work, the model of
the school-workshop, seems to be a solution that simply has been imposed
by reality as the only possible way to organize professional education.
It does not give the impression that at the base, there is a philosophy,
or principles, that would have recommended this solution for reasons
that are, shall we say, "humanist." There are not entirely lacking,
however: "At the moment, the best way to undertake or set forth on the
socialization of culture is the mode of professional training" (CAS,
155), he writes in 1951. On the occasion of the foundation of the
Professional School of Mondragon, he also voiced his hope that it would
contribute to overcoming the spirit of class struggle (EP, I, 9) and to
the social regeneration of Mondragon (Ib. 18), as well as to the
emancipation—strangely, not of the worker, but—of man (Ib. 19).

We must suppose, without a doubt, that Arizmendiarrieta's social ideas
are at the base of this; but he has yet not manifested a concrete
reflection centered on the topic of education and work.

Very much the realist, Arizmendiarrieta advocates for staggered
professional training ("we must not must subtract strength from work"),
in close connection with the industrial setting and with development
plans, keeping in mind employment possibilities, etc. (CAS, 155).  "We
think the advancement of professional training would go by the safe path
and would go far in with a kind of school or a training plan that
facilitated the students with placement in a work center for part of the
workday, to then be able to attend the professional training center for
the other part of the day. In summary, we advocate for that formula that
allows for work simultaneous with study or professional training at an
adequate center.  In this case, to establish a school or a center, it is
not indispensable to have costly, complete, or complicated facilities
from the beginning. In large part, professional training would be
ensured in work centers. And youth would not resist, but rather, would
happily go to the professional training centers and use part of the
workday for this.  Even businesses would lose nothing and would
contribute to this large work by demanding their own apprentices study
in this way at professional training centers for a set time" (Ib.
157-158).  In summary: a model of pragmatic argumentation, entirely
devoid of humanisms or philosophy.[^1]

[^1] The arguments that Arizmendiarrieta uses generally remind us very
much of the considerations of K. Marx in *Capital* on the education
clauses in English laws on factories, which imposed the obligation of
teaching as a condition of work for children. This imposition, as Marx
observes, contributes, on the one hand, to increasing production, and at
the same time to a more complete development of the person, educated in
the combination of study and work; on the other hand, the system of
division of the workday into half work and half study turn each of the
halves into rest and relief from the other, with means this system turns
out to be more agreeable for the child and, as an added benefit, more
effective than either of the two activities alone and uninterrupted.
Marx is not surprised, then, that the children of the factories learn as
much or more in half the time than the other students do in the whole
day.  Because of all this (contribution to greater productivity, fuller
development of the person, pedagogical system more aligned with
psychology of the child), Marx does not hesitate to consider the
combination of work and study "the education of the future."
Arizmendiarrieta's pragmatic argumentation will always closely follow
these considerations by Marx.  In spite of this, the effort to turn
school into a school of work and, at the same time, turn work into a
creative activity that liberates humanity—which is to say, to acheive a
real work-study synthesis, is very much part the cooperative tradition
and has belonged to it since its origins. Marx himself reminds us of
this, in this context, with a reference to Owen, who is generally
considered the founder of modern cooperativism: "From the Factory system
budded, as Robert Owen has shown us in detail, the germ of the education
of the future, an education that will, in the case of every child over a
given age, combine productive labour with instruction and gymnastics,
not only as one of the methods of adding to the efficiency of
production, but as the only method of producing fully developed human
beings" (Arizmendiarrieta naturally talked about sports, and later about
leisure, instead of gymnastics).  Cfr. MARX, K., Das Kapital, D.
Kiepenheuer, Berlin 1932, 458 (Book I, Sec. IV, cap. 13, 9 a; the
translation is the author's).

It precisely demonstrates the originality and grandeur of this man, who
has been able to confront the problems with pitiless, crude, realism,
apparently without emotion; but, without losing any of his realism or
pragmatism, he has always accompanied action with a severe reflection on
it, sometimes even with utopian elements.  Arizmendiarrieta's ideas
emerge in direct contact with reality. Thus, his enormously rich concept
of work emerges, and, accordingly, the problem of professional education
takes on an unexpected dimension: the construction of a civilization in
which work and culture are not divorced.  Arizmendiarrieta is
surprising, because he overcomes concrete problems with reflection,
while still immersing himself in them, opening new horizons, and always
discovering new aspects and new relationships. In the '60s, the topic of
education and work appears in an entirely new framework.

The student, as he says, needs to feel that s/he is practicing a
profession that is committed to life and work and, to the extent
compatible with pedagogical efficiency, must combine study and work (EP,
II, 27).  Education, the development of the higher faculties through
training, is at the service of work (Ib. 68). "Let us unite WORK and
CULTURE, let us keep them linked in the service of a progressive
community, for good of man" (Ib. 86).

"(...) It is more and more interesting, as the young person advances in
training and age, to think and encourage his image as student to be
identified more with that of worker, if, in fact, we are interested in
work and culture not being two distant poles, and therefore ending up in
two antagonistic worlds or a coexistence not exempt from burdensome
servitude" (FC, III, 163).

Arizmendiarrieta has exerted himself in different directions to smooth
the gap that, in fact, exists between the world of labor and the world
of culture.

The first measure to take is for the student to live in contact with the
world of labor, learn to appreciate it, and have the experience of being
a worker for himself. A new effort, now from the perspective of the
worker, to close the gap, likewise demands that the worker become, in a
certain way, a student.  Without disdaining other, more immediately
"practical" aspects that motivate the need for "permanent education,"
this global vision of a world in which work and culture are not divorced
is the one that truly frames Arizmendiarrieta's thought on education.

"It is also important," Arizmendiarrieta writes, "that in the image of
the worker, a certain aspect of the student never be totally obcured,
having a disposition and willingness to cultivate his higher faculties
throughout life" (Ib. 164). In practice, Arizmendiarrieta proposes the
organization of special courses for workers, which they could attend
without suspending their commitments and responsibilities.

"Work and study should go hand in hand. We must never must stop
attending to the possibilities of those who work or underestimate the
work options of the many who get stuck or tired in their studies.
Equality of opportunity must continue to apply throughout life if, in
fact, we want our communities to be fluid" (EP, II, 91). The combination
of education and work, which had begun as a circumstantial, pragmatic
solution, ends up, as can be seen in this last text, becoming the basis
of the fluidity in community life: the student must be a worker, and the
worker must be a student.

But apart from aspects that are pragmatic, educational, philosophical
(or about a civilization of work), democratic (or about community
fluidity), the topic of education and work has yet another aspect, which
could be called equitable justice. "The excellencies of the principle of
educational opportunities should be combined with demands of equitable
distribution of the burden necessary for their realization. Must they
continue to be the exclusive burden of the community or, to ensure the
maintenance of the principle of equality of educational opportunities,
should it also be thought, for such purposes, that everyone will take
part to the extent of their possibilities, through provisions of school
self-protection or further commitments of solidarity? A timely awareness
that the processes of advancement, to the extent that while the level
reached will involve broader individual options that are perhaps
difficult to subject to community structures, it will be desireable to
keep them in force. There is a need to recharacterize them; which is to
say, their goodness and merit are not absolute. We need to demystify the
term “advancement”" (Ib. 109).

Once again we see that reflection closely follows the evolution of the
facts. The text cited is from 1973. The generation of the youth with
whom Arizmendiarrieta had started professional education in 1943, in
precarious post-war conditions, has been succeeded by a generation grown
up without constraints. "At the moment, children seem to have all
rights, without discerning exactly what duties they should take on. When
it comes to their studies, one would think they would want to pursue
them to the highest level of aptitude and aspiration at the expense of
others. If other options are discussed, which entail fuller realization,
like setting up a home, why not be able to enter into marriage at
such-and-such an age, disregarding what they can contribute, as long as
they can appeal to parents or society for a respectable right?  By
marginalizing or devaluing work as a personal resource for development
and improvement in everyone, will we be able to be sure we are meeting
all the suppositions of scholastic, human, or social advancement,
without imposing heavier burdens on those who have thus far been most
burdened?  What kind of changes and attitudes are urgent for us to be
able to set out towards the new frontiers of a more desirable and
liveable social and economic order for everyone?" (Ib. 110).

Arizmendiarrieta now opposes the division of life into two periods, one
of study (at the cost of those who work, naturally), the other of work,
which funds all the "rights" of those who feel they are in the period of
"advancement." "Parenting is going to be an undesirable profession to
the extent that all the excellences of social and human discoveries fall
so heavily on parents" (Ib.).

Existe una reserva o cantera humana inmensa promocionable en escalas y
niveles sorprendentes, caso de que el otorgamiento de oportunidades de
formación a los mismos tuviera la plena equivalencia de prestación
laboral ordinaria de los mismos a los efectos de disponer de ingresos
para cubrir sus necesidades familiares» (Ib. 110-111). -->

Study and work, rather than consecutive stages, should constitute
combined activities that endure. Youth must combine study and work,
while those who are grown have the right and duty to combine work and
study. "The new generations of youth should do credit to their awareness
and sensitivity by sharing, rather than monopolizing, useful economic
resources in formative processes with the adults. They must, likewise,
do credit to their vocation and commitment as innovators, coordinating
and syncronizing work with study, especially when the former can be
constituted as an economic support or school self-protection" (Ib.
111).  And also: "We're in favor of budget priorities for permanent
training, which must be so to be efficient. The effective development of
this training must shine light on better perspectives for those who
lacked opportunities at one time, but did not avoid effort that would
result in common well-being. There exists an immense human reserve or
storehouse at surprising scales and levels, in case that the provision
of training opportunities to them had full equivalence of their ordinary
work benefit for the effects of having revenue to cover their family
needs" (Ib.  110-111).

4. Education of the worker

One can summarize all Arizmendiarrieta's activity under this epigraph:
education of the worker. But, in a more restricted sense, we will
distinguish three fundamental concepts relative to education of workers,
and which correspond with three different periods of his thought, later
referring to the spirit of work and social peace, which must be fruits
of worker education. The basic concept of professional education is so
generalized that it encompasses practically all Arizmendiarrieta's
reflections on education. Permanent education, highlighted in the '60s,
means a rethinking of the educational problem, demanded by the new
reality that, at least in part, was the fruit of professional education.
Around '75, a new idea stands out, "active education," which is
announced as the promise of a cultural revolution.  We will go point by
point. We will conclude with some observations on the University.

4.1. Professional education

We will only dwell on this point very briefly, which is central both to
this work and to Arizmendiarrieta's thinking, since the various aspects
it encompasses are discussed in several sections.

Following the maxim that it is better to light a candle than curse the
darkness, in August 1943, Arizmendiarrieta addressed the industrialists
of the village of Mondragon, and in September, the people of Mondragon
in general, announcing the project of a Professional School. A late
writing (1961) lets us know that "because of political circumstances,"
the legal constitution of the League of Education and Culture as an
association had not been possible, which had forced him to seek the
solution of the Professional School "as a side project of Catholic
Action" (EP, II, 166).

Starting from the idea that "the social question is mainly moral and
religious" (Leo XIII) and from Arizmendiarrieta's personal conviction
that "moral and spiritual prosperity is always also translated into
greater general well-being" (EP, I, 9), the school proposes the "moral
and spiritual regeneration of Mondragon" (Ib. 11). "The Professional
School we are trying to establish should not only be an instrument of
material prosperity and progress, but also a very important factor in
social peace. It must not be an institution with no more objective than
the technical preparation of youth, which must maintain our industry at
its height, but an entity that, being informed by the Christian spirit,
must carry out a labor of spiritual resonance, strengthening social
peace, fighting class hatred, spreading the spirit of mutual charity,
favoring morality, inducing the sincere practice of religion" (Ib. 9).
This is how he expressed himself writing to businesspeople.

In the "Announcement to the People" (Ib. 18), the tone is rather
different.  "Intelligence is the immovable base of equality that God has
put in all men. Other things will be able to divide and distinguish men
rather arbitrarily, but this faculty puts everyone on the same level.
And intelligence not only is the noblest faculty of man, but also is the
best instrument that every man possesses to emancipate himself from
darkness and misery. Thanks to it, man has come to dominate the material
world and put it at his service, and through it will also come to take
possession of his own destiny. Culture is blood that always gives
lineage and nobility to man" (Ib. 19).

Surely, the urgency and need for professional teaching in a industrial
zone where, at fourteen years of age, after primary school, youth had to
decide the path of their lives, needed no legitimization or lengthy
reflection.  Indeed, Arizmendiarrieta has hardly developed any specific
reflections on professional education, apart from the common ones on
education in general.

In 1961, when many students had gone through the School and more
ambitious expansion plans had been developed, the question was raised:
what will happen if we have an excess of people with the highest level
of preparation?  This objection or fear of some people would appear many
times in Arizmediarrieta's writings in various ways. Arizmendiarrieta
responds: "That day, we will simply be starting an evolution that is
needed among us, that of having the highest quantity of science and
competence, which is to say, of the indispensable items of progress,
which are intelligence and willpower, at the minimum price. Science and
technique will be, as it were, proletarianized a bit. The gap in
remuneration will be reduced, and the social and economic scale that
puts men in a hierarchy will be more discreet. Is this not the best
solution that we can offer towards greater solidarity and brotherhood
among men? Is this not the best formula to accelerate the progress of
the people, making sure technological progress is within reach of all?"
(Ib. 265).

There is no doubt that Arizmendiarrieta had taken the very long-term
view when, in 1943, he decided on the foundation of a Professional
School in conditions that were beyond deficient.

4.2. Permanent education

The need for education is not limited to youth: in a world of quick and
constant technological processes, of successive progress in all fields
of science, one cannot live off of what one studied at an early stage of
life (CLP, I, 287). Professional retraining is becoming essential for
those who want to keep up with the level of demand. Modern man pursues
the conquest of leisure, but he must use his first marginal gains in
permanent training.  "The time has come (1963) to make it fashionable
and carry out the provisions necessary to promote this permanent
training.  Of course, the first thing is awareness of the need for it"
(FC, I, 190).  "Man is determined by his knowledge, given that to know
is to be able.  And in our time, education, to be effective, must be
essentially permanent" (EP, I, 154. Text of 1962): "[...] so that a
profession does not end up being straightjacket for our men" (Ib.).

Arizmendiarrieta reasons in various ways the need for permanent
education: the insufficiency of what was learned in youth or childhood,
options for advancement, the mobility necessary in society and business,
etc.  (EP, II, 299-300). But the most frequent reason is the need to
adapt to technological changes, which is to say, the operation.  "We
have to prepare for this growing evolution" (Ib. 148): this is both an
individual and community need, if we don't want both to remain cornered.
Arizmendiarrieta does not forget about the disabled or woman, who have
more need than anyone else to be included in permanent training courses
(Ib.  143-144); but generally, every worker must take on the idea of
constant learning and recycling, and even the idea of changing jobs
several times during his life.

Courses of permanent education must, therefore, serve the needs of the
labor market, closely and as directly as possible serving the ends of
employment policy and the advancement of work (Ib. 143).

But neither should permanent education should be reduced to mere
technical or work training. Permanent education must be understood "not
only from a professional point of view, but social and cultural too,
from the moment that we seek a new balance between man and his
environment. This consciousness is not yet widespread among us, and that
is due to the fact that, in a certain sense, we are still a culturally
underdeveloped collective, which has barely covered its primary needs
and which faces the danger of the consumerist fever for itself, and a
lack of other hopes and expectations" (Ib. 149).

Permanent education is, first and foremost, a personal right of every
worker, since it is the basic presupposition of his progressive
emancipation. But Arizmendiarrieta does not want to limit himself to an
abstract declaration of principles. He thinks that the right to the
permanent training should be officially recognized, instituted, and made
concrete in viable forumlae. For example, "let's say, for example, that
a man, because of the fact that he has worked for 10 years, should be
due 1 or 2 years of optional training, without this right meaning a cut
in his remuneration" (EP, II, 153; cf. FC, II, 145-146).

Seen at a collective and class level, permanent education shows the
character of a need, more than that of a right, because "this way, we
will become able to develop, without external and not always pure
paternalism, a new social order that is human and fair" (Ib. 337).

"The sense of this story teaches that, to be perpetuated and developed,
apart from rationalizing its production and maintaining its
competitiveness within the market, every enterprise must empower its
men; and even more so if it has emerged with a cooperative spirit, which
is to say, as an instrument of the working classes of our country for
their collective advancement; not so much from an economic point of
view, [even though that is an] indispensable base to be able to aspire
to higher perfection, as much as from a global view, of human beings
that are free, conscious and de-alienated" (Ib. 149).

4.3. Active education

"Active education," which is somehow reminiscent of Mao and the Chinese
Cultural Revolution ("we would try to socialize the experience and the
experience of the manual labor," CLP, I, 272) is considered by
Arizmendiarrieta like our revolution, "gure iraultza," which he
formulates as follows: "Euskotarrok geuk, eta batez be langilleok,
aurrerapide miñez ekin eta aurrera eruan bearrekoa gives geure laguntzaz
oñarritzen ditugun eziketa, ikaske224 ta eta abar, egin ditzala gure
gaztediak giza-aldaketa barriotan sakonago eta trebeago part harturik
eginkeraz.  Ikasleak orduren batzuetan lan dagiela lanpiderik baten"
(Ib.  291).

Active education, or revolution, consists, therefore, in the combination
of study and of work. "We need to challenge our scholars and students to
opt to use their time in useful activities to partly or fully
self-finance their training, which is why we advocate for what we call
active education, from the *ikastolaks* [*primary schools*] through
higher education, without being situated in life, from the beginning, in
a given class position, as laborers or workers and intellectuals, with
no further community connection" (Ib.  288). The concept of active
education is late in arriving.2  Does it signify something new in
Arizmendiarrieta's thinking?  What value we can give this "*gure
iraultza*" [*our revolution*]?  We believe, in fact, that this concept
presents nuances that not only allow us, but require us to distinguish
it from the mere combination of study and work. In the first place, we
must not overlook the fact that, while Arizmendiarrieta always advocated
for the combination of study and work, the concept of active education
was formally developed only around 1975, which is to say, in the last
years of his life, subsequent to his writings on permanent education,
and with an unusual insistence. The work that he refers to in this case
is also quite different from that of the simple collaboration of a
Professional School with a given business (it is likewise extended to
the *ikastolaks*) and, above all, responds to quite a different
proposal, in the end, from that of the subsidizing or self-financing of
a center that is always short on resources. Indeed, the work included in
active education is considered a patriotic activity, even an expression
of Basque patriotism with new characteristics (CLP, I, 291), as he
discusses elsewhere. It is a matter of humanizing and ecologically
transforming the country, rather than enriching or industrializing it:
"These spaces that surround all our urban centers, can they not offer,
with many of the advantages of urbanized zones, new opportunities for
the promotion of a complex of villages or cities that, together with the
factories, offer other centers of creative and utilitarian activity, so
that we can configure a nation of work with workers and citizens who are
not two classes or castes?  Definitively, we will have promoted a more
free, more autonomous, more comfortable nation, in its whole length and
breadth; communities with more natural and permanent affinities and
complementarities.

—It is justified to go on a march that culminates in the service of a
homeland that is the condensation of the past, being, at the same time,
of the children, which are its future" (Ib. 288).

2 So we believe, in contrast with Larrañaga, J., *Don José María
Arizmendi-Arrieta and the Mondragon cooperative experience*, Caja
Laboral Popular, 1981, 107-109.

There is, finally, in the concept of active education, an element that
only is explicable by from Arizmendiarrieta's deep concern for old age
(concerns that belong to the final stage of his writings), and it is
that active education referrs repeatedly to the elderly (Ib.). My
opinion is (the concept of active education had not acquired clear and
definitive contours in Arizmediarrieta's writings) that this idea
emerged from his general concern about "the two poles of life," children
and old people, who, in our society, are reduced to the state of
"useless," therefore cornered (in fact, active education seems to
include more "useful activities" than work itself).

In recent years, the term active education has undergone an evolution
that has made its meaning more concrete, on the one hand, and expanded
its content, on the other. Around 1970, its use is still very rare and
seems to mean that every activity can be understood and taken advantage
of as a field of learning. So, the Caja Laboral Popular is considered
(1971) a "messenger and protagonist of (1) education for work in
solidarity, (2) education for humanizing consumption, (3) education for
energizing leisure, which is to say, for the promotion of a humane,
fraternal, and progressive existence, on an expansive scale" (CLP, I,
231). That is, men and plans congregate around the Caja Laboral Popular
in order to carry out social transformations. "It is the man who is
born, but he is made through education, the operative mechanisms of
civilization, which, in turn, are perfected and made fruitful through
work, which, above all, must be process of man in search of a fuller
realization of himself" (Ib.). And all that can be summarized in a term:
active education. It is, as is seen, a concept of still very diluted
surroundings, which Arizmendiarrieta could not consider, in this way, as
the "gure iraultza" that, some years later, he would consider so
important and propose with such vigor. We are considering an ideal or
concept that has started out almost imperceptibly on its path and, like
so many times in Arizmendiarrieta, will need time to appear with its own
light and strength.

In any case, it is unquestionable that the idea of active education has
its roots, on the one hand, in the assessment of work as the means by
which man is self-realized; and on the other, in the concept of study
and work, harmonized, that Arizmendiarrieta manifests from the
beginning, but is now sharpened by the ever-clearer awareness of being
in the middle of a world in dizzying evolution. This awareness, which
has led to the demand for permanent education, will end up demanding a
reform of the very methods of education. "Every initiative,
responsibility, creativity, improvement, solidarity, etc., entails an
organizational and promotional dynamic, or one of adaptation, whose
requirements cannot fail to respond to a pedagogy that can be described
as humane and current" (EP, 11,259).

Everything changes, and "forces and conditions not circumscribed to a
country or sector of the population" (Ib. 260), an aspect that seems
important to us, drive this whirlwind in which the same thing can
contribute to revitalizing or disempowering communities and people. It
is an almost apocalyptic vision of the blind forces of development,
which contributes to the meaning that Arizmendiarrieta would give to
education in his last years, underscoring the need for education to be
permanent and active. The only thing not subject to arbitrariness among
all these accelerated changes, is, "with no one able to prevent it," the
active, organizing, and managerial capacity of people. Over and above
crisis and ups and downs, this capacity is the only guarantee for the
future.  But this capacity requires cultivation, which is what education
should be.

<!-- De este modo las ideas fundamentales quedan estrechamente concatenadas: 1. «El único patrimonio y valor que no tiende a desvalorizarse es el de la capacitación de los hombres: la formación». 2. Pero la formación «para ser eficiente debe ser permanente». 3. Y debe ser, no mantenedora, sino activa, «que procede en todo el proceso educativo a movilizar, adiestrar humana y socialmente a cohonestar el trabajo por lo que, independientemente de otras calificaciones más o menos retrospectivas peyorativas, tiene de entidad y sustantividad para encarnar y materializar la provisión de las propias necesidades como las atenciones que debemos a nuestros semejantes» (Ib. 260-261). -->

This way, fundamental ideas stay closely connected: 1. "The only wealth
and value that does not tend to devalue is that of the preparation of
men: training." 2. But training, "to be efficient, must be ongoing." 3.
And it must be not maintenance, but active, "which proceeds, throughout
the whole educational process, to mobilize, humanely and socially train
to reconcile work, which, independent of other rather pejorative
retrospective descriptions, has the entity and substantivity to embody
and materialize the provision for our needs as the attention that we owe
to our peers" (Ib. 260-261).

<!-- «Por educación entendemos, escribe Arizmendiarrieta en 1975, aparte del sistemático cultivo de las facultades humanas, la aplicación práctica de las mismas, que de esta forma configuran y equipan al sujeto humano, para no ser menos activo que contemplativo, consciente y responsablemente admirador y transformador del mundo en el que se ha encontrado. — Un buen proceso y método educativo es aquel en el que el educando es auténtico protagonista y, para serlo mejor, concurren en el mismo intereses y afectos, necesidades e ideales, en búsqueda de opciones de promoción, que se aceptan o se generan» (Ib. 264). -->

"By education, we mean," writes Arizmendiarrieta in 1975, "apart from
the systematic cultivation of human faculties, their practical
application. This way, they configure and equip the human subject, to
not be less active than contemplative, consciously and responsibly
admiring and transforming the world in which he finds himself. — A good
process and educational method is that in which the student is an
authentic protagonist and, to be so better, they concur in the same
interests and affections, needs and ideals, in search of options for
advancement, that are accepted or are created" (Ib. 264).

<!-- Una pedagogía humana y social actual, sigue Arizmendiarrieta, valora la iniciativa y creatividad, la responsabilidad y la integración comunitaria, superando las carencias individuales como las inercias colectivas, debe tratar de suscitar todas las energías potenciales, apelar y apoyar una educación activa y, como tal, no puede desdoblar autogobierno y autofinanciación, en la medida que fuera viable ponerlos en juego. Esta armonización e identificación de autogestión y autofinanciación adquieren relieve en cuanto a su vez, en la perspectiva de aceleración y cambio en que nos hallamos inmersos, la educación o la formación han de tener que ser permanentes para ser eficientes y podrán serlo en cuanto sea viable aplicar la educación a expensas propias. -->

"A current, human, and social pedagogy," Arizmendiarrieta continues,
"values initiative and creativity, responsibility and community
integration. It improves individual deficiencies like collective
inertia, must try to muster all potential energy, appeal and support
active education and, as such, cannot separate self-government and
self-financing, to the extent that it is viable to put them in play.
This harmonization and identification of self-management and
self-financing acquire prominence in turn, in the perspective of
acceleration and change on which we find ourselves immersed, education
or the training must have to be permanent to be efficient and will be
able to be so as for is viable apply education at its own expense.

<!-- En ningún momento ha pretendido Arizmendiarrieta desarrollar una teoría pedagógica general. Se interesa por la pedagogía más bien en relación con la cooperación, a cuyo servicio la somete expresamente (Ib.). Ahora bien, en el amplio concepto de la cooperación el núcleo está formado precisamente por la idea del trabajo. La educación activa traduce, a este nivel, lo que el principio cooperativo significa a nivel de producción: autofinanciación y autogestión. -->

At no time has attempted Arizmendiarrieta develop a general pedagogical theory. He is interested in pedagogy rather in relation to cooperation, to whose service subjects it expressly (Ib.). Now then, in the wide concept of cooperation the nucleus is formed precisely by the idea of work. Active education translates, at this level, which the cooperative principle means at the level of production: self-financing and self-management.

<!-- Bien mirado, no se puede decir que las llamadas Cooperativas Educativas, aunque lleven el nombre de la Cooperación, cumplan rigurosamente estos dos requisitos, ni el de la autofinanciación estricta (al modo de que la Escuela se autofinancie a través de su propio trabajo) ni el de la autogestión. Lo que es peor, a Arizmendiarrieta parece haberle molestado no poco la relación de estos centros con el mundo del trabajo, en general. ¿«Cuándo llegaremos a ver, ironizaba, que en los centros de educación de cualquier nivel y modalidad los asistentes se familiaricen con .las escobas, las “atxurras”, etc., no menos que con papel y lápices? ¿Cuándo será el día que se inicien todos a ser más amantes de la libertad, pero de la que desearan para sí y para los demás acreditándose en ser protagonistas que proceden más o menos espontáneamente a hacer por sí todo cuanto pudieran hacerlo, para que de esa forma el trabajo precisara de menos mercantilización o tendiera a ser traspasado de unos a otros para que al final de tal proceso o cadena unos cuantos se vieran obligados a tener que hacer tanto que nadie deseara hacer? —En torno a nuestros llamados centros de educación vemos demasiado personal en tareas serviles remediando sistemáticamente o, mejor dicho, tratando de disimular o atenuar los signos externos de una incuria, de una irresponsabilidad, o de un señoritismo de sus asistentes que se dice que se educan y se dice que son la promesa del futuro» (FC, IV, 233-234). -->

Well watched, it can't be said that the so-called Cooperatives educational, though they carry the name of cooperation, rigorously fulfill these two requirements, or that of strict self-financing (in the style of that the School it is self-financing through their own work) or that of self-management. Lo what's worse, a Arizmendiarrieta seems to have been more than a little annoyed with the relationship of these centers with the world of labor, in general. "When will we come to see, he said ironically, that in educational centres of any level and mode the assistants become familiar with .brooms, las “atxurras,” etc., no less than with paper and pencils? When will be the day that everyone begins to be be more lovers of freedom, but of that which desire for himself and for others crediting in be protagonists that come more or less spontaneously to do for himself as much as they could do it, so that that way work requires less commodification or tended to be pierced of some to others so that at the end of such process or chain a few are forced to have to do everything that no one would want to do? —Around our so-called educational centers we see too many staff in menial tasks systematically redressing or, rather, trying to disimular or attenuate the external signs of a mismanagement, of a irresponsibility, or about a señoritismo of their assistants that it is said that they educate themselves and it is said that are the promise of the future" (FC, IV, 233-234).

<!-- Aunque entendemos que el concepto de educación activa quiere formular a nivel pedagógico los principios cooperativos, de hecho Arizmendiarrieta no remarcará tanto, por razones comprensibles, los aspectos de autofinanciación y autogestión (por las que aboga «en la medida que fuera viable» (EP, II, 264), como el del valor del trabajo en sí mismo, independientemente de su valor como medio de autofinanciación. «La educación como proceso didáctico y existencia1 ha de involucrar la toma de conciencia y la práctica del trabajo. Pero en la práctica del trabajo no debemos escatimar, en la medida que ello fuera viable, una participación más amplia que la derivada de su ejercicio físico o mental (...) Tratamos aquí de algo más que trabajo ficción o afición o simple medio didáctico clásico. Del trabajo como medio humano y humanizante de permanente viabilidad o interés» (Ib. 262). La educación activa debe enseñar en las escuelas a conocer el trabajo como tal trabajo: no como un juego o pasatiempo útil, ni por lo que puede comportar. «Aun no es el trabajo en sí y por sí lo que se apetece, por mucho que se presuma de sensibilidad y conciencia humana social liberadora y promotora. Ejercen su magia las posibilidades de apelaciones paternalistas aun cuando por instinto o conciencia de personalidad aparentemente se repudiaran algunas manifestaciones. -->

Although we understand that the concept of active education wants formulate at the pedagogical level cooperative principles, in fact Arizmendiarrieta will not remark much on, for understandable reasons, the aspects of self-financing and self-management (which he advocates "to the extent that was viable" (EP, II, 264), like the of value of work in itself, independent of its value as a means of self-financing. "Education as a didactic process and existencia1 must involve awareness and the practice of work. But in the practice of work we should not skimp, to the extent that that was viable, wider participation that that derived from their physical or mental exercise (...) Here, we're dealing with something more than work fiction or afición or simple classic didactical method. From the work as a means human and humanizing of permanent viability or interest" (Ib. 262). Active education must show in schools to get to know work as such work: not as a game or useful passtime, or by what can behave. "Even isn't work in itself and for itself which you want, much as it is presumed of sensitivity and consciousness human social liberating and promoter. They exercise their magic the possibilities of paternalistic appeals even when by instinct or consciousness of personality apparently are repudiated some demonstrations.

<!-- Más que trabajo a secas es el trabajo con lápiz o pluma, con máquinas muy evolucionadas y bajo condiciones tales que no desdiga socialmente lo que se apetece y se acepta. De momento caen en vacío otras apelaciones y como alguien ha podido decirlo medio en broma medio en serio también en centros de educación que tienen amplio quorum las tendencias o militancias socialistas, maoístas o progresistas, habrá que esperar si no hemos de proveernos de bedeles a sueldo o profesionales para borrar las pizarras» (Ib. 263). -->

More than plain work is work with pencil or pen, with machines very evolved and under conditions such that is not socially out of keeping with which you want and is accepted. At the moment disappear into the void other appeals and like someone has been able say it half joking half serious too in educational centers that have wide quorum trends or socialist activists, Maoists or progressive, must be wait if we haven't of providing us of janitors a salary or professional to erase the blackboards" (Ib. 263).

<!-- La educación activa deberá conducir a los educandos a conocer «el interés y el valor del trabajo como realización personal y efectiva contribución social (...). El trabajo en cualquiera de sus modalidades, e independientemente de su cualificación» (Ib. 266). -->

Active education must conduct a the students to get to know "the interest and the value of work like personal realization and effective social contribution (...). Work in any of your modalities, and independent of its qualification" (Ib. 266).

<!-- El trabajo que no identificamos con una ganancia o interés puramente individual, escribe Arizmendiarrieta, es un valor que no cuenta en nuestra sociedad, ante nuestra sensibilidad y conciencia. Queda para panegíricos o comentaristas. -->

Work that we do not identify with a profit or purely individual interest, writes Arizmendiarrieta, is a value that does not count in our society, before our sensitivity and consciousness. Remains for panegyrics or commentators.

<!-- Todos disponemos de unas manos que son una maravillosa herramienta de la que podemos usar todo el tiempo que queramos. Asimismo todos tenemos a nuestro alcance unas opciones de trabajo que no precisan que nadie se ocupe de generarlas, ya que no pocas existen bajo tantas modalidades y oportunidades (limpiar ríos, arreglar el campo próximo a la ciudad, etc.), que realmente nadie pudiera disculparse de «no tener que poder hacer algo». -->

We all have some hands that are a wonderful tool of that which we can use any time we want. Likewise everyone we have within our reach some options of work that do not need anyone to takes care of generate them, since more than a few exist under so many modalities and opportunities (clean rivers, fix up the field next to the city, etc.), that really no one could make excuses about "no have to power do something."

<!-- «Si es efectivamente verdad que el hombre más que nace se hace por educación, hay que admitir que una educación que consienta que no nos adiestremos para poder emplear más ampliamente y mejor esta maravillosa herramienta de las manos y que para tratar de emplearlo en condiciones de mayor interés propio y ajeno todas las opciones de trabajo son buenas en sí y por sí, aún cuando carecieren de consonantes o simultáneas compensaciones más o menos inmediatas, la educación que comportará tales actitudes no es buena, no es humana y menos social» (FC, IV, 232). El hombre educado, continúa, es aquel que sabe proceder a crear su propio espacio de actividad y de libertad y, por tanto, se condiciona como idóneo para un proceso de autogestión y autofinanciación tales que nunca carezcan de proyección expansiva, fecundante, suscitadora de comportamientos análogos. Por ello, quien no se ha adiestrado y entrenado en trabajar «gratis et amore» no se ha hecho acreedor a la calificación de sujeto humano y social. Esta actitud tan elemental debe poder ser asimismo más o menos universal y para que lo fuere sin tropezar con problemas más complejos a nivel de posibilidades y opciones ordinarias, que brotan o existen en el ámbito respectivo, debemos tratar de que todos los educandos desde los primeros balbuceos de la educación echen mano de sus manos y se enfrenten con opciones de trabajo (Ib.). -->

"If is in fact truth that man more than being born is made by education, must admit that an education that consents that we do not instruct ourselves to be able to employ more broadly and better this wonderful tool of the hands and that to try to use it in conditions of greater personal interest and alien all the options of work are good in itself and for itself, even when they lack consonants or simultaneous compensation more or less immediate, the education that will behave such attitudes is not good, isn't human and less social" (FC, IV, 232). The educated man, continues, is he who knows proceed to create their own space of activity and of freedom and therefore, conditions himself as suitable for a process of self-management and self-financing such that never lack projection expansive, fruitful, suscitadora of behavior analogs. That's why, whoever has not been instructed and trained in work "gratis et amore" is not due the description of human and social subject. This attitude so elemental must be able to be likewise more or less universal and so that as it were without trip up with problems more complex at the level of possibilities and ordinary options, that blossom or exist in the setting respective, we should try to that all the students from the first babbling of education lend a hand of their hands and they face with options of work (Ib.).

<!-- Queda, pues, claro, que el concepto de educación activa, aún implicando el trabajo, sobrepasa la simple conjunción de estudio y trabajo sin más. Lo que sorprende, en cierto modo, es que este concepto de Arizmendiarrieta, e.d., que sus actitudes más radicales —anticlasistas, «anti-señorito»— procedan, no de su juventud, sino de los años de una madurez avanzada. «Subsiste más o menos camuflada una educación clasista, escribía en agosto de 1975, en tanto no procedamos a adiestrar y, sobre todo, a mentalizar con las excelencias y bondades del trabajo bajo diversas facetas a los educandos, a poder ser desde niños hasta adultos» (EP, II, 261). -->

It is clear, then, that the concept of active education, still meaning work, surpasses the simple conjunction of study and work with no more. What is surprising, in a certain way, is that this concept Arizmendiarrieta's, e.d., that their attitudes most radical —anti-classist, "anti-señorito"— proceed, not from their youth, but of the years of an advanced maturity. "subsists more or less camouflaged a classist education, wrote in August of 1975, insofar as we do not procede to train and, above all, a mentalizar with the excellence and gifts of work under various facets a the students, a to be able to be from children until adults" (EP, II, 261).

<!-- Parece que todo ello hay que verlo en relación también con el uso del concepto de «clase» que se observa en los -->

It seems that all that must be seen also in relation with use of the concept of "class" that is observed in the

<!-- últimos años de Arizmendiarrieta. Se notan en este período dos usos a primera vista extraños de este concepto: -->

recent years Arizmendiarrieta's. noticeable in this period two uses at first glimpse strange of this concept:

<!-- primero, la consideración de los niños y de los ancianos como la clase oprimida por excelencia (CLP, I, 294); segundo, -->

first, consideration of children and of old people like oppressed class par excellence (CLP, I, 294); second,

<!-- la constitución de una nueva generación, que ha gozado de excelentes oportunidades de formación profesional a costa -->

the constitution of a new generation, that has enjoyed of excellent opportunities of professional training a coast

<!-- del trabajo de sus padres, como clase privilegiada3. En este contexto adquiere todo su sentido y todo su valor el hecho de que Arizmendiarrieta haya considerado la educación activa como «nuestra revolución cultural» y que no emplee este término en sentido retórico, sino real. -->

of work of their parents, as a class privilegiada3. In this context acquires everything their meaning and all its value the fact that Arizmendiarrieta there is considered the active education like "our cultural revolution" and that do not use this term in a rhetorical sense, but real.

<!-- 3 LARRAÑAGA, J., Don José María Arizmendi-Arrieta y la experiencia cooperativa de Mondragón, Caja Laboral Popular 1981, 111. -->

3 LARRAÑAGA, J., Don José María Arizmendi-Arrieta and Mondragon cooperative experience, Caja Laboral Popular 1981, 111.

<!-- Algunos textos hacen pensar que el Arizmendiarrieta de los últimos años no se encontraba nada satisfecho de las Cooperativas Educativas existentes y que le rondaba la idea de un nuevo modelo de enseñanza basado más radicalmente en los principios cooperativos. (Por los mismos años buscaba una solución también «activa» a la jubilación). Estas reflexiones quedarán inconclusas. -->

Some texts do think that the Arizmendiarrieta of recent years was not at all satisfied with existing Educational Cooperatives and that it was about the idea of a new model of teaching based more radically in cooperative principles. (Around the same years was looking for a solution too "active" a retirement). These reflections will remain uncompleted.

<!-- «La educación activa y permanente, a la que nos referimos, escribía en noviembre de 1975, constituye un campo de interés y de actividad de tal trascendencia que para la Cooperación, entendida sustantivamente, cabe señalarla como uno de los objetivos netamente humanos y sociales, no carentes del consiguiente interés económico, acreedora a las apelaciones y movilizaciones más entrañablemente humanas y no menos trascendentalmente económicas. La cuestión así entendida contiene sustantividad y materia para la promoción de entidades cooperativas específicas, en las que sus protagonistas lleguen a las implicaciones socio-económicas directas y sustantivas. Tal vez estas cooperativas pudieran ser ya en sí la gran obra social auténtica y plenamente cooperativa. Accesoria o complementariamente, pueden ser sectores o campos de aplicación para las mismas, igual en el campo de la industria, el agrícola, los servicios o la investigación en todas las modalidades que pudieran corresponder a intereses más amplios y poliformes de la sociedad. -->

"Active education and permanent, to which we refer, wrote in November of 1975, constitutes a field of interest and of activity of such transcendence that for Cooperation, understood substantively, it is fitting to point to it like one of the objectives exactly human and social, not lacking the consequent economic interest, creditor a las appeals and mobilizations most touchingly human and no less transcendentally economic. The question thus understood contains substantivity and material for the promotion of specific cooperative entities, in which your protagonists reach member-economic implications direct and substantive. Perhaps these cooperatives could be already in itself the great social work authentically and fully cooperative. Accessory or complementarily, can be sectors or fields of application for the same, equal in the field of the industry, the agricultural, services or research in all modalities that could correspond to broader interests and polyform of society.

<!-- La existencia y la presencia de las Cooperativas Educativas, tal como las entendemos, no modifica ni altera en nada la razón de ser de otras modalidades, incluso de las que pudieran tener resonancias más próximas o afines, tales como las Escolares y las de Enseñanza. Sin minusvalorar en nada lo legislado o establecido referente a las Cooperativas, tratamos de señalar una laguna en cuanto a algunas de las características que, por ahora, pudieran precisar estas Cooperativas Educativas, para quedar respaldadas con discretas pero poco menos que indispensables providencias reglamentarias, al objeto de que las mismas no corran ciertos riesgos innecesarios, ni sus promotores pudieran ser motejados de heterodoxos, o de novedosos, por simple afán de singularización. -->

The existence and the presence of educational cooperatives, as we understand them, does not modify or alters in anything the reason for being of other modalities, even those that could have resonances closer or affiliated, such as las Scholars and those of Teaching. Without underestimate in anything what has been legislated or established concerning las Cooperatives, treat of point a lagoon regarding some of characteristics that, for now, may need these Cooperatives educational, for remain supported with discreet but nearly indispensable regulatory provisions, with the objective that the same do not run certain unnecessary risks, or your promoters could be labeled as heterodox, or about novel, by simple drive of singularización.

<!-- Tanto en el marco de Cooperativas Escolares, como de Enseñanza, pueden quedar resueltos algunos de los aspectos y exigencias de la educación activa, liberadora o promotora que se desee desarrollar, singularmente para los jóvenes hasta ese límite de su edad o mayoría precisa para ser trabajadores, o sujetos protagonistas de actividades diversas, aparte de las específicas del estudio o adiestramiento formal para el trabajo» (EP, II, 265). -->

Both in the framework of Scholarly Cooperatives, like of Teaching, can be resolved some of the aspects and demands of the active education, liberating or promoter that is desirable develop, singularly for youth until that limit of their age or majority makes it necessary to be workers, or active subjects of various activities, separate from those specific to study or formal instruction for work" (EP, II, 265).

<!-- 4.4. Espíritu de trabajo -->

4.4. Spirit of work

<!-- A diferencia de otros muchos, Arizmendiarrieta no ha hecho nunca literatura heroica de glorificación del proletariado. Al contrario, ha dicho sencillamente que no le gustaba y que había que hacer algo para cambiarlo. El proletario, hoy, le parece un monstruo. El hada obligada a existir interinamente bajo la forma de una serpiente venenosa y repugnante, interpretada en EP, I, 89 como alegoría de los hombres en general, de cualquier clase o condición, es identificada en CAS, 198 con «la masa proletaria, ese pueblo trabajador condenado a vivir en unas condiciones espirituales y materiales no muy halagüeñas»; esta masa es calificada de «temible y violenta, debido a circunstancias históricas, en las que no dejamos de tener responsabilidad todos». -->

In contrast to many others, Arizmendiarrieta has never created heroic literature of glorification of the proletariat. On the contrary, has said simply that he did not like it and that had that do something to change it. The proletarian, today, seems to him like a monster. To fairy-tale creature required a exist interinamente in the form of a venomous snake and repugnant, interpreted in EP, I, 89 as an allegory for men in general, of any class or condition, is identified in CAS, 198 with "the proletarian mass, that working people condemned to live in some spiritual conditions and materials not very promising"; this mass is described as "fearsome and violent, due to historical circumstances, in which we still have responsibility everyone."

<!-- Violencia, por un lado, y espíritu gregario, pasivo, por otro, son dos cualidades de la masa obrera que desagradan profundamente a Arizmendiarrieta. «La falta de cultura provoca en la mentalidad de los proletarios un sentimiento de inferioridad, cuya revancha encuentran muchas veces en la violencia. Por otra parte, la proclamación de muchos derechos de los proletarios, proclamación motivada por el noble propósito de querer reconocer la igualdad común de todos los hombres y la dignidad humana, no ha surtido los saludables efectos que eran de esperar, puesto que la falta de preparación intelectual y moral de los mismos para administrar por sí mismos sus propios intereses los ha hecho víctimas de una minoría de desaprensivos y de audaces. A pesar de todo, la inmensa masa de hombres de nuestra comunidad son seres totalmente pasivos como miembros de la comunidad: mejor dicho, la masa no acaba de transformarse en pueblo organizado y disciplinado. Y no está bien que la inmensa mayoría de una población tenga siempre el carácter de menores de edad vitalicios» (Ib. 154-155). -->

Violence, on the one hand, and gregarious spirit, passive, by another, are two qualities of the working masses that profoundly displease Arizmendiarrieta. "The lack of culture provokes in the mentality of the proletarians a feeling of inferiority, whose revenge they often find in violence. On the other hand, the proclaimation of many rights of the proletarians, proclamation motivated by the noble purpose of want recognize the common equality of all men and human dignity, has not assortment the healthy effects that were to be expected, given that the lack of intellectual preparation and moral of the same to administer for themselves their own interests has made them victims of a minority of unscrupulous and of bold. In spite of it all, the immense mass of men of our community are totally passive beings as community members: rather, the mass did not end up to transform into people organized and disciplined. And is not all right that the immense majority of a population always has the character of lifelong minors" (Ib. 154-155).

<!-- Arizmendiarrieta rechaza la violencia sobre todo porque en su opinión este procedimiento no conduce a ninguna liberación real. Esta sólo puede ser efectiva cuando se fundamenta en una transformación previa de las conciencias. En caso contrario la violencia puede eliminar unas formas de esclavitud, pero acaba conduciendo a nuevas esclavitudes. «Los que sientan hambre de justicia y ansias de superación social deben reconocer que el mejor recurso para modificar y mejorar nuestra situación no es la violencia, sino la acción formativa sobre las nuevas generaciones. Son nuestros hijos y nuestros jóvenes el punto de apoyo que necesita la modesta palanca de nuestra influencia y fuerza para provocar los cambios más radicales» (FC, I, 156). -->

Arizmendiarrieta rejects violence above all because in his opinion this procedure does not lead to any real liberation. This can only be effective when is based on a prior transformation of consciences. Otherwise violence can remove some forms of slavery, but ends leading to new slaveries. "Those who feel hunger for justice and anxiety about social improvement must recognize that the best resource to modify and improve our situation isn't violence, but formative action on the new generations. They are our children and our youth the fulcrum point that needs the modest lever of our influence and strength to cause the most radical changes" (FC, I, 156).

<!-- En el tema de la educación y de la violencia hay que tener en cuenta, primero, que la falta de educación puede ser ella misma una causa de violencia, no sólo indirectamente, en el sentido de que en tal situación pueden surgir más fácilmente actitudes irreflexivas, instintivas, de fuerza, sino directamente, porque los hombres se sienten injustamente despojados de un derecho elemental. «El que en una sociedad esas personas (e.d., trabajadores con conciencia de su derecho a la educación) de aptitudes especiales queden sin desarrollar sus facultades, por motivos ajenos a su voluntad y, sobre todo, muchas veces por manifiesta injusticia social, es un peligro para todos, pues han de ser un foco de rebeldía y descontento» (CAS, 102). -->

In the topic of education and of violence it must be taken into account, first, that the lack of education can be she herself a cause of violence, not only indirectly, in the sense that in such situation can emerge more easily attitudes thoughtless, instinctive, of strength, but directly, because men feel unjustly stripped of an elemental right. "The that in a society those people (e.d., workers with consciousness of their right to education) of special aptitudes queden without develop your faculties, by motives beyond their will and, above all, often times by manifests social injustice, is a danger to everyone, since have to be a focus of rebellion and discontent" (CAS, 102).

<!-- Argumentando a favor de la enseñanza profesional, Arizmendiarrieta procede de forma escalonada: el desarrollo económico, la industria, etc. tendrán sólo ventajas con ello (EP, I, 38); pero aún cuando la industria pudiera prescindir de la enseñanza profesional, la justicia social nos seguiría obligando a esforzarnos por ella (Ib. 39); pero si ni siquiera los motivos de justicia nos movieran, «hoy debemos movernos a ello por instinto de conservación y por conveniencia propia. El dotar de medios de preparación técnica o darles una formación profesional a los jóvenes que se sienten con afán de superación tiene mucha importancia hasta desde el punto de vista de conveniencia propia. El obrero que no encuentra en el trabajo que ejecuta aquella satisfacción que requiere aquella su sensibilidad y capacidad natural, es un individuo que inevitablemente sembrará insatisfacción alrededor suyo (...) Si a los jóvenes con aptitudes y deseos de perfección no se les da facilidades y se les hace posible ese ascenso a que se sienten inclinados por su misma naturaleza, esos tales nada tiene de particular que constituyan un foco de rebeldía, de descontento y de malestar» (Ib. 40-41). -->

Arguing in favor of professional teaching, Arizmendiarrieta proceeds in a stairstep order: economic development, the industry, etc. will have only advantages with that (EP, I, 38); but still when the industry could dispense with of professional teaching, social justice would continue to force us to work for it (Ib. 39); but if not even the motives of justice move us, "today we should move us to that by instinct of conservation and by our own convenience. Providing means of technical preparation or to give them a professional training a youth that feel with drive of improvement has much importance until from the point of view of our own convenience. The worker who does not find in the work that it executes that satisfaction that requires that its sensitivity and natural capacity, is an individual who inevitably will sow dissatisfaction around him (...) If a youth with skills and desires of perfection are not given facilities and make possible for them that promotion to that feel inclined because of their very nature, those such nothing has of particular that constitute a focus of rebellion, of discontent and of discomfort" (Ib. 40-41).

<!-- De la Escuela Profesional Arizmendiarrieta espera que sea no solamente un instrumento de prosperidad y progreso material, sino también un factor importantísimo de paz social (Ib. 9), que ayude a superar los odios de clase (Ib.). «Concebimos la nueva Escuela Profesional como un monumento que vamos a legar a las futuras generaciones como testimonio de que los hombres de hoy no hemos descendido el suave aleteo de ese nuevo espíritu social, que está llamado a transformar la faz del mundo, que se resiste a sucumbir bajo el alud del materialismo. Ya no debe tolerarse que quien no tenga alma de peón se quede condenado a serlo por falta de la sociedad y ello será en beneficio de todos, porque, indudablemente, es uno de los factores de la paz social» (Ib. 66). -->

From the Professional School Arizmendiarrieta hopes that is not only an instrument of prosperity and material progress, but also a very important factor in social peace (Ib. 9), that helps to overcome class hatred (Ib.). "We conceive the new Professional School like a monument that we're going to leave to future generations as testimony that the men of today we haven't inflected the smooth aleteo of that new social spirit, which is called to transform the face of the world, which resists succumb under the deluge of materialism. It should not be tolerated that one who does not have soul of a pawn is condemned to be one by lack of society and that will be in benefit of all, because, undoubtedly, is one of the factors of social peace" (Ib. 66).

<!-- Sería un grave malentendido confundir la paz social de que se habla con espíritu de sumisión, o con la renuncia a la lucha y a la superación. Muy al contrario, Arizmendiarrieta llamará al proletariado constantemente a la lucha, a la revolución incluso: pero su fuerza revolucionaria está, como lo juzga Arizmendiarrieta, no en la violencia, sino en el trabajo, que es su arma propia. El proletariado debe aprender a utilizar su trabajo como arma de liberación; para ello es necesario que previamente adquiera cultura. -->

It would be a serious misunderstanding confuse social peace that is talked of with spirit of submission, or with or the renunciation of the struggle and a improvement. Quite to the contrary, Arizmendiarrieta will call the proletariat constantly to struggle, even to revolution: but its revolutionary	strength is, in his judgement Arizmendiarrieta, not in violence, but in work, which is their weapon own. The proletariat must learn to use your work like weapon of liberation; for this is necessary that previously acquire culture.

<!-- Sólo la cultura podrá enseñarle al proletariado a reconocer el valor del trabajo, a aprender cómo quedar hombre trabajando y cómo hacerse más hombre trabajando (CAS, 158). Porque el trabajo está destinado a la liberación del hombre, a su perfeccionamiento material y moral (Ib.). El hombre debe poder desarrollar sus dotes personales y su dignidad en el trabajo mismo, sin que este signifique para él la asfixia de su vida espiritual, intelectual y moral. -->

Only culture will be able to teach the proletariat to recognize the value of work, to learn how to remain man working and how become more man working (CAS, 158). Because work is destined to the liberation of the man, a their material perfection and moral (Ib.). Man must be able to develop your personal qualities and their dignity in work itself, without this meaning for him the asphyxiation of their spiritual life, intellectual and moral.

<!-- Así el trabajo humanizado, transformado en fuente de satisfacciones e instrumento de servicio solidario, podrá humanizar al hombre mismo (EP, I, 226 ss.; 232). -->

So humanized work, transformed into source of satisfactions and an instrument of service in solidarity, will be able to humanize man himself (EP, I, 226 ss.; 232).

<!-- El espíritu de trabajo equivaldrá al espíritu de superación propia y comunitaria. «No se trabaja cada uno por sí y para sí exclusivamente, se trabaja considerando que de esta forma uno está por todos y todos están por uno» (FC, II, 119). No habrá descontentos ni zánganos, la moral de trabajo se nutrirá de la consideración de los altos fines a los que sirve (Ib.); ni se cotizará el trabajo en función del rendimiento económico exclusivamente, sino como medio de proveerse a las propias necesidades y como un servicio solidario a los demás, como un campo de autorrealización (CAS, 166). Será la emancipación del trabajo y la emancipación del hombre por el trabajo. -->

The spirit of work will equal the spirit of self-improvement and community. "One does not work for itself and for himself exclusively, se works considering that this way one is for all and everyone are about to one" (FC, II, 119). There will be no discontents or parasites, morale of work will be nurtured by consideration of the high purposes that one serves (Ib.); or it will be quoted work in function of the economic performance exclusively, but as a means of be provided a their own needs and like a service in solidarity to others, like a field of self-realization (CAS, 166). It will be the emancipation of labor and emancipation of man through work.

<!-- 4.5. Hacia la Universidad -->

4.5. Towards the University

<!-- El año 1968 renacieron en Euskadi las viejas aspiraciones a una Universidad propia. La Universidad se convirtió en el tema del día. Arizmendiarrieta también se refirió al tema y lo hizo, no podía ser de otra manera, desde la perspectiva de los intereses de la clase obrera. «Un perfil que no deberá faltar en las realizaciones que se lleven a cabo en la Universidad, escribió entonces, es el determinado por los imperativos y modulaciones de la justicia social en el acceso a los bienes de cultura, cuya posesión y disfrute condiciona más hondamente a los miembros de una comunidad (EP, II, 83). La Universidad deberá ser objetivamente «popular y social», o sea, deberá prestar atención a la aplicación práctica del principio de igualdad de oportunidades de educación. Esto significa que no podemos fijar la atención en el último escalón de promoción educativa y cultural sin interesarnos vivamente de los demás escalones que le preceden. -->

The year 1968 they were reborn in Euskadi the old aspirations for a university of its own. The University became the topic of the day. Arizmendiarrieta also referred to the topic and did so, it could not be otherwise, from the perspective of the interests of the working class. "A profile that must not be left out of las realizations that are carried out in the University, wrote then, is the determinant for the imperatives and modulations of social justice in access to the goods of culture, whose possession and enjoyment conditions more deeply a the members of a community (EP, II, 83). The University shall be objectively "popular and social," which is to say, must pay attention to the practical application of the principle of equality of educational opportunities. This means we cannot fix our attention on the last step of educational advancement and cultural without showing a lively interest in the the other steps that precede it.

<!-- Popular quiere decir también, explica Arizmendiarrieta, que la Universidad sea un foco motriz de desarrollo. Si muchas Universidades no cumplen este quehacer en las áreas de su influencia, esta inoperancia puede estar determinada por el clasismo prevalente en el alumnado, por la desvinculación social o falta de auténtica conciencia de solidaridad. A este respecto tampoco estaría de más que, en lugar de otorgar por sistema a las titulaciones académicas una consideración económica y social un tanto equivalente a la que en otros tiempos se reconociera a títulos nobiliarios, los mismos graduados trataran de apoyar sus aspiraciones en efectivas contribuciones al bien de los demás, en una mejor línea de solidaridad autentificada con hechos. Que esto ocurriera en una evolución acelerada, dice Arizmendiarrieta, sería una buena revolución, una revolución necesaria sobre todo (Ib. 89). -->

Popular also means, explains Arizmendiarrieta, that the University is an engine of development. If many Universities do not fulfill this task in their areas of influence, this ineffectiveness can be determined by the classism prevalent in the student body, by social disconnect or lack of authentic consciousness of solidarity. In this respect nor would it be excessive, instead of grant by system a academic degrees economic consideration and social somewhat equivalent to which in other times is recognized to noble titles, the same graduates trataran of support your aspirations in effective contributions to the good of others, in a better line of solidarity authenticated with facts. If this were to happen in an accelerated evolution, says Arizmendiarrieta, would be a good revolution, a revolution necessary above all (Ib. 89).

<!-- «Los que en el seno de una sociedad han tenido más oportunidades para capacitarse al máximo es de desear y esperar que se acrediten y correspondan a esa sociedad comprometiéndose a ser sus promotores y animadores, más que manteniéndose a la expectativa de las mejores opciones de empleo para los mismos para comenzar a compartir las tareas comunes» (Ib. 114). -->

"Those who in the bosom of a society have had more opportunities to be trained to the maximum it is to be desired and hoped that be credited and correspond to that society committing themselves to be your promoters and cheerleaders, more than maintaining themselves a the expectation of the best employment options for the same to begin to share common tasks" (Ib. 114).

<!-- Arizmendiarrieta se burla de la «universidaditis» de ciertos ambientes que sobrevaloran ciertos títulos y profesiones. Para cierta gente, nos dice, otras vías de formación no cuentan. Y no son pocos los padres que van cayendo en la trampa. «El estilo que nuestra poca sensatez y sentido práctico, que ha llevado a dejar de lado, incluso considerarlo ello mismo como exponente de inferioridad social, la utilización de la bicicleta o del ciclomotor o de los servicios públicos para las necesidades de nuestros desplazamientos, lanzándonos en masa a la compra del automóvil, otro tanto puede ocurrir en este momento con modalidades de enseñanza que no tuvieran carácter de vía directa y exclusiva a la Universidad, y tal puede acontecer a la formación profesional, máxime en aquellas zonas en las que los tales centros han preparado para el trabajo en grado elemental» (FC, III, 306-307). Todos los papás y mamás están empeñados, ironiza Arizmendiarrieta, en tener hijos listos, que adornan, más que honran con hechos, a los padres. -->

Arizmendiarrieta mocks of the "universidaditis" of certain environments that overvalue certain titles and professions. For certain people, tells us, others paths of training do not count. And there are many parents that fall into the trap. "The style that our little sensitivity and practical meaning, which has led to leave aside, even consider it that same as an exponent of social inferiority, the use of the bicycle or of the moped or about public services for the needs of our displacements, by throwing en mass a the purchase of the automobile, other both can occur at this moment with manners of teaching that did not have the character of direct and exclusive route to the University, and such can befall professional training, especially in those zones in which such centers have prepared for work to an elemental degree" (FC, III, 306-307). All the Dads and moms are empeñados, ironically comments Arizmendiarrieta, in to have children clever, that adorn, more than honor with facts, a parents.

<!-- «Aquí no estará de más reproducir el texto esculpido en piedra en una de las casas solariegas de Mondragón, en la de Artazubiaga, más conocida por el Centro. Debajo de un escudo en el que se exhibe una tea encendida sostenida por una mano y orlada del texto “Pro libertate combusta”4, en el dintel de la puerta principal se lee: “Solus labor parit virtutem et virtus parit honorem”. Los que no sabemos mucho latín, y tenemos prisa para acabar este comentario, traduciremos diciendo que “donde no hay esfuerzo no hay virtud y tampoco honor sin virtud”: es decir, los vagos holgazanes no deben contar en estas puertas, porque también hay otra en la que se lee: “por esta puerta sólo pasan las obras”» (Ib. 307)5. -->

"Here no will be of more reproduce the text sculpted in stone in one of the manor houses of Mondragon, in that of Artazubiaga, better known as the Center. Under a shield in which se exhibits a tea lit sustained by a hand and fringed of the text “Pro libertate combusta”4, in the mantel of the main door reads: “Solus labor parit virtutem et virtus parit honorem.” Those of us who don't know much Latin, and we are in a hurry to finish this commentary, translate saying that “where there's no effort there's no virtue nor honor without virtue”: which is to say, the vague loafers should not count in these doors, because there's also another in which reads: “through this door only pass the works”" (Ib. 307)5.

<!-- Por el momento Arizmendiarrieta siguió considerando que la verdadera Universidad no convencional, real, sería la institucionalización de la enseñanza permanente. «La Universidad que requiere establecerse no es la que pudiera consistir en un abanico amplio de modalidades y grados de enseñanza, sino aquella otra que pudiera tener por virtud fundamental otorgar cultura y formación a la totalidad de los hombres de toda edad y condición que sintieran necesidad de reactivar su formación o desarrollarla más a lo largo de su vida para mejor realizarse a sí mismos» (Ib. 156). -->

For the moment Arizmendiarrieta continued considering that the true University unconventional, real, would be the institutionalization of permanent teaching. "The University that needs to be established isn't that which would consist of a wide range of modalities and degrees of teaching, but that another that could have for fundamental virtue grant culture and training a the whole of men of all age and condition who felt need for reactivate their training or develop it more throughout its life for better self-realization" (Ib. 156).

<!-- Algo está fallando en todo el concepto de la organización e institución universitaria, comenta Arizmendiarrieta. Y algo debe ser cambiado, para que no siga ocurriendo que «los sujetos más capacitados o, al menos, los oficialmente calificados con mayor capacitación, sigan necesitando de padrinos hasta para emplearse a sí mismos, impotentes para promover por sí mismos ninguna actividad» (EP, II, 114). A base de estadísticas de diversos países sobre posibilidades de empleo que tienen los estudiantes al finalizar sus estudios, Arizmendiarrieta concluye: «la Universidad está fuera de la realidad y crea unas frustraciones que duran toda una vida» (Ib. 115). -->

Something is failing in the whole concept of the organization and institution university, Arizmendiarrieta comments. And something must be changed, so that does not keep happening that "the subjects most trained or, at least, the officially qualified with more training, still need godparents even to employ themselves, powerless to promote any activity for themselves" (EP, II, 114). On the basis of statistics from various countries on employment possibilities that have students al finalize your studies, Arizmendiarrieta concludes: "the University is outside of reality and creates some frustrations that last a whole life" (Ib. 115).

<!-- 4 Inscripción del Palacio de Báñez de Artazubiaga, de la que puede leerse la siguiente explicación en LETONA, J.-LEIBAR, J., Mondragón, Caja de Ahorros Municipal de San Sebastián, 1970, 98: «Un escudo de tosca factura, pero altamente significativo, campea sobre el balcón principal de entrada: dos brazos con hachas encendidas en las manos, dando fuego a una torre; la divisa reza así: Pro nostri generis libertate combusta (Quemado en aras de la libertad de nuestra estirpe). Hace alusión a Juan Báñez, quien por no sujetarse al señorío y abusos de los Guevaras de Oñate dio fuego a su casa solar de Artazubiaga de Bedoña y pasó a vivir a la villa realenga de Mondragón». -->

4 Inscription of the Palacio of Báñez of Artazubiaga, of that which can be read the following explanation in LETONA, J.-LEIBAR, J., Mondragon, Caja of Savings Municipal of Saint Sebastian, 1970, 98: "A shield of crude bill, but highly significant, campea on the main balcony of entry: two arms with flaming axes in the hands, setting fire to a tower; the divisa reza thus: Pro nostri generis libertate combusta (Burned in the interest of the freedom to our lineage). It alludes to Juan Báñez, who to avoid being subjected to the lordship and abuse of the Guevaras of Oñate set fire to their house solar of Artazubiaga of Bedoña and went to live in the villa realenga of Mondragon."

<!-- 5 Inscripción de la entrada del camposanto de Mondragón: «opera eorum sequuntur illos». -->

5 Inscription of the entryway of the camposanto of Mondragon: "operates eorum sequuntur illos."

<!-- 5. Educación y emancipación -->

5. Education and emancipation

<!-- «El mundo mejor que anhelamos y tratamos de construir ha de tener su mejor punto de apoyo en la movilización de los recursos educativos y se realiza en la medida que sensibilizamos a las nuevas generaciones con los altos valores, destinados a perfilar un nuevo orden social» (EP, II, 24). -->

"The world better that we yearn for and try to build must have their better fulcrum point in the mobilization of educational resources and is carried out to the extent that sensibilize the new generations with high values, destined to to profile a new social order" (EP, II, 24).

<!-- Al ser inaugurado el nuevo complejo educativo de Arrasate, Arizmendiarrieta lo calificaba de «cabeza de puente construida por unas generaciones, que comenzaron por revitalizar nuestro mundo de trabajo, que se encamina a la conquista de nuevas fronteras de justicia, de libertad y bienestar para todos» (Ib.). La educación es el presupuesto y la base de toda emancipación real. Esta idea es una constante en todos los escritos de Arizmendiarrieta desde los primeros hasta los últimos. Como un nuevo ilustrado juzga que «la cultura es uno de los principales factores de la civilización, ya que esta va progresando en el ritmo en que se va generalizando aquella, y para que las conquistas de los pueblos y masas tengan efectividad hace falta que estos tengan cultura suficiente como para administrarse a sí mismos»; por el contrario, a la ignorancia «hay que inculpar mucho del recelo e incomprensión que induce a los pueblos a odiarse y a luchar unos contra otros» (SS, II, 19). La educación será considerada como «el mejor servicio a la humanidad» (Ib. 95). -->

Because it is inaugurated the new complex educational of Arrasate, Arizmendiarrieta qualified him of "head of bridge built by some generations, that started by revitalize our world of work, that is headed for the conquest of new borders of justice, of freedom and well-being for everyone" (Ib.). Education is the budget and the basis of all real emancipation. This idea is a constant in all Arizmendiarrieta's writings from the first until the last. As a new illustrated judges that "culture is one of the main factors of civilization, since this progresses at the pace at which it becomes widespread that, and so that the conquests of peoples and masses have effectiveness is needed that these have enough culture to administer themselves"; on the contrary, a ignorance "must incriminate much of the suspicion and incomprehension which induces the people to hate and a struggle against each other" (SS, II, 19). Education will be considered as "the best service to humanity" (Ib. 95).

<!-- Arizmendiarrieta cree que estamos llegando ya a los umbrales de una sociedad que acabará resueltamente con los privilegios de la cultura, como otras épocas han acabado con otro tipo de prebendas. «La cultura como privilegio de una clase es una de las mayores rémoras de los pueblos en orden a su progreso, implica una servidumbre antisocial y antieconómica al propio tiempo, y los hombres y los pueblos que se percatan de ello sin mayor dificultad, tratan de socializarla a toda costa. La socialización de la cultura, el acceso indiscriminado a la misma de la población, la concesión de oportunidades de superación a todos al límite de su capacidad es un postulado fundamental de todo movimiento social de nuestros días. Las proclamaciones de los derechos humanos que se hagan sin su correspondiente respaldo económico y al propio tiempo cultural son efímeras concesiones de galería que no están llamadas a surtir mayores efectos» (EP, I, 256). -->

Arizmendiarrieta believes we are reaching the thresholds of a society that will resolutely put an end to the privileges of culture, like other times have put an end to another kind of perks. "Culture as the privilege of a class is one of the largest obstacles of peoples for their progress, means a servitude antisocial and wasteful at the same time, and men and the people that they realize of it without great difficulty, try to socialize it at any cost. The socialization of culture, indiscriminate access to the same of the population, the concession of opportunities of improvement to everyone to the limit of its capacity is a fundamental postulate of every social movement of our days. The proclamations of human rights that are made without their corresponding economic support and at the same time cultural are ephemeral concessions of gallery that are not called to fill more effects" (EP, I, 256).

<!-- 5.1. Igualdad de oportunidades -->

5.1. Equality of opportunity

<!-- «Una transformación social necesaria que calificamos de fundamental para vivir decorosa y libremente en un régimen justo y humano es hoy la realización del principio de igualdad de oportunidades de cultura y educación de las nuevas generaciones. —Una vez se proporcione a cada uno en el “arranque” de la vida recursos para su promoción en consonancia con su aptitud y voluntad, se le puede exigir que viva con arreglo a sus méritos personales y sea responsable sin disculpas» (FC, I, 79). Pero mientras una comunidad carezca de patrimonio social adecuado para esta promoción cultural y educativa de sus miembros en igualdad de condiciones serán difíciles de justificar, dice Arizmendiarrieta, las disponibilidades de fortunas privadas para atenciones no necesarias. -->

"A necessary social transformation that we describes as fundamental to live decorous and freely in a just and humane regime is today the realization of the principle of equality of opportunities of culture and education of the new generations. —Once is provided to everyone in the “boot” of life resources for its promotion in accordance with their fitness and willpower, it may be demanded that alive with arrangement to their personal merits and is responsible without excuses" (FC, I, 79). But while a community lacks of social heritage adequate for this cultural promotion and educational of their members in equality of conditions will be difficult to justify, says Arizmendiarrieta, the availability private fortunes for unnecessary attentions.

<!-- Enseñanza para todos, para que todos puedan promoverse al nivel de su capacidad y voluntad: este es un postulado fundamental de justicia social, al igual que lo es la concesión de una alimentación sana, u oxígeno, para desarrollar la vida vegetativa. Tan imperiosa es para el bienestar humano el desarrollo de la vida intelectual y moral de los ciudadanos como la alimentación sana para interés de todos (EP, I, 167). -->

Teaching for everyone, so everyone can be promoted to the level of its capacity and willpower: this is a fundamental postulate of social justice, just like lo is the concession of healthful food, or oxygen, to develop vegetative life. It is as important for human well-being the development of intellectual life and moral of citizens like healthy food for interest of all (EP, I, 167).

<!-- No podemos seguir considerando como asunto de simple beneficencia la concesión de oportunidades a los jóvenes independientemente de su respectiva situación económica. No es asunto cuya resolución pudiera considerarse como cuestión de más o menos dosis sentimental de los componentes de nuestra sociedad. No se puede encomendar a la medida de los sentimientos un factor tan básico y elemental de desarrollo cual es la promoción cultural. -->

We cannot continue considering as subject of simple benificence the concession of opportunities a youth independent of their respective economic situation. It's not subject whose resolution could be considered as issue of more or less sentimental dose of the components of our society. No can be entrust to the measure of feelings one factor so basic and elemental of development which is cultural advancement.

<!-- «Tenemos que considerar y abordar como cuestión de justicia social y tratar de realizar como postulado de justicia social la concesión de la igualdad de oportunidades de cultura y educación, cuya aplicación no ha de traer nada más que bienes bajo todos los aspectos» (Ib. 168). -->

"We have to consider and approach as an issue of social justice and try to carry out like postulate of social justice the concession of equality of opportunities of culture and education, whose application must not be bring nothing but goods in all aspects" (Ib. 168).

<!-- El principio de igualdad de oportunidades no viene exigido solamente por razones de justicia, sino también de eficiencia. «El principio de la igualdad de oportunidades prácticamente significa que los mejor dotados nada pierden y los menos aventajados ganan, y por tanto todos se benefician, ya que su aplicación nos conduce a una sociedad dinámica, fecunda y fuerte. La movilidad social y la proliferancia de iniciativas son algo que difícilmente se pueden lograr por otro camino» (FC, I, 260). En este caso todos los sectores de la población se ven implicados en el proceso de expansión y desarrollo de la comunidad. -->

The principle of equality of opportunities does not come demanded only for reasons of justice, but also of efficiency. "The principle of equality of opportunities practically means that the better gifted lose nothing and the least advantaged earn, and therefore everyone benefits, since its application leads us to a dynamic society, fruitful and strong. Social mobility and the proliferation of initiatives are somewhat that can scarcely be achieved another way" (FC, I, 260). In this case all sectors of population are involved in the process of expansion and development of community.

<!-- Algunos espíritus pusilánimes objetarán que una política de igualdad de oportunidades llevaría irremediablemente a una saturación de elementos para los puestos y profesiones de vanguardia o niveles superiores. Tal peligro no existe, responde Arizmendiarrieta. «La concesión de amplias oportunidades y la promoción promoverá en primer lugar una selectividad y un acomodamiento espontáneo de los hombres, un ajuste espontáneo en cada uno de los niveles, pues las capacidades son limitadas y nunca podrá sufrir estrangulamientos una sociedad por prevalecer hombres preparados en la misma y sí por falta de estos elementos» (EP, I, 168). Otra consecuencia será el «acortamiento de distancias entre diversas categorías profesionales y sociales, y bien está que en la Península se llegue a este acortamiento, ya que uno de nuestros males que claman al cielo son las diferencias abismales que existen» (Ib.). La concesión de oportunidades podría ser la fórmula para esta necesaria reducción de distancias y para el establecimiento de un régimen de mayor solidaridad entre todos los sectores y elementos. -->

Some spirits fainthearted will object that a policy of equality of opportunities would lead irredeemably to a saturation of elements for positions and professions vanguard or higher levels. Such a danger doesn't exist, responds Arizmendiarrieta. "The concession of wide-open opportunities and promotion will promote in the first place a selectivity and a spontaneous accomodation of men, a adjustment spontaneous in each of the levels, since capacities are limited and never will be able to suffer throttlinga society by prevail men prepared in the same and yes for a lack of these elements" (EP, I, 168). Another consequence will be the "shortening of distances between various professional and social categories, and well is that in the Peninsula be reached this shortening, already that one of our evils that cry to heaven are the abysmal differences that exist" (Ib.). The concession of opportunities could be the formula for this necessary reduction of distances and for the establishment of a regime of greater solidarity among all sectors and elements.

<!-- «Los pueblos que conservan mayores distancias entre las clases, escribe en otra ocasión, los pueblos de desigualdades irritantes son precisamente aquellos que mantienen el mundo de la cultura como un coto cerrado y adscrito a las clases sociales. Los pueblos con cercos culturales, los pueblos en los que el acceso a los niveles diversos de la cultura es algo que implica discriminación económica individual, son también los pueblos de castas, los pueblos de escalas amplias extremas en la percepción de rentas, lo mismo de trabajo que de capital» (Ib. 271). Propone como modelos a Suecia y Dinamarca, donde las diferencias entre el director de empresa y el portero son mínimas, según dice, y por eso mismo la solidaridad entre los miembros de la comunidad social o de trabajo es máxima y espontánea. -->

"People that preserve greater distances between the classes, writes on another occasion, the people of irritating inequalities are precisely those that maintain the world of culture like a closed preserve and attached to social classes. People with cultural fences, the people in which access to the different levels of culture is something which means individual economic discrimination, are also the people of castes, the people of scales wide-open extreme in the perception of rents, the same thing of work that of capital" (Ib. 271). Suggests as models a Sweden and Denmark, where the differences between the director of enterprise and the doorman are minimal, according to says, and because of just that solidarity between members of the social community or of work is highest and spontaneous.

<!-- Nuestro slogan y nuestra meta, concluirá, tiene que ser el de que entre nosotros no se malogre ningún talento y la promoción no represente precisamente la integración en una casta. «El aprovechamiento del talento de nuestros hombres, independientemente de su condición económica personal o familiar, es una premisa fundamental de toda acción social encaminada a la constitución de un orden social más humano y más cristiano» (Ib. 272). -->

Our slogan and our goal, will conclude, has to be the that among us is not lost no talent and promotion does not exactly represent integration into a caste. "The use of talent of our men, independent of their personal economic condition or family, is a fundamental premise of all social action leading to the constitution of a social order more human and more Christian" (Ib. 272).

<!-- 5.2. Socializar el saber -->

5.2. Socializar knowledge

<!-- Cuando en 1961 Arizmendiarrieta percibía «síntomas de una profunda evolución que se está fraguando: prácticamente todos admitimos también el advenimiento más o menos próximo de un nuevo orden social sin fuerza que pueda impedirlo» (EP, I, 127), seguramente se estaba refiriendo al naciente movimiento cooperativo. Pero el principio de que el fundamento del nuevo orden social tenga que ser la educación vale más allá de los límites de la experiencia cooperativa. «Nada hay que urja tanto para quienes no se resignen a dejarse arrollar por las circunstancias como esta capacitación cultural, profesional o social de las nuevas generaciones. La primera redistribución de bienes que se impone es la necesaria para que la educación y la cultura sean patrimonios comunes» (Ib.). -->

When in 1961 Arizmendiarrieta perceived "symptoms of a profound evolution that is concocting: practically all of us also admit the advent more or less next of a new social order without strength that can prevent it" (EP, I, 127), surely was referring to the nascent cooperative movement. But the principle of that the base of the new order social has to be education has value beyond the limits of the cooperative experience. "There is nothing that is so urgent for those who are not resigned to allow themselves to be overwhelmed by the circumstances as this cultural training, professional or social of the new generations. The first redistribution of goods that is imposed is the necessary so that education and culture are common wealth" (Ib.).

<!-- Arizmendiarrieta considera la socialización de la cultura como una medida que todos deben aceptar sin recelos, fuera de una minoría privilegiada, y como una reforma a la que deben proceder sin vacilaciones los pueblos. -->

Arizmendiarrieta considers the socialization of culture like a measure than all should accept without distrusts, outside of a privileged minority, and like a reform a the that should proceed without vacillations the people.

<!-- Vuelve a insistir en que todos saldrían beneficiados con tal medida, ya que se trata de hacer a todos «partícipes de las conquistas del espíritu humano distribuibles sin perder nada del propio patrimonio espiritual, que es eso precisamente la cultura, algo que se da y se comunica sin que por el hecho de dar y comunicar uno sienta que se merma su nivel» (Ib. 271). -->

He again insists that everyone would benefit from such a measure, since it's about make everyone "sharing in the conquests of the human spirit distributable without losing any of the own heritage spiritual, which is that precisely culture, something that happens and communicates without because of the fact that give and communicate one feel that diminishes their level" (Ib. 271).

<!-- La socialización del saber tendría rápidamente profundas consecuencias sociales. «Tras la socialización de la cultura viene inevitablemente una socialización de las fortunas y hasta del poder; diríamos que es la condición previa indispensable para una democratización y un progreso económico-social de un pueblo» (Ib.). -->

The socialization of knowledge would quickly have profound social consequences. "Following the socialization of culture comes inevitably a socialization of fortunes and even of power; would say which is the indispensable prior conditionfor a democratization and social-economic progress of a people" (Ib.).

<!-- Tampoco en este punto quiere Arizmendiarrieta quedarse a nivel solamente de derechos. La socialización del saber es, además de una exigencia de la justicia, una conveniencia social de primer orden, dada la dinámica de nuestra sociedad. En nuestro tiempo un mes supone tanto como un año en el siglo pasado; es decir, a los efectos de aplicación de descubrimientos, cambio de fisonomía industrial y económica de los pueblos, etc. el espacio de tiempo de un mes da hoy de sí tanto como antes un año. La creación de complejos industriales como Manchester, que costó medio siglo, podría hoy llevarse a cabo en el término de cuatro o cinco años. Pero este dinamismo social requiere que previamente se haya alcanzado un nivel medio cultural elevado. El dinamismo social está condicionado por el nivel cultural de cada sociedad. «No podemos, por consiguiente, vivir de rentas, no nos basta tener un pasado glorioso, necesitamos vivir alerta y en constante tensión, este es el precio de una etapa histórica cuyo signo es el progreso técnico» (Ib. 273). -->

Nor on this point wants Arizmendiarrieta remain only on the level of rights. The socialization of knowledge is, besides a demand of justice, a social convenience of the first order, given the dynamic of our society. In our time one month supposes as well as a year in the last century; which is to say, for the effects of application of discoveries, change of physiognomy industrial and economic of peoples, etc. the space for time of one month gives today of yes as well as before a year. The creation of industrial complexes like Manchester, that cost half a century, could be carried out today in the term of four or five years. But this social dynamism requires that previously has reached an average level cultural high. Social dynamism is conditioned by the cultural level of each society. "We can't, consequently, live on rents, it is not enough for us have a glorious past, we need live alert and in constant tension, this is the price of a historical age whose sign is technological progress" (Ib. 273).

<!-- Por otro lado sólo a través de la educación se puede alcanzar una participación ciudadana en la gestión de los quehaceres comunitarios. En una sociedad de cierto nivel de formación la participación de todos sus miembros se hace inevitable (FC, II, 111). -->

On the other hand only through of education can be reached a citizen participation in the management of community chores. In a society of a certain level of training the participation of all its members is made inevitable (FC, II, 111).

<!-- 5.3. Promoción social del obrero -->

5.3. Social development of the worker

<!-- Entre nosotros constituye un motivo de lamento constante el de la incomprensión y hasta indiferencia de los de arriba con respecto a los subordinados. Y a lo que difícilmente y en contadas ocasiones llegan los de arriba, confiesa Arizmendiarrieta, lo mismo en la organización del trabajo que de la política, suele ser a una actitud paternalista, que no nos resignamos a soportar una vez hemos alcanzado la conciencia de nuestra dignidad. -->

Among us constitutes a reason for constant lament that of the incomprehension and even indifference of those above with respect to subordinates. And to what hardly and in counted occasions arrive those above, confesses Arizmendiarrieta, the same thing in the organization of labor that of politics, is usually a a paternalistic attitude, which we are not resigned to endure once we have reached the consciousness of our dignity.

<!-- «¿Qué podemos esperar de ellos si la clave de su promoción ha sido su casta, su estirpe, sus recursos económicos individuales o familiares y su sentimiento de clase es tan fuerte como su naturaleza, o es de hecho una segunda naturaleza cuya fuerza es prácticamente irresistible? Las aspiraciones sociales de una comunidad, que quedan a expensas de lo que pudieran pensar y sentir un puñado privilegiado de hombres, que llevan encima todo el peso de su tradición e intereses limitados, corren peligro de no realizarse nunca. Por eso una comunidad que examina la naturaleza del problema de su emancipación y promoción no tiene por delante más camino que promover una elevación cultural masiva de sus propios miembros como primer paso de su propia liberación. Necesitamos dirigentes descastados y solamente vamos a tenerlos por esta vía de una acción cultural progresiva y promoción constante de nuevas fuerzas sanas y desprovistas de lastres de intereses de clase o casta» (EP, I, 272). -->

"What can we expect from them if if the key to their promotion was their caste, their lineage, your economic resources individual or family and their class feeling is as strong as their nature, or is in fact a second nature whose strength is practically irresistible? The social aspirations of a community, that stay at the cost of lo that could think of and feel a privileged handful of men, that take above all the weight of their tradition and limited interests, run the risk of never being realized. Therefore a community that examines the nature of problem of their emancipation and promotion has no path before it except promote a elevation cultural massive of their own members as a first step towards their own liberation. We need leaders outcasts and only we're going to have them this way of progressive cultural action and constant promotion of new healthy forces and devoid of lastres of interests of class or caste" (EP, I, 272).

<!-- Sin decir, claro está, que ella sola sea ya una acción suficiente, «la enseñanza es un elemento indispensable para la verdadera emancipación del obrero» (CAS, 103), entendiendo por tal una formación integral. Cuando hablamos de aprendices o de obreros no tenemos que seguir pensando como si forzosamente y con predeterminación física tuviéramos que contarlos siempre, con raras excepciones, entre los ciudadanos de segunda categoría de nuestra sociedad, a quienes les interesa prepararse para ser unos buenos productores y nada más, y como si fuera peligroso situarlos en un plano de capacidad desde el cual pudieran llegar a metas que hay que seguir teniéndolas reservadas para otras clases. Arizmendiarrieta no quiere escuelas profesionales que primero no sean escuelas de hombres. «Un necio, dice, es mucho más funesto que un malvado, porque el malvado descansa algunas veces y el necio jamás» (CAS, 103). Y: «Es verdad que los que tienen alma de peón mejor es que queden en peones, pero no hemos de pensar que las únicas almas de peón brotan en la clase humilde» (EP, I, 47). Los jóvenes trabajadores deben recibir en sus escuelas profesionales una formación integral que los capacite en todos los sentidos, que los haga hombres con quienes se pueda contar en el trabajo y en el ocio, para obedecer y para mandar, y que no queden excluidos de los disfrutes del espíritu por principio. «Nuestras escuelas deben tener presente esta necesidad y nuestros planes de estudio hay que establecerlos teniendo en cuenta estas necesidades, que si bien desgraciadamente no se formulan como verdaderas y fundamentales aspiraciones, ello se debe a que es tal el peso del complejo de inferioridad que asfixia a nuestros proletarios que a ellos mismos les parece demasiado pretender tanto. La minoría de edad de los productores no tiene término en este caso y ¿qué más se desea por muchos sectores?» (Ib. 242). -->

Without saying, it's clear, that she alone is already sufficient action, "teaching is an essential element for the true emancipation of the worker" (CAS, 103), understanding by such a wholistic training. When we talk about apprentices or of workers we don't have to to keep thinking as if necessarily and with physical predetermination we had that contarlos always, with rare exceptions, between the second-class citizens of our society, whom interests them be prepared to be some good producers and nothing more, and as if it was dangerous place them on a plane of capacity from which could reach goals that it is necessary to to keep having them reserved for other classes. Arizmendiarrieta doesn't want professional schools that are not first schools of men. "To fool, says, is much more disastrous that a evil, because the evil rests some times and the foolish never" (CAS, 103). And: "It is true that those that have soul of a pawn better is to remain in laborers, but we haven't of think that the only ones souls of peón brotan in humble classes" (EP, I, 47). Young workers should receive in their professional schools a wholistic training that the train in all senses, that the do men with whom can be count in work and in leisure, to obey and to command, and that not remain excluded from the enjoyment of the spirit on principle. "Our schools must have present this need and our plans for study must establish them keeping in mind these needs, that while disgracefully not formulated like true and fundamental aspirations, that is due to the fact that is such weight of the complex of inferiority that asphyxiation a our proletarians than to they themselves les seems too intend both. The minority of age of the producers does not have term in this case and what more se wishes to by many sectors?" (Ib. 242).

<!-- «Una de las consignas de nuestra acción social pudiera ser esta —escribía Arizmendiarrieta ya en 1945—: capacitemos a los obreros para administrar sus intereses, capacitemos a los obreros técnica y moralmente para el desempeño de todas las funciones que ponen en sus manos un mayor desarrollo de la justicia social y las nuevas corrientes de intervención. Para ello necesitan más cultura y más formación moral. Necesitan cultura, que la tiene monopolizada una sola clase y hay que abrir camino al acceso de los obreros a las Escuelas Superiores y hasta Universidades, cuando ellos reúnen condiciones especiales de aptitud» (CAS, 102). -->

"One of the slogans of our social action could be this —wrote Arizmendiarrieta already in 1945—: we train the workers to administer your interests, we train the workers technically and morally for the performance of all the functions that put in your hands a greater development of social justice and the new tendencies of intervention. For this reason need more culture and more moral training. They need culture, that the has monopolized just one class and must open a path to the workers' access to High Schools and even Universities, when they together special conditions of fitness" (CAS, 102).

<!-- Finalmente, la educación que busca la emancipación y promoción del obrero no puede quedar limitada a la escuela. Hay un adiestramiento indispensable para nuestra promoción auténtica, escribe Arizmendiarrieta, que no podemos alcanzar en las escuelas ni en la Universidad. Algo podemos esperar en la medida en que nuestros hijos vayan promocionándose en los diversos grados del saber a través de las instituciones laborales, si es que evitamos su evasión o desclasamiento. «Pero hay en la vida otros centros en los que no debemos descuidar el adiestramiento de hombres que un día sean capaces de hacer honor a su clase y al movimiento obrero. Estos centros son precisamente las empresas, desde cuya plataforma se llega al mundo económico y financiero, o cuando menos a un conocimiento adecuado de los problemas económicos y financieros, y hemos de pensar en esos centros y en esos hombres con los que será preciso contar en la medida en que nos vamos implicando cada vez más en los diversos campos de actividad y se afianza nuestra emancipación económico-social» (CLP, III, 143). -->

Finally, the education that seeks emancipation and promotion of the worker can't remain limited to the school. There's an essential training for our promotion authentic, writes Arizmendiarrieta, that we cannot reach in schools or in the University. Something we can wait to the extent that our children go promoting itself in various degrees of know through labor institutions, if we avoid their evasion or desclasamiento. "But there are in life other centers in which we should not neglect the training of men who one day are capable of make honor their class and to the worker movement. These centers are precisely the businesses, from whose platform se arrives al economic world and financial, or at least a adequate knowledge of the economic problems and financial, and we need to think about those centers and in those men with whom will be accurate count to the extent that us we're going meaning more and more in the diverse fields of activity and se guarantees our emancipation economic-social" (CLP, III, 143).

<!-- «(...) Saber es poder6 y para democratizar el poder hay que socializar previamente el saber. No hacemos nada con proclamar los derechos si luego los hombres cuyos derechos hemos proclamado, son incapaces de administrarse, si para poder actuar no tienen otra solución que disponer de unos pocos indispensables. -->

"(...) To know how to is poder6 and to democratize power must socialize previously knowledge. We do nothing by proclaim the rights if then men whose rights we have proclaimed, are unable to be administered, if to be able to act don't have another solution than to have a few indispensable.

<!-- No pensemos en otras estructuras de trabajo, en otros sistemas de organización sin riesgos de abusos o tiranías más o menos veladas si cada uno de los componentes de la comunidad no estamos mejor preparados para atender a tantos problemas tan complejos. -->

Let us not think of others structures of work, in other systems of organization without risks of abuse or tyrannies more or less evenings if each of the components of community we're not better prepared to serve many problems so complex.

<!-- La emancipación de una clase o de un pueblo tiene que comenzar por la capacitación más o menos masiva de sus componentes. No se mejora la suerte de las masas sin las masas. -->

The emancipation of a class or of a people has to begin by the training more or less massive of their components. does not improve the fate of the masses without the masses.

<!-- No olvidemos que la burguesía superó y destronó a la aristocracia cuando alcanzó una cultura superior y por tanto el proletariado estará en condiciones de iniciar su reinado social cuando sea capaz de sustituir o relevar a la burguesía por su capacidad y preparación técnica y cultural» (EP, II, 335-336). -->

Let us not forget that the bourgeoisie exceeded and dethroned the aristocracy when reached a higher culture and therefore the proletariat will be in conditions of begin their social reign when is capable of substitute or survey a the bourgeoisie by its capacity and technical preparation and cultural" (EP, II, 335-336).

<!-- 6. Educación y progreso -->

6. Education and progress

<!-- «¿Hace falta que alguien nos diga a quienes somos capaces de observar los fenómenos socio-económicos de nuestra propia periferia que la cultura y la técnica son la clave del desarrollo y no menos lo es su socialización efectiva de las transformaciones con las que soñamos?» (EP, II, 102). -->

"It takes that someone tell us a who we are capable of observe member-economic phenomena of our own periphery that culture and technique are the key of development and no less so is their socialization effective of the transformations we dream of?" (EP, II, 102).

<!-- 6 «Saber es poder»: haciendo suyo este viejo lema de F. Bacon («tantum possumus quantum scimus»), Arizmendiarrieta se sitúa en la línea de la filosofía moderna que, a través de Descartes («savoir pour pouvoir, pour prevoir»), Hobbes, etc., de la Ilustración en general, llega hasta nuestros días esperando de las ciencias y del consiguiente dominio de la naturaleza la liberación de la humanidad de sus males. Obsérvese, sin embargo, que aun sin renunciar al significado original, renacentista y cosmopolita, de aquel aforismo (BACON, F., Novum Organum, Nr. 129, Fontanella, Barcelona 1979, 115-118), Arizmendiarrieta ha desarrollado aquella sentencia a su manera, extendiéndola, en primer lugar, también al campo moral y, sobre todo, dándole un sentido eminentemente social y de clase. Este proceder es característico de la tendencia siempre independiente y sintetizadora de Arizmendiarrieta. -->

6 "To know how to is to be able": making theirs this old motto of F. Bacon ("tantum possumus quantum scimus"), Arizmendiarrieta stands in the line of modern philosophy that, through Descartes ("savoir pour pouvoir, pour prevoir"), Hobbes, etc., of the Enlightenment in general, arrives to our day waiting of the science and of the consequent domain of nature the release of mankind of their evils. Note, however, that still without renounce the meaning original, Renaissance and cosmopolita, of that aphorism (BACON, F., Novum Organum, Nr. 129, Fontanella, Barcelona 1979, 115-118), Arizmendiarrieta has developed that sentence in his way, spreading it, in the first place, too to the field moral and, above all, giving a meaning eminently social and of class. This proceed is characteristic of the tendency always independent and synthesising Arizmendiarrieta's.

<!-- En el siglo pasado, dice Arizmendiarrieta, el desarrollo industrial y económico de los pueblos estuvo condicionado fundamentalmente por los recursos naturales. En un segundo período, correspondiente a la primera mitad de nuestro siglo, han sido las finanzas y recursos económicos la clave de la expansión. Hoy vivimos en una tercera etapa, en la que la capacidad técnica y, por tanto, el potencial humano, constituye la más firme base de superación y progreso en todos los terrenos (EP, I, 117). -->

In the last century, says Arizmendiarrieta, industrial development and economic of peoples was conditioned fundamentally by natural resources. In a second period, corresponding to the first half of our century, have been finance and economic resources the key to expansion. Today we live in a third stage, in which technical capacity and therefore, human potential, constitutes the firmest base for improvement and progress in all lands (EP, I, 117).

<!-- Arizmendiarrieta no ha querido hacer grandes panegíricos de la educación al modo humanista abstracto, sin base real para sus afirmaciones. Siempre se ha interesado vivamente por estudios sobre la participación de la educación en el crecimiento económico, como factor productivo, tratando de probar, entre otras cosas, que las inversiones en educación resultan rentables (EP, I, 301; EP, II, 333). Cita con satisfacción estudios de Denison que atribuyen a la educación y formación nada menos que el 42% del aumento real experimentado por la renta per capita en EE.UU. -->

Arizmendiarrieta did not want make large panegyrics of education al mode humanist abstract, with no real base for their affirmations. Forever has had a lively interest in studies on the participation of education in economic growth, as a productive factor, trying to prove, among other things, that investments in education turn out profitable (EP, I, 301; EP, II, 333). He cites with satisfaction studies of Denison that attribute to education and training no less than the 42% of the rise real experienced by income per capita in EE.UU.

<!-- Hoy, nos dice Arizmendiarrieta, todos los pueblos se encuentran metidos en una verdadera carrera educacional. El clarinazo que hizo reconsiderar este problema de la promoción cultural al mundo occidental fue el Sputnik ruso, cuyo éxito sorprendió y se tuvo que explicar aceptando el elevado nivel técnico o de aplicación del progreso técnico de un país que había hecho de la educación el instrumento de su propio desarrollo. A continuación llovieron estadísticas por todas partes sobre el número de graduados o técnicos superiores que salían de las Universidades y Escuelas Técnicas de los diversos países, constatando no sin sorpresa que llevaba ventaja Rusia. Todos sabemos las medidas económicas adoptadas por el Senado norteamericano para incrementar los fondos destinados a la preparación masiva de técnicos y a la investigación, etc. (EP, II, 334). Arizmendiarrieta quiere probar que, si no por otros motivos, al menos por interés propio y egoísmo deberemos interesarnos todos por la educación. Si nos hacemos los sordos ante el clamor de los movimientos sociales que reclaman el derecho de los jóvenes a una formación profesional adecuada, al menos las pruebas de la experiencia y las estadísticas deberán convencernos de la necesidad de una amplia educación para un mejor desarrollo. «El rendimiento y la perfección de los operarios preparados cuidadosamente contrastan con los resultados de los que trabajan sin dicha preparación» (EP, I, 233-234). -->

Today, Arizmendiarrieta tells us, everyone the people are found tucked in a true eductional race. The warning signal that made reconsider this problem of cultural advancement al Western world was Russian Sputnik, whose success surprised and had to be explained accepting the high technical level or about application of technical progress of a country had done of education the instrument in its own development. Next rained down statistics everywhere on the number of graduates or higher technicians that were leaving of the universities and technical schools of various countries, constatando not without surprise that had the advantage Russia. We all know the economic measures adopted by the US Senate to increase the funds destined to the mass preparation of technicians and a research, etc. (EP, II, 334). Arizmendiarrieta wants prove that, if for no other reason, at least by personal interest and selfishness we will have to interesarnos everyone through education. If we are deaf to the clamor of social movements that demand the right to youth a adequate professional training, at least las proofs of the experience and statistics must convince us of the need to a broad education for a better development. "Performance and perfection of the workers prepared carefully contrast with the results of those who work without this preparation" (EP, I, 233-234).

<!-- No por ello renuncia Arizmendiarrieta a argumentos de carácter más ético: «(...). Hay que tener en cuenta que la formación técnica no sólo es deseable por ser factor decisivo del crecimiento económico. La técnica es, al fin y al cabo, un factor educacional que perfecciona al individuo no sólo para el quehacer económico, sino para la vida total. Por tanto, esta formación constituye un fin en sí misma, puesto que permite el libre desarrollo de la personalidad humana, penetrando en campos que sólo incidentalmente se refieren al aspecto económico» (Ib. 303). En la práctica es difícil separar lo que en la formación técnica existe de revalorización de la vida del hombre, de aquello referido a la pura actividad económica. Pero todos tenemos conciencia de que el saber técnico constituye una parte del saber cultural y, como tal, ejerce una influencia beneficiosa en el comportamiento humano, «como lo demuestran las estadísticas». -->

No therefore resignation Arizmendiarrieta a arguments of a more ethical nature: "(...). We need to take into account that technical training is not only desirable being decisive factor in economic growth. Technique is, in the end, an educational factor that perfects al individual not only for economic tasks, but for all of life. So, this training constitutes an end in itself, position which allows the free development of the human personality, penetrating in fields that only incidentally refer to the economic aspect" (Ib. 303). In practice it is difficult separate which in technical training there exists of revaluation of the life of the man, of that referred to the pure economic activity. But we are all aware that the technical know-how constitutes a part cultural knowledge and, as such, exerts a beneficial influence on human behavior, "as statistics demonstrate."

<!-- Aún con tan poderosos argumentos, Arizmendiarrieta no llegaba a convencer del todo a algunos paisanos mondragoneses, para que apoyaran sus planes de educación profesional. Y las mayores dificultades no las tenía por lo visto con los más pobres precisamente. «¡Qué gastos!, se dicen algunos. -->

Even with so powerful arguments, Arizmendiarrieta was not able to convince at all a some countrymen people of Mondragon, so that support your plans for professional education. And the greatest difficulties no las had by lo seen with the most poor precisely. "What expenses!, some say.

<!-- Pero, ¿se le ocurre a la gente poner en otra cifra para establecer un término de comparación lo que cada familia acomodada gasta anualmente en la instrucción y la educación de sus hijos? ¿Cuántos miles de pesetas? Y, ¿hay alguna familia acomodada que no se crea en el derecho de ganar lícitamente todas esas cantidades con las que poder hacer frente a los gastos familiares e incluso ahorrar lo suficiente para darles una buena dote que a cada hijo le deje en una posición económica mejor que la de sus progenitores? ¿Y será mucho gastar o mucho invertir para dar oportunidades de educación a todos los hijos de la inmensa masa de proletarios o gentes modestas las cifras que representa la Escuela Profesional? Al igual me encuentro con gente que se lleva las manos a la cabeza porque una institución pública gasta en su presupuesto x millones. Tal vez habría motivos en apreciar la forma en que gasta. -->

But, does it occur to people put in another figure to establish a term of comparison what each comfortable family spends annually in the instruction and the education of their children? How many thousands of pesetas? And, there is some comfortable family that no is created in the right to win licitly all those quantities with whom power face family expenses and even save enough to to give them a good dote that to each son le deje in a better economic position than the of their progenitores? And will be much spend or much invest to give opportunities of education a all the children of the immense mass of proletarians or modest people las statistics that represents the Professional School? Al equal I meet people who put their hands to their heads because a public institution spends in its budget x million. Perhaps would have motives in appreciate the way spends.

<!-- Pero la gente no se fija en eso sino en el simple volumen. Y, ¿no hay familias, no hay individuos, cuyas rentas o cuyo movimiento económico es superior a la de todo un municipio sin que no solamente no nos escandalicemos sino incluso hagamos un alarde de ello? Y tal vez, repito, habría motivo de alarma en la consideración de la fuente de que se nutren esos ingresos que son del pan del pobre disminuyendo lo poco que este tiene a su alcance mientras a las riadas de dinero de la gente gorda nadie le pone límites ni nadie echa mano de lo mismo para satisfacer las necesidades públicas» (Ib. 230). -->

But the people no was fixed at that but rather in the simple volume. And, there's no families, no there are individuals, whose rents or whose economic movement is greater than that of a whole municipality without not only we are not scandalized but even we do a boast of it? And perhaps, I repeat, would have reason for alarm in consideration of the source of that feed those revenue which are of the bread of the poor decreasing how little this has within its reach while a the floods of money of fat people no one puts limits on them or anyone echa hand of the same thing to satisfy public needs" (Ib. 230).

<!-- Los tiempos varían, dice Arizmendiarrieta, y los tiempos se imponen. «A quien no progresa la vida le arrolla» (Ib. 229). Hubo un tiempo en que era norma la distinción de clases incluso en la indumentaria; pasó el tiempo y esa norma ha desaparecido, como antes había desaparecido la marca con que se señalaba a los esclavos. «Hoy el tiempo está exigiendo la desaparición de otras marcas, la marca de la ignorancia y de la incultura es también algo que todos tenemos interés en ocultarlo, y para ocultarlo de verdad pedimos iguales oportunidades de superación y cultivo para todos nuestros hijos» (Ib.). -->

Times vary, says Arizmendiarrieta, and the times are imposed. "To who does not progress life it overwhelms" (Ib. 229). There were a time in which was norm the distinction of classes even in the clothes; passed the time and that norm has disappeared, like before there was disappeared the brand with which se pointed out a the slaves. "Today the time is demanding the disappearance of other brands, the brand of ignorance and of the incivility is also something than all we have interest in hide it, and to really hide it we ask equal opportunities of improvement and cultivation for all our sons" (Ib.).

<!-- Hoy se opone resistencia a la idea de que los trabajadores deban recibir formación profesional (texto de 1950); ayer sucedía lo mismo con la idea de la enseñanza primaria gratuita. «Lo decisivo para el desarrollo es el cambio de mentalidad», decía citando a Lopez Rodó (EP, II, 127). -->

Today opposes resistance to the idea that workers should receive professional training (text of 1950); yesterday happened the same thing with the idea of free primary education. "The decisive thing for development is the change in mentality," said quoting Lopez Rodó (EP, II, 127).

<!-- Aunque no es su actividad, sino su pensamiento, lo que nos hemos propuesto investigar, se hace necesario decir que Arizmendiarrieta ha realizado una labor incansable, no sólo en Mondragón, sino en Guipúzcoa y en Euskadi, a favor de la enseñanza profesional. Estaba plenamente convencido de que la modernización y desarrollo de la industria guipuzcoana dependía básicamente de la preparación técnica del personal. «El nivel actual de esta enseñanza (profesional) debe ser mejorado y nos obliga a un creciente esfuerzo si hemos de mantener nuestro puesto de vanguardia en el círculo industrial interior y más aún si se aspira a integraciones más extensas, pues la tecnología exige más especialización» (Ib. 285). -->

Although isn't their activity, but his thought, which we've proposed to investigate, is made necessary say that Arizmendiarrieta has worked tirelessly, not only in Mondragon, but rather in Guipúzcoa and in Euskadi, in favor of professional teaching. He was fully convinced that the modernization and development of Guipuzcoan industry basically depended on the technical preparation of the personal. "The current level of this teaching (professional) must be improved and forces us to a growing effort if we need to maintain our vanguard position in the circle industrial interior and further if se aspires to integrations more extensive, since technology demands more specialization" (Ib. 285).

<!-- Arizmendiarrieta contrapone el ejemplo del Japón, que viene desarrollando, nos dice, un extraordinario esfuerzo en el campo de la educación y de la enseñanza7. Un número elevado de ciudadanos con alto nivel cultural es la base que permite luego grandes inversiones. «A nuestro modo de ver la razón primera y fundamental del rápido desarrollo de las inversiones registrado por el Japón viene a ser precisamente el meteórico desarrollo que ha experimentado la enseñanza en este país, que ya antes de la segunda guerra mundial tenía fama de muy culto» (FC, II, 52). -->

Arizmendiarrieta contrasts the example of Japan, that is developing, tells us, an extraordinary effort in the field of education and of the enseñanza7. A high number of citizens with a high cultural level is the basis which allows then large investments. "As we see it the first and fundamental reason for the rapid development of the investments registered by Japan it becomes precisely the meteoric development that has experienced teaching in this country, that already before the Second World War was famous for being very cultured" (FC, II, 52).

<!-- ¿Qué deseamos para el día de mañana?, nos pregunta Arizmendiarrieta invitándonos a una meditación sincera. ¿Qué deseamos que sea nuestro país, nuestra región? ¿Aspiramos al nivel de los países industrializados, o nos conformamos con que nuestro país haga de sirvienta o de hermano menor en el concierto de las relaciones internacionales? «Si realmente queremos desarrollar la cultura, pero no solamente la de los demás, deberemos aprender a sustituir pasatiempos fútiles por otros que, elegidos de acuerdo con los gustos de cada uno y, por tanto, que puedan ser asimismo interesantes, nos permitan incrementar nuestra capacidad personal, lo cual nos permitirá obtener mayores frutos y satisfacciones de nuestras actividades futuras, al propio tiempo que seremos de mayor utilidad para la sociedad» (Ib. 53). -->

What we desire for tomorrow?, asks us Arizmendiarrieta inviting us to sincere reflection. What we desire that is our country, our region? Do we aspire to the the industrialized nations, or will we settle for our country be a servant or about little brother in the concert of international relations? "If really we want develop culture, but not only that of others, we will have to learn to substitute futile hobbies by others that, elected according to the tastes of everyone and therefore, that can also be interesting, allow us increase our personal capacity, which will allow us get more fruits and satisfactions of our future activities, at the same time that we'll be of greater utility for society" (Ib. 53).

<!-- «Mientras los chiquiteos ocupen cada día el tiempo que desgraciadamente es tradicional en nuestra región, tendremos que seguir refiriendo en nuestras conversaciones las hazañas que van realizando los demás, puesto que unos tomarán el tiempo para hacerlas y otros para comentarlas» (Ib.). -->

"While the chiquiteos occupy every day the time that disgracefully is traditional in our region, we'll have to to keep referring in our conversations the exploits that go carrying out others, given that some will take the time for make them and other to talk about them" (Ib.).

<!-- 7. Educación y cooperación -->

7. Education and cooperation

<!-- «La educación y la cooperación están vinculadas algo así como el trabajo y el hombre, que se autorrealizan individual y colectivamente sobreponiéndose a la inercia de la naturaleza y la impotencia originarias e individuales» -->

"Education and cooperation are linked something like work and man, that self-actualizing individually and collectively overcoming the inertia of nature and powerlessness original and individual"

<!-- 7 «Y para hacernos una idea del extraordinario esfuerzo que el Japón viene desarrollando en este campo de la educación y enseñanza, nos limitamos a reflejar los datos correspondientes al presupuesto por habitante para educación en cada uno de los países citados en 1962: Japón, 137,6% dólares. Rusia, 113,0%... España, 2,7%» (FC, II, 52). (EP, II, 258). -->

7 "And to make us an idea of the extraordinary effort that Japan is developing in this field of education and teaching, we limit ourselves to reflect the data corresponding to the budget per inhabitant for education in each of countries cited in 1962: Japan, 137,6% dollars. Russia, 113,0%... Spain, 2,7%" (FC, II, 52). (EP, II, 258).

<!-- Esta vinculación, incluso parcial identidad, entre educación y cooperación puede entenderse en dos sentidos: considerando a la educación como fundamento y base del espíritu cooperativo; en segundo lugar, la cooperación misma puede muy bien ser considerada como una escuela. Este último aspecto deberá ser expuesto con más detención en la parte segunda de este estudio. Por el momento nos limitaremos a recordar las palabras de Arizmendiarrieta: «Quienes se preocupen de conocer la génesis y el desarrollo de nuestra modesta experiencia cooperativa, verán la estrecha vinculación de educación y cooperación, hasta el punto que podemos afirmar que la experiencia cooperativa o comunitaria se resume en lo que da de sí lo que denominaremos un proceso de cooperación educativa consolidada con la educación cooperativa» (Ib. 259). Es un aspecto importante a tener siempre en cuenta, porque en esta visión está implícita la idea de que la experiencia cooperativa no es algo concluido, fijo e inamovible, sino abierto a ulteriores desarrollos, según la experiencia misma vaya sirviendo como escuela de educación permanente. -->

This connection, even partial identity, between education and cooperation can be understood in two senses: considering education as a foundation and base of the cooperative spirit; in second place, cooperation itself may well be considered as a school. The latter aspect shall be exposed with more detention in the part second of this study. For the moment us limitaremos to remember the words of Arizmendiarrieta: "Who worry of get to know the genesis and the development of our modest cooperative experience, will see the close connection between education and cooperation, to the point where we can affirm that the cooperative experience or community can be summarized in what it gives of itself which we will call a process of educational cooperation consolidated with cooperative education" (Ib. 259). It is an important aspect to always have in mind, because in this vision is implicit the idea that the cooperative experience is not something concluded, fixed and immobile, but open a other developments, according to experience itself serves as school of permanent education.

<!-- Por otro lado la educación es un presupuesto de la cooperación; esta tiene su génesis en un proceso educativo de transformación y maduración (CLP, III, 175). En doble sentido, nuevamente: primero, porque la cooperación supone un cierto tipo de personalidad, ideales, etc. «La génesis de esta Experiencia hay que situarla en el proceso de una acción educativa de profundo y actual sentido humanista, que entraría no solamente la toma de conciencia de valores humanos sino el consiguiente compromiso personal y social de aplicarlos, determinando una promoción socio-económica ineludible, reclamada por el bien común» (Ib. 182). En segundo lugar la educación es base de la cooperación en el sentido de que esta, al menos tal como ha encontrado su fórmula en la experiencia cooperativa de Mondragón, presupone un cierto nivel cultural comunitario. «No en vano en esta comarca la inquietud más hondamente compartida y el empeño más seriamente llevado a cabo constituyó la socialización del saber, entendiendo por ello no tanto la educación con grandes inversiones de una élite profesional cuanto la formación de amplia base social con la consiguiente mentalización de numerosos contingentes de jóvenes» (Ib. 167). -->

On the other hand education is a presupposition of cooperation; this has its genesis on a educational process of transformation and maturation (CLP, III, 175). In a double sense, again: first, because cooperation supposes a certain kind of personality, ideals, etc. "The genesis of this Experience must be placed in the process of educational action of profound and current humanist meaning, that would enter not only the awareness of human values but the consequent personal commitment and social of apply them, determining member-economic advocacy inescapable, claimed by the common good" (Ib. 182). Secondly education is base of cooperation in the sense that this, at least such as has found their formula in Mondragon cooperative experience, presupposes a certain level cultural community. "Not for nothing in this village the unease more deeply shared and the performance more seriously carried out established the socialization of know, this being understood not so much education with large investments of a professional elite how much the formation of wide social base with the consequent mentalización of numerous contingent of youth" (Ib. 167).

<!-- El cooperativista, nos dice Arizmendiarrieta, es en primera instancia un trabajador por cuenta propia. Esta definición aparentemente simple oculta una realidad muy compleja. No depende de otro, a quien le proporcione una plusvalía; pero tampoco tiene a otro que le vaya dando soluciones que él no haya previsto. El cooperativista se constituye en empresario propio de quien depende la continuidad, la productividad, la evolución o desarrollo de su actividad. En el mundo económico nuestro éstas son-unas condiciones que, como Arizmendiarrieta subrayará, entrañan plena entrega y mucha responsabilidad. -->

The cooperator, Arizmendiarrieta tells us, is in first instance a self-employed worker. This apparently simple definition hides a very complex reality. He does not depend on other, to whom he provides surplus value; but nor does he have another who gives him solutions that he has not foreseen. The cooperator is constituted as businessperson own on whom depends continuity, productivity, evolution or development of their activity. In our economic world these are-some conditions that, like Arizmendiarrieta will underscore, entail full devotion and great responsibility.

<!-- En estas condiciones el procedimiento para lograr unas condiciones de éxito no puede consistir en hacer cada uno lo que le agrada o mejor se le acomoda. Requiere sumo cuidado en el cultivo de las relaciones humanas: solidaridad, etc. El cooperativismo que aspira a vivir a expensas propias en tanto garantizará la satisfacción de su aspiración en cuanto constantemente se empeña en perfeccionarse a sí mismo en todos los dominios humanos. «Las ideas y la consiguiente mentalidad que promueven no son menos indispensables para la buena marcha de nuestras cooperativas que sus instalaciones y máquinas. Aquellas comprometen la eficacia de la cooperativa a la larga tanto como la energía eléctrica que pone en movimiento los talleres. De aquellas dependerá nuestra organización y proyección empresarial, son ellas la energía de que se nutren nuestros cerebros y nuestro potencial humano» (FC, I, 89). -->

In these conditions the procedure to achieve some conditions of success cannot consist of make everyone which pleases or better se le acomoda. It requires high care in cultivation of human relations: solidarity, etc. Cooperativism which aims to live at its own expense while will guarantee the satisfaction of their aspiration as for constantly strives to perfect itself in all human domains. "Ideas and the resulting mentality that promote no are less indispensable for good march of our cooperatives that their facilities and machines. Aquellas comprometen the efficiency of the cooperative over the long term as well as the electrical energy that moves the workshops. From those depend our organization and future of our business, are they the energy that feed our brains and our human potential" (FC, I, 89).

<!-- Arizmendiarrieta concibió un «Plan de Formación Cooperativa» —el escrito parece proceder de 1959— en cuyos prolegómenos manifiesta muy sintéticamente: no puede haber cooperación sin cooperadores; sólo existirán normalmente cooperadores si se forman (Ib. 1). La importancia que Arizmendiarrieta concedía a la formación puede deducirse de la respuesta a algunas cooperativas, envueltas por esas fechas en un cúmulo de dificultades económicas y no muy inclinadas a preocuparse por cursillos de formación de sus socios en tales circunstancias: «(...) la dedicación de tiempos y preocupación a la formación cooperativista con toda seguridad ayudará mucho a que, económicamente inclusive, la entidad adquiera más solidez, puesto que todos sus miembros podrán contribuir más efectivamente a que la misma se desarrolle y prospere» (Ib. 2). -->

Arizmendiarrieta conceived a "Cooperation Training Plan" —the writing seems come from 1959— in whose introduction manifests very synthetically: there cannot be cooperation without cooperators; only will exist normally cooperators if they are trained (Ib. 1). The importance that Arizmendiarrieta granted to the training can be deduced from the answer to some cooperatives, wrapped up by that time in a cluster of economic difficulties and not very inclined to worry about training courses of their members under such circumstances: "(...) the dedication of times and worry a cooperative training will certainly help a lot a that, economically including, the entity acquire more solidity, given that all its members will be able to contribute more effectively to the same develops and prospers" (Ib. 2).

<!-- «Uno nace hombre o mujer, pero no tornero o modelista, y mucho menos médico o ingeniero. Para llegar a ser buen oficial o técnico hacen falta muchas horas de aprendizaje o estudio y normalmente uno ha necesitado de maestros. -->

"One is born man or woman, but not turner or modelista, and much less doctor or engineer. For become good official or technical needed many hours of learning or study and normally one needs teachers.

<!-- Uno no nace cooperativista, porque ser cooperativista requiere una madurez social, un adiestramiento de convivencia social. Para que uno sea auténtico cooperativista, capaz de cooperar, es preciso que haya aprendido a domesticar sus instintos individualistas o egoístas y sepa plegarse a las leyes de la Cooperación. -->

One is not born cooperator, because be cooperator requires a social maturity, instruction in social co-existence. For one to be authentic cooperator, capable of cooperate, it is necessary there to be learned to tame your individualist instincts or selfish and knows submit to the laws of cooperation.

<!-- Se hace uno cooperativista por la educación y la práctica de la virtud» (FC, I, 24). -->

One becomes a cooperator through education and the practice of virtue" (FC, I, 24).

<!-- No basta que las ideas —la idea cooperativa, en este caso— sean buenas. -->

It is not enough for ideas —the cooperative idea, in this case— are good.

<!-- «Me ha admirado, escribe Arizmendiarrieta, la alegría de tantos aspirantes y aspirados, que se lanzan a solicitar el ingreso en las cooperativas, dispuestos a todo; no les arredran ni los turnos, ni los esfuerzos, ni los índices, ni los puestos más o menos desagradables, para que un momento dado ese espíritu, esas energías, esas promesas se desvanezcan con la misma rapidez que se instauraron» (FC, II, 34). Para ser cooperativo, aparte de su bagaje personal, que incluso ha sido modelado en el entorno familiar, fragua su personalidad en un proceso continuo de integración, aceptando o rechazando conceptos o situaciones que el medio ambiente le va presentando. Si las represiones a que se ha visto obligado tienen una gran carga afectiva, se originan los conflictos, que se manifiestan en el individuo bajo la forma de una inadaptación. El número de estos inadaptados que pululan por nuestras cooperativas, confiesa Arizmendiarrieta, es más elevado del que sería de desear. Las causas pueden ser diversas: la falta de reflexión en el momento de solicitar el ingreso, acuciados por sus familiares o compañeros; haber considerado la cooperativa como un trampolín para el lucro personal, etc., en definitiva, falta de un proceso educativo que signifique el necesario cambio de mentalidad, «ya que para aceptar el ideal cooperativo hace falta una calidad humana que cuesta tenerla» (Ib. 35). -->

"Me has admired, writes Arizmendiarrieta, the joy of many aspiring and aspirates, that launch request income in the cooperatives, ready for anything; not daunted them or the shifts, or the efforts, or the indexes, or positions rather disagreeable, so that a given moment that spirit, those energy, those promises fade with the same speed that were established" (FC, II, 34). To be cooperative, separate from their baggage personal, even was modeling in family setting, wrecks their personality in a continuous process of integration, accepting or rejecting concepts or situations that the environment presents to him. If repressions of which has seen forced have a large affective burden, originate conflicts, manifesting in the individual in the form of maladjustment. The number of these misfits that swarm by our cooperatives, confesses Arizmendiarrieta, is most high of the which would be of desire. The causes can be various: the lack of reflection at the time of request income, goaded by your family or peers; have considered the cooperative like a trampoline for personal profit, etc., definitively, lack of a educational process that means the necessary change in mentality, "since to accept the cooperative ideal is needed a human quality that costs have it" (Ib. 35).

<!-- A este respecto Arizmendiarrieta entiende la educación como una práctica cooperativa previa a la promoción cooperativa propiamente dicha. «Supuesto que todos sabemos que el hombre no nace sino se hace por la educación, la mejor inducción de práctica cooperativa es, precisamente, la que pudiera consistir en provocar en el hombre que encontráramos, unas apetencias que mejor, más honda y rápidamente, pudieran inducirle a su potenciación, que sin duda constituyen en este momento los recursos de la cultura, de la técnica, por cuyo motivo la práctica cooperativa, que debe anticiparse al movimiento cooperativo propiamente dicho, debe consistir en la convocatoria a una cooperación para la movilización precisa de creación y desarrollo del aparato conducente a la aplicación práctica de opciones de educación, a la realización posible de la igualdad de oportunidades. —Ante la imperiosa necesidad de conjugar los dos extremos de un circuito indispensable para las promociones deseadas, de hombres nuevos para promover estructuras nuevas o de hacer viables nuevas estructuras para nuevos hombres y nuevo orden socio-económico, las providencias indispensables, así como los presupuestos precisos, están constituidos por un quehacer, en el que más o menos podemos coincidir sin ulteriores compromisos unos y otros, muchos de los que más o menos estamos insatisfechos de las presentes realidades o limitaciones; este quehacer cuyo objeto pudiera ser apetecible desde los más variados ángulos de intereses y puntos de vista es educativo, tendente a potenciar tanto a los que nos han de relevar como a los que con nosotros pudieran tener que seguir compartiendo la lucha por la vida» (FC, III, 96-97). -->

In this respect Arizmendiarrieta understands education like a cooperative practice prior to cooperative advancement properly said. "supposed that we all know that man is not born but is made through education, the best induction of cooperative practice is, precisely, that which would consist of provoke in the man who we find, some apetencias that better, more honda and rapidly, could induce you to their empowerment, that without a doubt constitute at this moment the resources of culture, of technique, by whose motive the cooperative practice, that must to be anticipated al cooperative movement rightly said, must consist of the call to a cooperation for the mobilization precise of creation and development of the device conducive to the practical application of options of education, a the possible realization of equality of opportunities. —Faced with the overwhelming need to combine the two extremes of a circuit essential for the desired promotion, of new men to promote new structures or of making viable new structures for new men and new socioeconomic order, the providences indispensable, as well as the budgets precise, are made up of a chore, in which more or less we can coincide without outside commitments everyone, many of those who more or less we're dissatisfied of the present realities or limitations; this task whose object could be desirable from the most varied angles of interests and points of view is educational, tending a maximize both to which us must survey like to which with us could have to to keep sharing the struggle for life" (FC, III, 96-97).

<!-- Esta práctica cooperativa comienza por apoyar a nuestros semejantes para que puedan tener opciones de más amplio horizonte en la vida para realizarse cada uno a sí mismo; es el enseñarle a pescar, más que darle un pescado; es suscitar las potencialidades que duermen en su interior. «Un despilfarro que no puede tolerar ningún pueblo consciente de su posición es el de los hombres, mejor diríamos, el de la capacidad de trabajo de sus hombres» (FC, II, 106). El derroche de materiales nos escandaliza y, sin embargo, el derroche de energías humanas es muy superior, sin que nos preocupe tanto. -->

This cooperative practice begins by support for our peers so that can have options of broader horizon in life so each one can be self-realized; is the teach him to fish, more than give him a fish; is arouse the potential that sleep inside it. "A waste that can't tolerate no people conscious of their position is that of men, or, it would be better for us to say, that of the ability to work of their men" (FC, II, 106). Waste of materials scandalizes us and, however, the waste of human energy is very superior, without it worrying us so much.

<!-- No solamente de energías intelectuales o laborales, sino también de cambio, que se pierden por falta de adecuada educación. -->

No only of intellectual energy or labor, but also of change, that are lost for a lack of adequate education.

<!-- 8. Educación y orden nuevo -->

8. Education and new order

<!-- «(...) La hegemonía del capitalismo y de la burguesía se deben predominantemente a la influencia en la civilización y en las costumbres a través de elementos y recursos culturales, como son la prensa, la literatura, etc.» (FC, I, 279). Los trabajadores, si quieren hacer sentir su influencia en la vida pública y social, necesitan superar esta dependencia, apropiarse de los bienes de la cultura, necesitan formación propia (FC, II, 109-111). «El movimiento cooperativo, que acertadamente se ha definido como un movimiento económico que emplea la acción educativa o movimiento educativo que utiliza la acción económica, es muy natural que dé máximo rango a esta prerrogativa vital del hombre, cual es su derecho natural a participar de los beneficios de la cultura, que por otra parte es presupuesto indispensable para exigir a cada uno que viva con arreglo a sus méritos personales» (FC, I, 279). -->

"(...) The hegemony of capitalism and of the bourgeoisie are due predominantly to the influence on civilization and in customs through elements and cultural resources, as they are the press, literature, etc." (FC, I, 279). Workers, if they want make their influence felt on public life and social, need overcome this dependence, appropriate the goods of culture, need training of their own (FC, II, 109-111). "The cooperative movement, that rightly has been defined like an economic movement that employs educational activity or educational movement that uses economic action, is very natural that gives maximum range to this prerogative vital of man, which is their natural right to participate in the benefits of culture, that on the other hand is indispensable presupposition for demand from each that alive with arrangement to their personal merits" (FC, I, 279).

<!-- Los cooperativistas se han comprometido con responsabilidades, desde las necesarias para disponer o crear capital mediante el ahorro o el crédito, hasta la de imponerse una disciplina y una organización severas para que su actividad fuera fecunda; han prescindido de tutelas extrañas, etc., dando de esta forma un paso para la constitución de un orden nuevo, en el que cada uno viva en consonancia con sus méritos personales, en régimen de auténtica solidaridad. La superación del contrato de trabajo por el de sociedad debe significar algo más que la reconstitución de nuevos grupos de clases privilegiadas, de trabajadores «diferentes». Debe ser el primer eslabón de un proceso que culmine en un orden nuevo más humano. «Nuestro compromiso cooperativista no puede perder de vista la meta de un nuevo orden social y hemos de aplicar nuestro esfuerzo para su implantación en las áreas de nuestra influencia, si no queremos quedarnos con un cooperativismo de vía estrecha y en callejón sin salida» (Ib. 140). Para ello, para ir construyendo el nuevo orden, «la promoción cultural y profesional de las nuevas generaciones sin discriminaciones sociales y económicas es una necesidad apremiante bajo todos los aspectos para nosotros» (Ib. 141). No habrá emancipación y orden nuevo mientras se siga dependiendo culturalmente de otros. Hay que socializar el saber, para poder instaurar un orden nuevo. «¿Desconocemos las dificultades prácticas con que tropiezan extraordinarios empeños de revolución en tanto no logren dicha socialización en escala y grados suficientes para llevar a cabo las aplicaciones de los recursos técnicos?» (EP, II, 103). -->

Cooperators have committed to responsibilities, from those needed to dispose or create capital through savings or credit, to that of be imposed a discipline and an organization severe so that their activity was fruitful; have prescindido of guardianships strange, etc., in this way, taking a step towards the constitution of a new order, in which each one lives in accordance with your personal merits, in regime of authentic solidarity. The improvement of the labor contract by that of society must mean something more than the reconstitution of new groups of privileged classes, of workers "different." It must be the first link of a process that culminates in a new, more humane order. "Our cooperative commitment cannot lose sight the goal of a new social order and we need to apply our effort for its implantation in the areas of our influence, if we don't want to stay with a cooperativism of via close and in dead-end street" (Ib. 140). For this reason, to go building the new order, "cultural advancement and professional of the new generations without social discrimination and economic is a priority need in all aspects to us" (Ib. 141). There will be no emancipation and new order while there is a cultural dependence on others. We need to socialize knowledge, to be able to establish a new order. "Desconocemos the practical difficulties with which stumble extraordinary efforts of revolution while fail this socialization in scale and degrees sufficient to bring about applications of the technical resources?" (EP, II, 103).

<!-- La revolución cultural, la verdaderamente profunda, que transforma las conciencias para que estas transformen luego las realidades, requerirá necesariamente su tiempo. «No importará que seamos lentos en nacer, como lo es el hombre, si podemos ser dinámicos y fuertes en el vivir y progresar» (Ib. 182). «El mundo mejor que anhelamos y tratamos de construir ha de tener su mejor punto de apoyo en la movilización de los recursos educativos y se realiza en la medida que sensibilizamos a las nuevas generaciones con los altos valores, destinados a perfilar un nuevo orden social» (Ib. 24). -->

The cultural revolution, the truly profound, that transforms consciences for that these transform then realities, will require necessarily their time. "It will not matter that we are slow to be born, which is man, if we can be dynamic and strong in living and progress" (Ib. 182). "The world better that we yearn for and try to build must have their better fulcrum point in the mobilization of educational resources and is carried out to the extent that sensibilize the new generations with high values, destined to to profile a new social order" (Ib. 24).

<!-- Armonizando las dos máximas bíblicas «no sólo de pan vive el hombre» y «la verdad os hará libres», Arizmendiarrieta se atreve a afirmar que contar con el pan es para el hombre lo de menos, «si previamente no ha escatimado esfuerzos para esa elemental liberación y consiguiente emancipación social mediante la promoción y potenciación de sus facultades de trabajo y acción» (EP, II, 75). Más que a mejoras materiales momentáneas, la atención de los cooperativistas debe dirigirse sin escatimar costos y esfuerzos a la educación, haciendo realidad el principio de la igualdad de oportunidades. «No atenuemos lo que bajo ninguna vertiente, en ninguna coyuntura puede carecer de sustantividad e interés para obtener la prioridad, en orden a lo que pudiera ser necesitado por todos los hombres y todos los estamentos sociales: el acceso a los bienes con el más elevado coeficiente de reproductividad y el disfrute de lo que ha de contribuir a afirmar la máxima autonomía de cada hombre como el soporte más sólido de desarrollo de toda comunidad: eso es la cultura» (Ib.). -->

Harmonizing the two Biblical maxims "man does not live by bread alone" and "the truth will make you free," Arizmendiarrieta dares to affirm that have bread is the least of man's concerns, "if previously has not escatimado efforts for that elemental liberation and consequent social emancipation through promotion and empowerment of their faculties of work and action" (EP, II, 75). More than a material improvements momentary, the attention of cooperators must be directed without sparing expense or effort on education, making reality the principle of equality of opportunities. "No atenuemos which under none slope, in any situation may lack substantivity and interest to get the priority, for lo which could be needy by all men and everyone the estates social: access to the goods with the most high coefficient of reproductivity and enjoyment of what must contribute to affirm the highest autonomy of each man as the most solid support for development of every community: that is culture" (Ib.).

<!-- El cooperativismo, haciendo a los estudiantes trabajadores y que los trabajadores puedan a su vez ser estudiantes en cualquier etapa de su vida (Ib. 211), se sitúa «en el umbral de un nuevo tipo de sociedad sin clases y castas, sin señoritos ni desconsideradamente marginados» (Ib. 208). «Los unos deben poder acreditarse con algo más que aspiraciones de promoción cultural y académica; deben poder avalarlos con un comportamiento y una actitud que pueda ser garantía para un mañana más prometedor en que un Régimen de Solidaridad efectiva no les parezca ni heroica ni incómoda. Los otros, quienes hoy gestionan los escasos recursos destinables a la promoción indiscriminada y acreditada, deben también saber adoptar métodos de gestión coherentes con la naturaleza de los fondos sociales y exigencias maximalizantes de la acción formativa; deben saber ayudar a quien se sabe ayudar con lo que dispone, dinero o tiempo y sobre todo a quien sabe que el trabajo, bajo cualquiera de las modalidades, es un medio tanto de autorrealización como de testimonio y colaboración social» (Ib. 208-209). -->

Cooperativism, making students workers and that workers can in turn be students at any stage of its life (Ib. 211), stands "in the threshold of a new kind of classless society and castes, without a privileged class or desconsideradamente marginalized" (Ib. 208). "Los some must be able be accredited with something more than aspirations of cultural promotion and academic; must be able avalarlos with a behavior and an attitude that can be guarantee for a more promising tomorrow on which a regime of Effective solidarity does not seem to them either heroic or uncomfortable. The others, who today manage the scarce resources destinables a promotion indiscriminate and accredited, should also know how to adopt management methods consistent with the nature of social funds and maximalizing demands of formative action; should know how to help who is known help with what is available, money or time and above all a who knows that work, under any of modalities, is a medium both of self-realization like of testimony and social collaboration" (Ib. 208-209).

<!-- Para el establecimiento de un orden nuevo no basta con hombres nuevos; se requieren estructuras nuevas. Y quien quiere transformar las estructuras se encuentra con la realidad, no bella, pero históricamente confirmada, de que «el poder precede a la razón», poder que «es el determinado por una posición de fortaleza económica, cultural o social, que cabe reducirlos a un común denominador de poder efectivo» (FC, III, 98). -->

For the establishment of a new order it is not enough to have new men; are required new structures. And whoever wants transform the structures encounters reality, no lovely, but historically confirmed, that "power precedes the reason," power that "is the determinant for a position of economic strength, cultural or social, that fits reduces them to a common denominator of effective power" (FC, III, 98).

<!-- «En la práctica cooperativa que contempla la instauración de un nuevo orden, de un nuevo mundo, de un nuevo hombre, no debemos desentendernos de este resorte para la realización de nuestros fines y objetivos finales. Hoy cabría explicitarlo más añadiendo otra observación y es también verdad, que la fuerza de la razón de los medios derivados de la aplicación de los elementos que se derivan de nuestra racionalidad, son los que de hecho pudieran servirnos para medir nuestro grado de efectivo desarrollo o de organización. Es cierto que no dejamos de estar en subdesarrollo organizativo mientras no diéramos paso y curso a dichos medios de la razón en nuestra organización, lo cual significa la adopción de la previsión y, por consiguiente, de la planificación como providencia y expediente indispensable de promoción» (Ib.). Si a esto añadimos lo que para la conciencia cooperativa actual no puede en ningún momento dejarse preterido, como es la conciencia de libertad y de justicia y la solidaridad en ejercicio activo, ya tenemos el cuadro de medidas óptimas para dinamizar el movimiento cooperativo, camino de un orden nuevo real. «Socializado el saber es posible democratizar de hecho el poder» (Ib.), e.d., el poder podrá ser de todos, sin estar monopolizado por las clases que poseen en monopolio la cultura. -->

"In the cooperative practice which includes the establishment of a new order, of a new world, of a new man, we should not avoid this spring for the realization of our final ends and objectives. Today would fit make it explicit more adding another observation and it is also true, that the strength of reason of the media derivatives of the application of the elements that are derived from our rationality, are the ones that in fact could serve us to measure our degree of cash development or about organization. It is true that we are still organizational underdevelopment as long as we do not take the step and course a these media of reason in our organization, which means the adoption of foresight and, consequently, of planning like providence and file indispensable of advancement" (Ib.). If we add to that lo that to the conscience cooperative current can at no time leave aside, which is the consciousness of freedom and of justice and solidarity actively exercised, we already have the cadre of optimal measures to energize the cooperative movement, path of a real new order. "Socialized knowledge is possible democratize in fact power" (Ib.), e.d., power may be of all, without be monopolized by the classes that possess in monopoly culture.

<!-- Arizmendiarrieta cree que con el movimiento cooperativo el nuevo orden queda ya instaurado. La razón está ya ahí, debe acceder al poder: y ha empezado ya a hacerlo. «Contamos ya con hombres con sensibilidad y capacidad personal como para que su encuentro y su conjunción se presten al más amplio abanico de participaciones, de relevos, de proyección en condiciones de una convivencia fluida», etc. «Dadas estas condiciones, nada ha de tener de particular que se reproduzca el fenómeno singular del pasado, en el que existe una transformación y un cambio radical operados sin revolución violenta, cuando como clase la burguesía reemplaza y deja atrás a la aristocracia, a todo su mundo de estructuras creadas a la medida de aquella, cuando con su gran espíritu de trabajo, que es el poderoso recurso creador del hombre, y su efectivo desarrollo cultural y técnico, ha podido dejar atrás a aquel; es un hecho, la burguesía ha relevado a la aristocracia y ha montado unas estructuras consonantes con su espíritu y aspiraciones en la medida que la burguesía fue reconstituyéndose con el trabajo y la cultura. Ahora lo que hace falta es que el proletariado sea capaz de hacer otro tanto con la burguesía para dar paso a un mundo social y humano, fluido y dinámico» (Ib. 99). -->

Arizmendiarrieta thinks that with the cooperative movement the new order remains already established. Reason is already there, must access the power: and has begun already to do it. "We already have men with sensitivity and personal capacity so that their meeting and their conjunction They lend to wider fan of shares, of relay, of projection in conditions of a fluid coexistence," etc. "Given these conditions, nothing must have of particular that is played the phenomenon singular of the past, in the that exists a transformation and a radical change operated without violent revolution, when as a class the bourgeoisie replaces and leaves behind a the aristocracy, to all their world of structures created a the measure of that, when with their large spirit of work, which is the powerful resource creator of man, and their effective cultural development and technical, has been able leave behind a that; is a fact, the bourgeoisie has relieved the aristocracy and has mounted some structures consonant with their spirit and aspirations to the extent that the bourgeoisie was reconstituting with work and culture. Now what is missing is for the proletariat is capable of doing other both with the bourgeoisie for give way to a social world and human, fluid and dynamic" (Ib. 99).

<!-- 9. Ocio educativo -->

9. Educational leisure

<!-- «Si estamos en una trayectoria económico-social que tiene a la vista una meta no lejana, cual es la conquista y civilización del ocio, pasando para ello por una mayor fecundidad de nuestro trabajo mediante la aplicación decisiva del progreso técnico, sepamos que esta ruta requiere igualmente para disfrutar del trabajo que del ocio, decorosa y dignamente, un mayor cultivo de nuestras facultades superiores (...)» (EP, I, 130). -->

"If we're in a trajectory economic-social who has in sight a goal no distant, which is the conquest and civilization of leisure, passing for this by a greater fertility of our work through the decisive application of technical progress, we know that this route requires equally to enjoy work that of leisure, decorous and worthily, a greater cultivation of our higher faculties (...)" (EP, I, 130).

<!-- El ocio, como espacio para dar satisfacción a tantos y tan entrañables anhelos, aficiones e incluso caprichos, está al alcance de la mano. «Pero el ocio por sí mismo, carente de opciones y capacidad activa discrecional, tiende a paralizar más que a tensar y estimular al hombre. Su perspectiva debe movernos a dar un contenido más amplio y previsor para el trabajo y el ocio a la formación, al despertar, cultivar y encauzar las inquietudes humanas. -->

Leisure, like space to give satisfaction a many and so entrañables desires, hobbies and even whims, is within reach. "But leisure by himself, lacking in options and capacity active discrecional, tends to paralyze more than to tighten and stimulate to man. Its perspective must move us to give a content wider and farsighted for work and leisure a the training, al wake up, cultivate and channel human concerns.

<!-- Constituye un buen desafío para nuestra creatividad y capacidad gestora el afrontamiento de los nuevos problemas que representa el sujeto humano evolucionado a los efectos de promoción de nuevas condiciones coherentes con su sensibilidad y apentencias» (CLP, I, 267-268). -->

It constitutes a good challenge for our creativity and management capacity the afrontamiento of the new problems that represents the human subject evolved for the effects of advancement of new conditions consistent with its sensitivity and apentencias" (CLP, I, 267-268).

<!-- Trataremos de resumir a continuación los pensamientos más destacables de Arizmendiarrieta en torno al ocio. -->

We will try of summarize below the thoughts more destacables Arizmendiarrieta's concerning leisure.

<!-- 9.1. Diversiones de la juventud -->

9.1. Diversions of youth

<!-- Los dos escritos de Arizmendiarrieta que se ocupan de las diversiones de la juventud datan del año 1945. «En la zona industrial, nos dice, existe el problema de la ociosidad, que muchas veces no se puede solucionar más que con el fomento del deporte. Eso de que todos los días desde las seis o seis y media estén desocupados y sin saber qué hacer tantos hombres y jóvenes, es una tentación muy mala para los mismos. En parte se soluciona con la enseñanza profesional y clases nocturnas, pero siempre queda flotando una masa de los que no saben qué hacer» (PR, I, 87). -->

The two writings Arizmendiarrieta's that occupy themselves with the entertainment of youth date from the year 1945. "In the industrial zone, tells us, there exists the problem of the idleness, that often times can't solve more that with the promotion of the sport. That that every day from las six or 6:30 to be unemployed and without knowing what to do many men and youth, is a very bad temptation for the same. In part is solved with professional teaching and night classes, but always remains floating a mass of those that do not know what to do" (PR, I, 87).

<!-- Las tres grandes diversiones que se le ofrecen al joven, tres grandes problemas, son el cine, la taberna y el baile. El joven tiene necesidad de expansión, no sólo física, sino social y afectiva. Arizmendiarrieta no considera recomendable presentarle al joven un código de prohibiciones: «frente al joven hay que adoptar una postura positiva, afirmativa» (CAS, 121). No por ello hay que renunciar a exigir el cumplimiento de las normas legales en los establecimientos de diversión o esparcimiento de los jóvenes: condiciones de luz de los salones de cine, normas relativas a menores, etc. Pero la única verdadera solución puede consistir en ofrecer ocupaciones alternativas. -->

The three large entertainment that are offered to the young person, three big problems, are the cinema, the tavern and the dance. The young person has need for expansion, not only physics, but social and affective. Arizmendiarrieta no considers advisable to introduces the young person a code of prohibitions: "before al young person must adopt a positive stance, affirmative" (CAS, 121). No therefore must renounce demand the fulfillment of the legal norms in the establishments of fun or recreation of youth: lighting conditions of movie theaters, standards relative to younger, etc. But the only true solution can consist of offer alternative occupations.

<!-- Arizmendiarrieta no siente gran estima por el cine o el baile, pero es la taberna lo que le desagrada sobremanera. «¿Hay quien crea que se puede mantener la llama del idealismo dentro de ese ambiente?» (Ib. 124). Arizmendiarrieta propone como alternativa la creación de clubs, centros bien dotados que, a la par de sitios para congregarse y conversar, fueran lugares de esparcimiento y elevación o superación. «Centros con su bar, con su restaurante, con su sala de juegos con su sala de lectura, de radio. Centros con mesas de billar, de ping-póng, fútbol de mesa, damas, dominós, revistas, libros, bolos, ranas, etc. Unos centros completos, bien aireados y limpios, que hasta la misma limpieza y luz contribuye a elevar al hombre» (Ib. 126). Estos centros congregarán a los mejores, los estimularán a la superación, con el ejemplo de los estudiosos o curiosos; se ofrecerá a los jóvenes un margen de iniciativa para la organización y mantenimiento del centro mismo, para que vayan asumiendo responsabilidades. Desde estos centros se fomentará el arte, la música, los bailes regionales; se formarán orfeones y coros, grupos artísticos. Y, sobre todo, se fomentará el deporte» (Ib. 127), porque sirve para el mejor desarrollo del cuerpo, pero sirve también, y ante todo, para el desarrollo de las más hermosas virtudes naturales: arrojo, valentía, lealtad, espíritu de disciplina, espíritu de sacrificio, fraternidad, generosidad. Arizmendiarrieta considera el fútbol, la pelota y los bolos como los deportes más apropiados en el verano; en invierno el billar. Da una gran importancia al montañismo, «por el que se siente una preferencia especial en estas zonas industriales» (PR, I, 87-88). -->

Arizmendiarrieta does not feel great esteem for the cinema or the dance, but it is the tavern which displeases exceedingly. "There are those who creates that se can maintain the calls of the idealism within that environment?" (Ib. 124). Arizmendiarrieta proposes as an alternative the creation of clubs, centers well gifted that, together with sites to congregate and converse, were places of recreation and elevation or improvement. "Centers with their bar, with their restaurant, with their game room with their reading room, radio. Centers with pool tables, of ping-pong, foosball, ladies, dominos, magazines, books, bowling, frogs, etc. Some centers complete, well aerated and clean, that even the same cleaning and light contributes to elevate to man" (Ib. 126). These centers will congregate the best, will stimulate them a improvement, with the example of the scholars or curious; se will offer a youth a margin of initiative for the organization and maintenance of the center itself, so they will take on responsibility. Since these centers will be promoted art, music, regional dances; will form glee club and choirs, artistic groups. And, above all, will be promoted sport" (Ib. 127), because serves to the best development of the body, but serves too, and foremost, for the development of the most lovely natural virtues: bravery, courage, loyalty, spirit of discipline, spirit of sacrifice, fraternity, generosity. Arizmendiarrieta considers soccer, the ball and the bowling like the most appropriate sports in summer; in winter billiards. He gives great importance to mountaineering, "whereby is felt a special preference in these industrial zones" (PR, I, 87-88).

<!-- Para las chicas no parece tener muchas alternativas que ofrecer: grupos o coros artísticos; organización de rifas, etc., que les servirá asimismo para ir asumiendo responsabilidades, aprendiendo a organizar con seriedad y orden, etc. A las representaciones teatrales de los jóvenes les ve un gran inconveniente: pérdida de tiempo que se podría aprovechar en causas mejores. (A pesar de ello Arizmendiarrieta organizó numerosas representaciones teatrales, tal vez porque de este modo se llegaba al menos a ocupar un tiempo que de otra manera sería irremediablemente perdido). «El problema de las distracciones de las chicas es más difícil de solucionar, aunque, por otra parte, no sea tan urgente como el de los chicos» (Ib. 88). -->

For the girls no seems to have many alternatives that offer: groups or choirs artistic; organization of raffles, etc., that will also serve them to take on responsibilities, learning to organize with seriousness and order, etc. At theatrical presenations of youth les sees a large inconvenient: waste of time that se could take advantage in causes better. (In spite of that Arizmendiarrieta organized numerous theatrical presenations, perhaps because in this way se arrived at least a occupy a time that would otherwise be hopelessly lost). "The problem with the distractions of the girls harder to solve, although, on the other hand, it is not as urgent as the the boys" (Ib. 88).

<!-- 9.2. Preparación para el ocio -->

9.2. Preparation for leisure

<!-- Arizmendiarrieta tiene la impresión de que todas esas inmensas posibilidades del ocio nos pillan desprevenidos; no estamos preparados para la llamada «civilización del ocio». -->

Arizmendiarrieta has the impression that all those immense possibilities of leisure us caught off guard; we're not prepared for the so-called "civilization of leisure."

<!-- «El que el ocio nos aplaste o que nos proporcione un valioso margen para nuestro perfeccionamiento depende de lo que seamos capaces de descubrir en la vida: si no sabemos ver y palpar más que a lo animal la civilización del ocio no contribuirá a mejorar la suerte humana en forma considerable, como los buenos pastos o las despensas repletas no les proyectan hacia planos superiores a los animales» (FC, II, 206). -->

"The that leisure us crushes or that provides us a valuable margin for our perfection depends on what we are capable of discovering in life: if we don't know see and feel more than a lo animal the civilization of leisure will not contribute to improve human fate considerably, like the good fields or las despensas repletas no les proyectan towards blueprints above animals" (FC, II, 206).

<!-- La preparación para el ocio debe empezar en la fase educativa. Para que el ocio sea circunstancia favorable para el desarrollo humano, para el cultivo del hombre como hombre, es preciso que en la fase de preparación para la vida el joven no sólo adquiera un saber técnico, inmediatamente dirigido al rendimiento en el trabajo, sino que cultive sus facultades superiores de forma que más adelante sea capaz de aplicarlas en los más variados campos de promoción espiritual, estética, etc., de forma que cada vez se vaya haciendo un ser más humano y más perfecto, más sensible hacia sus semejantes y capaz de apetecer una convivencia densa y fecunda en todos los aspectos (Ib. 205). -->

The preparation for leisure must begin in the educational phase. For leisure is favorable circumstance for human development, for cultivation of the man as man, it is necessary that in the phase of preparation for life the young person not only acquire a technical know-how, immediately directed al performance in work, but cultivate their higher faculties such that later is capable of apply them in the most varied fields of spiritual advancement, aesthetics, etc., such that every time se vaya making a more human being and more perfect, more sensitive towards their peers and capable of wanting a coexistence dense and fruitful in all the aspects (Ib. 205).

<!-- También hay que desmasificar a los jóvenes; hay que prevenirles contra los riesgos de inhibiciones o pasividad en el disfrute de los medios que la civilización y el progreso ponen a nuestro alcance; hay que inducirlos constantemente a la iniciativa y al ejercicio de la responsabilidad en los dominios del pequeño mundo que a cada uno le rodea; hay que hacer a sus espíritus capaces de sintonizar activamente con los infinitos mensajes y conciertos que cruzan por nuestros espacios vitales (Ib. 206). -->

It is also necessary to desmasificar a youth; must prevenirles against the risks of inhibitions or passivity in the enjoyment of the media that civilization and progress put within our reach; must induce them constantly a initiative and al exercise of responsibility in the domains of the small world that surrounds everyone; there is to do to their spirits capable of tune in actively with the endless messages and concerts which cross by our vital spaces (Ib. 206).

<!-- 9.3. Reforma del calendario laboral -->

9.3. Reform of the labor calendar

<!-- Para optimizar el tiempo dedicado tanto al ocio como al trabajo Arizmendiarrieta considera que se hace necesario reformar el calendario tradicional y nuestras costumbres relativas al tiempo. Reconoce que la tradición y la rutina pesan mucho en este campo; hay una inercia social que no es fácil superar. -->

To optimize the time dedicated both to leisure and to work Arizmendiarrieta considers is made necessary reform the traditional calendar and our customs relative to time. He recognizes that the tradition and the routine weigh a lot in this field; there is a social inertia that is not easy overcome.

<!-- «No obstante, dado que el mundo se nos ha dado no tanto para contemplarlo cuanto para transformarlo y acomodarlo a nuestras necesidades y condiciones, hemos de activar nuestras conciencias y pensar en toda la complejidad de problemas que entraña la vacación» (FC, II, 205). -->

"Notwithstanding, given that the world has been given to us not so much to contemplate it how much to transform it and accomodate it a our needs and conditions, we need to activate our consciences and think about every the complexity of problems that involve the holiday" (FC, II, 205).

<!-- Algunas sugerencias que Arizmendiarrieta se ha atrevido a concretar han sido: que los días de vacaciones, en lugar de ser todos en agosto y para todos al mismo tiempo, pudieran ser desdoblados, al menos para quienes lo prefieran de este modo, «entre la segunda quincena de diciembre y primera de enero y agosto, absorbiendo algunas otras fechas y redondeando el número de días susceptibles de calificarlos como jornadas de vacación» (FC, IV, 240); que se trasladara al sábado, al haberse generalizado como día libre y familiar, todo aquello que pudiera prestarse a mejor aprovisionamiento y más amplia participación de todos, en cuanto redunda en provecho del hogar: esta medida podría contribuir incluso a revitalizar las tradicionales «azokak» o ferias. -->

Some suggestions that Arizmendiarrieta dared to specify have been: that days of vacations, instead of be everyone in August and for everyone at the same time, could be desdoblados, at least for those who prefer it in this way, "between the second quincena of of December and first January and August, absorbing some others dates and rounding the number of days susceptible of calificarlos like days of holiday" (FC, IV, 240); that is moved to Saturday, by having widespread like day free and family, all that which could prestarse a better provisioning and wider participation of all, as for results in advantage of the home: this measure could contribute even revitalize the traditional "azokak" or fairs.

<!-- Se trata de prever y planificar mejor la actividad, para dar paso a una participación más racional, más amplia y humana de los hombres en la vida. En relación a la Iglesia Arizmendiarrieta se pregunta: «¿Hasta cuándo o mediante qué Concilio o Sínodo ha de tratar la Iglesia de acomodar su calendario al mundo del trabajo cuando tanto se habla de la dignidad de este y de la honra y gloria de Dios que entraña el hombre que se empeña en mejorar o complementar la naturaleza que ha heredado?» (Ib. 242). -->

It's about forsee and plan better the activity, for give way to a participation more rational, wider and human of men in life. In relation to the Church Arizmendiarrieta wonders: "How long or through what Council or Synod must treat the Church of accomodate their calendar to the world of work when there is so much talk about the dignity of this and of the honra and glory of God that involve the man who strives to improve or complement nature that has inherited?" (Ib. 242).

<!-- 9.4. Organizar y humanizar el ocio -->

9.4. Organize and humanize leisure

<!-- Como si las cosas de este mundo sólo tuvieran un sentido al perseguir unos fines concretos, con una organización determinada, Arizmendiarrieta quisiera organizar también el ocio. -->

As if things of this world only had a meaning al pursue some concrete ends, with a given organization, Arizmendiarrieta would like organize too leisure.

<!-- Si el hombre ha nacido para la actividad, argumenta Arizmendiarrieta, no se puede decir que el ocio constituye por sí mismo un estado apetecible. El ocio debe consistir, por tanto, en cambiar de actividad, más bien que en dejar toda actividad. El «problema del ocio necesitado de organización humanista» es nuevo; por eso debemos ser nosotros los promotores de unas nuevas condiciones de ocio, que redunden en cultivo y perfeccionamiento de la personalidad (FC, III, 12-13). La promoción personal puede orientarse bien hacia formas de convivencia más amplia y más hondamente compartidas, bien hacia el cultivo de nuestras facultades intelectuales o profesionales. -->

If man was born for the activity, argues Arizmendiarrieta, it can't be said that leisure constitutes by himself a desireable state. Leisure must consist, therefore, in change of activity, rather than in leave every activity. The "problem of leisure in need of humanist organization" is new; which is why we should be we promoters of some new conditions of leisure, that result in cultivation and perfection of the personality (FC, III, 12-13). Staff advancement can well be oriented towards forms of coexistence wider and more deeply shared, well towards cultivation of our intellectual faculties or professional.

<!-- Sería de desear que las vacaciones, el ocio en general, pudieran constituir unas jornadas de reflexión. «Si el hombre se diferencia fundamentalmente de otros animales por su capacidad de reflexionar, debemos aprovechar las vacaciones para pensar cuál es nuestra misión particular en la vida» (Ib. 207). -->

It would be of desire that vacations, leisure in general, could constitute some days of reflection. "If man is fundamentally differentiated from others animals for his capacity for reflection, we should take advantage vacations to think which is our particular mission in life" (Ib. 207).

<!-- En el período de trabajo un buen tanto por ciento de gente «cede su facultad de pensar a los jefes»; fuera de la fábrica nos encontramos, en la sociedad de consumo, con que «unos espabilados crean necesidades para quedarse con economías conseguidas con sudores». En definitiva el hombre resulta fuera y dentro de la fábrica un ente pasivo a quien le dictan lo que debe hacer y dejar de hacer. La propaganda le impone cómo debe usar su ocio: veranear aquí, beber tal refresco, visitar esta sala de fiestas. -->

In the period of work a good both percent of people "cedes their faculty for thought a bosses"; outside of the factory we find ourselves, in the consumer society, with which "some awfully clever people create needs to keep economies collected with sweats." Definitively man turns out to be was and within the factory a passive entity to whom they dictate what he must do and not do. The propaganda imposes how must use their leisure: summer here, drink this soft drink, visit this nightclub.

<!-- Por lo demás, «algo nos falla en nuestra sociedad cuando una labor genuinamente humana como es el trabajo no la consideramos muchas veces como una actividad creativa del hombre. Quizás tenemos la mentalidad de que el trabajo es algo que nos imponen otros, cuando debía ser una voluntad personal de laborar en la construcción de un mundo mejor» (Ib. 206). -->

In spite of all this, "something is failing us in our society when a labor genuinely human which is work we do not consider it often times like a creative activity of man. Perhaps we have the mentality that work is something that others impose on us, when had to be a will personal of labor in the construction of a better world" (Ib. 206).

<!-- 9.5. Ocio y solidaridad comunitaria -->

9.5. Leaisure and community solidarity

<!-- Nuestra sociedad tiene un grave problema de desequilibrio entre los gastos personales y los públicos. El ciudadano con ciertas posibilidades resuelve su problema de transporte comprando un coche particular; los estudios de sus hijos enviándolos a un colegio caro; el problema de su descanso alquilando o comprando una casa de campo. Los ciudadanos con menos medios tratan de hacer lo mismo a escala más reducida. Mientras tanto tenemos unos servicios públicos de transportes muy deficientes, unas escuelas públicas sin los suficientes medios, para no decir nada de los servicios municipales de parques públicos, etc. «El individualismo es una característica de nuestra sociedad» (Ib. 207). -->

Our society has a serious problem of imbalance between personal expenses and the public. The citizen with certain possibilities resolve their problem of transportation buying a private car; studies of their children sending them to an expensive high school; the problem of their rest renting or buying a country house. Citizens of lesser means try to make the same thing on a smaller scale. Meanwhile we have some public transport services very deficient, some public schools without sufficient means, to sat nothing of municipal services of public parks, etc. "Individualism is a characteristic of our society" (Ib. 207).

<!-- Mejor que individualismo habría que decir egoísmo. Mientras las vacaciones se van convirtiendo en un echar la casa por la ventana una vez al año como mejor nos place, se desatiende a lo largo del año la promoción de centros y medios de esparcimiento o expansión para niños y ancianos (Ib. 14). -->

Better that individualism should to say selfishness. While vacations are becoming a throw out the house by the windows once a year however it best pleases us, is disregarded throughout the year the promotion of centers and means of recreation or expansion for children and senior citizens (Ib. 14).

<!-- Deberíamos pensar en poder aplicar a lo largo de los muchos meses más opciones de disfrute, más solidaria y comunitariamente. La construcción de parques, piscinas, lugares de esparcimiento precisos para que la totalidad de las poblaciones de nuestros núcleos urbanos pudieran tener magníficas opciones a lo largo de doce meses no significaría en términos económicos mucho más de lo que en plan individualista y solitario gastamos en pocas semanas (FC, II, 211). ¡Qué feos y pobres son no pocos de nuestros pueblos, exclama Arizmendiarrieta, qué mezquindad de espacios verdes, qué poco sentido de exigencias comunitarias! Y ¡qué vergüenza debiera darnos vivir como pudientes individualmente, sin parar a pensar que a nivel público no existen providencias ni presupuestos para proporcionar a los niños cobijo, juegos o expansiones adecuadas o que, en el otro polo de la vida, quienes tienen derecho a disfrutar de algo por toda una vida dedicada al trabajo apenas encuentran un hueco para disfrutar de sosiego y paz! (Ib. 209-210). -->

We should think about be able to apply throughout the many months more options of enjoyment, more solidarity and communally. The construction of parks, pools, places of recreation precise so that the whole of the populations of our urban centers could have magnificent options throughout twelve months would not mean in economic terms much more than in individualist plan and solitary we spend in a few weeks (FC, II, 211). What ugly and poor are no few of our towns, exclaims Arizmendiarrieta, what pettiness of green spaces, what little meaning of community demands! And what a shame for us live well-off individually individually, without stopping to think that at the public level don't exist providencias or budgets to provide a the children shelter, games or expansions adequate or that, in the other polo of life, those who have a right to enjoy something for a whole life dedicated to the work hardly encounter a hollow to enjoy peace and quiet! (Ib. 209-210).

<!-- 10. Educación y personalidad -->

10. Education and personality

<!-- La educación constituye la base tanto del desarrollo de la personalidad individual como del progreso social, aspectos ambos que en el pensamiento de Arizmendiarrieta son indisociables. «El hombre se hace humano por la educación. La civilización progresa aceleradamente siempre por la acción formativa o educativa en la línea de búsqueda de valores humanos y sociales» (EP, II, 258). -->

Education constitutes the base both of personality development individual like social progress, aspects both that in Arizmendiarrieta's thinking are indissoluble. "Man is made human through education. Civilization progresses faster always because of training or education in the line in search of human and social values" (EP, II, 258).

<!-- El objetivo de la educación es formar transformadores del mundo. Cumplirá este objetivo «cuando ayude a meter una idea de servicio, porque formar es servir. El mundo avanza debido a la gente que ha trabajado y ha dejado un mundo un poco mejor» (Ib. 220). Por eso las escuelas de formación profesional deben ayudar al alumno a conocer sus aptitudes, para que sepa elegir el puesto de trabajo que mejor vaya con su personalidad; deben enseñarle que trabajo es servicio. Lo que dichas escuelas no deben hacer nunca es preparar técnicos individualistas, que no tengan ideas de solidaridad. La escuela debe «ayudar a todo alumno a desarrollar su personalidad; por tanto el estudio y el trabajo deben servir para formar hombres responsables y constantes, que salgan con una mentalidad de construir un mundo más justo» (Ib. 221). -->

The objective of education is to train transformative of the world. He will fulfill this objective "when helps a put in an idea of service, because to train is to serve. The world advances due to people who has worked and has left a world a little better" (Ib. 220). Therefore professional training schools should help the student to get to know your skills, so he knows how to choose the job that goes best with their personality; should teach him that work is service. What these schools should never do is prepare individualist technicians, who have no idea about solidarity. The school must "help every student a develop their personality; therefore the study and work should serve to form responsible men and constant, to leave with a mentality of building a more just world" (Ib. 221).

<!-- Ya estos textos nos revelan los caracteres más destacados de lo que Arizmendiarrieta entiende por una personalidad: voluntad de construir un mundo nuevo, sentido de trabajo como servicio, responsabilidad y constancia, solidaridad. -->

Ya these texts reveal to us the most highlighted characteristics which Arizmendiarrieta understands by a personality: willpower of building a new world, meaning of work like service, responsibility and constancy, solidarity.

<!-- Una nota llamativa es que en el fondo Arizmendiarrieta parece identificar la personalidad bien desarrollada, madura, que se pretende alcanzar en el proceso educativo, con el líder, plenamente entregado a sus ideales, que lleva tras de sí al pueblo. Tal vez por haberse encontrado en un clima tantas veces acusado de apático, fuera por la situación de la postguerra, fuera como consecuencia del Estado absorbente, Arizmendiarrieta pone como objetivo de la educación el despertar en los jóvenes el constante afán de superación, no por motivos o intereses egoístas, sino con miras de generosidad y nobleza. -->

A note striking is that in the end Arizmendiarrieta seems identify the well-developed personality, mature, that se tries to reach in the educational process, with the leader, fully committed to your ideals, that carries behind it al people. Perhaps because he found himself in a climate so many times accused of apathetic, was by the post-war situation, was as a consquence of the absorbent State, Arizmendiarrieta puts as an objective of education the wake up in youth the constant drive of improvement, not by motives or selfish interests, but with view of generosity and nobility.

<!-- «Los pueblos conscientes y valerosos remontan rápidamente los baches de las guerras y de las calamidades» (FC, I, 320). La educación debe crear en los jóvenes ese espíritu de superación que es el camino de la madurez tanto individual como comunitaria. -->

"Conscious people and valient quickly overcome the hard times of wars and of the calamities" (FC, I, 320). Education must create in youth that spirit of improvement which is the path of maturity both individual and community.

<!-- Para poder transformar el mundo, o para poder superar los baches de las guerras y de las calamidades, hacen falta ideales, hace falta una conciencia sólida. Para que en el mundo nuevo a construir los trabajadores aparezcan como mayores de edad, dueños de sus propios destinos, deben poseer cultura, no sólo saber técnico. Pero, antes de nada, es preciso reformar las mentalidades. Hace falta una mentalidad nueva, abierta, en marcha hacia nuevos horizontes. «Hay que renovar las herramientas y las máquinas, pero sobre todo hay que renovar la mentalidad de los hombres que están destinados a ejercer un señorío sobre esos elementos. Hay que actualizar los métodos de organización del trabajo para que la aplicación de las conquistas del progreso técnico sea fluido (...). Los hombres y sus costumbres no tienen que ser tapones para obtener unos resultados que, por otras parte, todos apetecernos» (FC, II, 77). -->

To be able transform the world, or to be able to overcome the hard times of wars and of the calamities, needed ideals, is needed a solid conscience. For in the new world to build workers appear like adults, owners of their own destinations, should possess culture, not only technical know-how. But, first and foremost, it is necessary reform mentalities. It takes a new mentality, open, underway towards new horizons. "We need to renew the tools and the machines, but above all must renew the mentality of men who are destined to exercise lordship over those elements. We need to update the methods of organization of labor so that the application of the conquests of technical progress is fluid (...). Men and its customs do not have to be plugs to get some results that, by other part, everyone apetecernos" (FC, II, 77).

<!-- La rutina es uno de esos males que obstaculiza el desarrollo de los hombres y de las instituciones, y es aceptada regularmente por unos y otros como si fuera una fórmula inofensiva de convivencia; para muchos no ofrece más que ventajas por cualquier lado que se le mire. La historia, que suele hacer constar celosamente las tensiones y las revoluciones, que no pocas veces parecen haber tenido mayores saldos negativos que positivos, pasa por alto sin ninguna referencia las consecuencias negativas, fatales, de las paralizaciones que implican convivencias humanas impuestas por la fuerza de la simple rutina, de la costumbre. La rutina ahoga la vida social. -->

Routine is one of those evils that blocks the development of men and of institutions, and is regularly accepted by everyone as if it was a formula inoffensive of coexistence; for many does not offer more than advantages by any side that se le looks. The story, that usually acknowledge jealously tensions and revolutions, that not infrequently seem have had more negative results that positive, goes to high without none reference the negative consequences, fatal, of the shutdowns that mean human coexistence imposed by force of the simple routine, of the custom. Routine chokes social life.

<!-- Para despertar a las masas de su letargo y arrancarlas de su rutina hacen falta líderes. Por eso la verdadera personalidad está llamada, en el pensamiento de Arizmendiarrieta, a ser líder. «Regularmente suelen ser una minoría los que repudian la rutina como obstáculo para la puesta a punto constante de los resortes y recursos humanos para una promoción progresiva de toda la comunidad; no contamos en esta minoría a los inconformistas patológicos o a los individualistas recalcitrantes con horizontes utópicos o estrechos. El progreso requiere la colaboración de los más, pero contando por delante con el impulso creador e innovador de los menos. Interesa por consiguiente que la colaboración de aquellos sea capaz de superar el peso de la inercia, de la costumbre; debe revitalizarse con la asimilación y puesta en circulación de las energías innovadoras de los pocos capaces de ver más lejos y de descubrir y aplicar nuevas fórmulas» (Ib.). -->

To wake up the masses of their lethargy and arrancarlas of their routine needed leaders. Therefore the true personality is called, in Arizmendiarrieta's thinking, to be a leader. "Regularly they're used to being a minority those who repudiate the routine as an obstacle to the setting a point constant of the springs and human resources for a progressive promotion of the whole community; we do not count in this minority a non-conformists pathological or a the individualist recalcitrant with utopian horizons or close. Progress requires the collaboration of the most, but counting ahead with the creative impulse e innovative of the least. Interesa consequently that the collaboration of those is capable of overcoming the weight of inertia, of the custom; must be revitalized with the assimilation and setting in circulation of the innovative energy of the few capable of seeing further and of discovering and apply new formulas" (Ib.).

<!-- 10.1. Una mentalidad nueva -->

10.1. A new mentality

<!-- En todas las épocas ha habido hombres y pueblos que han cedido a la tentación del desaliento, al carecer de iniciativa o de ritmo para integrarse en un mundo pletórico de vida y de oportunidades. Han aceptado el mundo tal como lo han encontrado, prefiriendo vivir con la sensación de que el mundo estaba hecho y acabado. «Nuestro problema son los hombres cerrados» (FC, I, 320). -->

In all times has been men and towns that have ceded to the temptation of discouragement, to the lack of initiative initiative or about rhythm for be integrated in a world overflowing with life and of opportunities. They have accepted the world just as they found it, preferring live with the sensation of that the world was made and finished. "Our problem are closed men" (FC, I, 320).

<!-- Esta actitud ha sido siempre estéril, cuando no perjudicial, tanto para el individuo como para la comunidad. Pero en nuestros días es, dice Arizmendiarrieta, tanto más perjudicial, cuanto que vivimos en un mundo de cambios acelerados. El curso de los acontecimientos económicos, sociales, tecnológicos es tal que no hay lugar a esperas, como tal vez se podía hacer en tiempos pasados. Hoy se modifican en diez años las cosas y las circunstancias más que antes en cien. No podemos engañarnos pensando que este ritmo evolutivo es un fenómeno pasajero y que pronto volveremos a la calma: es un proceso consustancial con la vida que cuenta con resortes nuevos. -->

This attitude was always sterile, when not harmful, both for the individual to community. But in our days is, says Arizmendiarrieta, the more perjudicial, how much that we live in a world of accelerated changes. The course of events economic, social, technological is such that there's no place awaiting, as perhaps could be done in past times. Today are modified in ten years things and the circumstances more before in a hundred. We can't fool ourselves thinking that this evolutionary pace is a passing phenomenon and that soon calm will return: is a process consubstantial with life that has springs new.

<!-- «Prácticamente resulta tan fatal para un pueblo que quiere mejorar su suerte la revolución que destruye, como el conservadurismo que impide la evolución y, por tanto, la adaptación a las necesidades variables de cada momento» (PR, I, 95). Todo lo que se hace, una vez llega a ser, está sujeto a la ley imperiosa del cambio, sometido por lo mismo al proceso de adaptación, so pena de destrucción propia. «El agua estancada se pudre» (Ib.). -->

"In practice turns out to be as fatal for a people who wants improve their luck the revolution that destroys, like conservatism that impedes evolution and therefore, the adaption to the variable needs of every moment" (PR, I, 95). Everything what is done, once comes to be, is subject to the irresistible law of change, subjected therefore to the process of adaptation, under penalty of its own destruction. "Stagnant water putrifies" (Ib.).

<!-- «No cabe duda que la primera cosa que necesita lo mismo un hombre que un pueblo, una persona que una sociedad, cuando se trata de desenvolverse bien en la vida son ideas claras, una mentalidad ágil y flexible, una disposición interior que procede de la manera de pensar y sentir de cada uno capaz de acomodarse a las exigencias en constante evolución de la vida. -->

"There is no doubt that the first thing that needs the same thing a man that a people, a person that a society, when it's about develop well in life are clear ideas, a mentality agile and flexible, an internal disposition that proceeds from way of thinking and feel of everyone capable of accomodate demands in constant evolution of life.

<!-- El mayor obstáculo que frena el progreso de los pueblos constituyen esas íntimas barreras, que llamamos cultura, ideología, mentalidad o espíritu de cada uno. -->

The greatest obstacle that slows the progress of the people constitute those intimate barriers, that we call culture, ideology, mentality or spirit of everyone.

<!-- A este respecto nos da mucho que aprender la historia. Hoy mismo confiesan los expertos enviados por las Naciones Unidas a diversos países subdesarrollados para promover su elevación de nivel de vida, con disponibilidades de capital a discreción, que lo que no pueden vencer es precisamente la resistencia que opone a sus planes la actitud mental de dichas poblaciones. -->

In this respect gives us a lot to learn the history. Just today confess experts sent by the United Nations a diverse underdeveloped countries to promote their elevation of standard of living, with availbility of discretionary capital, that what can't overcome is precisely the resistance that opposes to their plans the mental attitude of such populations.

<!-- Los molinos de viento tardaron en ser aceptados y aplicados dos siglos porque aquellos pueblos y hombres tardaron ese tiempo en evolucionar en sus ideas, en sobreponerse a su rutina, en vencer la inercia de una sociedad en la que las ventajas que ofrecían los molinos de viento quedaban velados por los inconvenientes que representaban aquellos otros hombres que temían quedarse sin su trabajo habitual de empujar las piedras. -->

Windmills took time to be accepted and applied two centuries because those towns and men took that time in evolve in their ideas, in overcome their routine, in overcome the inertia of a society where las advantages that offered the windmills of wind remained veiled by the inconveniences that represented those other men who feared remain without their usualt work of push the stones.

<!-- La primera tarea que tenemos que aceptar los cooperativistas es ponernos a la altura de las circunstancias en nuestra manera de pensar, juzgar o sentir para evitar que transcurra el tiempo sin resolvernos a adoptar las providencias que requiera nuestra empresa» (FC, I, 44). -->

The first task that we have to accept cooperators is put us a the height of the circumstances in our way of thinking, judge or feel to avoid that elapses the time without resolving to adopt the providences that required our enterprise" (FC, I, 44).

<!-- En nuestra civilización concurren dos factores que no han intervenido en el pasado, o han tenido sólo una influencia relativa: son la experiencia científica y las técnicas de producción. Estos dos instrumentos han contribuido a dilatar un mundo limitado, relativamente estable, con posibilidades inmensas. Pero no se puede olvidar que, como se ha dicho, nada hay menos técnico que las causas del progreso técnico mismo: los hombres no son capaces de adoptar nuevas técnicas más que para la realización de ideales anteriormente concebidos (Ib. 321-322). De ahí la necesidad de una educación bien llevada, que permita a los hombres posicionarse debidamente en un mundo en perpetuo cambio. «La educación, entendiendo por tal el complejo de ideas y concepciones que adopta un hombre, es la clave del desarrollo y desenvolvimiento de un pueblo» (Ib. 322). -->

In our civilization concur two factors that have not intervened in the past, or have had only one relative influence: are scientific experience and the techniques of production. These two instruments contributed to open a limited world, relatively stable, with immense possibilities. But it cannot be forgotten that, like as has been said, nothing there is less technical that the causes of technical progress itself: men are not capable of adopting new techniques more that to the realization of ideals previously conceived (Ib. 321-322). From there the need to an education well carried, that allows men position duly in a world in perpetual change. "Education, understanding by such the complex of ideas and conceptions that adopts a man, is the key of development and development of a people" (Ib. 322).

<!-- ¿Cómo se fragua un presente ágil y vigoroso, promesa segura de porvenir venturoso, sin sacrificar estérilmente un pasado respetable? «Para vivir al día nada necesita un pueblo o un hombre como actualizar su bagaje espiritual o institucional a las exigencias del presente y del porvenir» (PR, I, 96). -->

How wrecks a present agile and vigorous, promise safe of happy future, without sacrifice sterile a respectable past? "To live of the day nothing needs a people or a man like update their spiritual baggage or institutional a the demands of the present and of the future" (PR, I, 96).

<!-- «Estar al día es algo más que vivir al día; la vida no son las hojas del calendario que se suceden y se pueden dispersar sin que al parecer pierda su interés cada momento. La vida implica una continuidad de profundas raíces; las conquistas de cada día son peldaños que se apoyan los unos en los otros; así se labran los patrimonios, llamados a ser puntos de apoyo firmes de un progreso indefinido y fecundo» (Ib. 95). La actualización y puesta al día de ciertos elementos materiales, como son las viviendas, herramientas, etc., es algo que se logra con relativa facilidad, más o menos espontáneamente, «pero ya es otra cosa por lo que se refiere al dominio de las ideas y de las instituciones en consonancia con las mismas. La mentalidad social es algo que cuesta mucho ponerla al día» (Ib. 96). Exige una profunda y constante labor educativa. -->

"To be of the day is something more than live of the day; life no are the pages of the calendar that occur and se can disperse without it would seem loses your interest every moment. Life means a continuity of deep roots; daily conquests are steps that are supported by each other; thus tilled the wealth, called to be points of support firms of a progress indefinite and fruitful" (Ib. 95). Updating and setting of the day of certain material elements, as they are the houses, tools, etc., is something that manage with relative facility, more or less spontaneously, "but already is something else which is why refers to the domain of ideas and of institutions in accordance with the same. The social mentality is something that is hard catch it up" (Ib. 96). It demands a profound and constant educational labor.

<!-- «Los pueblos avanzan en la medida que se potencian en ambos aspectos del despliegue y cultivo de las inteligencias y del desarrollo del espíritu asociativo. — La mejor providencia que pueden adoptar cara al futuro está por consiguiente en esos dos campos: la preparación de las nuevas generaciones, la puesta a punto constante de las fuerzas de trabajo, mediante su adecuada promoción cultural y social» (Ib. 98). -->

"People advance to the extent that they are empowered in both aspects of the deployment and cultivation of the intelligences and of development of the associative spirit. — The better providence that can adopt facing the future is consequently in these two fields: the preparation of the new generations, the setting a point constant of the workforce, through their adequate cultural promotion and social" (Ib. 98).

<!-- La razón fundamental por la cual los diferentes países no se encuentran hoy en el mismo grado de desarrollo técnico no es de carácter geográfico, etc., sino, en opinión de Arizmendiarrieta, la concepción de la vida y el espíritu de los moradores de cada región. «A eso le llamamos aquí mentalidad de cada pueblo y es eso lo que hace falta que sea abierta, es decir, admita la perfectibilidad humana, la posibilidad de modificar el contexto social, la cooperación con otros y un amplio juego de iniciativas» (FC, I, 322). -->

The fundamental reason that the different countries are not found today in the same degree of technical development isn't of a geographical nature, etc., but, in Arizmendiarrieta's opinion, the conception of life and the spirit of the inhabitants each region. "To that we call it here mentality of each people and is that what needs to is open, which is to say, admit human perfectibility, the possibility of modify the social context, cooperation with others and a broad set of initiatives" (FC, I, 322).

<!-- Convendrá subrayar el aspecto de que nada hay menos técnico que las causas mismas del progreso técnico. En efecto, la reforma de mentalidad que Arizmendiarrieta urge no se refiere tanto a la disposición, relativamente fácil, de aceptar con agrado la más moderna tecnología y estar incluso dispuestos a realizar los estudios necesarios para su buen empleo. Esa tecnología no puede llegar si antes no existe espíritu de cooperación, desarrollo de iniciativas, modificación del contexto social, e.d., no puede provenir de «americanitos», sino solamente de la unión y cooperación desinteresada, generosa. «Es indudable que el signo más esperanzador de una colectividad es saber unirse para construir, para edificar lo que interesa y mirar al porvenir» (Ib.), Este es el secreto que tienen que descubrir las masas trabajadoras ansiosas de emancipación; su emancipación será efectiva cuando hayan procedido a este esfuerzo. La llave que abre definitivamente el mundo cerrado haciéndolo abierto y progresivo es, sin duda, la actividad científica y técnica. Pero, anteriormente, «el conflicto entre el mundo acabado, cerrado y el abierto está planteado fundamentalmente en el ámbito de la educación» (Ib. 323). -->

It will be fitting underscore the aspect of that nothing there is less technical that the very causes of technical progress. Indeed, the reform of mentality that Arizmendiarrieta urges does not refer as much to the availability, relatively easy, to willingly accept the most modern technology and be even ready to carry out the studies necessary for their good job. That technology can't arrive if before doesn't exist spirit of cooperation, development of initiatives, modification of the social context, e.d., can't come from "americanitos," but only of unity and cooperation disinterested, generous. "Undoubtedly the most hopeful sign of a community is know unite to build, to build up which interests and look to the future" (Ib.), This is the secret who have to discover the working masses anxious for emancipation; their emancipation will be effective when have proceeded to this effort. The wrench that opens definitively the closed world doing it open and progressive is, without a doubt, scientific activity and technique. But, previously, "the conflict between the world finish, closed and the open is raised fundamentally in the setting of education" (Ib. 323).

<!-- «Se impone un cambio radical de mentalidad» (Ib. 321), que ayude a desarrollar el diálogo, la convivencia, el espíritu de cooperación generosa, iniciativas nuevas, la adaptación a los cambios que implica la evolución constante. «Nos deben preocupar las zonas subdesarrolladas que pueden existir en todas partes, pero en especial las que cubren los sombreros, las boinas» (Ib.). -->

"is imposed a radical change of mentality" (Ib. 321), that helps to develop dialogue, coexistence, the spirit of generous cooperation, new initiatives, the adaption to the changes that means constant evolution. "We should be worried about underdeveloped zones that can exist everywhere, but especially las covering hats, caps" (Ib.).

<!-- «La clave de nuestro porvenir no es la revolución ni es el conservadurismo; es la apertura generosa a las exigencias del tiempo, mediante la promoción de una amplia acción educadora y un proceso de mancomunación, que en cualquiera de los campos de aplicación, integre a los hombres como hermanos» (PR, I, 98). -->

"The key to our future isn't the revolution or is conservatism; is being generously open to the demands of the time, through the promotion of a wide educational action and a process of community-building, that in any of the fields of application, integre men like brothers" (PR, I, 98).

<!-- Existe un grave peligro de entender la educación profesional técnica en un sentido muy restringido, olvidando que el vigor y la energía de una sociedad, o de una entidad —como pueden ser las cooperativas— no radica en sus tasas de inversión o en su capacidad de producción, en su tecnología, entendida esta materialmente, sino en su caudal de ideas y en su espíritu social, que constituyen el verdadero motor de todo progreso. De todos es conocida la afirmación de que el hombre está hecho para vivir en sociedad. Ya pretenda prescindir de los demás aislándose, ya se mezcle en su vida hasta el punto de dejar disolverse su propia personalidad en el alma colectiva, no puede eludir esta exigencia interna de su naturaleza en ningún caso. De su medio social adquiere sus más preciados dones y, por otra parte, «en la comunión con los demás encuentra el hombre su provecho verdadero; haciéndoles el don de su persona adquiere su propia personalidad, pues la verdadera expansión reside en la salida de sí mismo, es decir, en la destrucción del egoísmo» (FC, I, 232). «El espíritu social supone el olvido de sí mismo. Toda formación que combate el egoísmo, favorece, pues, la expansión de las virtudes sociales. Así, bajo este ángulo, es como debe enfocarse el papel de la educación» (Ib. 233). -->

There exists a serious danger of understand technical professional education in a very narrow sense, forgetting that the vigor and the energy of a society, or about an entity —like can be the cooperatives— does not lie in your its investment rates or in his capacity for production, in its technology, understood this materially, but rather in their flow of ideas and in its social spirit, that constitute the true engine of all progress. From everyone is known the affirmation of that man is made to live in society. and pretend dispense with of others isolating himself, he mixes himself in his life to the point of allow to be dissolved their own personality in the collective soul, can't avoid this internal demand their nature under any circumstances. From their social environment acquires your most valuable gifts and, on the other hand, "in the communion with others finds man their true benefit; making the gift of their person acquires their own personality, since the true expansion lies in leaving oneself, which is to say, in the destruction of selfishness" (FC, I, 232). "The social spirit supposes forgetting oneself. All training that combats selfishness, favors, since, the expansion of social values. So, under this angle, is like must focus the paper of education" (Ib. 233).

<!-- Un peligro de nuestra civilización radica precisamente en que el dominio que vamos alcanzando sobre los bienes y energías de la naturaleza externa no marcha a compás con el avasallamiento de lo elemental y zoológico que existe en nuestra propia naturaleza humana. Así nos encontramos con el peligroso fenómeno de que «la sociedad moderna no se ha desarrollado, en el aspecto intelectual y moral, proporcionalmente a la energía que ha llevado por medio de la ciencia y de la técnica. Por lo cual el problema vital de nuestra sociedad consiste en si disponemos de fuerza suficiente para subordinar la civilización técnica a las fuerzas espirituales y morales, que dicho con otras palabras quiere decir, si habrá manera, forma o instrumento para lograr ese equilibrio mediante un rápido desarrollo y cultivo de las fuerzas morales y espirituales del hombre (EP, I, 75). Vivimos en un siglo de gigantescos progresos y, al mismo tiempo, de las más grandes tragedias humanas. Salpicaduras de esa historia, dice Arizmendiarrieta, nos han alcanzado a todos. Pero, para que no se repitan, no basta con condenar los atropellos y los crímenes, o confiar en la generación espontánea de la virtud. Por otra parte, está más que comprobado que los medios externos solos y las precauciones humanas y los expedientes políticos tampoco bastan. «El mismo hierro, ha dicho un sabio, puede ser para segar la mies o para matar, como la razón humana puede servir a los fines más generosos o a las más abominables maldades. El estudio de la ciencia (...), disciplinado con miras a la dominación, puede incluir a una civilización de egoísmo y de materialismo, que no es más que una barbarie sabia» (Ib.). -->

A danger of our civilization lies precisely on which domination we are reaching over the goods and energy of external nature no march a compás with the avasallamiento of lo elemental and zoo that exists in our very nature human. So we find ourselves with the dangerous phenomenon that "modern society has not been developed, in the aspect intellectual and moral, proportionally to the energy that has taken via of science and of technique. By which the vital problem of our society consists of if we have of enough strength to subordinate technological civilization a spriritual forces and moral, which means, in other words,, if there will be way, way or instrument to achieve that balance through a quick development and cultivation of the moral forces and spiritual of man (EP, I, 75). We live in a century of gigantic progress and, at the same time, of the greatest human tragedies. Splashes of that story, says Arizmendiarrieta, have reaches us all. But, lest they be repeated, it is not enough to have condemn the clashes and the crimes, or trust in the spontaneous generation of virtue. On the other hand, is more than proven that external means alone and human precautions and political expediencies nor are enough. "Iron itself, a wise man has said, can be to harvest grain or to kill, like human reason can serve the most generous ends or a the most abominable evils. The study of science (...), disciplined for the purpose of domination, can include a civilization of selfishness and of materialism, which is no more than a wise barbarism" (Ib.).

<!-- 10.2. Cultura -->

10.2. Culture

<!-- «Un centro de formación debe ser ante todo un centro de información, ya que la formación no es tanto llevar de la mano, sino que las personas, conociendo sus inquietudes, precisan documentación para saciar esa hambre de cultura y de responsabilidad» (FC, III, 266). La formación debe hacer hombres, antes que líderes (FC, I, 233) y para ello no basta con transmitir mero saber técnico, debe poner al educando en vías de cultura, en el más amplio sentido, y de responsabilidad. La educación debe atender a toda la riqueza de posibilidades y necesidades que late en el hombre. -->

"A training center must be first and foremost an information center, since the training is not so much take of hand, but people, conociendo your concerns, need documentation to quench that hunger for culture and of responsibility" (FC, III, 266). Training must make men, before leaders (FC, I, 233) and for this it is not enough to have transmit mere technical know-how, must put al educating on the way to culture, in the broadest sense, and of responsibility. Education must serve the whole wealth of possibilities and needs that late in man.

<!-- La persona tiene unas raíces de las que se alimenta (historia, comunidad en la que se halla inserta) y una multitud de necesidades propias que satisfacer: la educación debe atender a ambos aspectos, buscando una promoción integral del hombre. «Designamos la imperiosa necesidad de esta polivalencia y polifacetismo con la calificación genérica de cultura» (EP, II, 188). -->

The person has some roots of which is fed (story, community in which is inserts) and a multitude of needs of one's own to satisfy: education must serve both aspects, seeking holistic promotion of man. "We designate the overwhelming need to this polyvalence and polifacetismo with the general description of culture" (EP, II, 188).

<!-- Se trata de educar a los jóvenes, o a los hombres, sin despojarlos de lo que ya de algún modo los determina, e.d., su experiencia, su historia o posicionamiento en la vida. Al contrario, la educación debe conjugar lo que la naturaleza o la historia nos hayan proporcionado con los elementos de promoción más adecuados de cara al futuro (Ib. 188-189). -->

It's about educate youth, or men, without despojarlos of what already somehow the determines, e.d., their experience, their history or positioning in life. On the contrary, education must combine which nature or the history have provided to us with the elements of advancement most approporiate looking to the future (Ib. 188-189).

<!-- Cultura en Arizmendiarrieta parece oponerse al mero saber técnico, profesional, teniendo dos vertientes, una intelectual y una moral: «de una vez para siempre diremos que la acepción que nosotros estamos dando a la palabra cultura es esta de formación intelectual y moral» (CAS, 156). -->

Culture in Arizmendiarrieta seems oppose mere technical know-how, professional, having two tendencies, a intellectual and one moral: "eleven and for all we'll say that the definition that we are giving to the word culture is this of intellectual training and moral" (CAS, 156).

<!-- Arizmendiarrieta no ha dejado de insistir en el aspecto de la formación intelectual: «La inteligencia es la facultad superior que debe guiar al hombre. -->

Arizmendiarrieta continues to insist on the aspect of intellectual training: "Intelligence is the higher faculty that must guide man.

<!-- El riesgo mayor del hombre es el que se deriva de la falta de cultivo de sus facultades superiores» (FC, I, 321). Pero ha insistido siempre mucho más en la formación integral, y esta lleva en él connotaciones más morales que intelectuales, porque, a su juicio, «la formación moral individual debe seguir siendo el fundamento de la formación social» (Ib. 233). -->

The greatest risk of man is that which is derived from the lack of cultivation of their higher faculties" (FC, I, 321). But has insisted always much more in holistic training, and this carries in him connotations more moral that intellectual, because, in his judgment, "individual moral training must continue to be the base of social training" (Ib. 233).

<!-- Sin un fondo sólido de cualidades humanas y cristianas, la formación social será semejante a una casa construida sobre arena, quedará reducida a algunas fórmulas destinadas a superponer a la personalidad, radicalmente egoísta, actitudes superficiales más o menos utilitarias, que pueden servir en las relaciones públicas, pero no podrán resistir los embates del egoísmo. «Enseñar solamente cómo se han de comportar los hombres los unos con los otros, sin atacar su egoísmo, es arar en el mar (...). Antes de enseñarles las relaciones públicas y la cortesía, hay que acostumbrarles a olvidarse de sí mismos» (Ib.). -->

Without a solid foundation of qualities human and Christian, social training will be similar to a house built on sand, will be reduced some formulas intended a superimpose a the personality, radically selfish, attitudes superficial more or less useful, that can serve in public relations, but can not resist the attacks of selfishness. "Show only how must behave men with each other, without attacking their selfishness, is like plowing the is (...). Before teach them public relations and courtesy, must get them used to forget about themselves" (Ib.).

<!-- La educación deberá ser, por tanto, integral, incluyendo «la formación profesional, inspirando el espíritu de equipo, de solidaridad, de iniciativa, de servicio; la cultura física, movida por el espíritu de equipo y de solidaridad; la formación moral, creando hábitos cristianos de obediencia, de orden, de disciplina, de respeto a los demás y educando el sentido del esfuerzo, de la veracidad, de la responsabilidad, del trabajo y de la fidelidad al deber de estado; la formación religiosa, colocando en el alma los fundamentos del espíritu social» (Ib.). -->

Education shall be, therefore, integral, including "professional training, inspiring team spirit, of solidarity, of initiative, of service; physical culture, moved by team spirit and of solidarity; moral training, creating Christian habits of obedience, of order, of discipline, of respect for others and educating the meaning of the effort, of the veracity, of responsibility, of work and of the fidelity al duty to state; religious training, placing in the soul the fundamentals of social spirit" (Ib.).

<!-- El período de formación no debe ser entendido «como simple fase de preparación para que un joven sea una máquina más productiva» (EP, I, 241). -->

The period of training should not be understood "like simple phase of preparation so that a young person is a more productive machine" (EP, I, 241).

<!-- Es indispensable y urgente, dice Arizmendiarrieta, que nuestros aprendices salgan de la Escuela con una visión clara de lo que puede y debe ser el mundo a la luz de los principios cristianos aplicados al mundo del trabajo y de sus relaciones. Deben salir preparados y sabiendo que se van a encontrar con unas realidades económico-sociales superables, con unas estructuras transformables y, por tanto, más que con ánimo resignado, con espíritu templado para poder luchar por el establecimiento de un orden social más humano y justo. -->

It is indispensable and urgent, says Arizmendiarrieta, that our apprentices leave the School with a clear vision of what can and must be the world in the light of Christian principles applied to the world of work and of their relationships. They must go out prepared and knowing that they are going to find some realities economic-social surmountable, with some structures transformable and therefore, more that with resigned mood, with spirit tempered to be able to struggle for the establishment of a social order more humane and just.

<!-- «El joven que hoy se sumerge en el mundo del trabajo sin un ideario social claro y positivo es un náufrago de su vida religiosa, o un cobarde, o un traidor al movimiento obrero» (Ib. 247). Lo cual es muy comprensible, dice Arizmendiarrieta, dado el ambiente que reina en las fábricas. -->

"The young person which, today is submerged in the world of labor without a social ideology clear and positive is a shipwreck of their religious life, or a coward, or a traitor to the worker movement" (Ib. 247). This is very understandable, says Arizmendiarrieta, given the environment that reigns in the factories.

<!-- «La formación social que hemos de darles ha de aspirar a crear en ellos un estado de ánimo abierto e inquieto de forma que para ellos la formación religiosa y moral no se reduzca a la simple aceptación resignada de las realidades temporales o sociales que se ofrecen, sino que tengan capacidad para interpretarlas como lo que son, circunstancias o fases de una evolución a cuya realización más rápida deben coadyuvar los hombres» (Ib. 246). -->

"The social training that we have to to give them must aspire to create in them a state of mind open and restless such that for them religious training and moral is not reduced to the simple resigned acceptance of worldly realities or social that are offered, but that have capacity to interpret them as they are, circumstances or phases of an evolution a whose realization fastest should contribute men" (Ib. 246).

<!-- Podríamos, pues, concluir, que aunque Arizmendiarrieta se ha ocupado preferentemente por la educación profesional en un medio industrial y obrero, tanto el concepto de educación como el de cultura tienen para él un sentido predominantemente moral. Incluso en este aspecto destacan claramente dos elementos: los ideales, que alimentan el espíritu de superación constante, y el espíritu desinteresado y generoso de entrega al bien común. -->

We could, since, conclude, that although Arizmendiarrieta has occupied himself preferentially by professional education in a medium industrial and worker, both the concept of education like that of culture have for him a meaning predominantly moral. Even on this aspect stand out clearly two elements: ideals, that feed the spirit of constant improvement, and the disinterested spirit and generous of dedication to the common good.

<!-- Una sociedad progresiva necesita de hombres con ambición de mando y vocación de líderes. Pero «el que ambiciona el mando y quiere ejercer el mando en una sociedad cultivada, no tiene otra fórmula mejor que destacar precisamente por su generosidad y disposición hacia los demás» (FC, I, 234). -->

A progressive society needs men with ambition for command and vocation of leaders. But "that which has ambitions of command and wants exercise command in a cultivated society, has no better formula than highlight precisely because of their generosity and disposition towards others" (FC, I, 234).

<!-- Por otra parte, sociedad progresiva es aquella que está dispuesta a seguir a tales líderes. «Los componentes de una comunidad que aprenden a poner sus ojos en los que destacan por su nobleza y generosidad, no han de equivocarse ni quedar defraudados» (Ib.). -->

On the other hand, progressive society is that which is prepared to follow such leaders. "The components of a community that learn to put your eyes in the that stand out for his nobility and generosity, must not make a mistake or remain defrauded" (Ib.).

<!-- 10.3. Líderes -->

10.3. Leaders

<!-- Arizmendiarrieta sueña con una especie de Plan de Desarrollo que fomente «la marcha del pueblo hacia la grandeza humana» (FC, II, 25). La sociedad moderna cuenta con instrumentos enormes para moldear la personalidad de sus miembros: prensa, radio, TV, etc., y la variada trama de instituciones como son la escuela, la empresa, etc. Hay que eliminar en ellos todo lo que pueda constituir elemento deformador. Arizmendiarrieta no dejará de ser un puritano en este sentido. -->

Arizmendiarrieta dreams of a sort of Development Plan that encourages "the march of the people towards human grandeur" (FC, II, 25). Modern society has instruments enormous to mold the personality of their members: press, radio, TV, etc., and the varied plot of institutions as they are the school, the business, etc. We need to remove in them everything that can constitute element deforming. Arizmendiarrieta will continue to be a Puritan in this regard.

<!-- Pero «el verdadero moldeador del hombre es otro hombre» (Ib.): de ahí la importancia y el valor educativo de las relaciones humanas (las llamadas suenan fácilmente a vacío, a hipocresía) y de ahí también la necesidad de líderes ejemplares. El pueblo no seguirá «si no está movido por un hombre que va por delante en el camino hacia la grandeza humana» (Ib; cfr. CAS, 104-105). -->

But "the true molder of man is other man" (Ib.): from there the importance and the educational value of human relations (the so-called sound easily a vacuum, a hypocrisy) and from there too the need to leaders copies. The people will not follow "if isn't moved by a man that goes ahead on the path towards human grandeur" (Ib; cf. CAS, 104-105).

<!-- La educación del hombre por el hombre no se reduce solamente a los líderes: «toda persona tiene algo que enseñar a los demás» (FC, III, 266), todo contacto humano puede resultarnos enriquecedor, si nos situamos en actitud abierta y receptiva. No hay duda de que quien tiene más conocimientos por edad, por estudios, tiene mayores posibilidades, pero toda persona tiene algo que dar y debemos aprovechar toda la capacidad de la gente que quiera dar algo. Eso no excluye, sin embargo, que ciertas personas asuman una función educativa más destacada, como es el caso de los líderes, o dirigentes. -->

Education of man by man no is reduced only a the leaders: "every person has something to teach others" (FC, III, 266), all human contact can be enriching, if us we place in attitude open and receptive. There is no doubt that who has more knowledge by age, by studies, has more possibilities, but every person has something to give and we should take advantage the whole capacity of people who want to give something. That does not exclude, however, that certain people assume an educational function more highlighted, as in the case of the leaders, or leaders.

<!-- «Una sociedad que intente seriamente planificar el desarrollo de la grandeza humana, necesita contar con una plantilla suficiente de hombres competentes dispuestos a cargar con los puestos de mayor responsabilidad y calidad sin exigir por ello un nivel de vida individual y familiar superior al del resto del pueblo» (FC, II, 25). Esta será la única manera de superar la mentalidad crematística, según la cual a mayor responsabilidad o a mayor calidad de trabajo corresponde automáticamente mayor retribución, y de tratar de organizar las relaciones humanas por valores más altos, entendiendo siempre responsabilidad como servicio. -->

"A society that tries seriously plan the development of human grandeur, needs have a sufficient roster of competent men ready to carry with positions of greater responsibility and quality without demanding therefore a standard of living individual and family above that of the rest of the people" (FC, II, 25). This will be the only way to overcome the mentality crematística, according to which a greater responsibility or a greater quality of work automatically corresponds greater pay, and of trying to organize human relations by higher values, always understanding responsibility like service.

<!-- Recordando la figura de Juan XXIII, Arizmendiarrieta nos dirá (1964) que una Iglesia servidora y no dominante, dialogante y no anatematizante, viviendo sencilla como un pastor en medio de sus ovejas, con unos cuadros funcionales que están servidos por hombres pobres, sin espíritu de carrera, podría ser un troquel formidable para hacer una sociedad a su imagen y semejanza. «Sería un ejemplo viviente de que es posible estructurar una sociedad en la que se tendiera a vivir ese principio de que “a cada uno según sus posibilidades exigirle, y darle según sus necesidades”; principio que trataron de vivirlo los primeros cristianos de Jerusalén y que luego se lo arrinconó a los conventos y que modernamente los movimientos socialistas han intentado convertirlo en principio básico para la estructuración de la sociedad temporal» (Ib. 26). -->

Remembering the figure of John XXIII, Arizmendiarrieta will tell us (1964) that a servant Church and not dominant, in dialogue and not condemning, living simply like a shepherd in the middle of your sheep, with some cadres functional that are served by poor men, without spirit of career, could be a strong mold to make a society in its image and likeness. "It would be a living example that it is possible structure a society where there was a tendency to live that principle that “to everyone according to your possibilities demand, and give him according to your needs”; principle that tried to live it the first Christians of Jerusalem and that then was pushed off into convents and that in the modern day the socialist movements have tried turn it into basic principle for the structuring of worldly society" (Ib. 26).

<!-- El cooperativismo es fruto de una cristalización de actitudes y aspiraciones generosas, vertida hacia la grandeza humana, y aspira a convertirse en un núcleo alrededor del cual vayan cristalizando otras actitudes semejantes. -->

Cooperativism is the fruit of a crystallization of attitudes and aspirations generous, poured out upon human grandeur, and aspires to become a nucleus around which crystalize other similar attitudes.

<!-- Responsabilidad de sus jefes y líderes, visibles y ocultos, es la de encarnar altamente la actitud de servicio desinteresado, ajeno a los afanes de medro individual en lo económico, en el dominio sobre los demás, para convertir el movimiento cooperativo en un amplio cauce por donde pueda entrar el pueblo. «El hombre es una gran energía. Necesita que se le encauce, que se le ofrezcan a su medida y alcance empresas que le entusiasmen, hombres que le convenzan con su vida» (FC, II, 26). -->

Responsibility of their bosses and leaders, visible and hidden, is that of highly embody the attitude of disinterested service, outside of desires for personal gain in economics, in domination over others, for turn the cooperative movement in a broad path by where can enter the people. "Man is a large energy. He needs to be channeled, to be offered within his means and reach businesses that inspire him, men who convince him with its life" (FC, II, 26).

<!-- Cuando existe un clima propicio y unos cauces adecuados, las decisiones que parecían heroicas dejan de ser fenómenos muy raros y propios de superdotados para convertirse en patrimonio de un número muy amplio. «Las cumbres humanas y cristianas están al alcance de los hombres de buena voluntad» (Ib. 27). -->

When there exists a favorable climate and appropriate channels, the decisions that seemed heroic stop being very strange phenomena and themselves of supergifted to become heritage of a very broad number. "Human summits and Christian are within reach of men of good will" (Ib. 27).

<!-- Arizmendiarrieta confiesa que, al haber defendido el principio de «exigir a cada uno según sus posibilidades y darle según sus necesidades», no han faltado quienes le motejaban de utópico. Y efectivamente sería absurdo, dice, pretender aplicar ese principio de la noche a la mañana y en un cien por cien. -->

Arizmendiarrieta confesses that, by having defended the principle of "demand from each according to your possibilities and give him according to your needs," there have been those who branded him as utopian. And indeed would be absurd, says, intend apply that principle overnight and one hundred percent.

<!-- «Pero no es absurdo, sino urgente, aplicar un Plan de Desarrollo progresivo del espíritu encerrado en ese principio» (Ib.). La historia avanza en ese sentido, en opinión de Arizmendiarrieta, y la Revolución Francesa y la moderna revolución social suponen un gran avance en este proceso. «Aquel que sea capaz de creer más y albergar una esperanza mayor en las posibilidades del hombre, aquel será capaz de seguir empujando a la humanidad hacia adelante» (Ib.). -->

"But not is absurd, but urgent, apply a Development Plan progressive of the spirit enclosed in that principle" (Ib.). The story advances in that sense, in Arizmendiarrieta's opinion, and the French Revolution and modern social revolution assume a big advance in this process. "He who is able to believe more and store a greater hope in the possibilities of man, that will be capable of to keep pushing to humanity forward" (Ib.).

<!-- «Bastan unos cuantos, en cada localidad, en cada estructura, que vivan ese principio, para que el clima de esa localidad, de esa institución, se eleve, y se difunda en diversos grados esa misma actitud» (Ib.). -->

"A few are enough, in every locale, in every structure, that live that principle, so that the climate of that locale, of that institution, is elevated, and is spread in diverse degrees that same attitude" (Ib.).

<!-- 10.4. Las pasiones -->

10.4. Passions

<!-- Todos llevamos, en términos de Arizmendiarrieta, una bestia dentro contra la que tenemos que luchar constantemente (SS, II, 157), fundándonos para ello en la luz de la fe, pero también en la luz de la razón (SS, II, 68). -->

We all carry, in Arizmendiarrieta's words, a beast inside against the that we have to struggle constantly (SS, II, 157), basing ourselves for this in the light of faith, but also in light of reason (SS, II, 68).

<!-- «Apelar más a la reflexión que a los instintos debe estar en el ánimo de todo el que auténticamente quiere contar con el hombre, o dicho de otra forma, respetar más el dictado de la conciencia que de las pasiones. Lo más profunda y auténticamente humano es su pensamiento y capacidad de reflexión» (FC, III, 196). -->

"To appeal more to reflection than to instincts must be in the mood of everything that authentically wants have man, or said another way, respect more the dictates of the conscience than of passions. What is most profoundly and authentically human is his thought and capacity for reflection" (FC, III, 196).

<!-- Arizmendiarrieta, especialmente en sus escritos pastorales y en los dedicados a la formación de la juventud, no ha dejado de pintar las pasiones, la tentación, el peligro, con los colores sombríos propios de la Iglesia vasca, calificada a veces de jansenista. Pero llama la atención su visión fundamentalmente optimista incluso de las pasiones, aún sin desconocer sus aspectos negativos. Querer matar las pasiones, dice, equivale a mutilar al hombre: se trata de saber orientarlas, aprovecharlas. Pretender que el hombre, creado a imagen y semejanza de Dios, pero destinado a vivir en el mundo, en un mundo que le es hostil (FC, I, 32-33) y que debe transformar con su trabajo, sea puro espíritu y voluntad, es angelismo. Tener cuerpo significa estar equipado también «por unas pasiones, por unos anhelos, por unos estímulos naturales de acción», que deben ser reconocidos y correspondidos (FC, I, 239). «El hombre no necesita anular sus pasiones, sino domesticarlas. El hombre no debe dejar de lado sus aspiraciones de superación, sino disciplinarlas y jerarquizarlas. El hombre no debe llegar a la cooperativa para ejercer la contemplación, sino para canalizar y conjuntar su acción en el contexto de sus sacrificios y compensaciones (Ib. 240). «Hay pasiones en el corazón humano que no se pueden ahogar, aniquilar y se deben orientar, encauzar» (PR, II, 2). -->

Arizmendiarrieta, especially in their pastoral writing and in those dedicated to the formation of youth, continues to paint passions, temptation, danger, with the colors dark themselves of the Basque Church, sometimes described as Jansenist. But is noteworthy their fundamentally optimistic view of even passions, even without disown your negative aspects. To want kill passions, says, is equivalent to mutilate man: it's about know orient them, take advantage of them. To intend that man, created in the image and likeness of God, but destined to live in the world, in a world that is hostile (FC, I, 32-33) and that must transform with your work, is pure spirit and willpower, is angelism. Having a body means be equipped too "by some passions, by some desires, by some natural stimuli of action," that must be recognized and correspondidos (FC, I, 239). "Man does not need nullify your passions, but domesticate them. Man should not leave aside your aspirations of improvement, but discipline them and put them in a hierarchy. Man should not reach the cooperative for exercise contemplation, but to channel and conjoin its action in the context of your sacrifices and compensation (Ib. 240). "There's passions in the human heart that cannot be choked, annihilate and should orient, channel" (PR, II, 2).

<!-- Tesis de amplias consecuencias en sus planteamientos sociales, que le llevará, por ejemplo, a aceptar en buena medida las doctrinas liberales, si no como ideales, al menos como realidades que se corresponden con la naturaleza humana, aceptando de este modo incluso el egoísmo (moderado, orientado) como positivo en definitiva para la comunidad y para el desarrollo. Considerará el afán de lucro como algo legítimo en sí, como «resorte poderoso que actúa en el campo económico y al que no pocos le otorgan categoría de algo insustituible e insuperable» (FC, I, 325). Arizmendiarrieta critica, por un lado, la poca atención que se presta a los aspectos negativos del afán de lucro; pero, por otro, se niega a ignorar los aspectos positivos del mismo. -->

Thesis of wide-open consequences in their social approaches, that will lead him to, for example, to accept in good measure liberal doctrines, if not as ideals, at least like realities that correspond with human nature, accepting in this way even selfishness (moderate, oriented) like positive definitively for community and for development. He will consider the profit drive like something legitimate in itself, like "powerful wellspring that acts in the economic field and which no few grant it category of something irreplaceable e unsurpassable" (FC, I, 325). Arizmendiarrieta criticises, on the one hand, the little attention that is provided to the negative aspects of the profit drive; but on the other hand, refuses to ignore the positive aspects of the same.

<!-- De este modo las pasiones, los instintos, aportan su dinámica y su fuerza al programa global de hacer más hombre al hombre. -->

This way passions, instincts, provide their dynamics and its strength al global program of doing more man to man.

<!-- Evidentemente «el ímpetu y la bravura de los instintos, por legendarios que sean, no ayudan a superar las carencias ni a remediar la impotencia» (CLP, I, 284). Nuestra lucha requiere la fuerza y el empuje de las pasiones, pero estas deberán siempre estar sometidas a la razón, a la conciencia, que cuando estuvieran debidamente desarrolladas podrían incluso suplantar a aquellas. «Es evidente que sin fuerza no se obtiene todo lo que la propia conciencia legitima e incluso demanda. Pero en la medida en que dispongamos de una fuerza radicada en la conciencia, en la unión y en la solidaridad de los humanos, damos prioridad y prevalencia a la misma sobre el juego y la explosión de los instintos» (Ib. 284). La función que asumen las pasiones en su concepto debe, pues, ser entendida como provisional, como una concesión a las realidades circundantes, pero que el hombre debe siempre tender a superar, guiándose en su acción más por la razón y la conciencia. «El hombre dominado por el instinto de ganar, decía dirigiéndose a los trabajadores de Ulgor, es un producto natural, condenado como tal a ser lo que sería el trigo, la vaca o el árbol frutal abandonado a sus leyes y a su suerte. Mezquino o padrastro» (CLP, III, 28). Insiste en que con ello no quiere significar que el afán de lucro sea malo en todos los casos, o innecesario y rechazable, «sino sencillamente que el hombre y el mundo regidos por tal resorte natural no alcanzan las posibilidades que podrían alcanzar. Quedan cortos para lo que pudieran dar de sí. Es decir, el resorte del lucro, el afán de ganar, tiene que estar supeditado a otros valores, a otras metas y solamente cuando se ejercen con esa supeditación es cuando estos resortes dan buenos resultados, resultados en consonancia con la categoría y dignidad del hombre» (Ib.). Estas observaciones van íntimamente unidas a la opinión de Arizmendiarrieta de que las virtudes son incluso económicamente rentables. El trabajo, el desarrollo, pueden tener como motivación el afán de lucro; pero más poderoso resorte que las pasiones resultan a la postre las virtudes, «el grado de responsabilidad que cada uno ha puesto en juego al trabajar sin necesitar vigilancias externas, la fluidez de su espíritu que ha hecho grata la convivencia, la generosidad que siempre hace fácil su acoplamiento, la solidaridad que le induce a mirar espontáneamente por los demás, la sensibilidad y la delicadeza que intuyen la trascendencia de las cosas pequeñas; en una palabra, la honradez que acredita al hombre» (FC, II, 67). La práctica de tales virtudes, aunque difícil de contabilizarse, es la mejor inversión que puede realizarse para el buen funcionamiento y desarrollo de las cooperativas (extensible a todo el campo del trabajo). «Una mayor dosis de estas virtudes por parte de cada uno de los cooperativistas asegura a favor de nuestras empresas cooperativas un margen de seguridad y de posibilidades más logrado que por las simples inversiones y organización externa de las tareas» (Ib. 68). La simple organización de las tareas y las inversiones acertadas no garantizan todavía, por sí solas, el buen desarrollo; por el contrario, donde reinan las citadas virtudes, ni la organización ni las inversiones constituirán problema alguno insuperable, «puesto que una buena concepción de los valores espirituales o humanos da por supuesta la implicación de los materiales o económicos» (Ib.). -->

Obviously "the impetus and the bravery of instincts, as legendary as they are, don't help to overcome deficiencies or a remediate powerlessness" (CLP, I, 284). Our struggle requires the strength and the push of passions, but these must always be subjected to the reason, a the conscience, that when were duly developed could even supplant those. "It's obvious that without strength is not obtained everything that the own consciousness legitimates and even demands. But to the extent that we have a strength based in the conscience, in unity and in the solidarity of humans, we give priority and prevalence a the same on the game and the explosion of instincts" (Ib. 284). The function that assume passions in its concept must, then, be understood as provisional, as a concession to the surrounding reality, but that man must always tend to overcome, being guided in its action more for the reason and the conscience. "Man dominated by instinct to win, said being directed to the workers of Ulgor, is a natural product, sentenced as such to be which would be wheat, cow or the fruit tree abandoned to your laws and to their fate. Mean or stepfather" (CLP, III, 28). insists that with that doesn't mean that the profit drive to be bad in all cases, or unneccesary and rejectable, "but simply that man and the world governed by such natural wellspring do not reach the possibilities that could reach. They fall short in what they could give of themselves. That is, the spring of profit, the drive to win, has to be subject to other values, to other goals and only when are exercised with that subordination is when these springs give good results, results in accordance with the category and dignity of man" (Ib.). These observations are closely connected with Arizmendiarrieta's opinion that virtues are even economically profitable. Work, development, can have like motivation the profit drive; but more powerful spring that passions turn out a the dessert virtues, "the degree of responsibility that each one has put in game al work without needing outside monitoring, the fluidity of their spirit that has done pleasant coexistence, generosity that always makes easy their connection, the solidarity it induces look at spontaneously by others, the sensitivity and the delicacy that sense the transcendence of small things; in a word, the honorability that is a credit to man" (FC, II, 67). The practice of such virtues, although difficult to quantify, is the best investment that can be carried out for the good functioning and development of the cooperatives (extensible to the whole field of work). "A larger dose of these virtues on the part of each of cooperators assures in favor of our cooperative businesses a margin of security and of possibilities more achieved that by las simple investments and external organization of tasks" (Ib. 68). The simple organization of tasks and secure investments do not guarantee yet, for itself alone, the good development; on the contrary, where reign these virtues, or the organization or the investments will constitute any insurmountable problem, "given that a good conception of spiritual values or human takes for granted the implication of the materials or economic" (Ib.).

<!-- 10.5. Fábula china del viejo tonto -->

10.5. Chinese fable of the old fool

<!-- Entre las muchas «sabidurías» chinas —dichos, refranes— que eran, al parecer, muy del gusto de Arizmendiarrieta, se encuentra también la fábula del Viejo Tonto que trasladaba las montañas: una fábula de la fe en el hombre, según la interpreta Arizmendiarrieta. -->

Among the many "wisdoms" Chinese —these, sayings— which were, it would seem, very of the taste Arizmendiarrieta's, is also found the fable of the Foolish Old Man that moved the mountains: a fable of faith in man, as interprets it Arizmendiarrieta.

<!-- «Es la historia de un Viejo que vivía en tiempos antiguos en el Norte de la China, conocido por el Viejo Tonto de la montaña del Norte. -->

"It is the story of an Old Man that lived in ancient times in the North of China, known for the Foolish Old Man of the mountain from the North.

<!-- Al sur de la puerta de su casa había dos montañas, una se llamaba Taijand y otra Wangwu, que obstruían su salida. -->

To the south of the door to their house there were two mountains, one was called Taijand and another Wangwu, that obstructed their exit.

<!-- Con gran decisión condujo a sus hijos a desmontar las dos montañas, armados de azadones. -->

With great decision condujo their children to disassemble the two mountains, armed with hoes.

<!-- Otro anciano, denominado el Viejo Sabio, los vió y dijo riendo: ¡Qué tontos sois al intentarlo! Desmontar dos montañas tan grandes está fuera del alcance tuyo y de tus hijos. -->

Other old man, called the Wise Old Man, saw them and said laughing: What fools you are for trying! Take down two mountains so large is beyond your reach and of your children.

<!-- El Viejo Tonto respondió: Cuando yo muera, quedarán mis hijos; cuando ellos mueran quedarán mis nietos; y así hasta el infinito. Pero esas montañas por muy altas que sean no pueden crecer. Con cada cesto que se les quite irán disminuyendo más y más. ¿Por qué no vamos a poder desmontarlas? -->

The Foolish Old Man responded: When I die, will remain my sons; when they die will remain my grandchildren; and so to infinity. But those mountains as high as they are can't grow. With each basket taken from them they will diminish more and more. Why would we not be able to disassemble them?

<!-- Así fue como refutó la equivocada idea del Viejo Sabio y se puso con decisión a desmontar las montañas día tras día. -->

That was how refuted the mistaken idea of the Wise Old Man and started to with decision to disassemble the mountains day after day.

<!-- Esto terminó por llegar al corazón del Dios de los cielos, quien envió a la tierra dos seres celestiales, para que se llevaran al hombro las dos montañas» (FC, II, 27-28). -->

This ended with arriving at the heart of the God of the heavens, who sent to earth two celestial beings, to carry on their shoulder the two mountains" (FC, II, 27-28).

<!-- La lectura (secularizada) de Arizmendiarrieta llega a esta conclusión: «La fe termina conquistando al pueblo que, enardecido ante el testimonio de los “Viejos Tontos”, se lanza, como seres enviados por el Cielo, a trasladar las montañas encima de sus potentes espaldas» (Ib.). -->

The reading (secularized) Arizmendiarrieta's arrives at this conclusion: "Faith ends up conquering the people that, roused by the testimony of the “Old Fools,” is launched, as beings sent by Heaven, to move the mountains above your powerful backs" (Ib.).

<!-- Esta es la fuerza del hombre en su comunidad de trabajo. -->

This is the strength of man in its community of labor.

<!-- El trabajo, un bien escaso. -->

Work, a scarce good.

