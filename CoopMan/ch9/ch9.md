CAPITULO IX

EL (DES)ORDEN ESTABLECIDO

Ante los estragos de la revolución industrial y del capitalismo la clase obrera ha buscado desde el principio su defensa en la asociación. Ya se ha visto que Arizmendiarrieta no se cansa de repetir que la fuerza de los trabajadores reside en su unión. Pero aquella asociación espontánea inicial se dividió muy pronto en tres formas de organización obrera, con frecuencia opuestas: la organización sindical, de carácter preferentemente reivindicativo; la organización política, que dará lugar a los distintos socialismos de Partido, pretendiendo emancipar al proletariado a través de la conquista del Estado o de su reforma; y la organización cooperativa.

Cuando Arizmendiarrieta, tras diversos tanteos, se decide por el cooperativismo, es muy consciente de haber elegido la vía históricamente menos prestigiada. A esta dificultad inicial vendrá a sumarse el clima radicalizado que se vive en Euskadi desde los años 60.

Efectivamente, tras un primer período histórico, en el siglo XIX, en el que se creía que el movimiento obrero había encontrado en la cooperativa el arma revolucionaria más poderosa, el interés por la misma fue decayendo, siendo suplantada por el sindicalismo revolucionario. Posteriormente la consideración del sindicalismo ha declinado también, pasando a ser considerada igualmente como reformista. Así se ha venido a preferir la lucha política como la única vía revolucionaria, no simplemente reformista.

Arizmendiarrieta ha tenido que explicar y justificar el sentido de su experiencia cooperativa en esta situación difícil, tanto a sindicalistas como a revolucionarios (no menos que a los conservadores). En este capítulo trataremos

655

El (des)orden establecido

de ver los aspectos principales del pensamiento de Arizmendiarrieta en medio de tanta crítica.

También este capítulo está, por tanto, determinado por la polémica.

Arizmendiarrieta define en ella su posición frente a las críticas de las nueva izquierdas.

Su opción por la cooperativa no significa en modo alguno la exclusión de los otros campos de acción. Al contrario, en su mente, sólo la coordinación de los tres campos podrá lograr la emancipación real de los trabajadores.

Arizmendiarrieta, con Mounier, ha calificado siempre el orden actual como
«el desorden establecido». Ha sido muy consciente del enorme esfuerzo que requerirá, partiendo de la educación y la enseñanza, su superación en todos los frentes. Dedicado en cuerpo y alma a la cooperación, no por ello ha descuidado otras formas de lucha obrera. Estuvo a punto de ser deportado en las huelgas de 1956, por estar considerado «unánimemente», en expresión del
Gobernador Civil, como principal responsable de las mismas en
Mondragón1. El Archivo Arizmendiarrieta guarda numerosos testimonios de su presencia activa en campos extracooperativos (multas, despidos, Convenios colectivos, campañas de reforma de la empresa, etc.), aun después que las cooperativas se hubiesen consolidado. Pero estos aspectos, quizá menos conocidos en su mismo entorno, por no haber sido Arizmendiarrieta nada amigo de la agitación ostentosa, sino de la intervención callada, corresponderían a una biografía, más bien que a nuestro estudio de su pensamiento2.

Un íntimo conocedor ha escrito refiriéndose al primer período de su actividad en Mondragón: «Las conferencias prematrimoniales; la preparación de la juventud previamente a incorporarse a filas; las primeras lides en el foro del Ideal Cinema para dar constancia de que las organizaciones paternalistas no eran solución válida para el futuro, que reclamaba la reforma de las estructuras. Todo era ocasión para sentar las bases de una sociedad sin clases, apoyada en hacer todo por los demás, de vaciarse por los demás»3. Con este espíritu, Arizmendiarrieta no podía sino sentirse muy próximo a sus críticos desde la izquierda. Sin embargo dos razones fundamentales le separaban

1

Carta de Arizmendiarrieta al Gobernador Civil de Guipúzcoa, del 4 de mayo de 1956. La correspondencia mantenida por este motivo con el Gobernador Civil se conserva en el Archivo Arizmendiarrieta.

Valgan de ilustración, a modo de ejemplo, les siguientes recriminaciones de Arizmendiarrieta a un alto cargo ministerial, que estaba ligado a la Unión Cerrajera, tras una conferencia de doctrina social pronunciada por aquel: días más tarde Arizmendiarrieta le escribe lamentando «que otras manifestaciones y actuaciones suyas no estuvieren en la línea de aquellas ideas, ya que algunas de las intervenciones en la Junta General de accionistas de Unión Cerrajera S.A. de estos últimos años (...) han llegado al público como exponente de su actitud no concorde con lo que la doctrina social y las circunstancias demandan». «Ud. conoce muchos de nuestros problemas —continúa— e incluso no pocos de la misma empresa a través de versiones no siempre objetivas. Creo que el día que se persuada Ud. que tiene muy mala información, información muy interesada, y actúe sin otras interferencias con arreglo a su conciencia mejor informada, no tendremos que lamentar diferencias, que efectivamente han existido».

Carta de Arizmendiarrieta a X.X. (omitimos el nombre), Madrid, del 28 de marzo de 1963 (Archivo
Arizmendiarrieta).

2

3

ORMAECHEA, J.M., Una solución a tiempo para cada problema, TU, Nr. 190, nov.-dic. 1976, 32.

656

Euskadi

profundamente de ellos. La primera era su convicción de que sólo la conjunción (cooperación) eficaz de los tres frentes (cooperativo, sindical, político) haría posible una emancipación obrera real (Arizmendiarrieta no creía en una emancipación meramente política); que las guerras mutuas, por tanto, entre aquellos tres sectores, rompían la unidad del movimiento obrero, necesario para su emancipación, ya de por sí bastante endeble. En segundo lugar,
Arizmendiarrieta no estimaba menos que sus oponentes los ideales de igualdad y de solidaridad, pero estaba íntimamente convencido de que, al menos respecto al cooperativismo, el ritmo realista de avance de la historia hacia la utopía era el que marcaba él, y no el que querían imponerle a la fuerza sus jóvenes críticos.

Arizmendiarrieta creía firmemente en la validez de la fórmula cooperativa como instrumento de emancipación obrera y de transformación social
(quizá radicaran, en última instancia, en esta fe las más hondas diferencias entre Arizmendiarrieta y sus críticos). Estaba convencido de que era esta la vía que convenía mejor a la historia y a las tradiciones del pueblo vasco. Y estaba convencido igualmente de que, en Euskadi, el campo de acción más prometedor en las realidades actuales era el de la acción cooperativa. Nunca se resignó a las tesis de su incapacidad de desarrollo o de su inevitable degeneración. Confiaba en la madurez del movimiento obrero vasco y en su capacidad de desarrollar y ampliar (lenta, pero incesantemente) la cooperación a nuevos campos, sin dejarse avasallar por el capitalismo circundante, hasta lograr su plena emancipación. Con la condición, siempre, de que se acertara a coordinar la acción cooperativa con la sindical y política, en orden a los fines comunes.

Nunca ha creído que el cooperativismo concreto que se estaba desarrollando en Mondragón significara ya el ideal. Arizmendiarrieta ha entendido esta experiencia, no como el punto de llegada, sino como un punto de partida, con todos los defectos que es preciso aceptar de partida, con espíritu de ir superándolos en común esfuerzo. El ideal, dirá Arizmendiarrieta, está siempre alto y lejano. Esto valía, por otra parte, no menos para luchadores políticos y sindicalistas, que para los cooperativistas. Hoy por hoy ninguno de ellos puede medir sus realizaciones con el ideal mejor que los otros. Condición para avanzar, para todos igualmente, será saber actuar con realismo, sin renunciar a los ideales.

1. La reconstrucción de Euskadi
Comenzaremos nuestra exposición por el tema nacional vasco, por ser esta una cuestión que subyace, de algún modo, a toda la polémica de Arizmendiarrieta con las nuevas izquierdas. Sin limitarnos a sólo los aspectos po657

El (des)orden establecido

lémicos, expondremos de modo sistemático los puntos esenciales de su pensamiento acerca del cooperativismo y la reconstrucción de Euskadi.

Volvamos, pues, una vez más, a los primeros años de la postguerra.

1.1. Ida y vuelta de una crisis
El 1 de Abril de 1939 había terminado la Guerra Civil: ese día era el Día de la Victoria, para los vencedores.

Desde ese momento quedó establecida la falsilla sobre la que debían redactarse las palabras y la expresión, la acción y la conducta. Las líneas impuestas eran nítidas y forzadas, aunque, con algo de fortuna, una cierta ambigüedad dejará en ocasiones algún espacio para la tolerancia. Con mejor o peor suerte Arizmendiarrieta procuró utilizar ese espacio —hecho de silencio y ambigüedad obligados— para transmitir su mensaje y promover su acción social.

Sin embargo, había algo sobre lo que no cabía veleidad alguna: el tema
«Euskadi» quedó proscrito por varias décadas.

«Lo que urge en la enseñanza profesional industrial en España es...» (EP,
I, 249); «hay un salto tan grande que dar por el retraso que llevamos en España en la cosa social» (Ib. 231); «una consoladora realidad social de España»
(Ib. 236), etc., son, sin duda, modos de expresión que en sí nada tienen de sorprendentes, pero que, en la posterior evolución socio-políticamás libre, con un contexto político renovado, podrían ser susceptibles de valoraciones negativas. En Arizmendiarrieta fluyen sin dificultad, al menos hasta los años
60. Todavía en 1962 podía decir Arizmendiarrieta: «... tratamos de seguir adelante en la promoción económica y social de nuestra zona, como el mejor servicio social al pueblo español» (Ib. 297); entre los asistentes al acto oficial se encontraba el Director General de Enseñanza Laboral.

Desde fines del s. XIX, el socialismo y el nacionalismo vascos se han opuesto mutuamente en feroz competencia4. Con anterioridad a la guerra se han conocido algunos ensayos, tanto por parte de los socialistas como de los nacionalistas, para superar esta dicotomía en una síntesis, que asumiera por igual reivindicaciones sociales y nacionales, pero los resultados no han podido ser duraderos5. En este sentido nosotros destacaríamos ahora a los llama-

CORCUERA, J., Orígenes, ideología y organización del nacionalismo vasco 1876-1904, Siglo XXI,
Madrid 1979. ESCUDERO, M.-VILLANUEVA, J., La autonomía del País Vasco desde el pasado al futuro, Txertoa, San Sebastián 1976. OLABARRI, I., Relaciones laborales en Vizcaya (1890-1936), L.

Zugaza, Durango 1978. SOLOZABAL, J.J., El primer nacionalismo vasco, Tucar, Madrid 1975. VARELA, S., El problema regional en la segunda República española, Unión Ed., Madrid 1976.

4

5 BELTZA,

El nacionalismo vasco (de 1876 a 1936), Mugalde, Hendaye 1974. FUSI, J.P., Política obrera en el Pals Vasco 1880-1923, Turner, Madrid 1975. ID., El problema vasco en la II República,
Turner, Madrid 1979.

658

Euskadi

dos «sacerdotes propagandistas»6. Puede ser recordado entre ellos J. Markiegi, predecesor de Arizmendiarrieta como coadjutor de Mondragón, fusilado por las tropas de ocupación7.

El joven Arizmendiarrieta de la anteguerra se nos aparece plenamente integrado en esta tradición, tratando de sintetizar aspiraciones sociales y nacionales en un mismo esfuerzo.

La guerra ha significado una dura prueba para Arizmendiarrieta. Después de la guerra no ha tenido obstáculos para seguir desarrollando sus preocupaciones sociales. Ha abandonado enteramente, por el contrario, la temática nacional. Sólo poco a poco, a la par con la lucha del pueblo vasco, recuperará su actitud original. Ha dejado, por ejemplo, de cultivar su lengua materna; su interés por la cuestión nacional, incluso por los aspectos más culturales de la misma, que con tanto interés había seguido en la Academia Kardaberaz, ha desaparecido por completo. Desde 1949/1950, superada ya la postguerra inmediata, asistimos a un renacer de las letras vascas: inútilmente buscaremos en su biblioteca ningún libro euskérico de estos años. Es más, tampoco en las listas de libros, que aún se conservan, de la biblioteca de los jóvenes de la Acción Católica, por él organizada, encontraremos un sólo título euskérico, ni siquiera de estudios históricos o antropológicos del País Vasco.

Arizmendiarrieta ha podido pensar, no sin razón, que en aquellas circunstancias la más mínima sospecha de simpatías nacionalistas habría significado un serio obstáculo para su labor social. En su decisión de ser «sólo sacerdote» y de desarrollar en cuanto tal una labor social eficaz, ha sacrificado radicalmente sus intereses culturales nacionales, llegando a abandonar, no sólo el cultivo público de su lengua materna, sino incluso el privado: cuando veinte años más tarde empiece de nuevo a escribir en euskara, su lengua materna está empobrecida y su prosa es tosca. Parece haber pensado también —en años posteriores se manifestará en este sentido— que, dada la dureza de la represión y la debilidad del pequeño pueblo vasco, los intentos de reconstrucción nacional, atacando directamente por la vertiente linguística y cultural, ya no tenían salida. Sólo a través del fortalecimiento económico, especialmente del cooperativo, sería viable la reconstrucción nacional.

Seguramente es vano el intento de reconstruir el estado de conciencia en crisis de los años de la inmediata postguerra a base de datos muy posteriores.

Con los años Arizmendiarrieta volverá a interesarse por el tema nacional, pero en su concepto de Euskadi nunca volverán a primar los caracteres cultu6
Sirva de ejemplo Aitzol, quien como promotor de las letras euskéricas ha dado su nombre a toda una generación de escritores, cfr. MICHELENA, L., Historia de la literatura vasca, Minotauro, Madrid
1960, 146, P. de Larrañaga es autor de diversas obras (Mendiko eguna, 1921; Arrate, 1926), incluso de una ópera en euskara (Amets larria, 1923).

Orixe ha llegado a colocar al prosista Markiegi a la par de Lizardi, cfr. ONANDIA, S., Euskal literatura, Etor, Bilbao 1975, vol. IV, 281-283. Véase también BORDAGAIN, Jose Markiegi apaiza, Euzko
Deya, Nr. 44, 29 de abril 1937. Markiegi fundador de la ikastola de Mondragón en la República:
ERREZOLA, M., 1937 aurreko ikastolen edestirako jakingarriak, Jakin Sorta, Nr. 6, 1972, 273-274.

7

659

El (des)orden establecido

rales tradicionales (lengua, etc.). Podemos así observar uno de los desarrollos más interesantes que se han dado en la postguerra: para Arizmendiarrieta la patria, más que la tierra de los padres (ab-erri), es la tierra de los hijos, y Euskadi estará constituída ante todo por el trabajo de sus hombres. El concepto central de su filosofía de la historia es el del trabajo, y los valores culturales y espirituales son asumidos en torno a él. Esta visión determinará también su concepto de patria (Euskadi) y de las obligaciones ciudadanas hacia ella. Más que en el pasado la patria está en el futuro. Nosotros, interesados en el desarrollo de su pensamiento, no en su biografía8, debemos exponer algunas razones, que pueden darnos la explicación de esta actitud de Arizmendiarrieta en la postguerra y de su ulterior desarrollo.

Sea la primera la actitud de la Iglesia vasca oficial y en particular la espiritualidad sacerdotal del Seminario de Vitoria. Arizmendiarrieta siempre ha insistido extremadamente en que su obra no era política y en ningún caso podía ser entendida como tal (podemos dejar de lado su concepto de la política, que se refiere invariablemente a los partidos políticos). Ha entendido su obra como simplemente humana, anterior a toda política.

Desde antes de la guerra, la política era tabú en el Seminario de Vitoria
(entendiéndose generalmente por política el nacionalismo vasco). La misma investigación científica de la antigua religión de los vascos, como la realizada por el Prof. Barandiarán, será sospechosa de concomitancias políticas, hasta de «judeo-masónicas»9. A. de Onaindía ha descrito muy claramente el celo vigilante antinacionalista que reinaba en aquel Seminario10. El testimonio más válido nos lo da el entonces Obispo de Vitoria, Mons. M. Múgica, quien acabaría desterrado, lo mismo que los dos sacerdotes citados, Barandiarán y
Onaindía. Tras subrayar el énfasis que él mismo ponía en alejar a los seminaristas de toda política, Mons. M. Múgica concluye: «En tal grado se inculcaba en el Seminario la necesidad de que el sacerdote se mantenga al margen de todo partido político que en uno de los programas oficiales de la Ratio Studiorum se obligaba a los alumnos a saber desarrollar este tema: Cómo el sacerdote que se adhiere a un partido político compromete los intereses de la
Religión contribuyendo a hacer ineficaz su ministerio sagrado. —Si entre seiscientos seminaristas caía alguna rarísima vez uno en la tentación de hacer algún acto político —por supuesto, a ocultas de la vigilancia— era el propio
Rector quien me denunciaba el caso, para imponer al culpable la sanción coAunque ha sido acusado con frecuencia de los dos extremos contrarios, no parece que Arizmendiarrieta, indudablemente abertzale o patriota, a su modo, amante de su país y consagrado a su servicio, pueda ser honestamente acusado de traidor y de abandono de la causa vasca; por otra parte, sólo por desconocimiento o por mala voluntad puede ser considerado como «nacionalista» insolidario o chovinista en la forma en que lo hiciera, por ejemplo, El Alcazar, 14 de octubre de 1981. Que Arizmendiameta no encaje en esquemas ajenos no significa que no tuviera sus esquemas propios sobre el tema.

Tampoco parece que se deje hipotecar por ningún partido o grupo político, aunque hoy en día no falten intentos de interpretarlo en este sentido.

8

9

UGALDE, M. de, Hablando con los vascos, Ariel, Barcelona 1974, 21-23.

10

ONAINDIA, A. de, Ayer como hoy. Documentos del Clero Vasco, Axular, Saint Jean de Luz 1975,

5-9.

660

Euskadi

rrespondiente»11. El clima de la postguerra, con Mons. Lauzurica de Administrador Apostólico de la Diócesis y Rufino Aldabalde de responsable espiritual del Seminario12, apenas necesita comentario. F. Urbina, distinguiendo en los 40 años de franquismo tres generaciones de sacerdotes, ha escrito a propósito de la primera: «La generación de la guerra, duramente probada, y que en la mayoría de los otros Seminarios fue un foco de ‘nacional-catolicismo’ impuesto a los estudiantes (en todas partes se leyó, en el refectorio, la
Historia de la Cruzada), mientras que en Vitoria, con las heridas de la persecución nacionalista (de signo invertido a la de los otros lados), se refugió en un espiritualismo intenso que se quería ajeno a la política, formulado con la expresión de D. Rufino: ‘Sólo sacerdote, todo sacerdote, siempre sacerdote’»13.

Otra razón, de raíces más filosóficas, va de las acerbas críticas al nacionalismo de Maritain y de Mounier, a quienes sigue el primer Arizmendiarrieta, al ideario socialista, que este suscribirá a partir de 1946 ó 1947. El objeto de las críticas de los personalistas franceses ha sido el nacionalismo de los Estados modernos, en especial el alemán y el italiano, pero no pocos de los aspectos acusados (racismo, exclusivismo, etc.) eran fácilmente reconocibles también en el nacionalismo vasco. En cuanto al socialismo, es preciso reconocer que ante el hecho nacional vasco, este ha desarrollado en Euskadi tradicionalmente, más que una teoría propia suficiente, un absoluto desprecio del movimiento nacionalista en todas sus vertientes, desde sus aspiraciones políticas hasta sus esfuerzos culturales, muy particularmente su doctrina social. Sin demasiada fatiga intelectual, el socialismo vasco de anteguerra se ha limitado a calificar reiteradamente al movimiento nacionalista de aldeano, troglodita, sentimental, filosofía de campanario y espíritu de pequeñez. Las tradiciones vascas serán tonterías, el euskara un despreciable dialecto, la patria una cuestión de burgueses14. Nada nos hace suponer que Arizmendiarrieta haya suscrito nunca tales tesis en su literalidad. Pero algunas de las críticas socialistas han hecho mella en él. Sin duda se debe a la influencia socialista su abandono del «culturalismo» y su nuevo concepto de pueblo vasco como comunidad de trabajadores específica. También las burlas a la estrechez mental de los nacionalistas parecen haberle afectado, al menos temporalmente. Según algunos testimonios, entre 1940 y 1950 Arizmendiarrieta se habría manifestado partidario de un universalismo indiferenciado (una lengua única para todo el mundo, etc.), desdeñoso de lenguas y culturas minoritarias.

11

Mons. M. Múgica, Imperativos de mi conciencia, en: ONAINDIA, A. de, op. cit., 110-111. Ib. 7,
Onaindía cuenta los problemas que podía suponer, incluso para un profesor del Seminario, el simple hecho de suscribirse a la prensa diaria nacionalista.

12

Sobre Mons. Lauzurica y R. Aldabalde véase ONAINDIA, A. de, Hombre de paz en la guerra, Ed.

Vasca Ekin, Buenos Aires 1973, 46-54.

13

URBINA, F., Formas de vida en la Iglesia en España, en: Iglesia y Sociedad en España 1939-1975,
Ed. Popular, Madrid 1977, 32.

14

AZURMENDI, J., PSOE eta euskal abertzaletasuna, Hordago, Donostia, 1979. FUSI, J.P., El
PSOE y el problema vasco, Historia 16, Nr. 1, mayo 1976, 71-76.

661

El (des)orden establecido

Tal vez no convenga sobrevalorar estas posiciones, pasajeras, además de escasamente documentadas, propias en todo caso de una época de ansiedad y de incertidumbres colectivas, surgidas en el vacío de futuro que siguió a la guerra. Su interés reside para nosotros en haber dado pie a una evolución en el pensamiento de Arizmendiarrieta, cuyo resultado vamos a tratar de exponer. Como siempre, esta evolución se desarrollará siguiendo muy de cerca la realidad social de su entorno, que es lo que para Arizmendiarrieta constituye su objeto inmediato de observación y reflexión, así como el campo de las exigencias concretas. En ello precisamente se nos manifestará su personalidad en su doble vertiente: Arizmendiarrieta ha transformado su entorno, pero su entorno ha transformado también a Arizmendiarrieta.

Conocidas las prohibiciones oficiales y la persecución del euskara en la postguerra15, nadie se sorprenderá de la completa ausencia del mismo en los primeros escritos de Arizmendiarrieta: por unos veinte largos años, la única presencia de este idioma será una breve frase («goazen, goazen guztiok birgiña amagana»), primeros versos de un canto mariano, en un pequeño artículo con motivo de las flores de mayo (PR, I, 101). Valga lo mismo, con algunas excepciones, para su actividad pastoral: en las numerosas veladas teatrales, que Arizmendiarrieta organizaba con la juventud, no se encuentra un sólo título euskérico (Ib. 34; Ib. 159-162). En el informe de las tandas de Ejercicios realizados por los trabajadores en 1941, frente a 22 tandas en español, hay tan sólo 4 en vascuence (Ib. 25). Sin duda estos datos podrían hacerse fácilmente extensivos a cualquier Parroquia de las Diócesis vascas.

Por el contrario, la adversidad del clima no le ha impedido atender, en la práctica pastoral, a algunas manifestaciones de la cultura popular vasca, como Olentzero, Bizar-zuri, danzas, canto y txistu, desde sus primeros años en Mondragón (PR, I, 33: 1941; Ib. 40: 1942)16.

El 25 de noviembre de 1944 miembros del clero vasco en el exilio dirigieron una extensa memoria a S.S. el Papa Pío XII, exponiendo la situación a que se veía reducida la Iglesia vasca bajo los nuevos gobernantes y acusando, entre otras cosas, los daños que se seguían de la prohibición del euskara en la enseñanza religiosa17. Inmediatamente (1945) Arizmendiarrieta se hace eco

15

NUÑEZ, L.C., Opresión y defensa del euskara, Txertoa, San Sebastián 1977. TORREALDAI, J.M.,
Euskararen zapalkuntza (1936-1939), Jakin Nr. 24, 1982, 5-73.

16

Para un juicio histórico de estos hechos deberá tomarse en consideración el clima de la postguerra.

BELTZA, El nacionalismo vasco en el exilio 1937-1960, Txertoa, San Sebastián 1977, 74, refiriéndose aún a épocas más tardías, escribe: «(...) Se renuevan las represiones indiscriminadas sobre los aspectos no directamente políticos de la cultura peculiar vasca. Así el 1 de abril de 1948 la censura prohibe terminantemente escribir en euskara en siete publicaciones religiosas de Guipúzcoa: en la Navidad de
1949, un símbolo de estas persecuciones puede ser el Olentzero, cuyos organizadores se vieron obligados a mil gestiones diferentes, incluyendo la traducción de los villancicos y la obtención de un aval político para cada uno de los miembros del grupo, para ser al final prohihido». Arizmendiarrieta tenía organizados ya en 1943 varios grupos de danzas y una Banda de txistularis (PR, I, 51).

17

«Que sean puestas en vigor en la diócesis de Vitoria las normas generales de la Iglesia relativas al uso de las lenguas indígenas a fin de evitar graves males en la enseñanza religiosa, de no agravar el pavoroso problema del abandono de la religión por el pueblo y de poner término al gran escándalo que siempre ha constituido en el pueblo vasco el hecho de conducirse la Iglesia como si en estas cosas obra-

662

Euskadi

de esta acusación, protestando, igualmente, de que «las generaciones nuevas de estos pueblos, en los que se habla el vascuence, no saben ni el catecismo, después de varios años de Escuela» (PR, I, 93).

Luego sigue un largo silencio, tanto en sus escritos como en los archivos de sus actividades. Durante quince años le vemos a Arizmendiarrieta absorto en el tema social. De repente una carta manuscrita de 1961 nos sorprende. El
26 de marzo de 1960 moría en el exilio el Lehendakari José Antonio Aguirre, sucediéndole automáticamente en la presidencia D. Jesús María de Leizaola.

He aquí la carta que Arizmendiarrieta escribe al nuevo Lehendakari en el exilio:
Vía Hendaya. 19-7-61
Dn. Jesús M.ª de Leizaola
París
Mi distinguido y querido amigo: La lectura de sus dos trabajos Líneas generales de formación de la Economía Vasca y Constantes y variables de la misma me ha gustado mucho y el tiempo se me ha hecho corto. Creo que es una gran esperanza en estos momentos tener un Presidente economista, que haciendo honor a nuestro espíritu secular pueda confiarse en que nuestro pueblo acabe encajando plenamente en Europa como un eslabón de dos zonas18.

En estos años 60, por un lado la obra cooperativa se ve consolidada, y el empuje del nacionalismo joven, por el otro, es cada día más fuerte: Arizmendiarrieta, sensible a cuanto suceda en su entorno, inicia una nueva aproximación hacia la temática de la etnia vasca y sus reivindicaciones culturales y políticas, de forma más decidida y clarividente.

En 1963, exponiendo el proyecto de creación de un hogar infantil, la reivindicación del euskara no necesitará basarse ya en motivos religiosos o de apostolado. «No se trata de suplantar la acción del hogar sino de complementarla en consonancia con las necesidades y posibilidades de nuestro tiempo.

Podemos cultivar los valores más entrañables del idioma, música, folklore, etc., en una grata convivencia, que puede prolongarse hasta los seis o siete años» (FC, I, 258).

Este mismo año, aprovechando el aire renovador introducido por Juan
XXIII en la Iglesia y la convocatoria del Concilio Vaticano II, un numeroso grupo de sacerdotes envió un informe a la Secretaría del Concilio. Luego «en la década del 65 al 75 asistimos a un vertiginoso crecimiento de la contestación sacerdotal»19. En este nuevo clima también Arizmendiarrieta podrá mara al dictado de una política antivasca», cfr. IZTUETA, P.,: Sociología del fenómeno contestatario del
Clero vasco: 1940-1975, Elkar, Donostia/San Sebastián 1981, 143.

18

Carta de Arizmendiarrieta a D. Jesús María de Leizaola (París), del 19 de julio de 1961 (Archivo
Arizmendiarrieta). Tratándose de un dato completamente aislado no es fácil medir su significación exacta. Podría tratarse de entablar una primera relación con el Gobierno Vasco en el exilio, motivada por los intereses que por las mismas fechas Arizmendiarrieta tenía en Venezuela para su Escuela Profesional, cfr. Plan de cooperación de Liga de Educación y Cultura con la Corporación Venezolana de
Fomento de Guayana, abril de 1961 (EP, II, 166-171).

19

VARIOS, Herria-Eliza Euskadi, 1978, 15-16.

663

El (des)orden establecido

nifestar abiertamente sus preocupaciones por el pueblo vasco. Su vieja sentencia, «la rebeldía humana siempre es invencible» (SS, I, 60), se confirma.

El movimiento de recuperación nacional se extiende por todos los pueblos de
Euskadi y Arizmendiarrieta llama a sus cooperativistas a unirse a este movimiento (1968): «La visión cooperativa del hombre y de la comunidad humana, apoyada en la toma de conciencia de los valores entrañables de la persona humana como de la comunidad respectiva, no se reduce a la aceptación de la necesidad de reforma y reestructura de la empresa, célula elemental organizativa para potenciar nuestro trabajo, sino que referida a otros campos y planos de interés y actividad humana, nos impone un empeño y una participación revisionista y la afirmación de una más amplia esfera de actividad autónoma, de iniciativa y de responsabilidad, concibiendo el bien común como fruto derivado de una armonización de peculiaridades personales y comunitarias, y no de conjunciones violentas en aras de homogeneizaciones desvitalizadoras. —La conciencia cooperativista impone que nos sumemos al clamor de protesta y nos unamos a la inquietud universal de respeto directo al hombre y a las comunidades en las que aquel trata de realizarse plenamente, no sólo en el ejercicio de su trabajo, sino también como miembro vivo de las comunidades en las que por historia o experiencia estuviera inserto. No se le puede negar a nadie el espacio vital preciso, pero este no pasa de ser un simple término vacío mientras a cada uno no se le reconozca la autonomía de acción y realización. Al hombre y a las comunidades humanas» (FC, III,

102).

Arizmendiarrieta ha tenido su propia visión de lo que significa «hacer país» y del papel que a él en esta labor le correspondía, así como del que creía no corresponderle en absoluto. No le correspondía, en primer lugar, ningún papel político. Le correspondería, por el contrario, el papel de crear una economía vigorosa y humana, que a su juicio es el fundamento real de toda sociedad libre. En segundo lugar, quería que el ejemplo cooperativo sirviera de modelo para otras iniciativas populares, para que estas se vieran animadas por el mismo espíritu de colaboración y solidaridad, evitando banderías. Sin duda aun sentía el horror de las comunidades políticamente divididas y enfrentadas que acabaron en la guerra civil. Convencido de que esta labor no sólo era posible, sino que debía ser llevada a cabo con total independencia de los partidos políticos, tratará de mantener un equilibrio difícil a igual distancia de los dos frentes. No podrá evitar ser objeto de suspicacias por ambas partes.

Aparte de los recelos que el desarrollo cooperativo suscitaba por sí mismo, las acusaciones más o menos veladas de ser foco de politización y subversión son también relativamente tempranas. Ya en 1965 nos encontramos con la acusación, no sabemos si fundada, de una «participación masiva de los alumnos de la Escuela Profesional en la celebración del Aberri Eguna de
Vergara»20. A pesar de los esfuerzos de Arizmendiarrieta por disipar recelos

20

Carta de Juan de Aizpurua a Arizmendiarrieta, del 15 de abril de 1965 (Archivo Arizmendiarrieta).

664

Euskadi

de este tipo, estos subsistirán, hasta llegar en 1969 a provocar muy serias dificultades con el Gobernador Civil de Guipúzcoa Oltra Moltó, quedando gravemente amenazada la subsistencia de la Escuela Profesional de Mondragón21.

Sorprende no poco que exactamente por los mismos días hayan surgido también las críticas del bando contrario. Por decreto del 3 de junio de 1965 le fue concedida a Arizmendiarrieta la Medalla de Oro al Trabajo. El Ministro de Trabajo, Sr. Romeo Gorría, le impuso personalmente la medalla el 25 de agosto de 1966. «Franco’k ba daki nor saritzen duen, eta nor gartzelaratzen.

Eta egia aitortzeko, batez beste, gutxitan ibiltzen da oso oker», escribió Tx. con tal motivo. En un duro artículo titulado A. Arizmendiarrieta, euskaldunen etsai22, Tx. afirmaba: «(...) Eskuindar eta españazale kooperatibismo horrek, zernai hortik esanagatik ere, kalte baizik ez digu ekartzen gaur, eta ez besterik ekarriko biar ere». Tx. vertía una doble acusación sobre Arizmendiarrieta y sobre el cooperativismo de Mondragón: «(...) Gauza ezaguna da
Arizmendiarrieta’k aspaldiko bere abertzaletasuna zokoratu egin duela; eta
Arrasate’n euskera baztertu egin dala, Españia’ko kondaira español modura irakasten dala, eta, itz batez, españolkeria baizik ez dala zabaltzen. —Beste alde batetik euskaldun langillea langille-klaseko lagun bat da. Langilleen problemak bere problemak dira. Eta txatxukeria da klasearen problemak eta kooperatibakoak bereztea. Ots: hauxe gertatzen ari da. Kooperatiba bakoitza mundutxo zirtzil bat biurtzen ari da: langilleek ez dute nai ere izaten gaiñerako langilleekin bat egin. Oporketa gertatzen danean, kooperatibetako langilleek ez dute atera nai izaten; ze berek ez omen dute problemarik... Zer da jokabide ori? Oraindik ere entzun bearko al dugu kooperatibismoa dala langilleen soluzioa, eta kapitalismoaren eriotza? —Kapitalismoaren babesean egiten dan kooperatibismoa, saldukeri utsa da; ez du ezertarako balio ezpada langille-klasea zatikatzeko, zokokeria geitzeko (gu gure zokoan ongi bizi gera; besteak or konpon), eta kapitalismoari koipe piska baten bidez indarra eta iraupena emateko baizik».

En el Archivo Arizmendiarrieta se conserva diverso material relativo a estas dificultades, en especial el acta de una tensa entrevista con el Gobernador Civil la tarde del 16 de julio de 1969. En folio firmado por Arizmendiarrieta, sin fecha, destinado a «estimados amigos», que no se especifican, se les informa a estos de que el Sr. Oltra Moltó se mantiene en la línea de sus amenazas y presión, y de que se han tenido informaciones confidenciales confirmatorias «del plan trazado por dicho señor de meter en varas, mejor dicho, de conseguir que se rindan a sus pies “los poderosos y arrogantes cooperativistas mondragoneses”, empeñados en protagonizar unos “afanes de emancipación” inconfesables y peligrosos, que en no poca parte pueden influir en la “actitud levantisca” que se acusa en el pueblo guipuzcoano y que bien pudiera contagiar al resto del País Vasco si es que realmente no está ya muy tocado de dicho mal y pueden tener reflejos en las más variadas facetas de la vida en un futuro no muy lejano. El se considera muy capaz para ello y sobre todo su celotipia requiere que este sector un tanto indiferente a su “alteza real” se acerque y cuente con él. Todo esto se encubre con una mal disimulada cortina de humo de una hipotética actitud de resistencia para cuya confirmación o constatación por otra parte el
Sr. Oltra Moltó no tiene el más leve motivo en lo referente a la Escuela y Mondragón más que el decir que entre los que se detienen en Mondragón la casi totalidad son cooperativistas» (Archivo Arizmendiarrieta).

21

Tx., A. Arizmendiarrieta, euskaldunen etsai, en: Documentos, Hordago, San Sebastián 1979, vol.

IV, 396. Este artículo, enviado para su publicación en la revista Zutik de ETA, no fue publicado en aquella por «la oposición manifiesta de todos los de Baiona» (Ib. 393).

22

665

El (des)orden establecido

Para nuestro propósito de conocer el pensamiento de Arizmendiarrieta resulta altamente esclarecedora una polémica (privada) del año 1966. Un reconocido intelectual vasco, devolviendo el ejemplar de la revista «TU» de junio de 1966, escribía airado: «Les agradeceré no me envíen más su publicación TU. Durante bastante tiempo he venido siguiéndola para ver si respondía de algún modo a mis preocupaciones humanas. Pero veo, ya definitivamente, que no es así. El «gran ausente» de su publicación es «el hombre vasco» (entrecomillados y subrayados del original). Y como este hombre es el que más me interesa, porque es el que tengo más a mano, veo con desagrado la labor de desarraigamiento y descastamiento económico que ustedes llevan a cabo. En suma, su hoja contribuye a destruir las cosas que yo más quiero y para eso me basta y me sobra con el marxismo»23.

Al momento Arizmendiarrieta ha garabateado en letra tortuosa un borrador de respuesta, que transcribimos íntegro:
1. Ud. es un burgués, que supone que todos están en el mismo plano, en el que pueden dar margen a sus elucubraciones, prescindiendo de problemas económicos: la despensa ya está.

2. No todos hemos de tocar la misma flauta. No presumimos de estar en todo. Podríamos24 conocer su solidaridad? Es con los hombres o con las estrellas? Con aquellas me comprometo a nada.

3.

El vasco... apenas tenemos autorizado25.

«Siempre me ha gustado tentarle —escribe J.M. Mendizabal— y le dije que había oído como crítica hacia él y hacia su obra que no tenía carácter vasco. Se me puso bravo y me probó que el hecho y la realidad vasca estaban en la raíz de muchas de sus realizaciones. Y, por supuesto, afirmó que los que estaban en la luna o montados en una nube eran esos que criticaban la sólida realidad y realización vasca de Mondragón»26. Diversos testimonios coinci23
Carta manuscrita de X.X. (omitimos el nombre) a D. Juan Leibar Guridi (director de la revista
TU), no fechada (Archivo Arizmendiarrieta). Creemos que tanto esta carta como el escrito de Tx. deben ser situados para su comprensión en el contexto de 1966, más concretamcnte en la lucha entablada en ETA y en torno a ella sobre los «felipes» y el «españolismo», que acabará con la expulsión de ETA, en diciembre de ese mismo año, de Iturrioz y su grupo (actualmente EMK). GARMENDIA, J.M., Historio de ETA, L. Haranburu, San Sebastián 1983, 181-227. ORTZI, Historia de Euskadi: el nacionalismo vasco y ETA, Ruedo Ibérico, París 1975, 313-323.

24

25

Lectura dudosa.

Folio manuscrito, firmado «Arizmendi», no fechado (Archivo Arizmendiarrieta)

26

MENDIZABAL, J.M.: D. José María sacerdote, TU, Nr. 190, nov.-dic. 1976, 16. A Arizmendiarrieta, personalmente, y al cooperativismo arizmendiano, o mondragonés, se le ha solido reprochar en años pasados su falta de sensibilidad por la cuestión vasca, llegando a ser considerado este fenómeno como un caballo de Troya introducido astutamente por el régimen franquista en el pueblo vasco. Creemos que este análisis bastará para dejar manifiesto lo infundado de tales interpretaciones, cuando no su patente falsedad. Pero no menos falsa e infundada nos parece la opinión contraria, últimamente más frecuente, que pretende explicar el origen y/o el desarrollo de esta experiencia a partir de un pretendido nacionalismo vasco, cfr. SAIVE, M.-A., Mondragon et les aspects doctrinaux du projet coopératif,
Annales de l‘économic publique, sociale et coopérative, Nr. 3, 1981, 369-379. VARIOS. Mondragon
Co-operatives: Myth or Model, The Open University, Co-operatives Research Unit, London 1982,
40-41, 72, 81, 92, 102, 115, Esta explicación (empleada lo mismo para dar razón de su éxito y auténtico enraizamiento social, como para tacharlo de insolidario y contradictorio con el esencial socialismo coo-

666

Euskadi

den en afirmar que en sus últimos años Arizmendiarrieta era muy sensible a esta objeción. Su reacción ante la carta arriba transcrita es una prueba más de ello. Luego, el 22 de julio de 1966, siguió la respuesta, firmada por Juan
Leibar, pero redactada de hecho por Arizmendiarrieta según confesión del firmante.

«Habla Ud. —replica ahora— de “el gran ausente” de nuestra publicación: el hombre vasco. El gran ausente de nuestra sociedad es EL HOMBRE; el hombre libre, con derecho al trabajo, a la igualdad de oportunidades de cultura, de salud, de ocio; el hombre, libre de la tiranía religiosa, de la tiranía del poder y de la tiranía del dinero. En eso estamos empeñados: trabajamos con lealtad, sin utopías, con los pies en la tierra; tendremos nuestros errores y procuramos corregirlos. Su nota puede ser un toque de atención para una revisión de nuestra doctrina»27.

El 31 de julio, nueva carta. Tras atacar duramente el microcentralismo de las capitales de Provincia («Ud. vive en San Sebastián y no tiene nuestros problemas; otros viven en Madrid y no comparten los problemas de nuestras provincias») y defender la necesidad del desarrollo comarcal, pasa a la cuestión de la política:
«Estoy de nuevo con Ud. contra esos slogans que se oyen a menudo entre gente bien acomodada al socaire de una política o entre aquellos que no admiten más que su partido, el partido imperante: “Estamos así muy bien sin política y cada uno a su trabajo”. “No me hable Ud. de política”. “La política para los políticos” y otras salidas peregrinas.

Toda política, si no es puro sentimiento, tiene su programa: nuestra política tiene su programa, su doctrina, sus principios... A mí y a otros muchos les gusta y convence. Me refiero a nuestro cooperativismo, cuya doctrina me parece (dentro del campo social y político) la más perfecta de cuantas conozco. Otros abundan en el mismo parecer y por eso la defienden y propagan a punta de lanza. Téngase en cuenta que los promotores de nuestro cooperativismo viven esta experiencia, habiendo quemado previamente todas sus naves.

Si por política se entiende la intervención directa en las gestiones de gobierno,
Mondragón, tal vez, sea el primer pueblo que ha organizado libremente elecciones de concejales del Ayuntamiento presentando elementos de la oposición; Mondragón ha organizado huelgas y manifestaciones políticas: no hay otro pueblo en Guipúzcoa ni en España entera que tenga tantos jóvenes u hombres maduros sometidos al Tribunal

perativo) puede tener su origen en informaciones tendenciosas o insuficientes, quizá en la dificultad que entraña todo diagnóstico del hecho social vasco en las actuales circunstancias radicalizadas; no ocultaremos que, lamentablemente, en algunas ocasiones parece basarse también en una desorbitada interpretación de datos reales, cuyo contexto y significado parecen desenfocarse (puntuación del euskara para los puestos de trabajo, especialmente directivos; ayudas a las ikastolak y al euskara; escaso número de inmigrantes en puestos directivos, etc.), incluso en graves inexactitudes informativas de base (racismo de apellidos, etc.). Por otra parte, la cuestión del nacionalismo y del socialismo sigue siendo candente, en este caso aplicado al cooperativismo, especialmente para ciertos doctrinarios, para quienes esta oposición, surgida en el siglo XIX en el contexto de los países hegemónicos centroeuropeos, parece haberse convertido en un dogma inamovible, válido para todos los países y circunstancias del mundo. En este estudio el tema no puede recibir sino un tratamiento general, limitado además al pensamiento propio de Arizmendiarrieta.

Carta de Juan Leibar a X.X. (omitimos el nombre), San Sebastián, del 22 de julio de 1966 (Archivo
Arizmendiarrieta).

27

667

El (des)orden establecido de Orden Público: no habrá otro pueblo que tenga tantos desterrados o prófugos políticos»28.

Tal vez por experiencia propia, Arizmendiarrieta estaba resquemado de nacionalismos abstractos, que no saben aportar nada positivo para el hombre concreto lleno de necesidades. «Déme Ud. UN HOMBRE en las condiciones arriba señaladas —escribía en la primera de las cartas— y le daré al
HOMBRE VASCO, por el que todos suspiramos. Y déjeme el adjetivo
“vasco” mientras nuestros hijos no tengan oportunidades de formación, ni puestos de trabajo adecuados, ni libertad de contratación en el trabajo, ni derecho a la propiedad...»
Sin embargo esta polémica parece haber significado efectivamente una llamada de atención hacia aspectos que Arizmendiarrieta tal vez no había atendido suficientemente. Así lo reconoce él mismo, añadiendo: «Nosotros coincidiremos con Ud. en muchos puntos, en lo esencial; tal vez tengamos discrepancias de forma».

A partir de 1968 Arizmendiarrieta empieza de nuevo a escribir periódicamente en euskara en todos los números de la revista TU. Esta pequeña revista, nacida en septiembre de 1960 con el nombre de Cooperación (a ciclostil), para ser distribuida en las cooperativas, y que habrá de cambiar de nombre en julio de 1965 (por estar ya registrada otra publicación con el título anterior), «ha sufrido un secuestro (del N.º de Navidad - 134 de 1971) y dos serias admoniciones, una por escrito y otra telefónica, con amenaza de sanciones y de supresión de la revista»29. El secuestro fue debido a un artículo titulado
Eguberriak eta espetxeak, relativo a los presos políticos, firmado por J.B., que en la Delegación Provincial de Guipúzcoa del Ministerio de Información y Turismo fue juzgado de «naturaleza y alcance subversivos»30. Las citadas admoniciones tuvieron lugar por motivo de dos artículos de Arizmendiarrieta, la primera por presunta apología de la violencia31, la segunda por la utilización de los términos «euskotarrak» y «Euskadi»32. Será en esta revista, y en euskara, donde Arizmendiarrieta expondrá más claramente la significación nacional vasca, que a su juicio corresponde a la experiencia cooperativa33. Dejando de lado, por el momento, aquellos textos, expondremos dicha

Carta de Juan Leibar a X.X. (omitimos el nombre), San Sebastián, del 31 de julio de 1966 (Archivo
Arizmendiarrieta). Como se ha advertido, el autor de la carta era Arizmendiarrieta, aunque la firmara
Leibar como director de la revista TU.

28

29

Hoja inédita titulada Información, de Juan Leibar, noviembre de 1973 (Archivo Arizmendiarrie-

ta).

30

Oficio del 15 de enero de 1972, Nr. 107 (Archivo Arizmendiarrieta).

31

Oficio del 4 de mayo de 1972, Nr. 1305 (Archivo Arizmendiarrieta).

Borrokan ala alkartasunean, TU. Nr. 155, octubre de 1973, Reproducimos íntegro este artículo en el Anexo en euskara, Nr. 1.9.11. En el Archivo Arizmendiarrieta obran informes detallados de ambas admoniciones, así como del secuestro.

32

Sin contar sus escritos y conferencias bilingües, Arizmendiarrieta ha publicado en euskara, entre
1968 y 1976, 57 artículos sobre diversos temas, predominando los temas cooperativos.

33

668

Euskadi

significación valiéndonos preferentemente de textos suyos en lengua española.

1.2. Espíritu cooperativo, espíritu del pueblo vasco
Recordemos, nuevamente, que Arizmendarrieta se halla envuelto en una larga controversia con las nuevas izquierdas. Es en este contraste, y en tonos polémicos, donde ha desarrollado el sentido que a sus ojos corresponde al cooperativismo en relación a los problemas peculiares de la sociedad vasca. No cabe duda de que Arizmendiarrieta ha descubierto y aceptado de los mismos adversarios, que implícita o explícitamente combate, muchos aspectos nuevos que le veremos destacar ahora. Su idea fundamental se mantendrá: la vía más adecuada de emancipación real de los oprimidos no es la de la confrontación, sino la de la cooperación (trabajo y unión), también —y quizá especialmente— en el caso del pueblo trabajador vasco.

Se pueden distinguir en Arizmendiarrieta tres modos, que se corresponden a tres fases de una evolución, de fundamentación del espíritu y movimiento cooperativo. En una primera fase se subraya el fundamento teológico: Dios ha constituido al hombre como cooperador suyo a fin de llevar a cabo la obra de la creación; la cooperación responde a una vocación transcendental humana (FC, I, 24, ss). Podríamos decir que esta visión se seculariza y, en una segunda fase, el hombre es considerado como naturaleza cooperativa, por un lado, y, por otro, la cooperación aparece como exigencia y producto del desarrollo, especialmente de la «herramienta» (en el sentido más amplio: vías de comunicación, etc.): «El desarrollo de la herramienta, que hoy es máquina (...) ha implicado un proceso de asociación, de mancomunación inevitable de esfuerzos, de conjunción de propósitos y planes»
(EP, I, 332). Finalmente, desde los años 60, el espíritu y movimiento cooperativos de Mondragón serán vistos por Arizmendiarrieta como reflejo y encarnación de las virtudes tradicionales y del espíritu del pueblo vasco. Evidentemente no se trata de tres modos mutuamente excluyentes, sino complementarios, en un proceso que va de lo más general a lo más concreto.

Antes de entrar a desarrollar este tema debemos subrayar que el «hombre cooperativo» de Arizmendiarrieta, e.d., su concepto mismo del hombre, tal como ha sido expuesto en el primer libro, llega a identificarse prácticamente con su concepto del hombre vasco, no sólo en sus últimos escritos, sino ya desde los primeros. Una comparación de las virtudes que allá, en los primeros escritos, se exigen y se ensalzan como cualidades del hombre maduro, con las virtudes que en los últimos años se destacan como cualidades propias del hombre vasco, muestra una clara continuidad en los conceptos fundamentales: lealtad, firmeza (FC, IV, 207, 212), «zintzotasuna» (CLP, I, 233), honradez (FC, IV, 71, 128), espíritu de trabajo (Ib. 250), entereza (SS, II,
16), etc. Hemos subrayado que hacen falta ideales, pero que no bastan: que

669

El (des)orden establecido

es preciso que se traduzcan en obras. «Suele decirse: “no basta afirmar que una cosa es buena, que es verdadera, también hay que hacerla”. Pero si acertado es este principio no lo es menos que su aplicación se ajusta especialmente a nuestros hombres y a nuestro pueblo, ya que “convertir la palabra en acción” constituye una de sus más peculiares características. Se trata, en efecto, de un rasgo, de una virtud, con la que nos complace identificarnos, porque nos enaltece» (CLP, I, 225). Hemos tratado de resumir finalmente su concepto del hombre maduro en la expresión «ser, más que tener», slogan muy querido de sus primeros años. Una vez más vemos cómo el hombre de
Arizmendiarrieta coincide siempre, en el fondo, con su idea del hombre vasco: en 1970, definiendo la experiencia cooperativa, describiría a sus hombres como «exponentes del espíritu de un pueblo más propenso a la acción que a la especulación, a ser que a tener, a progresar que a dominar, amante y celoso de su libertad y de sus fueros, de su espacio vital para la autorrealización más pluriforme en el trabajo y, por el trabajo, en provecho común» (CLP, I,

236).

Arizmendiarrieta «confía en las virtudes de nuestros hombres y voluntad de superación de nuestros pueblos, forjados tanto en la lucha tenaz con la naturaleza como en otras contrariedades que hayan podido llegar a su alma. No solamente hemos de recuperarnos, sino también progresar, sin dominar, y para ello hoy, por mano de Caja Laboral Popular, debemos tratar de crear y actuar» (Ib. 208).

Esta evolución se puede observar hasta en detalles y aspectos particulares. Un elemento fundamental del concepto del hombre de Arizmendiarrieta, como se ha visto en capítulo aparte, es el trabajo. El trabajo es, en un primer momento, vocación del hombre, puesto por Dios en el paraíso para trabajar; el trabajo es, luego, medio en que se autorrealiza el hombre, fragua donde se forjan sus virtudes (EP, I, 226). Sin embargo, históricamente el trabajo no ha sido apreciado, ha sido considerado en las diversas culturas más bien que «el trabajo esclaviza el alma, de forma que diera lugar a pensar con el filósofo griego: “Una constitución perfecta jamás hará un ciudadano de un obrero, las peores ciudades serán aquellas en que predominen los obreros”»
(EP, I, 226). No ha sido así entre los vascos: «siempre ha tenido solera en nuestros pueblos el espíritu de trabajo» (CLP, II, 53), destacaba Arizmendiarrieta ya en 1960.

Que esta visión, aunque no pudiera ser expuesta ostentosamente, estaba ya presente de algun modo en el momento mismo de la aparición de las cooperativas, lo muestran las siguientes palabras de Arizmendiarrieta sobre la fundación de su primera empresa cooperativa, Ulgor, en 1956: «La naturaleza, no muy benévola ni generosa, nos ha inducido a utilizar el trabajo para transformarla, y una raza que crece y se multiplica como la nuestra ha hecho del trabajo la base de su promoción económica y social. El roble, que en el pasado ha sido el símbolo de nuestro país y de nuestro régimen de vida social, admite que al presente le representemos con los atributos del trabajo humano para expresar las esencias del pasado con las realidades del presente en la

670

Euskadi

construcción de un orden social evolutivo, como el que trata de impulsar esta
Experiencia Cooperativa» (CLP, III, 157).

En los años 60 las alusiones al espíritu del pueblo vasco, en contexto cooperativo, se multiplican: «Nadie ignora que la iniciativa y capacidad empresarial ha sido la clave de la posición que ocupa en el aspecto económico nuestra región» (CLP, I, 184). O se destacarán la fidelidad y responsabilidad administrativas: «una de las características predominantes de nuestras condiciones étnicas siempre ha sido la buena administración económica del presupuesto de gastos familiar» (FC, III, 109); o la seriedad y honradez, «tan es cierto que aún ahora en Argentina, cuando de realizar transacciones se trata, se rematan algunas operaciones con la expresión palabra de vasco» (Ib.), etc. Pero es sobre todo en las polémicas de los años 70 cuando estos aspectos serán expuestos por Arizmendiarrieta más explícitamente. Nos limitaremos a recoger algunos textos, que no creemos necesiten de ulteriores comentarios.

El cooperativismo es, según nos dice Arizmendiarrieta en 1971, «una experiencia que trata de ser un proceso vital, expansivo, en correspondencia a la inspiración de los valores humanos y sociales de la conciencia activa de los hombres y pueblos de nuestra tierra» (CLP, I, 238). «Esta experiencia corresponde a un nuevo espíritu de confianza en el hombre y en su capacidad.

Revive en este caso el sentido de libertad, dignidad y justicia, fehacientemente acreditadas en las instituciones tradicionales y democráticas de nuestra tierra y, por tanto, exponentes de la idiosincrasia de sus hombres. Una de nuestras características ha sido el sentido práctico, el de saber actuar en el ámbito de las posibilidades sin indiferencia ni renuncia a los ideales (...). Las radicalizaciones contravienen a las cualidades más constantes de nuestro pueblo y a las virtudes humanas y sociales de sus hombres» (Ib. 241-242). «El proverbial sentido práctico que han acusado nuestros antecesores sigue vigente y, tal vez, se vaya acentuando una mayor concienciación de la problemática concreta de nuestra tierra. Es de desear que lo cualitativo en sus aspectos humanos más apetecibles siga obteniendo una mayor atención» (Ib. 273).

1972: «Los cooperativistas debemos sentirnos comprometidos para acreditar las posibilidades de la autogestión, es decir, de los hombres que resuelven libremente y por sí mismos problemas arduos dejando constancia de su elevación de miras como de su proyección más allá de la coyuntura en escala y en niveles que precisa se actúe un país que tiene en su haber en el pasado el haber conseguido un nivel envidiable de desarrollo como de haber sido cuna de una capacidad democrática de gestión en sus instituciones populares»

(FC, IV, 131).

1973: «Nuestra cooperación entraña una proyección polifacética y se apoya en una sociedad pluralista, libre y democrática. Esta es la fuerza que fluye de la idiosincrasia de nuestro pueblo y se anida en lo más entrañable de sus hombres: no se ha de detener su proceso, sino materializarse cada vez más ampliamente en revolución o cambio, en libertad, conduciéndonos a un Comunitarismo democrático, dinámico y eficiente» (Ib. 180).

671

El (des)orden establecido

1974: «La Experiencia, en coherencia con el profundo espíritu democrático de nuestro país, y en búsqueda eficaz de libertad —no pocas veces y bajo diversas modalidades negada o regateada al ciudadano y al pueblo—, ha tratado de buscar y ganar dicha libertad y dicho bienestar por los propios ciudadanos y trabajadores» (Ib. 207).

Los cooperativistas deben considerar su actividad como parte integrante de las numerosas actividades que se llevan o pueden llevar a efecto, según las tradiciones del pueblo vasco, para una promoción integral humana, aunque el Estado absorbente haya apagado la tradicional actividad social vasca. «Si nuestra atención se vuelve a proyectar sobre nuestra tierra se encuentra con que hay otras instituciones que secularmente han sido autogestoras en la promoción y resolución de una extensa gama de problemas comunitarios, como son, sin género de duda, los organismos de administración local, cuya crisis o falta de vitalidad tiene amplias y profundas repercusiones directas en el campo de otras iniciativas y actividades» (CLP, I, 240). Las cooperativas deben colaborar en la revitalización de ese espíritu autogestor.

1.3. Cooperativismo y desarrollo económico de Euskadi
Arizmendiarrieta se mostraba muy crítico en relación con la industria vasca. Esta empezó a desarrollarse entre nosotros muy tempranamente, llena de empuje y energía. En 1832 se construyó, dice, el primer alto horno en Bilbao. Alrededor de la siderurgia surgió gran número de industrias de transformación del acero, siendo la más importante la industria armera, que lleva consigo una gran cantidad de técnica acumulada, bien en el conocimiento del material con sus tratamientos, bien en sus transformaciones. Esta ha sido la base de nuestra industria que, junto con el comercio y gran contacto existente con los mercados occidentales más desarrollados, fue capaz de crear una clase dinámica, propulsora de este desarrollo industrial (FC, IV, 35-36). Este desarrollo continuó hasta los años 30 de nuestro siglo.

A partir de los años 30, especialmente en las décadas de los 40 y 50, se vislumbran unos desarrollos de la producción, no a base de inversiones e innovaciones técnicas en nuestras industrias, sino a costa de importar mano de obra no cualificada. Esto se dió fundamentalmente debido al proteccionismo arancelario y a las estructuras sociales: al no tener competencia los empresarios optaron por no invertir tanto ni en investigación, ni en renovación técnica. Los beneficios estaban asegurados (Ib.).

Pero una industria que no sigue su propia dinámica y agresividad, dice
Arizmendiarrieta, cae rápidamente, como se pudo ver en cuanto el año 1959 se intentó, de la mano de Ullastres, cierto aperturismo. «Cuando nuestra industria entra en contacto con mercados y productos cada cual más agresivos, no es capaz ya de levantar la cabeza, ya que no puede competir ni en calidad

672

Euskadi

ni en precios. Nuestra industria se declara antirrentable, tambaleante e insostenible. Le falta técnica y le sobra mano de obra» (Ib. 37). A este primer golpe viene a añadirse el hecho de que «en esta época se ha frenado todo tipo de alicientes en nuestro país para la inversión de capital, creando fuera de aquí zonas privilegiadas por el fisco y con grandes facilidades para una mayor rentabilidad, aparte de las trabas creadas a nuestros industriales cuando han querido hacer ampliaciones y mejoras en nuestra infraestructura. Ahí están todos los Planes de Desarrollo que no han tenido en cuenta ni en lo más mínimo las condiciones naturales de nuestro país. No obstante todavía nos quieren demostrar que somos una de las zonas más privilegiadas, a base de dar las cifras de la renta per cápita, no dándonos cuenta de que lo importante hoy es saber el desarrollo de nuestro índice de crecimiento. Así podemos decir que en los últimos años ha ido decreciendo a marchas alarmantes» (Ib. 37).

Por todo ello, ya en 1971 Arizmendiarrieta advierte el grave estado de la industria vasca: de país industrial, dice, estamos pasando a un subdesarrollo agudo. Un país en desarrollo importa técnicos e investigadores, no mano de obra barata, no cualificada; realiza grandes inversiones en investigación, creación y desarrollo de empresas punta; exporta técnica; crea las bases de una economía consistente y autónoma. Un país subdesarrollado, por el contrario, compra técnica; presta muy poca o ninguna atención a las inversiones, prefiriendo el desarrollo cuantitativo, con introducción masiva de mano de obra no cualificada en las industrias, etc. etc. Arizmendiarrieta detalla defectos concretos de la empresa y de la gestión empresarial (Ib. 38-39), sin que ahora podamos detenernos en ello. Y añade que las entidades bancarias y de ahorro enclavadas aquí, salvo Caja Laboral Popular, realizan sus inversiones, al menos en un tanto por ciento muy elevado, fuera de aquí, lo que supone claramente una constante descapitalización del País (Ib. 39).

A nosotros no nos interesan tanto sus análisis críticos del estado de la industria vasca (Ib. 45 s~., etc.), como la función que frente al mismo cree que le corresponde al movimiento cooperativo, o sea, cómo inserta Arizmendiarrieta el movimiento cooperativo en el entorno económico de Euskadi.

En 1969 Arizmendiarrieta destacaba ya el alcance social y político del hecho cooperativo. «Caja Laboral Popular trata de salir al paso de los riesgos de debilitamiento de nuestro pueblo, tratando de potenciarlo por la vía más directa, el ahorro y la inversión, y el método más eficiente, por la mancomunación de planes y actividades y la solidaridad de los hombres» (CLP, I,

205).

Por lo demás, en un período de descapitalización, de fuga de capitales y de empresarios hacia regiones más favorecidas por los Planes de Desarrollo,
Arizmendiarrieta cree llegada la hora de que los trabajadores se decidan a llenar ellos mismos los huecos que van dejando los empresarios «trashumantes en busca de lucro» (FC, III, 137). En este sentido el cooperativismo ofrece una alternativa a las vías de desarrollo hasta hoy habituales: en lugar de patrono/empresario debe alzarse ahora el obrero/empresario.

673

El (des)orden establecido

«Nuesto País, obligado a vivir y desarrollarse por lo que hoy somos capaces de crear sus hijos aquí mismo, sin reservas de “América” y “americanos” vueltos con “bolsitas”, necesita ensayar nuevos métodos de promoción. O
¿es que las fuerzas creadoras, las masas de trabajadores, están resignadas a contribuir al desarrollo como en el pasado?» (Ib. 256). El cooperativismo ofrece, pues, en medio del receso, un modelo que permita emprender un nuevo desarrollo pujante. Y Arizmendiarrieta confía en que los trabajadores se decidirán por esta vía, porque «en nuestra región, que se ha caracterizado por espíritu de iniciativa y de cooperación humana efectiva en el pasado, en el presente dicho espíritu ha cobrado otra índole y categoría» (Ib. 270). «El amor al país se ha de demostrar por lo que todos fuéramos capaces de hacer sin necesidad de esperar manás o soluciones de americanitos, aunque estos fueran indígenas» (CLP, III, 221).

Por otra parte la solidaridad regional exige del movimiento cooperativo la expansión por toda la región vasca en crisis, la mancomunación y coordinación de empresas cooperativas. «¿Por qué las cooperativas punteras y con capacidad endógena (...) no extienden su capacidad creadora por la región, dando vida y nacimiento a nuevas entidades técnicas que absorban fuerza de trabajo disponible y movilicen los nuevos potenciales sofocados en su desarrollo? Este es el desafío que la región lanza a las empresas situadas en unos centros definidos, a las que sin negarles virtud y mérito por su pujanza, les coloca en la encrucijada de mostrarse solidarios» (FC, III, 135; cfr. FC, IV,

161-163).

«Si estudiamos el desarrollo en los últimos años de las fuerzas productivas de nuestro país, históricamente comprobamos (y los datos técnicos y económicos lo confirman), que vamos hacia una regresión técnica y económica y no precisamente por propia voluntad. Carecemos de industrias punta, de suficientes centros universitarios, vías de comunicación que correspondan a nuestras necesidades, se dificulta el desarrollo de nuestra mediana y pequeña empresa, industria, etc. Sin embargo, como minoría consciente (aunque no se puede precisar en qué grado), estamos presentes los cooperativistas con nuestras industrias, nuestra entidad de crédito y nuestros problemas queriendo hacer frente con todo ello a esta situación en la medida de nuestras posibilidades.

El camino es largo, duro y con muchas penosidades. Los problemas a los que hemos de hacer frente son tanto internos como externos. Estos últimos son conocidos por todos. Los primeros, pueden ser consecuencia de una deficiente dirección técnica, por ausencia de verdaderos directivos capaces de llevar a cabo nuestra empresa o tarea,-y también por las clásicas luchas internas generalmente motivadas por grupos anárquicos y seudorevolucionarios, que ante situaciones como esta no saben responder sino poniendo en peligro todo planteamiento serio y coherente, ya que su incapacidad les convierte en un grupo más de reaccionarios. No ofrecen más que sentimentalismos y paternalismos que nos conducen o quieren conducir a la era de nuestros antepasados los pastores y agricultores baserritarras y, si no quedan conformes, volveremos todos a las cavernas tan buenas e ilustrativas de nuestro país. Es más popular esta mentalidad que la de ser unos verdaderos empresarios agresivos, eficaces y con una conciencia clara que es lo que necesita nuestro país para hacer frente a esta situación económica y social tan deplorable que estamos padeciendo.

El sistema cooperativista tiene que ofrecer, si quiere ser dinámico y objetivo, una alternativa a la situación económica actual de nuestro pueblo. Esto lo conseguirá en

674

Euskadi parte, pasando de un aumento cuantitativo de empresas asociadas a una cualificación de ellas. Pero estos cambios no vendrán como el maná, sino después de hacer unos estudios económicos y sociales de acuerdo con nuestra realidad, disponer de suficiente capital para realizar las inversiones, etc. etc.

Tiene que ser un trabajo basado en la unión coordinada de todo el sistema cooperativo y aprovechando al máximo todo el capital o fuente que proceda del exterior si es que existe. Para esta labor tenemos en un lugar privilegiado a nuestra entidad de crédito, Caja Laboral Popular, cuya misión en estos momentos es sacar la mayor rentabilidad al ahorro, prestar colaboración a las empresas asociadas, realizar estudios comarcales de expansión, etc. Esto exige el tener que aglutinar en sus filas gente estudiosa capaz de llevar a cabo empresas que anárquicamente sería imposible realizar.

Una exigencia más para llevar a cabo nuestro objetivo es que saneemos las empresas actuales dejando de mantener, como lo hacemos actualmente, empresas en ‘estado de coma’ que viven a base de suero y que no hacen sino distraer todas las atenciones hacia ellas malgastando cantidad de fuerzas a un coste muy elevado. Mal nos vamos a ver si no solucionamos esta epidemia crónica en el país y que también acecha a las empresas cooperativistas» (FC, IV, 45-46).

El cooperativismo debe mostrar madurez, superando sus propias barreras, asumiendo su parte de responsabilidad en el futuro de todo el país, sin caer en radicalismos que ningún bien reportan y con realismo. «Las Cooperativas no nacen para actuar de guerrilleros sociales ni para deteriorarse como reductos burgueses, sino para mantener vivos y operantes valores humanos y sociales en el seno de un pueblo viejo y con solera de resistencia y capacidad renovadora, digno de mejor suerte» (CLP, I, 279).

Este es el servicio que el movimiento cooperativo presta a la patria (término raramente empleado por Arizmendiarrieta, que prefiere valerse de términos como «tierra», «País», en el mismo sentido; en euskara «lur» y «erri»).

Porque ¿qué es la patria? «Para unos tiende a ser fundamentalmente condensación del pasado y otros, más bien que tierra de los padres, piensan que hay que aceptarla y promoverla como la tierra de los hijos» (FC, IV, 251). Arizmendiarrieta se inclina por la segunda visión, según la cual, más que vivero de tradiciones, la patria es plataforma de acciones promotoras y previsoras; más que un pedazo de territorio, los hombres que lo pueblan y han de vivir en el mismo (Ib. 278). Los cooperativistas, más que al pasado, deben mirar hacia el futuro, partiendo de la idea de que estamos en marcha hacia la tierra de promisión. «Se nos impone ser forjadores del futuro» (Ib. 251).

1.4. La educación al servicio de las virtudes étnicas
En 1961, ante la necesidad de ampliar la Escuela Profesional, que no daba abasto a todas las solicitudes por una parte, y la penuria de medios crónica, por otra, Arizmendiarrieta piensa en una solución que parece bastante aventurada: «Los vascos tenemos muchas simpatías y buenas relaciones con
Venezuela, donde hay muchos paisanos nuestros y creemos que sería fácil

675

El (des)orden establecido

disponer anualmente de unos cuantos que se dispusieran a marcharse a Venezuela (...). La entidad patrocinadora Liga de Educación y Cultura y, por tanto, la Escuela Profesional, aceptarían con gusto la participación de la Corporación Venezolana de Fomento de Guayana, considerándola como una entidad patrocinadora equiparada para todos los efectos sociales y económicos a las demás y disponiéndose a promocionar para la misma anualmente unos cuantos alumnos. No había de constituir ninguna dificultad contar anualmente con unos 10 alumnos que se comprometieran a marcharse a Venezuela con buena formación humana, social y técnica» (EP, II, 169-170). Citamos el proyecto, que al parecer no llegó nunca a realizarse, como una muestra más de la energía y decisión con que Arizmendiarrieta estaba decidido a llevar adelante su programa de desarrollo de educación profesional, dispuesto para ello a recurrir a cualquier medio.

Por los mismos años Arizmendiarrieta exigirá repetidamente la descentralización de la enseñanza y reiterará la necesidad de que la iniciativa ciudadana asuma este quehacer, tanto en relación a la enseñanza profesional como, sobre todo, de la enseñanza primaria. «No es una suposición gratuita que la enseñanza primaria, que es la base para todo, va a requerir en nuestra región la atención de las fuerzas vivas para que pudieran atenuarse las dificultades de una administración central y unitaria frente a las diferencias regionales económicas y sociales difícilmente superables» (FC, I, 259) En este mismo artículo se da a conocer el proyecto de creación de un hogar infantil, donde se pudiera cultivar el euskara, el folklore vasco, etc. La enseñanza debe estar al servicio de las necesidades de un pueblo, y también de sus características y virtudes peculiares (Ib. 258), que vale tanto como decir «al servicio de la comunidad y de la región como de sus valores» (EP, II, 190).

Arizmendiarrieta considera de máxima relevancia la educación profesional. Conociendo el influjo mítico de títulos más sonoros, invita a padres y educadores a tomar conciencia del valor del trabajo, para que «nuestros centros de formación consoliden nuestras mejores virtudes raciales y sociales y no caigamos en innecesarios riesgos de frustraciones y esfuerzos poco eficientes. Cara a cada uno de los aspirantes a promoción cultural o técnica o simplemente laboral, nuestro actual nivel de desarrollo permite que demos preferencia o desde luego, prestemos atención prevalente a las aptitudes e idoneidad de nuestros jóvenes, así como al cultivo y al mantenimiento de un auténtico espíritu de trabajo y de solidaridad entre los mismos» (Ib. 68).

Alguna vez critica Arizmendiarrieta la pedagogía clásica de ser más mantenedora que promotora (Ib. 259). «Todo lo que sea iniciativa, responsabilidad, creatividad, superación, solidarización, etc., conlleva una dinámica organizativa y promotora o de adaptación a cuyos requisitos no puede dejar sin respuesta la Pedagogía acreedora a la calificación de humana y actual»
(Ib). En este sentido Arizmendiarrieta ha considerado no pocas veces la cooperación como una escuela, como una pedagogía, que promociona y apresta a la clase trabajadora para un futuro a la medida del hombre. Igualmente puede ser considerada la cooperación como una escuela del pueblo vasco:

676

Euskadi

«Precisamente la Cooperación bien interpretada nos convoca a la concertación de nuestros esfuerzos y a la superación de las realidades presentes en todos los campos de presencia humana, sin desvincular el trabajo y la gestión, el ahorro y la inversión, el desarrollo y la transformación, el mejoramiento personal y comunitario, el campo y la industria, el descanso y el ocio, para corresponder a lo heredado o recibido más o menos gratuitamente con nuestro esfuerzo, para ensamblar el presente en el futuro, apoyándolo en la solera de nuestra tierra y transformarla en Pueblo o País revitalizado» (Ib.).

La idea de la educación permanente, tan firmemente arraigada en la mente de Arizmendiarrieta, se encuentra también enlazada al propósito de la supervivencia del pueblo vasco como comunidad y comunidad libre, dueña de sí misma. Porque esta no puede ser una labor de minorías, sino que debe constituir el quehacer de todos. «Nuestra época es también aquella en la que nuestra conciencia social nos exige y promete más. Este compromiso con nuestro ámbito de convivencia, que ha de ser racional y consciente, nos tiene que llevar a crear un ‘nosotros’ comunitario y solidario, que en primer lugar nos haga ver el derroche de energías, ocios y tiempos libres, a fin de concienciarnos en la necesidad de organizar diversos tipos de actividades fuera del trabajo que formen y eduquen a nosotros y a los que nos rodean, con el claro objetivo final de hacernos con una personalidad propia que permita la supervivencia de nuestro pueblo como tal. Y todo esto no lo pueden hacer solamente uno o dos más o menos inquietos, sino que es labor de todos los que creen en el progreso de la humanidad y quieren lo mejor para nuestro pueblo. Aquellos sólo podrán ejercer de orientadores y quizás el evitar pasos en falso. Hemos pues de preparar y buscar los medios más adecuados para que nuestra comunidad lo sea de hombres libres y autogobernantes. Acordémonos de que no hay casualidades sino consecuencias» (Ib. 151).

1.5. Solidaridad e interdependencia
El interés y el esfuerzo por el desarrollo del propio pueblo vasco no deben hacernos olvidar las exigencias de la solidaridad con zonas más pobres o atrasadas (FC, I, 283, 330 ss.), ni las relaciones de interdependencia que imperan en el mundo moderno. Las nuevas actitudes hacia afuera que se imponen en el mundo de hoy exigen nuevos comportamientos en casa.

«Nuestra región se halla en un enclave magnífico si sabemos actuar en la escala idónea para acomodarnos a las nuevas áreas y para ello no sufrir desaliento en el esfuerzo, —escribe Arizmendiarrieta con los ojos puestos en el
Mercado Común. De ello deben tener conciencia no solamente las fuerzas directamente comprometidas en las tareas concretas, sino todo el país, toda la comunidad, si no queremos resignarnos a quedar desplazados o caer en colonialismos molestos» (CLP, III, 221).

No es difícil percatarse de la limitación de nuestras posibilidades, aun aprovechando óptimamente todos los recursos. Cuando nos percatamos de

677

El (des)orden establecido

las movilizaciones multinacionales de las nuevas iniciativas empresariales y de la necesidad de poder concentrar y aplicar cuantiosos recursos, tanto humanos como económicos, en los entes necesarios para un desarrollo en línea con las exigencias actuales, con el mercado y con la aceleración del tiempo, no podemos menos de pensar en revitalizar directamente, o previas transformaciones, nuestros mecanismos de desarrollo. «Si la tarea que nos impone la coyuntura o la vida es ardua y difícil, ello debe ser razón para que quienes tomaren en boca el bien y la promoción del país tuvieran presentes todas sus fuerzas, todas sus reservas» (Ib.).

Ya no puede entenderse la libertad como aislamiento, que equivaldría a la ruina. En Euskadi son muy fuertes los anhelos de libertad, pero es preciso que nos preguntemos cómo debemos entenderla y realizarla. «Quienes admiran y celebran las gestas de los que supieron morir por la libertad, no deben dejar de reflexionar, si al menos todos tratamos de ser acreedores y dignos de disfrutarla por mucho que ello nos cueste» (CLP, I, 274).

«Estamos obligados a tener que ser un pueblo de trabajadores, pero también de mercaderes. Hay que contar con mercados para adquirir mercancías y para vender otras. Es decir, que el intercambio es vital en nuestra condición y el intercambio lleva aparejada una dependencia. Esta dependencia hemos de hacerla viable a través del intercambio de nuestros productos, que cuanto más apetecibles sean para otros por su calidad y demás condiciones de adquisición, será más viable una interdependencia entre iguales o amigos (...). Para seguir disfrutando de bienestar, como para ser libres, hemos de disponernos a trabajar mejor, en condiciones humanas y sociales mejores y con productos y excedentes más universalmente apetecibles por su calidad o idoneidad para promoción de todos sus destinatarios»
(Ib. 275).

La toma de conciencia de necesidades vitales presentes y futuras de nuestro país, escaso en materias primas o recursos naturales, denso en población, nos lleva a hacernos cargo de la necesidad de tener que apoyar nuestros soportes existenciales en el trabajo de transformación de materias primas en mercancías. Para disponer de las primeras, lo mismo que para disponer de aplicación o destino de las segundas, precisamos ser conscientes y responsables de los imperativos de convivencia y desenvolvimiento en interdependencia mutua. «Hemos de disponernos resueltamente a capacitarnos para trabajar cada vez mejor, de forma que nuestros productos o mercancías lleven en su calidad y funcionalidad la impronta de nuestro genio, no menos que de nuestro sentido de justicia para el intercambio, que evidencie la equivalencia de lo que ofrecemos y lo que pretendemos recibir, sin tener que recurrir a otras tretas o artes que no otorguen nuevo valor a las mercancías, y sí envilecimiento a nuestro espíritu de equidad, justicia y libertad» (FC, IV, 216).

Mientras no estemos dispuestos a actuar de este modo «deberemos considerarnos más como simples trovadores de la libertad que en posesión de las efectivas opciones que la misma libertad exige» (CLP, I, 275).

678

Euskadi
«Somos un pueblo en el que es más lo que queda por hacer que lo que está hecho; somos un pueblo joven y pletórico que debe percatarse de que las condiciones en las cuales ha de tener que confrontarse con otros colectivos no son precisamente las que pudiéramos prefijarlas nosotros, sino las que en el seno de un mundo que rompe con tantas ataduras del pasado y cobra conciencia de sus nuevas posibilidades han de derivarse del protagonismo que en la propia conducción como la gestión de los respectivos asuntos han de tratar de ejercer.

Pocos colectivos, como el nuestro, asentados en espacios no pródigos en materias primas, necesitan apelar y apoyarse en la materia gris, en el desarrollo de su creatividad como de sus relaciones con la periferia. No estamos como para tratar de marchar hacia adelante con aires de desafío y menos aún de fuerza entendida en su sentido más elemental y universal, so pena de que por fuerza vayamos a interpretar nuestra competencia, nuestra honestidad.

En resumen, hemos de aceptar vivir y desenvolvernos sin dramas ni actos heróicos, con el trabajo, con la cultura, hacia los que polarizamos nuestro cerebro y nuestro corazón, en cuyas exigencias incluímos la ilusión por el bienestar de todos y cantamos para ello con la implicación y responsabilidad de todos, de forma que la democracia siga siendo efectiva y expansiva en todos los campos de nuestra actividad y de nuestra relación y convivencia. Así hemos de consagrar nuestro amor a la libertad» (FC, IV, 252-253).

1.6. Por el trabajo a la libertad

En 1965 Arizmendiarrieta veía al pueblo vasco cargado de potencialidades, necesitando sólo que alguien señalara el camino, para marchar decididamente por la vía de su promoción. «Los pueblos de nuestra región, con alto potencial de trabajo, con fuerte sentido asociativo, con el no pequeño sentido común y práctico que caracteriza al pueblo vasco y con una fecunda riqueza de pequeñas y grandes instituciones comunitarias con los objetos sociales más diversos, son comunidades que pueden entender perfectamente esta convocatoria hacia la promoción. Sin embargo, hasta ahora, no se ha dado cauce natural oportuno a toda esta riqueza y valor potencial de nuestro pueblo, porque no se ha sabido interpretarlo o, si se quiere, no se ha sabido darle una expresión y materialización definida, traducible en instituciones o entidades concretas alrededor de las cuales galvanizar un esfuerzo, justificar una dedicación» (CLP, I, 118). La fuerza del pueblo vasco reside en el trabajo. El ha transformado el país y él debe construir el futuro. «Nos hace falta poca capacidad de análisis para aceptar que en cuanto a lo que nos hace más entrañable nuestra tierra, nuestra región, como más apetecible y llevadera la relación y la convivencia humana con el lubrificante de un bienestar, se lo debemos a la capacidad de trabajo de nuestros predecesores y conciudadanos» (EP, II, 107).

No con gritos, ni con violencias, sino con trabajo y unión se construirá la libertad: «Lanean edo lanerako ondo ornitzen ditugun gizonak dira gure erria askatu ta jasoko daben gudariak. Gizonen eta erriaren askatasun-egarri

679

El (des)orden establecido

ez gara, gizonki jokatzeko ta beti zintzotasunean bizitzeko eskubide orixe bear dogulako baño» (Ib. 234).

«Precisamos34 de la revolución basada en el trabajo y no en los mitos; hemos de conseguir la unión, apoyada en la verdad y nunca en la mentira, la hipocresía y el error (...). En el pasado hemos sido poco dados a cruzadas y mitos, que a otros han podido servirles para reactivar fuerzas latentes; en la presente superexcitación y simplificación de los espíritus, algo de ello ha podido tener cierta acogida entre nosotros. Pero hoy, viendo claro, decimos que “si la revolución es para mañana, esta revolución no será la nuestra”. No nos inquietemos por esto y trabajemos intensamente, tanto para la maduración de los problemas como por realizaciones que, en todo caso, precisaremos. —Si la revolución, tal como la hemos entendido, es para más adelante, no será difícil que nuestro trabajo de profundización y movilización madure oportunamente en la fuerza concreta que nuestro pueblo precise y demande—. Caja Laboral Popular encarna la divisa, ya consagrada, de ‘Trabajo y
Unión’, tal como la requiere hoy nuestro pueblo, conduciendo y animando un nuevo proceso de desarrollo, con todo lo que este término significa para la sana filosofía social personalista y comunitaria» (Ib. 223-224).

Arizmendiarrieta se opone radicalmente a todo tipo de soluciones utópicas, que considera peligrosas para el futuro de nuestro pueblo, ya suficientemente castigado. «Por si fueran pocas las veleidades y frustraciones que registra la historia de nuestro pueblo, un elemental sentido práctico nos impulsa a cambiar nosotros lo que efectivamente podemos cambiarlo; y sobre todo cambiar aquello que transformado pudiera sernos punto de apoyo para ulteriores evoluciones de toda índole» (FC, IV, 112). El camino de la libertad es el camino del trabajo, que ha sido el fundamento histórico de la personalidad de nuestro pueblo: «Las fuerzas vivas que, efectivamente, son manantial de renovación y energía, son las constituídas en la órbita del trabajo. Nuestro pueblo es consciente de que su nivel de bienestar y fuerza ha procedido del potencial de trabajo de sus hijos. Estas reservas y contingentes de trabajo han sido los ejércitos con los que hemos promovido nuestra personalidad histórica y más propiamente se nos conoce en el mundo» (Ib).

Arizmendiarrieta recalca una y otra vez el argumento de que estamos en una tierra deficiente en cuanto a recursos naturales, forzada a vivir y a fortalecerse con el trabajo de sus hombres, de sus productos industriales. «Debemos entender que lo fundamental es impulsar, apoyados por el trabajo comprometido, el proceso que se expresa mejor mediante obras que por retórica.

34

Las reflexiones que siguen reproducen, en buena medida literalmente, textos de Mounier, especialmente de su artículo titulado Revolución contra los mitos, de marzo de 1934. Para Mounier, como para
Arizmendiarrieta, la hipótesis de que la revolución pudiera «no suceder» carece de sentido; el único problema pleno de sentido es cómo esta será, no si será. La única revolución aceptable para estos personalistas es la de ahora mismo, no la «de mañana», la que siguiera a una tranformación de las estructuras. «La auténtica revolución, escribe por otra parte Mounier, la revolución de los pobres, basada en el trabajo, contra los poderosos, debe ser una revolución contra los mitos», cfr. GOGUEL, F.-DOMENACH, J.M., Pensamiento político de Mounier, Zyx, Madrid 1966, 30-40.

680

Euskadi

En solidaridad progresiva domésticamente madura y proyectable colectiva o comunitariamente hacia objetivos y realizaciones que potencian al pueblo, resuelto a protagonizar por sus fuerzas y medios su desarrollo y evolución sin hipotecas de ningún género» (Ib. 199).

El cooperativismo ha contraído un compromiso ineludible de contribuir a la creación de una nueva sociedad, promoviendo las transformaciones socio-económicas que entrañaren nuevas formas de sociedad y convivencia, con las consiguientes mejores oportunidades de educación, de salud, de trabajo y descanso, cuya consecución acredita el interés y la actualidad de un movimiento social.

Existe evidentemente el peligro del consumismo, de la opulencia individual. La promoción del trabajo puede entonces acabar copiando gustos y modas de la burguesía. «Pero, por otra parte, a otros contingentes nos seduce y atrae hacernos eco y corresponder a convocatorias de cambio mediante reacciones individualistas y como tales ineficaces, o tal vez con simples gestos circunstanciales o coyunturales. Desde luego sin proceder a más profundo examen y ponderación de la índole y alcance de los propios actos. Sin toma de conciencia y snobismo revolucionario. Las contradicciones en las que se incurre delatan por sí la bondad o el acierto de no pocas afirmaciones revolucionarias que para ser efectivas deben sustentarse en un auténtico compromiso que nos conduzca a algo más que a cambios superficiales o de indumentaria» (Ib. 198). El cooperativismo debe saber evitar ambos peligros. Debe ser lo que es, que con ello es ya una aportación al cambio, manteniéndose fiel a sus ideales.

«Si nos mantenemos en nuestras posiciones colectivistas dentro de lo posible o viable es porque tratamos de que nuestra realidad cooperativa fuera más que artículo de escaparate o tópico medianamente camuflado de subsidios de idealismos irrealizables sin fuerza y sin tiempo. Por eso insistimos en que nuestra experiencia es una contribución efectiva al cambio y nos remitimos a lo que a los ojos de todos se cambia donde sabemos actuar con las armas del trabajo, la unión y la mejora constante en nuestra periferia, bajo diversas modalidades de desarrollo educativo, cultural, económico, asistencial, etc., superando todo lo que en las fuerzas del trabajo pudiera significar discriminación y barrera, pero no con simples gestos episódicos o folklóricos, sino mediante todo aquello que es susceptible de convertirse en fuerza y apoyo para un futuro mejor a promover con arreglo a las condiciones de infraestructura económica, social o condicionamientos históricos del País» (Ib.

199).

«Hay que afrontar realidades más que hipótesis y reflexiones sobre datos y hechos concretos más que sobre puras formulaciones ideológicas.

No ha quedado tan lejos un pasado de nuestro pueblo no desprovisto de lecciones como de problemas de la más variada índole para cuantos hemos nacido en esta tierra. Cuando uno tiene que escuchar algo cuya evidencia es difícil de negar como que
“los que olvidan el pasado están condenados a repetirlo” no puede menos de tratar de “pensar y reflexionar” por sí mismo, sin vivir a expensas de otros y, menos aún, manipulado por otros.

681

El (des)orden establecido
Hoy entre nosotros, pueblo zurrado y alertado, cabe pensar que el problema no es si cambiará, si serán reemplazadas las formas de sociedad que dejan insatisfecho al hombre, sino el de si este cambio se realizará al margen del hombre, de sus hombres, o al azar, sin dirección humana, sin racionalidad, sin peso específico de nuestros intereses comunitarios.

La educación y la actividad cooperativa, por el hecho de cimentarse en una implicación polivalente y participación efectiva en la gestión y desarrollo, constituye un factor singularmente esperanzador para la promoción y orientación de cuanto afecta a la vida y suerte de nuestro pueblo.

No nos aferramos al pasado, pero tampoco especulamos con el futuro por la simple sonoridad de consignas brillantes o formulaciones prometedoras en tanto no descubramos que nuestro pueblo no es un colectivo con tan afortunado enclave o pródiga naturaleza que pudiera confiar en que otros factores extraños o ajenos y no el trabajo, el compromiso, la organización de las propias fuerzas le han de servir en definitiva. En su más profunda esencia el cooperativismo encarnado entre nosotros ¿no es acaso una amplia convocatoria para el trabajo, un esfuerzo para optimar su regulación y prestación, que de hecho se convierte en prosperidad y bienestar de los pueblos? Indudablemente tiene defectos, ni tampoco es la solución universal de todos los problemas.

Actuar instintivamente u obrar a lo loco, como encomendarse a simples promesas de futuro sin un presente que avale o garantice en alguna medida los propósitos, no es correcto, no es síntoma de madurez de nadie. Un futuro, por muchas ilusiones con las que se quiera revestirlo, no nos basta sin un presente de responsabilidades y de acción previsora, a los que dejamos de creer en los reyes magos» (FC, IV, 156-157).

En última instancia no se trata de conquistar la libertad; la libertad no es un fin en sí mismo, sino más bien el medio en el que el hombre, por el trabajo, aspira a fines más altos. «Trataremos no tanto de luchar para ser libres, sino de llevar a cabo nuestro empeño siendo libres en la medida que pudiéramos ser agrupados en el trabajo, que es el único procedimiento para crear algo» (FC, IV, 259). A este efecto «la empresa comunitaria es una institución elemental para que nuestra realización personal y social se encamine por derroteros oxigenados por la libertad hacia otros objetivos más ambiciosos y más amplios que nos han de poder tutelar en el disfrute de dicha libertad»
(Ib.).

1.7. Por la unión a la libertad
En sus últimos años Arizmendiarrieta ha asumido plenamente los símbolos (roble, etc.) y terminología del nacionalismo vasco, incluso los neologismos más recientes («iraultza») creados dentro de este movimiento. Pero su concepción del trabajo y de la unión como elementos base de toda vida social supone un concepto nacional bien distinto del que un día el joven Arizmendiarrieta había conocido y defendido. Así lo reconoce expresamente al declarar que el nacionalismo vasco necesita de características nuevas: «Gure euskotasun eta abertzaletasunak ezaugarri barriak bear ditu eta jokabide barrietara bultzatu bear gaitue» (CLP, I, 291).

682

Euskadi

Estos nuevos modos de comportamiento exigidos por Arizmendiarrieta pueden resumirse en los dos conceptos de trabajo y unión; la libertad que se anhela no es solamente nacional o política, es la plena libertad social de una comunidad de trabajadores característica. «Gure lurraldean indarkeri, jauntxokeri, ezberdintasun eta aurrerapide eragozpen kutsurik dan artean larri ta ernai bizi ta tinko jokatu bearra daukagunok goraka ta bide barri billa ibilli bearra daukagu» (Ib. 262). El movimiento cooperativo constituye, a los ojos de Arizmendiarrieta, como un pequeño núcleo, en torno al cual debe congregarse el pueblo vasco, la clase trabajadora, para marchar juntos hacia la libertad. «Taldetsu diardugu, baña alkartasunean ardaztu ta indarbarritu al gengikezanak asko ta asko dira gure inguruetan» (Ib.). La unión es la tarea más urgente que se impone. Es, además, lo que nosotros mismos podemos y debemos hacer, sin esperar que nos lleguen soluciones desde fuera.

«Zer da oraingoz gure errian dagon zirinik mingarriena? Zer da geuk geure gogoz eta eskuartez lenen egin geinken aldeketarik bizigarri ta indargarriena? Indargeturik gagoz batasunik ez dogulako ta batasunik ezin izan gengike ditugun “desbardintasunak” ditugun artean gure errian. Askatasuna ongarri izan al dagigun zuzentasun ta bardintasun mardulagoa bear dogu, ta lanbideak pozgarriago ta eruangarriago izan al dadizan alkartasun sendoagoa.

Demokrazi jokabideak gure errian sustraitsuak izan dira ta barriztu ta indartu bearrekoak ditugu. Ba dira saillak demokrazi jokabidez, geurenez, iñori begira egoteke, sortuarazo geinkezanak. Zuzendaritza ta agintaritza barriak sortuarazo dagiguzan al dogun neurrian, aldaketa mamintsuago ta zaillagoak sorrarazi dagiguzan al danik lenen. Indarkeririk gabe, demokrazi-bidez sorrarazitako aldaketak izaten dira indarkeri gabe irauten dabenak. Auxe izango da gaurkorako gure Eusko Indarra; guri jagokun eta guk bear dogun iraultza egiteko bidea, ekonomi-arlo ta gizartearazoetan bear dogun eraberritzea. Indarkeriz egiña geienetan indarkeriz gorde edo eutsi bearrekoa izan oi da.

Mundu zabalean euskotarron ezaupiderik entzutetsuena azkatasun-miña da; orain zuzentasun-zaletasunez guritu dagigun eta Lanak eta Alkartasunak ekarriko dauskue geure errien aurrerapena» (CLP, I, 248-249).

Al movimiento cooperativo, como escuela de unión y solidaridad en vías de promoción personal, le corresponde un gran papel también en el proceso de liberación del pueblo vasco. Los fundamentos sobre los que aquel se basa deben ser igualmente los fundamentos sobre los que ha de ser posible la libertad del pueblo vasco, e.d., el trabajo, la unión. «Lo que va con el hombre va con el pueblo» (FC, IV, 113).

La concienciación que este proceso supone de base, ya que implica la participación responsable de todos y cada uno, hace que sea árdua la tarea. Por otra parte otros procedimientos o métodos que prometen libertad pueden tener fácilmente más audiencia, porque el aspecto liberador del movimiento cooperativo no es suficientemente conocido. «No desconocemos que nutridos contingentes, tal vez por falta de una concienciación, cuyos valores prácticamente han sido ignorados o al menos preteridos por los medios de comunicación o la acción educativa en curso, permanecen un tanto impasibles o extraños a fórmulas que pudiéramos calificar de indígenas o concordes con

683

El (des)orden establecido

nuestro secular espíritu democrático, liberal y práctico a la vez que comunitario y dinámico. De momento son los productos de importación los que tienen audiencia más amplia. Tal vez ocurra que algunos contingentes por inexperiencia, superficialidad o snobismo hayan optado por el mercantilismo aparentemente más universal» (Ib.). Sin embargo Arizmendiarrieta está convencido de que el pueblo vasco reconocerá el alto valor de este camino y lo seguirá. «La afirmación que hacemos de que el pueblo va con la cooperativa corresponde tanto a la toma de conciencia de las virtudes más constantes y características de nuestro pueblo como a la observación de la simbiosis existente entre el contingente más significativo y representativo del pueblo y las iniciativas cooperativas que tienen óptima acogida en el seno de nuestras comunidades locales» (Ib. 114).

«Alkartasun bidez azkatasun alde» es el significativo título de un artículo de Arizmendiarrieta (1971) que indica suficientemente su posición. «Zuzen jokatzeko alkartzen gara ta Alkartasun bidez guaz Azkatasun alde» (CLP, I,
234), etc. No hay otro camino de libertad que la unión. Pero la unión de que nos habla Arizmendiarrieta no es una unión meramente táctica: es un principio básico de vida social, simultáneamente presupuesto y consecuencia del trabajo, especialmente del trabajo cooperativo. Es la unión en el trabajo, sin olvidar nunca que este debe ser entendido, según Arizmendiarrieta, como base de vida social y desarrollo.

En este sentido, pues, el cooperativismo «puede y debe ser un aglutinante para aunar las buenas voluntades y modelar con una proyección socio-económica consonante con nuestra idiosincrasia popular y las exigencias más depuradas de promoción personal y comunitaria, a través de un equilibrado juego de iniciativa y responsabilidad, de acción privada y colectiva. La conciencia y la estima de la libertad y dignidad conducen a la servidumbre de la Solidaridad, a quien no puede dejar de considerar dichos valores apetecibles también para los otros. Y nuestro Cooperativismo no es más que la solidaridad encarnada en la esfera de las actividades económicas» (FC, III, 61).

Una vez comprometidos a otorgar al trabajo lo que le corresponde, o resueltos a apoyar el desarrollo con los resortes naturales que deben hacerlo apetecible, nos encontraremos cerca del cooperativismo. «Nosotros en el Cooperativismo que hacemos estamos en concierto familiar con todo el que haya podido hacer suya la conciencia activa y liberadora del trabajo, como también estamos identificados, dice Arimendiarrieta, con los anhelos de un pueblo, como el nuestro, que no necesita saber ni analizar mucho para sentir que ha sido y es el trabajo, más que una naturaleza heredada o que nos hubiera tocado en suerte, lo que nos hace soportable y apetecible la existencia»
(Ib.).

«Alkartasun girotan sorrerazi geinkez euskotarrok gure izate barru-barrukoenari jagokozan jokabide barriak ekonomi ta gizaketa arazo ta arloetan.

Alkartasunez eraturiko batasunez lortuko dogu gure lurralde aske ta zuzen eruateko indarra.

684

au

aurrerapidean

Euskadi
(...) Alkartasunak erakutsi dausku iñoren mende ta ugazabatza gabe lan egin eta aurrerapidetsu jarrai dagikegula bata bestearen lagun zintzo izaten dakigunok. Izan al dagike beste jokabiderik morroitza ta indarkeri gabe onurakor, leiaka ta ugari ogibideak zuzenarazotzeko, ugaritzen doan erriari jagokonez?
Lan Kide Aurrezkiaren egingoa giza-arazoketa ta ekonomi-itzultzerako askatasun eta zuzentasun kemen bizi-bizia gure lege-zarretan txertatu ta ogibide barriak bizibarritzea da» (CLP, I, 259-260).

Una cuestión violentamente debatida en los años 60 y 70 fue la de nacionalismo y lucha de clases (marxismo); su posible síntesis o incompatibilidad.

No será aceptable para todos, pero no hay duda que Arizmendiarrieta ha realizado a su manera una síntesis de los dos extremos, entendiendo como clase nacional a la clase trabajadora: nunca se ha referido a Euskadi más que refiriéndose a los trabajadores; pero a los trabajadores Arizmendiarrieta nunca los ha visto como masa, sino como pueblo. «Vivimos en el seno de una comunidad y de un pueblo de hombres y no de proletarios» (EP, II, 292).

Actitud absolutamente coherente, por otra parte, con su concepto de ciudadanía basada en el trabajo.

Comentando en un artículo la muerte del Presidente francés Pompidou, que se supone ser el fin del gaullismo, y la revolución portuguesa de abril de
1974, tras el desastre de la guerra de Angola, Arizmendiarrieta escribe:
«Desde nuestro hondo valle vasco nos preguntamos a quién sirve el imperio colonial. Y desde nuestra óptica de trabajadores llegamos a la conclusión de que a nuestra clase no» (FC, IV, 196). Es más que una acertada frase periodística: es la síntesis de la famosa dicotomía de reivindicaciones de clase y reivindicaciones nacionales, objeto de tantas polémicas estériles, asumidas en una fórmula plástica.

«Para algunos, escribe, los acontecimientos subsiguientes, tanto franceses como portugueses, han tomado un cariz indeseado, porque abiertamente señalan el fracaso de los grandes mitos políticos» (Ib. 195). Y añade, en una clara alusión a Franco: «Desde nuestro punto de vista creemos que el fracaso de los hombres carismáticos y sus sucesores es inevitable, porque el mal está en el sistema en el que se cobijan. Por eso, a nuestros hombres de mando preferimos elegirlos periódicamente, dejando libres los caminos para que surjan los hombres que lleven consigo nuevos carismas» (Ib.).

«Los trabajadores tenemos ideas claras. No nos gustan los hombres que se autodefinen como líderes indiscutibles en solitario, o son declarados como tales por camarillas interesadas» (Ib.).

«Al final, continúa Arizmendiarrieta, una mañana, y de repente, la clase dirigente portuguesa del régimen disuelto se ha encontrado totalmente desasistida de su pueblo. Esto después de más de 40 años, en los que los políticos se han creído la conciencia y los intérpretes de los sentires de sus súbditos.

—No se puede pretender saber lo que piensan los trabajadores. No se puede pedir que pongan sus mejores ilusiones en la tarea si no se dialoga con ellos utilizando los caminos más claros y leales. Son ellos mismos, con sus propias

685

El (des)orden establecido

palabras, los que tienen que plantear sus problemas y sus esperanzas» (Ib.

196).

1.8. «Hacer un país humano»
Trataremos a continuación de recoger las preocupaciones dominantes en los escritos de Arizmendiarrieta, artículos y apuntes de los últimos meses de su vida, centrándonos en dos complejos temáticos, la revolución y la democracia, y que se pueden resumir en la expresión «hacer un país humano» (FC,

IV, 287).

«Es tal el diluvio de convocatorias políticas que no pocos de los que hemos estado actuando en predios económico-sociales y laborales con los más ambiciosos objetivos de promoción de un nuevo tipo de sociedad en nuestro
País vamos quedando perplejos, con la duda de si merecerán la prioridad las nuevas opciones a las que se nos invita, o deberíamos seguir firmes por donde vamos con la aspiración neta de proceder a un tipo de sociedad sin lucha de clases, en lugar de levantar parapetos para la defensa, sin transformación más radical, sin participación global e universal de todos en las tareas, como nosotros podamos vaticinar» (CLP, I, 293).

La marea política del postfranquismo y del renacer democrático, más que a Arizmendiarrieta, podía provocar una grave desorientación a muchos cooperativistas. Esto era, sin duda, la razón que le movía a volver al tema repetidamente. Por el peligro, en primer lugar, de que las opciones de partidos diferentes trajeran, a la larga, la división entre los cooperativistas. «Hoy que nos enfrentamos con tantas solicitudes, tan diversas y hasta contradictorias para actuar, hemos de poder examinar y tomar posiciones para que, entre nosotros los cooperativistas y en aras de lo que nos hemos comprometido a llevar en común, sea conveniente prevenir y proveer para que entre nosotros el veneno de la desconfianza y del distanciamiento no dificulte lo que básicamente nos hemos comprometido a realizarlo con esfuerzo común» (FC, IV,

177).

Al peligro de la desunión parece haberse añadido el de cierto complejo en los cooperativistas, que, en la euforia de las grandes promesas, podían considerar su propia labor como intranscendental. «De una y otra forma, chocan entre nosotros afanes de redención ecuménica y parcial, en cuanto que parece que para poder hablar de revolución sin complejos todo debe ser universal, total, instantáneo y avasallador» (Ib. 266-267). Arizmendiarrieta no cree en tales revoluciones; sólo cree en la labor lenta y profunda que transforma las conciencias y, por la fuerza de las conciencias transformadas, puede causar las requeridas transformaciones sociales. «Es evidente que la fuerza más universal e imperiosa, con la que ha contado el hombre en el correr de la historia, para promover los cambios más profundos y duraderos, es la concien686

Euskadi

cia. —Es un tanto absurdo que quien se mueva a impulsos de la conciencia y actúe con dignidad y libertad, se sienta acomplejado como si estuviera realizando algo que no es lo más importante o la forma más actual y universal de afrontarlo» (Ib. 266).

Respecto a los políticos Arizmendiarrieta ha abrigado sus dudas, su antigua desconfianza, hasta los últimos días de su vida (Ib. 255). Y respecto a la
«democracia» en los últimos meses precisamente no ha dejado de insistir en que esta no puede ser sólo política. «La libertad no nos sirve en solitario, sin acompañamiento de justicia» (Ib. 279).

«Tal vez quepa sospechar en estos momentos en que aparecen tantos devotos de la democracia si se han puesto a pensar que en la medida que ello sea un fenómeno humano protagonizado por fuerzas conscientes y responsables, debe ser una vía directa y noble hacia posiciones más netas y transparentes de socialización del saber, del poder, del tener y del hacer. —La democracia leal y honestamente sentida y practicada no puede detenerse en sus formalidades y expedientes administrativos de sufragio, sino que ha de tener su impacto y reflejo tanto en los campos educativos y sociales, como en los económicos y financieros mediante el consiguiente proceso de institucionalización. —Los diversos estamentos del pueblo han de recuperar la conciencia de sus responsabilidades y traducirlas en compromisos consiguientes para que un nuevo clima de emancipación se acredite entre todas las fuerzas de nuestro pueblo» (Ib. 267-268).

Arizmendiarrieta cree que, desde una filosofía cooperativista, la democracia debe realizarse en socialismo. Entiende que el movimiento cooperativo adquiere su valor adecuado cuando es entendido como un esfuerzo de crear un socialismo adecuado a las cualidades y características del hombre vasco. «Las empresas cooperativas se han generado con el talante de que sirvan para nuevo modelo de relación y convivencia y participación para la prestación del trabajo y, por extensión, para la promoción ulterior de un nuevo orden económico-social en nuestra tierra, sin exclusivismos, en posición de competencia, y nueva modalidad de opción para sus ciudadanos y trabajadores, como exponente vivo de libertad y democracia realizadas en campo no poco polémico y complejo» (Ib. 258). Arizmendiarrieta quiere que el cooperativismo sea entendido como un modelo de socialismo propiamente vasco, aunque modelos importados encuentren por el momento más audiencia. E insiste en que su modelo no debe ser entendido como mera fórmula económica más o menos acertada, reducida a una específica forma de organizar la empresa, sino como una acción orientada a «hacer país», como se dirá por esas años, a levantar de su postración a todo el pueblo vasco. «Indudablemente en este empeño no ha estado ausente para muchos una actualizada forma de servir a nuestro pueblo, a sus intereses» (Ib.). Arizmendiarrieta espera que la democracia posibilite la ampliación de estos ensayos, para que el pueblo vasco asuma por sí mismo la autogestión, la responsabilidad individual y colectiva, en todos los campos de problemática humana, cultural, social, etc. El Es687

El (des)orden establecido

tatuto y los Conciertos Económicos deben tener como consecuencia que los principios de autogestión y cooperación tengan aplicación en todos los niveles de la vida social (Ib. 254). «Democracia es también socialización» (Ib.

267). Y la socialización debe encontrar aquí fórmulas adecuadas al carácter y a las tradiciones vascas. «La solera de democracia y actividad y dinamismo de nuestro pueblo requieren nuevas formas consonantes de promover un socialismo de auténtico rostro humano, eficiente, creador y promotor de recursos de sí escasos para una zona tan densamente poblada, procediendo a ello con la agilidad y posiblidades todas que dispusieren sus ciudadanos y trabajadores en régimen democrático irrenunciable y libre, que una comunidad consciente no puede dejarlos en suspenso para tratar de recuperarlos posteriormente por conductos extraños» (Ib. 258-250).

Arizmendiarrieta se declara por el socialismo, un socialismo de rostro humano. Siempre que se ha referido a la democracia, en estos meses, ha sido añadiendo la exigencia de socialismo en alguna forma. Consecuente con toda su trayectoria de viejo luchador por el cooperativismo exige, dentro de la exigencia general del socialismo, de modo destacado, la socialización del saber, sin la cual no puede haber libertad ni democracia (Ib. 285-286).

«En marcha hacia una sociedad sin clases, sin desesperar, no simplemente interclasista, por lo que para ser protagonista y constructor de esa sociedad no basta con proclamaciones revolucionarias, si no hay efectiva participación económica, social y política, así inseparables» (CLP, I, 295). Sigue convencido de que el verdadero camino hacia la democracia sin clases la ofrece el cooperativismo: «Promoción integral, individual y comunitaria, protagonizada por el pueblo, por los propios sujetos afectados, se hace viable a través de la cooperación, en marcha hacia una nueva sociedad sin clases, fraternal, dinámica, justa» (Ib. 296).

Esta es la revolución en la que sueña Arizmendiarrieta, una revolución que no trata de comenzar de cero y, por ello, no precisa de tener que destruir para proceder luego a construir; revolución a llevar a cabo día a día, paso a paso (pero sin pausa). ¿O es que acaso es posible conducir o practicar una revolución sin hondas transformaciones de nuestras respectivas conciencias?
(FC, IV, 279-280). La revolución tiene que contar con el factor tiempo (Ib.

271).

Todo intento de revolución total, instantánea, le parece, no sólo peligroso, sino próximo al fascismo. «¿No es esto acaso la lección fascista y totalitaria, cuya experiencia dejó tan malparada a la humanidad? Desde luego nos equivocaríamos si pensáramos que las ideas y los posicionamientos fascistas y totalitarios desaparecieron. El buen observador contempla sus brotes por doquier. —Demasiado pronto se han olvidado tantas servidumbres, tantos crímenes y tanta falsa promoción de pueblos y colectividades, ya que perviven no pocos con tales añoranzas, en la medida que es el abecé de la experiencia humana, que donde se respetan los hombres y cuenta la libertad para todos hace falta tiempo, siempre bien aprovechado si se actúa con previsión, como

688

Euskadi

también el consenso de los ciudadanos y de los trabajadores exentos y libres de fascinaciones de pocos que presumen de pensar por todos. Lo que de esta forma de hecho les ahorran a sus adeptos es algo más que su consentimiento, al utilizarlos como simples instrumentos ciegos: es su dignidad y libertad. ¿Es acaso esto lo que necesita nuestro pueblo en la nueva etapa de emancipación?» (Ib. 267).

Las bases de la revolución son la unión y el trabajo; y la libertad es su condición indispensable. «Lo que no debemos olvidar nunca es que la libertad que dejemos hipotecar es algo de nuestra dignidad, y que no debemos permitirnos perderla ahora para tratar de recuperarla más adelante; ello significaría que somos capaces de vivir sin dignidad. Es algo que contradice a nuestro espíritu secular y por ahí no pasaremos, aún cuando aceptemos pasar por otras penurias» (Ib. 265). La revolución no sólo es para la libertad: debe ser también en libertad. Arizmendiarrieta no admite otra revolución, porque sólo de esta manera puede aquella efectivamente ser protagonizada por quienes deben hacerlo: por todos.

«La revolución deseada, para que sea un salto hacia adelante y hacia el futuro, presupone siempre la aceptación de presupuestos no menos en aras del interés ajeno que del propio. Es por lo que nos parece acertada, escribe
Arizmendiarrieta, coincidiendo una vez más literalmente con sus maestros personalistas, la expresión de quien dice que hay que ser revolucionario por necesidad de plenitud y no por rencor. —La búsqueda de esa plenitud nos impone la plena movilización de nuestras respectivas facultades, su cultivo y desarrollo más que un simple desahogo emotivo y circunstancial. Todos necesitamos actuar más, pero sin dejar a la zaga el reflexionar. La teoría y la praxis son inseparables, el juego de la inteligencia y de la imaginación debe preceder a una plena movilización del sujeto, que no puede desligarse de su entorno, de sus semejantes. Nada unilateralmente ni en solitario. Nada con apresuramiento, pero tampoco por simple inercia. Nada automáticamente, pues todo es susceptible de mejora o superación.— Estos son los presupuestos humanos con los que hemos de hacer frente al reto más irresistible de nuestro tiempo, en el que la cuestión fundamental no es si cambiarán las cosas, sino si los cambios servirán a los hombres y a los pueblos, sin desarrollo como simples cosas» (Ib. 269).

«No nos basta pensar que estamos haciendo alguna revolución. Es preciso asegurar que la estamos protagonizando nosotros. Asegurar si nuestra participación en la misma no es la de simples autómatas, satélites teledirigidos que bien pueden hallarse a la postre con que la revolución deseada y llevada a cabo no es para afirmar nuestros valores irrenunciables de libertad, de dignidad, o la promoción y desarrollo de nuestros pueblos» (Ib. 270).

«No nos olvidemos del hombre para la revolución, ni en la revolución, ni en la construcción» (CLP, I, 296).

689

El (des)orden establecido

1.9. Anexo en euskara35
1.9.1. Barritsuak eta barriketak

Zenbat eta erreztasun geiagokin bizi garan, orduan eta geiago eruan bearko ditugu barritsuak eta barriketak. Onetan gaur aldizkingi ta izparringiei begira asten ba-gara laster arkituko gara eruakizun onekin. Autu korapillotsuenetan eta sokonenetan edozeñek diardu ta besteei erakusbideak emoteko ez dau eragozpenik sentitzen.

Kooperatiba sailletan, beste arazoetako antzez, auxe jazoten da. Batetik asko pozten gaitu kooperatiba arazoak azaltzaille asko izateak, naiz ta beste batzuetan min artu aztertzeko edo jakiteko edo ikasteko ardura gitxikin jarduteak.

Geienetan sail onetan mingarri izaten dana auxe da: ezer egiten ez dabenak edo gitxien egiten dabenak zerbait egiten dabenei kontuak artu bearra. Besteai kontuak artzeko orduan zorrotzenak eurak, gitxien egin izan dabenak, izan oi dira. Besteai eskatzen jakona zenbat bakotxari kostatzen jakon dakienak geienetan begiratsuaguak izan oi dira.

Zorrotza eta ziatza izan nai dabenak ikasi begi norberenetan eskuzabal ta biotzbera izaten.

Besteak zer egiten ete daben arduratsuegi dabillenak izan lezake bakoitzarenetan atzeregi ibiltea, norberenetan nasaiegia izatea. ALKARTASUNIK ez da sakontzen norberekoikerietan indartsu dabillenen artean. a) Euskaldunki
Bai. Euskaldunok euskaldunki jokatu bear dogu, geure izatea ukate gabe. Baña euskaldunki jokatzea zer dan aztertu bear dogu.

Euskaldunok ez gara bizi bakarrik Euskalerrian: emen baño kanpuan, beste errialdeetan geiago bizi gara. Orretan ez gagoz bakarrik: Euskalerrian euskaldunetan beste bizi dira beste endakoetan.

Euskaldunontzat ¿eratsu danez jokatzea zer da?
¿Euskaldunki jokatzeko zer da egin eta erakutsi bear doguna euskaldunok?
¿Zer izan da munduan zear euskaldunoi entzutea ta onartzea merezi izan dauskuna?
Inportantzi aundia emon izan dautsagu guk eta gu beste lurraldeetan onartu gaituenakin gaur inportantzia emoten dautzeguen bere gaiei?
Edonun ekandu izan gara euskaldunok eta edonun onartu izan gaituenean ¿zergaitik izan da? Zuria edo baltza, orixka edo gorriska izateari inportantzia emon dautsagulako? ¿Emen gure artean gure izate bereziari begiratuz gaurkoz pozten gaituana ez al da izan geure arteko bardintasun-zaletasuna, alkarrenganako begirapena, zuzentasuna?
¿Euskaldunki jokatzea zer da edo zertan ipiñi bear dogu?
¿Batzuek morroi, mendeko edo giza-sasdikitzat artzea?
¿Gure artean beste tokietan guretakoak gobernatzerik onartu ezin genezaken erara kanpotarrak tratatzea?
Gaur Euskalerritik kanpora emen baino euskaldun geiago ba ditugu, izan lezake emetik urte asko baño len oraindik geiago izan bearra. Euskaldunki jokatzeko aurrera begiratu dagigun eta aurrera be gu eta gure tartekoak ondo izan gaitezen zerk kalterik aundiena egingo dauskun aztertu dagigun. b) Aurrera beti
Ondo da al dan artean zarrari eustea, baña iñoz be gero palta egingo dauskunei utzi gabe.

35

Eranskin honek hitzez hitz ematen ditu Arizmendiarrietaren textuak; baina textuetako okerren batzuk zuzendu egin dira euskarazko pasarte guztietan, irakurlean alferrikako zailtasunik ez gehitzeko.

Idazlan guztiok TU-LANKIDE aldizkarian eman ziren argitara. Orain Arizmendiarrietaren Idazlan
Guztien bildumako Varia liburukian daude jasota.

690

Euskadi
Geruago ta geiagok, ez bakotxak deritxon modura, baizik ingurukua izan daikeen bizikeraren erara bizi izaten ikasi bearko dau.

Gaurko munduari izakera emoten dautsen aukerak nundik datozen edo zerk sortzen dituan begira dagigun burubide ta jokabideak.

Onetan ez daukagu beste biderik zerbait al izateko ALKARTASUN SAILLETAN aurrera egin baño. Alkartasunik ezin lezake indartu bakoizkeria baztartzen ez danean. Bereizkeri zaletasun aundiegia dogula dirudi gaur gure artean. Kutsu kaltegarria dogu guretzat bereiztasun gogo au.

Atzerritarki era askotara jokatu genezake.

Euskaldunok dogun entzute ona galtzea atzerritarki jokatzea da.

Aurrean dakusgun munduak eta bizikerak gizonari, bai lan egiteko, bai gizabidez jokatzeko eskatzen dautsana lortu gabe iñor gure artean atzeraztea, atzerritarki jokatzea da.

Atzerritarki uste baño geiago ibilli genezake euskaldunki gabiltzalakoan.

Penagarria iristen badautzagu orain-arte emen bizi izan bear eben askok ogi ta bizibide-billa kanpora urten bearra izatea, aztertu dagigun lenago zergatik beste lurraldeetara juan bear izaten eben eta ez dagigun artu oraindik orrela jokatu bearren izan lezakezala gure ondorenguak.

Euskaldunki jokatzeak sail eta ongarri askori begiratu bearra dakasgu.

Atzerria aldean daruagu, tartean daukagu Alkartasuna, teknika eta beste aurrerakuntzak lantzen ez baditugu: morroitzari iges egiteko ez danentzat atzerririk iguingarriena etxean bertan izan lezake.

(1968,

Iraila)

1.9.2. Gizonki ta euskaldunki

a) Giza min
Giza-min eta euskal-arnas zaleei ekonomi-arazoak aipatzea, dabiltzen burubideai aurrerapidea emoteko baño uxatzeko obea dala dirudi, batzuen eritxiz. Ekonomi-autuak diru-zitzak eta aundikeriak jota dabiltzanentzat egokiagoak dirudie. ¿«Materialismo» zikiñean murgilduta ez al gabiltz geienok?
Ekonomi sailletan arduratsu diarduen Kooperatiba-zalek ¿ez al dakusguz giza-min eta euskal-arnas zaletasun edo ardura gabe?
¿Egia al da?
Giza-minberatuok eta gizatasunez geure jokabideak eratu nai ditugunok ¿gizonki jokatzeko oiuka edo beran-berarizko kutsu billa ibilli bear al dogu?
Gizonei alkartzeko bideak zabaldurik eta alkartasunezko erabagiteko bultz egiñarazotea ¿ez al da gizonki jokatzea?

jokabideetan-zear euren

arazoak

Iñor aintzat artzeke ta bakarti baño gizarte-arazoak eratu ezindako bizikerari eustea ¿ez al da gizatasun-egarriak izan daiken gizarte eragozpenik kaltegarrienetarikua?
Azkatasun-oiuak gizatasun izan bear dabe.

agerpidetzat

artu

daikeguzan

alkartasun-miñetan eldutakoak

Kooperatibak alkartasun-bidez lanbideak onuragarritzen eta ogibideak sortuarazoten diarduenez, ezin ukatu genezake gizatasun-alde dabiltzanik eta gaur dagokigun azkatasun-miñari erantzuten ez dautsenik. b) Ekonomi-saillak gizatasunez kutsupetzeko
Ekonomi-arloetan gizonok askotan edozetariko izaki uts edo abere bagiña bezela oinpetuak izan gara: Batzuen irabazi-egarri asekaitza, beste batzuen aundikeria edo zikoizkeria ta ekono-

691

El (des)orden establecido miarazo edo lansailletako jokabide biurrien ondorenen barri danok dakigu: gizatasun gabe eratutako saillak izan dira.

Kooperatiben burubidez ekonomi-sail orreik, arazo orreitan zer ikusi daben danen gogoz ta begirapenez eratu bear dira. Ona emen kooperatibak dakarguen onura bat. Kooperatibak giza taldeak diranez ta taldeko guztiak erabagikide diranez, gizatasun-bidean gagoz danak edo geienak, gizakaitz izan ezin daikenez.

Alkartasunean sortuarazi daikezan burubide edo jokabideak berez izan daikez gizakor edo gizabera. Alkartasunak berez garuaz gizatasun-jokerara, bakartasunak bakoizkerira bezela.

Kooperatiba-arauak gizarozteko onuratsuak ditugu. Gizatasun-oiuka geiago egingo dugu alkarrenganako begiratsuaguak izaten ikasten badogu.

baño

gizatasun-alde

d) Morroitza latza
Azkatasun gabe gizonak gizonki ezin jokatu daike.

Azkatasun-gogoa beste edozein gogo baño sustartsuagua ta sakonagua da: azkatasun-gabe bizi baño il naiago izan daben asko dira gizartean.

Baña bakarti azke tea baño asko geiago daike. Tartekoen edo tasunak alkartasunera

izateak ezin ase daike gizon danaren gogoa. Bakarti azke izatea basati izaez litzake ta gizonki jokatu nai dabenak norbere azkatasunakin ezin ase ingurukuen azkatasun-miñez jardun bearra izan oi dau. Azkatasun-zalegaroaz.

Alkartasuna ta azkatasuna gizatasunaren agerpen ito-ezindakoak dira. Kooperatibak alkartasun-bidez azkatasun-alde diardue, morroitza geienori iguingarri ta astun egin lezakegun sailletan: lanbide ta ogibide sailletatik morroitza uxatzen.

Badakigu gizonok azkatasuna sail guztietan bear dogula. Baita era askotara eratu dezakena be. Gure erria indartzeko gure gizonak indartu dagiguzan: gizonaren eskuarterik onenetarikoa lana danez, lanbideak eratu ta lanbideetatik otseintza edo morroitzak uxatzea egingo onuratsua da danontzat.

Euskalerrian baño Euskalerritik kanpora asko be euskaldun geiago bizi garena gogoratzekoan, ezin aztu genezake lenago, orain eta geruago be gure tartetik iges egin bearrean asko izango dirala, ekonomi sailletan gure erriari indar aundiagoa emoten ez ba-dautsagu.

Euskaldunki jokatzea ez da soñu erreza. Soñu gozoz bizikera garratzak izan genezakez: oiu egiten baño zintzotasunez jokatzen geiago alegindu gadizan.

«Zuzentasun-miñez ba-turutak.

eta

zintzotasunez

alkartasun-bidez azkatasun-alde»

dirausku

kooperati-

(1969,

Urtarrila)

¿Ez al da geienok onartu genezaken gogoa?

1.9.3. Berezeki ta naroki

Naroki bizi al izateko berezeki jokatu bear dogu: berezeki izaterik ez jagoko iñori obeto bizi ezin al izatekotan.

Egia da, oraingoz sail askotan bereziki jokatzera jo bear dogu obeto izan al gadizan. Teknika ta ekonomi arloak orixe eginbearrori dakarzkue: lege zarrak berritu egin bear dira ta ez lenago ona zalako, edo orain lengoa ta zarra dalako eutsi. Berezeki izatea bizibideak berenez dakargun egingoa da bakoitzak eta aldi bakoitzean egoki eratu dagikugezan geure arazoak.

Auxe da euskaldunok geure zanetan daruagun gogo bizia ta urteen buruetan ezerk zapaldu ta ito al izateke gaurdaño tinko ta indartsu baztar guztietan gure enda zaindu daben jokabidea.

Eutsi edo iraunarazo bearreko gauzatan baño geiago daukaguz aldatu ta egokiarazotekotan, arlo ta arazo askotan: ekonomi, gizarte, politika ta erlijio sailletan.

Orretan dakusgu zelako bizikera-zale geran: zertan ta noraño ekonomi arloen eta jokabideen bidez eratu ta ornitu bear ditugun gogakizun batzuek: zeintzuk diran eta zelan ase bear ditu-

692

Euskadi gun beste egarri batzuek edo arlo arein bidez ase ezin leikezan zeintzuk gauzeren gogo garan aztertu ta erabaki dagigun.

Ideologi lañoak aide batera itxi ta, bakoitzak bere gogo ta asmoak, bere egarri ta zaletasunak obeto asetu dagikeguz burruka bero ta bizi gitxiago ta auzi-erabakitze erosogoak izango ditugu geure artean.

Gure lurralde ta inguruei begiratuaz: gure gaurko egitaroak biar eta geruago izango ditugun eragozpen edo eskuarteak kontuan arturik eratuaz ekin dagigun alkartasun bizitan: auxe bedi euskaldunok gaurkoz izan dagikegun berezeki izateko gogo bizia: au oraingoz badagikegu naroki izatea berez-berez jatorkikegu.

Gabiltzan adi ingurua begiraturik aurreruntz urrun ikusirik eta egunik alperrik galtzeke: geuk geuretik eta geurenez jaso bear izango dogu gure Euskalerri maitea: ez dagigun aztu euskaldunok, edo emen bizi geranok, geure buruak ondo lantzen ez ba-ditugu ta alkartasun-bidez indarberatzen ez bagara berezeki bizi izatea egarrigarri barik iguingarri izan dagikegula.

Berezeki ta naroki izateko zarrik azteke barri-zale gadizan: gure ibai ta mendien edergarrien gure gizonak dagizan.

(1969,

1.9.4.

Martxoa)

Azkatasun-miñetan

Azkatasun-miñetan bururik galdu gabe ibilli gaitezen eta azkatasuna gizakioi jagokunezkua izan dagian, ogibide-arazuetan ez gaitezen atzera geratu.

Kooperatiba-ardurak ezin utzi genezakez alde batera gure gizon eta erriak ogibide onak izan dagikeezan. Ogibiderik ez daukanak ez al-daroa beregan morroitzarik garratzena?
Iñoren mende era askotara izan genezake, gizonok eta erriok.

Azkatasunak bere oñarriak bear ditu, ikurriñak baño lenago.

Gure gazteen bear ditugu.

barneetan

azkatasun-miñakin jakin-mina ta

alkartasunzaletasuna

sustartu

Orrela jokatu ez badikegu egun larriak ezin uxatu al izango doguz gure tartetik.

Baditugu gure inguruan lenago gora egiñarren oraintsuago bera doazen giza-talde ta erriak.

Orain urte gitxiren buruan egiten da naiz gora ta bera. Ez dagigun alperrik egunik galdu egizko azkatasun miñetan bagagoz edo oñarritsu gure Euskalerriaren alde jokatu nai badogu.

Bakoizkeri ta norberekoikeriai uko dagiogun: alkartasun burubideetan ezi ta ekandu gagizan. Barriztu ta gaurkotu dagiguzan gure bazkunde ta elkarteak: danetarikuak: kooperatibak be bai. Batez be gure errialdeetan lenik sorturik erri bakotxak bere biztanleentzat sorturikoak: sakabanaturik eta indarge dagokuzenak. Alkarturik edo baturik indartsu izan legikezanak. Errimallatan giza-mallatan baizen egoki eratu genezake gaurkoz bear genduken alkartasuna.

Azkatasun-miñetan garanok

alkartasun-malletara jo

dagigun.

(1969,

1.9.5.

Maiatza)

Eusko-jokabideak

Euzko-gogoa itoko ete-dauskuen bildurretan geiagotan eta ez gitxi be.

ta samintasunean bizi izan

gara

bein

baño

Iñork ito ezin dagikun gogo ori geuk ostera alperrik galdu izan oi-dogu.

693

El (des)orden establecido
Gizonkiago beti joka geinkegunean, edo gure bazter, uri ta inguruak edergarriagotu geinkezenean, ¿zenbat ardura ta alegin egin izan oi-ditugu orretarako?
Aundikeriz kutsututa euzko-jokabide bakarrak beste gizon-talde edo errialdeak egin izan dituen antzera ikurriñetan, oiu-alaritzeetan edo aizeak eta denporak berez daroakigezan jokabideetan jarri izan oi-ditugu.

Gure artean alkartasun-min sakonagua sortuarazoten, gure etxe ta kaleak edo uriak edergarriagotzen ardura gitxi izan dogu.

Guk maite dogula diñogun Euskalerria edertoguatzen aterik-ate, baserririk-baserri, ikastolarik-ikastola, leiorik-leio, saillik-sail, edozeiñek egin genezakeguzan edergarriak edo apaingarriz jantziten gitxi egin oi-dogu.

Aoz danok dirudigu oso abertzale zintzo, egitez oso gitxi, gure erriai ta gizataldei ondo jetorkegiozen egingoak egiten gitxi baño gitxiago.

Ba-daukagu naiko zeregin gure inguru guztiari jagokon edergarriak erazoten. Danok edo geienok orretara jo ba-daikegu zerbait egin al-izango dogu urteotan egin diran itxurabakokeriak leuntzen edo edergarritzen.

Euzko-jokabideetariko bat auxe da: lañoetara baño bera geiago aldatu genezakegunok lurraldean zerbait obeto eratu dagikegun.

begiratu:

lañoetan

gitxi

Iñoren baimenik gabe egin genezakeguzan egingoak ez dira gitxi.

Bakoitzak beretik zintzo jokaturik jaso lezakezan saillak zabalak dira.

Euzko-espirituz ta arnasa barriz biziarazo dagigun inguruan daukagun guztia. Alkartasuna, zuzentasuna, azkatasuna ta aurrerapidea bekigu gure gogo-min: oñarritu dagiguzan bakoitzak dagikegunez.

(1969,

Azaroa)

1.9.6. Erri-zale ta askatasun-miñez

ERRIA ta ASKATASUNA aotan, barriketan, erretzago erabilten dira biotzez eta egitez indartu oi-diran baño. Gure ERRIA makurturik edo oinerapeturik dakusgunean odol-bizitan edozer egingo geunke askatzeko: olaxe dirudi baña ez da olan. ¿Kultura ta ekonomi-sailletan zer egiten dogu?
ERRIA egitez jaso nai dogunok iñoren menpeko izan ez dadin lanaren buruz ornitu bear dogu kultura ta ekonomi-sailletan. Baña benetako askatasun-zaleak ba-gara geuretzako nai ez dogun otseintzarik iñori ezin opa izan dagikegu. Naiz gure alde naiz gure buruen morroitzak ezin onartu dagikeguz. ERRIA lurrak edo lurraldeak utsik ez dira, berton bizi garan gizonok baño: erriaren askatasuna gizonon askatasuna da.

¿Uxatu edo eten geinkezan morroitzak apurtzen alegintzen al gara?
Askatasun-miña gizatasun agerpenik sakonenetarikoa da. Baña askatasun-min sakonaren uka ezindako ezaugarria norberetzat gura doguna iñori opa izatea ta bakoitzari ondo ez dirudion morroitzarik iñori ez ezartea da.

¿Ba ete daukagu adorerik danontzako onuragarri izan dagikezan morroitzak alde batera utzi ta beste era batera gure gizartea eratzeko?

(1970,

Urtarrila)

1.9.7. Aztertu bearreko gauzak

Askatasuna ta alkartasuna biak bear ditugu danok, baña ez dagigun aztu biak ondo jasotzeak badituala bere korapilloak, danok edo geienok orren diperenteak izanik.

694

Euskadi
Orretan ez dogu esan nai danok, bakoitza bere sail eta giroan, obeak egiteko ondo ez zaigunik etorriko ta orretarako danok eskakizun ori aurrera atara al izateko jokatu bear ez dogunik.

Baña berbak aizeak eruaten ditu ta egiñak bear ditugu alkartasunpean ondo jokatu al izateko.

Arekin eta emenguekin, an eta emen, iparrean eta egoaldean, toki danetan gizonki jokatzeko saillak aztertu ondo ta.

(1970,

Azaroa)

1.9.8. Danok danon bearrean

Euskotarrak ba-ditugu gure miñak eta asarreak gure jakintza eta izatea beste batzuek biurritu ta legortzen dauskuelako. Aldi onetan edozenbat batzar eta azterketa egin ditugu gure problemak eta auzien sustraiak zelan dagozan jakiteko ta komeni diran aldakuntzak sortuarazoteko.

Batzar eta billera oneitan danetan entzun dogun kezka ta larria auxe izan da: zapalduta gagozala, eusko-kultura ito-agian dagola: askatasun geiago bear dogula: orretarako iñork ezin ukatu dagikula eskubiderik: beste batzuek guk eurak errespetatzea nai badabe eurak be gure eskubideak aintzat artu bear dituela.

Emen danok jakin dakigunez kultura-arloetan erdel-kulturapean gagoz erdeldunak erdeldun diranez ta jokatzen dabenez ez dauskue onik egiten.

eta

orixegaitik

Erdeldun edo erbestekoak eurak be baditue beste sail edo arlo batzuetan euskotarrontzako gozatzu ez diran zerikusiak eta kezkak.

Ez dogu pentsatu bear areik dana txarto ta danean gure aurka diarduela gure aldetik eta guretarrak ezetariko gogorketa ta zuzenbiderik baño egiteke.

Euskotarrok ezin aaztu dagikegu emen gure tartean iñoren mende gagozan arren erbesteetan gure mende be badagozala ekonomi-arloetan eta egintzetan.

Emen eusko-kultura erbeste-kulturapean eta an, erbestean, ekonomi-eginkizunik asko ta ekonomi zerikusi andiak gure ekonomi ta ekonomi sailletan dabiltzan gizonen mende edo arietan jokatu bearrean.

¿Nork ez daki Euskotarren asmoz ta burubidez zuzenduriko egintzak sustrai zabalak dituela ta sustrai orren bidez edo emen eginda ortik zear saltzen ditugun tresnen bitartez geuk be beste batzuek eta beste batzuen ekonomi-eskuarteak aprobetxatzen ditugula?
Onetan, naiz ta zabalgarri edo esangarri izan ez arren, badira ukatu ezin geinkezan eta guretzat onak eta beste batzuentzat orrein ongarri ez diran zerikusiak.

Norbaitek esan ebanez, alde batetik ikusi genezakez bi milloi euskotar edo geitxuago erbeste-kulturapean, euren jakintza edo kulturari jagokonez jokatu eziñik eta bestetik egoi-aldean ogeitamar milloe edo geitxuago ipar-aldeko talde txiki areen ekonomi-zuzendaritza edo ekonomi-arietan kezkatsu edo miñez: batzuek askatasun miñez eta bestetzuek zuzentasun egarriz: kultura zapaltzaleen mende eta gizarte-zuzentasun eskale.

(1971,

Apirila)

1.9.9. Errudun

Gure erria maite izateko errugea danik ez dogu oldoztu bear: gure izatea ta izanbide iturri ta ustar dalako maite dogu: maite dogulako aintzat artu bear ditugu ta ondo aztertu bere akatsak ta onberatzen alegindu.

Norbaitzuk azkenengoan giñuzan jardunen batzuekin mindu egin zirala diñoskue: geuk be ba-genduzala beste errialde batzuek txarresteriko zerikusiak geure artu-emonetan giñuan eta

695

El (des)orden establecido esan egikela ba-genkiala iñoren bearrizanak geure alde onuratzen. Guk orren errurik ez genduala artu-emon orreik eruateko kapitalismoak jarritako maillak baño ta mailla orreik ezin genduzala eten? ¿Orreik eten al izan ba-gengikez ez ete genduke iñoren, besteen bearrik izango?
Danok daukagu geienetan uste dogun baño beste geiagoren bearra...

Elkarte-erakunde barriak surtuarazo arren ezingo gara iñoren laguntzarik gabe biziko. Iñoren bearren izango geranez, iñorekin zelan eratu geure zerikusiak aztertu ta egokitu bear dogu.

Iñoren jauntxokeriagaz erretz iguindu izan oi gara.

Arrokeri ta jauntxokeri gabeak al gara gure ingurutakuen edozein zinurkerik iguindu ta gogaikatzen gaituanok?
Españiarren jauntxokeriaren atsa edonun aipatu oi da: jauntxokeri orren sustarrak sakonak dirala esan oi da: luzaro lurralde zabaletan jaun eta jabe ibilli ta bizi izan dira Europa’ko bazterretatik Ameriketa’ra arte. Euskotarrok be tarteko izan giñan eta kutsuren batzuek ezarriko ez jakunik ezin izan legike.

Euskotarrok batzuen eritxiz jauntxokeriara baño arrokerirako berezko jokera aundiagua izan dogu. Orretan Ortega’k esango dau gure arrokeri ori soilla izan dala, arrokeri-putza, sakongea, ez gizakaitza, giza-min azalkoia baño.

Dana dala gure lurraldeak mamintsuak, guriak eta emankorrak izan ez arren gure erriak beti aurrera egin izan dau ta beste lurralde onuragarrietan bizi diranen maillatara eldu gara.

LANA izan da gure erriaren indarra ta onurea.

Bakarti-berak izan arren auzo-miñik gabeak ez gara izan, edo auzoerakundeen bidez jokatu izan oi dogu.

Iñoren onurarik geuregandu ez dogunik ez diñogu: iñoiz iñor atzipetu ez dogunik eta otseindutzeke ibilli garanik be ez.

(1971,

Maiatza)

1.9.10. Eskuarte gabeko eskubideak

Eskubideak gora ta eskubideak bera jardun izan oi dabe beste batzuek. ¿Giza-miñez ta askatasun-zaletasunez izan oi dira askotan olako jardunak?
Nor zer dan jakin al izateko, nork zer esaten daben baño, nork zer egiten daben adiago artu bear da. Gizonak eta erriak eskuartu gabeko eskubidez ezin ase genezakez, kultura ta ogibidezko eskuartez euren buruen jabe izaten laguntzeke. Eskuarte orreik erriei ta auzoei eskuratzen jakenean «iraultza-albizteri» gitxi bear izango ditue.

Zapaldurik eta zapaldurik gagozala oiuka zerbait egitekotan diardugunean lenagoko sorgin-kontuetaz eibartarrak esan oi ebana gogoratu dagigun.

Eibartarrak pistolak sorgiñak Euskalerritik. kubideak eskuarteekin ren jaube lanaren eta

egiten ekin eutsonean eta pistolak saltzen asi ziranean iges egin ei-eben
Uste dogunez lenagoko sorgiñen antzera zapalketak uxatuko ditugu esalkartzen eta orretarako ezjakintasunak urrundurik bakoitza bere burualanerako alkartasunaren bidez elduarazo daikenean.

Langilleok lanbideen jaube ta zuzendari egin gadizan barritsuen begira izan ez gadizan.

(1972,

Urria)

1.9.11 a. Borrokan ala alkartasunean

Euskotarrok burrukalari, bulartsu ta indartsuenak izanda be, burrukak irabazi baño galdu seguruago egingo ditugu. Gaurko munduan mobitu izan oi diran talde ta eskuarteak kontuan

696

Euskadi arturik, ezin ukatu geinke burruka ta matxinadetan erabilli izan oi diran jokabide ta tresnakin baño geiago al izan genezakena gizatasunari jagokozan indar ta jokabideen bidez: baketan burrukan baño geiago al izan genezake gure erriaren alde ta batez be gure erritarren onerako, aurrerapiderako.

Burruka oiuak eta turutak entzun eta jardun oi dabenean, ez da pentsakera txalogarria, baketan gizatasun-indarrez, au da, zuzentasun eta aurrerapide jokabidez obeto oñarritu gengikela gure erria ta gure erriaren eskubideak, baieztu ta zabaltzea.

Indarkeri-girotan askatasun gitxi onerazi ta ongarritu izan oi da. Indarkeriz erein oi dana indarrez jaso bearra izan oi da. Indarkeri girotan bizi izan diran gizaseme ta errialdeak nekez be nekez tinkotu oi dira begirapenean ta alkartasunean izateko: burrukarako ta indarkerirako jokabidea edonoiz sortuarazoten da edozeñen kaltegarri ta ondamendi biurtu. Gizatasuna gizatasunean gozatu ta guritu bearrekoa da.

(1973,

Urria)

1.9.11 b. Azkatasuna obeto izan al izateko

Larriago ta neketsuago barik geroago ta obeto danok edo geienok izan al gadizan bear dogu azkatasuna. Banaka batzuentzako edo talde batzuentzako ongarri dagiken azkatasuna ez da guk gizatasunez ta alegin guztien bidez geureganatu bear dogun azkatasuna.

Egia da azkatasun ori lortu al izateko lokarri batzuek eten bearra izango doguna ta baita erakunde batzuek aldatu bearra be bai. Lokarriak etetekoan eta erakundeak aldatzekoan ikusi ta neurtu dagigun, ez bakarrik geuk geure taldeetan onerazi gengikenari, baita gure erriak neurri ta ongarribidez onartu bearrekoa be, naiz ta bakoitzaren edo batzuon eritxiz onerazigarria izan ez.

Azkatasuna zuzentasunakin josi bearrekoa da. Zuzentasuna ingurukoen eskubide ta eritxiak be onartu bearra daroan jokabidea da.

Eginda baño egiteko daukaguna gure errialdean zabalagoa ta sakonagoa danez, gure giza-erakundeak aurrerapidetsu ta indartsu izateko moduan antolatu dagiguzan danon artean. Orretarako kontzientziak indarbarritu dagiguzan jakitez, artuemonbideak zabaldurik, alkarri bakoitzaren eritxiak entzun-arazoaz eta batez be alkar aintzakoagotzat artuaz.

Euskadi aotan artzen dogunok bakoizkeritik iges eta alkartasunean eta batasunean indargarritu bearra daukagu ta iñork be uka ezindako eskubideei eldurik iñork uka ezindako INDARRA gerekotzakotu: gizatasun-miñez jaiotzen dan eginbearra, gizatasuna bizien izakerazotzen daben askatasun eta zuzentasuna sail guztietan ta bakoitzak eta danon artean sorrarazo dagikeguzan erakundeetan oñarritu, sustartu, laneko ta urietako arazoetan, ikasketa ta auzoetako jardunetan, ekonomi ta giza-gintza arlo guztietan. Geuk geurez aldatu genezaken gauza guztiak giza-girotan ta gizatasun aurrerapiderako dagikegunez ezingo gaitu iñork ixilerazi ta baztertu.

Egingo au ez da egun batzuetan egiñalekoa: egingo au egiñarazo ta oñarritzeko EKIN TA
EKIN, JARDUN eta jardunari EUTSI bearra dogu: Orretarako, gudari burubide aldizgarrietan baño, aldakera egingo korapillotsu ta sakonetan ezi bear aundiagoa dogu.

(1973,

Urria)

1.9.12. Edozetarako

Kooperatiba-sail eta arazook, sail eta arazootan zerikusirik dogunok, geuk geurenez jokatu al izateko trebetasuna ta gañerako jokabideak geuregandu dagiguzan: iñoren otsein eta morroi

697

El (des)orden establecido izateke ta ezetariko jauntxokeririk gabe jokatu ta bizi al izateko norbere gogoz eta alkartasun zuzenbidez egin al dagiguna dagigun beti.

Kooperatiba erakundeen kutsu ta indarra ez da besterik askatasun eta zuzentasun miñez norberak ingurukoen ongarri ta alkartasunpean jokatuarazotera bultzatzen gaituan gogo-bizia baño.

Norbaitzuek esango dauskue askatasun eta aurrerapide miñetan irakiten dagon erriarentzat iraultza jokabide ori luzeegia edo astirotsuegia dala: baita beste batzuek esan al izango dauskue etenbearreko lokarriak eta gainberatu bearreko indarkeriak presa larriagoa ta indarketa biziagoa bear dituenak dirala be.

¿Lasterka elduarazo eziñeko egingorik asko ez al dira giza-aldaketan onartu bearrekoak edo eten ondoren barrituarazo bearreko lege ta erakunderik ez al da bear giza-aldeketa ta elkarte iraultzeak gizagarritsuak, onargarriak izan al dagikeguzan?
Zer deuseztu, apurtu edo birrindu bear dogun pentsaterakoan, zer jarri, egin edo jaso bearko dogun eta zerekin eta zelan egin eta jaso al gengikegun be kontuan euki dagigun. Orain eta gero be geuk geuronez egin dagikeguna egitea ongarri ta bearreko izango da geuri jagokun erakunde ta politika jokabideetan ibilli al izateko.

(1974,

Azaroa)

1.9.13 a. Askatasuna ta alkartasuna

Aldaketak poztu egin bear gaituzte izatez aldakorrak geranez eta orrez gañera obeto bizi al izateko aldaketak izan bear aundiagoa dogunez ikusmirazale izatea baño.

Iñoren eta iñungo otseinpean bizi izatea zer dan ondo dakigunez ikasi bearrekoa auxe da: alkartasunean indartsu ta aurrerapidetsu nola jokatu, amesgarri dan neurrian geure izakera ta bizimoduaren eskabideai ondo erantzun al izateko. Alkartasunak txalo asko izan oi ditu ezer eziñean eta askatasun gabe gizakiok izan oi geranean, baña ezin uka dezakeguna da askatasun-miñetan alkartasun girorik asko ito izan oi dirala.

Askatasuna bear-bearrekoa dogu gizakiok gizatasunez bizi al izateko, baña baita alkartasuna be gure artean indartsuak indargeak menperatu ezin dezakezan.

Euzkadi’n agerrerazi bear doguna, ezer izan dagikenik sendoen eta erri bezela gure ezagungarri izan bearrekoa, askatasuna ta alkartasuna buztarri bat egitea da: buztarri orren bidez izan genezake indartsu eta edonun eta edozeiñen artean ongarritsu ta onartuak.

Buztarri ori beste ezertan baño egokiago gure izakerari ta etorkizunari jagokonezko egiñeraziko dogu eta LANERAKO zar eta gazte, ogirik bear dogun guztiok ekanduaz. Aurrera egin al izateko ta geure buruak tente ta tinko edonun agerrarazitzeko LANA da gure eskuartea: guk ez daukagu erri bezela iñori ezer kendu bearrik, iñungo ezeren egarriz bizi bearrik, lanerako egiñak eta lanaren bidez gizakioi jagokun erri ta aberria egiten dakigunez.

(1975,

Azaroa)

1.9.13 b. Ekin eta jarrai

Beste gizatasun giro bat sorrerazi bear dogunez, leendik egindako ario ta saillak aurrera eroan bear ditugu, batez be erakunde orrein bidez, mundu guztian ager genezakenez, egunean baño egunean artu-emon aipagarriagoak izanez, ogibiderik iñori be palta ez dagigun eta gure Erria, naiz ta lurrez zabala ez izan, gure erritarrak edonun onartuak izan dagikezan.

698

Euskadi
Politikakeriz banandu ta sakabanatzeko arriskurik izan ez dagigun gure artean, lanbidez eta aurrerapidetan tinko iraun dagigun bearrekoa da, alderdikeriz gain lankide ta aberkide sutsuago ta indartsuago izan gaitezen.

Iñoiz izan dogun premiñarik aundiena daukagu, alkartasun-buztarri pean jokatuz, aldaketarik sakon eta onuragarrienak aurrera eramateko Euzkadi'ren onerako. Ez dogu esan nai beste egitekorik ez danik eta bestelako saio guztiak gutxi inporta dabenik be erriaren onerako, alderdikeriz tinkotu baño auldu geiago egiteko arriskua izango dogula baño, gure artean munduko errialde geienak euren buruak jasotzen eta euren buruen jaube egiñaz sustartzen ari diranean.

Aurrera beti ta gora, naiz ta aberatsak izan ez, edo naiz ta beste batzuek baño ugariagoak be ez: edozein baño alkartsuago jokatuteko izanez, gutxik geiagorik egin al izango digute.

Urteotan giro larritan bizi bearra izan dogu, baña geure artean giro barririk beste iñork ez digu sortuaraziko geuk alkarrenganako begirapenez izan ezik: indarkeri ta odolik ez dogu bear eta geure askatasuna sustartu bear dogu geure erriaren izateari ta munari jagokonez.

Iñoren promesa ta inguruko edo auzokoen esanetara baño geure eusko-gogoaren eta izatearen eskabideetarako trebeagoak izan bear dogu. Demokrazi miñez ta jokabidez geure artean arazo guztietarako eraturik, munduan iñork ez digu aterik itsiko ta gure erriko gizaki ta lanak be onartuko dizkigute.

(1975,

Azaroa)

1.9.14. Sasikeria

Ixil-ezkutuka bizi bearra izan dogunez edozetarako ixil-ezkutukako bizikera edo jokabide orreik sasikeriari indar aundia emon diote. Sasi-maixu, sasi-zuzendari eta azpikeri eta narrastasunetan lotuak edo naastuak gabiltzanok asko eta asko omen gara.

Langilleriak berenez eta beretik ba dau indarrik beste giza-taldeek baño sakonago eta trebeago jokatzeko gure Euzkadi'ren etorkizunaren alde. Gure erri maite onen arpegian eta izakeran Europa zabalean beste ezer baño edergarriago eta trebeago izango genduke askatasunean eratutako alkartasuna eta lanean eta lanaren bidez aritutako gure erri-tankera.

Sal-erosketaz eta merkatarien eskuz eta oñarriz baño langilleriaren trebetasunez eta egitez jasotako oparotasuna gizagarritsuagoa eta iraunkorragoa izango da erri aurrerapidetsuarentzako.

Sobietar Langilleriaren jokabide eta irtepiderik ezin izan gengike gure Euskadi berezi eta txiki onetan, ezta Suizako merkatarien egokitasunik be, baña bai gure artean iñon baño egokiago eta sakon eta bizikorrago suspertu eta eratutako lankidegotzaren demokrazi eta pizkortasuna.

Gure lankidegotza gure lurralde oneitan bizi geran guztiok alkarganatu eta dan-danontzako izakera gizatsuagoaren oñarri izan bearrekoa da.

Gure artean giza-eziketa lan girotan eraturik langilletzaren bidez illarteraño gizatsu ta bardintsu bizi izateko izan bearrekoa da, alkartasun kutsu bizigarritan.

(1976,

Urtarrila)

1.9.15. Gure Iraultza

Euskotarrak geuk eta batez be langilleok aurrerapide miñez ekin eta aurrera eruan bearrekoa da, geure laguntzaz oñarritzen ditugun eziketa, ikasketa eta abar, egin ditzala gure gaztediak giza-aldaketa barriotan sakonago eta trebeago parte arturik eginkeraz. Ikasleak orduren batzuetan lan dagiela lanpiderik baten. Lanpide barri orreik sortuarazoteko ezin palta izan dagi-

699

El (des)orden establecido ke bestelako langintzarako palta izan ez dan laguntzarik alkartasun eskakizunez. Gure euskotasun eta abertzaletasunak ezaugarri barriak bear ditu eta jokabide barrietara bultzatu bear gaitue.

Aurrerapiderik agiri ez duten lurralde eta basterren jaube baster orreik landu eta jaso al izango dituenak egin dezaten, gure Lan Kide Aurrezkia’k be eskurik iñori ez dio ukatuko, naiz orretan ikasleak naiz langilleak ekiten dioten, ikasketarik galdu gabe, edo bestetzuek beste lanbide edo lan-saririk galtzeke. Pizberritu eta saritu bearrekoa gure Erri osoa edergarritzeko serbitu dagikegun lanak izan bear du. Lurralde estuan bizi dan Erriak lenen egin bearrekoa lurralde ori alik ondoen lantzea da.

Lanean eta lanaren bidez zetorkigula gure nortasuna eta askatasunaren oñarririk sendoenak onerizten dogunok, lana erosotu bear dogu, batzuek lorak jorratu eta azitzeko, beste batzuek ollandak zaintzeko eta baratzak barazki gozoz jantzi al izateko, sasiak eta larrak kentzeko edozeiñek daki jakin bearrekoa, eta oraingoz jantzi gabe daudenak nola landu eta ornitu be ikasgarri eta ikasbearrekoa da.

Gure erriotako ume, gazte, emakume, zerbait egin al dagikeen guztiak Lan Kide izango gera, gure langilleri guztiaren pozgarri eta giza-lagun, gure Erri maitearen askatasun eta nortasun izpi barriakin mundu zabalean ikusi eta ezagutu izan al gengikeguzan. Auxe da Lan Kide
Aurrezkia’ren egun-abar barri gizatsua.

(1976,

Otsaila)

1.9.16. Beso eta biotz biak bat

EUZKADI herri txiki, lurralde estu eta Aberri ugaldekorra da. Izadiak eder egin ginduzan, baña lanak egin gaitu bizigarri; alkartasunak eta batasunak, naiz eta txikiak izan, indartsu eta aipagarri egiten gaitu, etxean eta erbestean, alkarrengana zintzo eta edonorentzako errespetutsu geranez.

Herri haundi eta zabalagoen artean iñork ez ginduke gutxieretziko, begirapen, askatasun eta lagunkide bezela ikusten bagaituzte. Guk gure ABERRIA jasoteko, liraintzeko eta bizigarritzeko ez daukagu iñor zapaldu edo otseinpetu beharrik eta naiko zeregin daukagu lurralde au garbitzen, jorratzen edo landutzen, danontzako txukunagoa, probetxugarriagoa eta maitagarriagoa izan dakigun. Kale eta uri barriztuak, ibar, mendi eta ibai garbiak, lurralde zabala ez dogun arren, badaukagu nun ekin eta jarrai landuten, azi eta abono zabaltzen, loraz edo zugatzez jantziten, sasiak eta larrak besterik agiri ez diran basterretan. Lotsatu egin behar genduke etxetxuetan, bakoitzak bere etxe-zulorako ainbat apaingarri, eta inguruak abandono utsean, ezertarako be zerbitzen ez dutela ikusita.

ABERRI EGUNAK zalapartatsu ospatu, ainbat abertzaletasun oiu egin eta gero ainbat lurralde eta baster ikusten diran baizen zatar ikusita lotsatu egin behar genduke. Ez lotsatu, baizik ekin eta jarrai; geurea, danona, danon artean edergarritzen, noiz eta nork ezer ordainduko ete digun zain izateke.

Gure ABERRI EGUNAREN ospakizuna IRUÑAn ez degu agerrerazi bakarrik, baster eta toki guztietan baño eta danon artean. Gure lurraldea estua dalako eta zabala ez dalako beste iñungo lurralderik baño jantziago, loratuago, garbiago eta probetxugarriago egiten agerrerazi behar dogu danok gure aberri-miña. Barriketak ez, lanak baño, jaso al izango dau gure ABERRIA. Euzkadin lanean zintzo diardogun guztiok alkar onhartu behar dogu aberkide bezela eta alkarrekin jarrai beti aurrera eta goraka. a) Entzundakoak eta aintzat artu bearrekoak.

Esan lezake, odola bakarrik ez, baita pentsakera be bai, beriala bur-bur irakiten jartzen zaigula aspaldi askori, itxumustuan aruntz eta onuntz ibilli bearrean jarriaz geienok, edo asko ta asko.

700

Euskadi
Aldi larrian bizi geralako, beste ezer baño palta aundiagoa egiten diguna, entzun edo ikusi oi ditugunak sakonago aztertzea da, zalapartadan ezer egiteke.

Naiz jolastokietan naiz ikastetxeetan eta edonun be, egunean baño egunean musika edo soñu eta jardunik aipagarriagoak izango ditugu: ez dogu uste danok batera dantzan ekiteko edo danok iñoren esanetara jartzeko izan bear dutenik. Bakoitzak jakin bear du noiz eta norekin dantzan egin bear duan eta noiz eta nortzuekin burua bat egin legikean.

Sindikato eta Politika Alderdi bakoitzakin zer egin gengikean edo nola jokatu bear genduken aztertzen ikasi bear dogu. Eta orrez gain baita alkarren artean alkarrekin aztertu eta erabakiak artu be bai, sasoiz alkar atzipetu ez dagigun eta beti alkarri alkarrekin ekindako sailletan lagundu al dagiokegun.

Badakigu bai orain eta lenago be dirua poltsikoan dutenak aize gutxi bear dutela buruan alkartasunari iges egiteko edo irri egiteko be. Gure arteko gazterik askok ez al dute elgorri au, egonezin edo urduri dakikezan aize eta eskuarte geiegi bakoitzari izerdi geiegi kosta izateke?
Beste aldetik baita izan legike bakoitzak leendik dituan eskuarte edo eskubideeri geiegi lotuak izatea eta egin bear liraken giza-aldaketak eragoztea be.

Norbaitek aspaldi esan omen zuan anarkia-bideratzen ginduzana «batzuek ezindakoa iñori eskatzea eta beste batzuek aldatu bearrekoa aldakaitz egiñarazoten alegintzea» besterik etzala.

Astirik gabe, alegiñik iñork egiteke, aurrerapiderik ez degu urratuko: sindikatuak, Alderdiak eta Kooperatibak, danak bear ditugu eta ber-barrituak betorkiguz, baña bakoitzak ikasi dagigun aurrez edo alik ondoen zein zertarako edo nolakoa zertarako bear ditugun.

Soil-soil egingo diogu giza-erakunde eta izakera barriotan kooperatibar edo biztanle guztiok nola egoki jokatu al gengiken aztertzen. b) Kooperatiba eta Alderdiak
Kooperatiba eta Alderdien artean naitanaiezkoak dira artuemanak naiz eta bakoitzak bere gisara jokatu al izateko be. Iñork uka ezindakoa da kooperatibatar zintzoa izateko ez dagola politika arazoei iges egin bearrik, eta politika-arazoetan Alderdiak izan oi dira jokabide bereziak eratu eta aurrerapidetu al dagikezan elkarteak.

Kooperatiban alkartasunean bizi diranak, alkartasunari jagokonik ukatzeke, bakoitzak izan dakigez bere artuemanak Alderdiekin bakoitzaren kontzientziari ondo derizkionekin. Aztertu eta egin bearrekoa izango litzake, bakoitzarentzat eta danontzako, kooperatibetan kooperatiba elkarteari emondako itzak edo elkarte orretan oneritziko erabaginik norberak, bakoitzak bere buruz eta egitez, bere kolkotik eta jokabidez, eragoztea edo konprometitu ezin legiken neurrian iñor konprometitzea. Kooperatibako arau eta erabagiei zor zaiotena onartuz eta errespetatuz arazook erabakitzen ikasi bear degu guztiok.

Edonok dakiana da Alderdietan eztabaidarik asko eta alkar-ezin-ikusirik ugari sorrerazi oi dana: alaz eta guzti be Alderkiak bear-bearrekoak dirake. Kooperatibetako arazoak eta egin bearrak aintzat izanez beti beste arazo eta eginbearretan parte artzen ikasi bearrean gaude geienok, naiz eta lenago olako ardurarik izaten oituak izan ez arren.

Txoper izaten be ez al dogu geienok ikasten oñez ibilli bearra izanarren? Kooperatiben izenean eta kooperatiben erantzupenekin zer ikusirik legikeenetan zur eta trebe jokatu bear dogu
«etxekalte» alderdiok izan ez dagikeguzan. «Etxealde» izan al dagikeguzan be aztertu bearrekoak izan dagikez artuemon batzuek. d) Kooperatiba eta Sindikatuak
Kooperatiben eta Sindikatuen egin bear eta arloetan artuemon aundi eta zabalagoak izan legikez EKONOMI aldetik, gure erria eta erriok aurrerapidetzeko dugun oñarri bikoitza, lurraldea izadiak damazkuna, eta lana eta lanaren eskuz indarbarritu bearreko ekonomia ditugunez eskuarte eta jokabiderik ongarrienak danontzako.

Baña Sindikatu eta Kooperatiben artean baditugu iñork
Sindikatuak ugazaba indartsuagoekin langilleak trebeago eta tuak dira eta eginbear orreri erantzun bear diote; langilleen kin erosotuaz. Kooperatibetan ez dute ugazabarik, ugazaba

uka ezindako bereizkeriak. Lenengo indartsuago jokatu al izateko sorlan-sari eta baldintzak kapitaldunebe langilleak eurak eurenez dirake-

701

El (des)orden establecido nez: orretarako kooperatibetan lan-saria iñoren errotik ezin norberetu legike, baizik norbere lan eta kapitaletik baño: kooperatibatarrak lan-utsaren bidez bizi bearra duten arren, lanaren onurak kapital biurtu dituzten ezkero, aldiko eta aurretiko lanaren onuraz bizi al izateko iñoren morroitzarik edo otseintzarik gabe langillea euren buruz eta erabagiz eratu bear dituzte lantegietako baldintza eta arau guztiak. ALKARTASUNA da kooperatibarren artean beste ezer baño aurrez eta lenago oneritzi bearrekoa: lenengotik lanean an diranen arteko alkartasuna eta batasuna, naiz eta batasun eta alkartasun orren eskabidez eta ariz beste lantegi eta langilleen artekoa be aintzat artu bearrekoa izan.

Kooperatiba eta Sindikatuak artueman sakon eta onuratsuenak izan bear dituzte giza eta ekonomi egitura aldaketak egiñarazoteko.

Giza eta ekonomi aldaketei erantzuteko kooperatibak dituzten egiturak aurrerakoi eta aurreratuak diranez, egiturok indartzen alegiñak egin eta langilleak euren buruz, iñoren bitartekotasun gabe, giza eta ekonomi erakunde barriak eruan al ditezkeena oneritzi eta agerrerazi bear dute.

Ekonomi arazo eta egin bearretan sustarrak ez dituan demokraziak lur legor eta ariñean erein eta landutako landara edo zugaitzaren antzekoa dirudi: itxuraz indartsuagoa izatez baño.

(1976,

Martxoa)

2. ¿Lucha de clases?
En ningún lugar se ha detenido Arizmendiarrieta a hacer un análisis concreto de la lucha de clases. Lo acepta desde el primer momento como un hecho, que deplora, y trata de ver cómo puede ser superado. Se podría decir que no se ha interesado por el problema de la lucha de clases más que de paso, o de soslayo, desde los temas que en cada ocasión estaba tratando y ofrecían una referencia.

Por lo demás Arizmendiarrieta considera la lucha de clases como un aspecto entre otros de la gran crisis o delirio histórico que caracteriza a la era moderna. En un mundo en el que la verdad, la autoridad, el orden, la libertad, han perdido todo su valor, o han pervertido su sentido, la lucha de clases es un efecto más, aunque sea el más llamativo y virulento. La lucha de clases es, más concretamente, un efecto del liberalismo, porque, en la concepción liberal, la libertad se ha convertido en la opresión del débil, en expresión de
Lacordaire (SS, II, 147). «La libertad sin ley favorece injustamente al poderoso y oprime al débil, al pobre que no tiene para luchar contra el otro ningún recurso y siempre se verá abatido y dominado por la necesidad. Este liberalismo ha sido indudablemente una de las causas más profundas de la presente crisis social. El mundo se ha dividido en dos grupos: por un lado encontramos inmensas masas de proletarios carentes hasta de lo más indispensable y, por otra, el grupo de los privilegiados que, explotando a esas masas, ha ido acumulando todas las riquezas» (Ib.).

Por otro lado, en la raíz de la lucha de clases se encuentran igualmente el mito de la revolución, la fe en la violencia, etc., que caracterizan especial702

¿Lucha de clases?

mente a nuestro siglo XX, en opinión de Arizmendiarrieta, y que este rechaza con no menor decisión. La cuestión de la lucha de clases se plantea, pues, para Arizmendiarrieta, como la cuestión de superarla urgentemente. No se trata ni de orientarla, ni de corregir algunos defectos que pudiera tener, o de aprovecharla para ciertos fines honestos dentro de su proyecto; la lucha de clases queda desde un principio rigurosamente excluida de su concepto. Ello no significa, evidentemente, que ignore su realidad.

Es preciso añadir que, ya desde sus primeros escritos, Arizmendiarrieta se muestra convencido de que las diversas experiencias, tanto democráticas como totalitarias, para superar la lucha de clases, han fracasado de tal modo que no queda otra solución que intentar una tercera vía (SS, II, 156).

2.1. Concepto de clase: privilegios
«La doctrina católica, que defiende la desigualdad de clases, condena la lucha entre ellas. Pero propugna la igualdad de oportunidades para todos los hombres y la justa distribución de la riqueza» (FC, I, 145). Esta expresión un tanto sorprendente puede resumirnos bastante bien el concepto de clase que emplea Arizmendiarrieta.

Aunque se proyecte como ideal lejano el principio de «cada uno según sus posibilidades, a cada uno según sus necesidades», para el momento actual el orden que Arizmendiarrieta propugna podría ser calificado de «meritocrático», si tal palabra existiera. Es indispensable que en la base exista una real igualdad de oportunidades para todos y en el otro extremo igualmente una justa distribución de la riqueza. Pero, dentro de estos dos extremos, se admiten diferencias sociales, según las «diferencias naturales», responsabilidad de cada uno, trabajo, etc., e.d., se rechaza también rotundamente todo igualitarismo. No se trata de eliminar de raíz toda diferencia, sino de asegurar que estas no tengan otro origen que el mérito personal y no traspasen los límites tolerados por la dignidad y solidaridad humanas.

Sin querer comprometerse, al parecer, en una definición universal de base económica del concepto de clase, Arizmendiarrieta asegura, casi como una evidencia, que hoy la raíz de las grandes diferencias sociales sí es económica. «Es principalmente lo económico lo que determina las jerarquías sociales en nuestro tiempo. La sangre, el talento, etc., poco vienen a representar en nuestros días» (CAS, 27). Sin embargo Arizmendiarrieta, que parece haber conocido muy tempranamente, antes que a los escritores laboristas, el
Manifiesto Comunista (CAS, 54) y tal vez El Capital (Ib. 105), sin mostrarse muy impresionado, no recalca tanto la importancia de la posesión económica como tal, sino su aspecto de privilegio. Privilegio de poder, especialmente de educación y cultura. Siempre insistirá en que el factor posesión, por sí sólo, va perdiendo importancia en la sociedad moderna. Compara nuestra socie703

El (des)orden establecido

dad, valiéndose de una imagen de Sertillanges, con los antiguos cuadros del juicio final: unos pocos elegidos triunfan en la luz, mientras los demás se agitan, o arden, o yacen somnolientos, pegados a la sombra de los sepulcros. Y juzga severamente: «Si bien en el juicio final esa diferencia está justificada por la virtud o buenas obras, en nuestra sociedad no tiene más motivo que la simple posesión, justa o injusta, de bienes y riquezas» (Ib.). Se rechazan las diferencias que vienen derivadas de privilegios, que hoy tienen ante todo fundamento económico. El concepto de clase parece, pues, estar definido fundamentalmente por la relación a los privilegios de origen. El siguiente comentario nos muestra bien el sentir de Arizmendiarrieta: «La burguesía, mientras ha sido creadora, operante o promotora, ha sido aceptada. ¿Quién la tolera cuando se dedica a vivir a expensas de lo que los otros hacen sin contribución al riesgo y a la aventura? Si estimamos que ha pasado su hora es porque ha adoptado el papel de Sancho» (FC, II, 247). Queda también patente el lugar que el idealismo y la acción ocupan en la filosofía de Arizmendiarrieta.

Se comprende que la «lucha de clases» particular que combatirá Arizmendiarrieta se resuma en el grito de «abajo los privilegiados» (FC, I, 30). Se impone reformar el concepto de herencia (Ib.; cfr. cap. III, 1); ofrecer igualdad de oportunidades de educación y socializar la cultura (FC, I, 31, ss.); revisar la escala de diferencias salariales, etc. «Tenemos que pensar en vivir de nuestro trabajo, iniciativa y esfuerzo» (Ib. 30).

Sin embargo el trabajo mismo debe ser «rescatado». Porque tal como se da hoy en la sociedad es la primera fuente de las diferencias sociales. Por un lado, se reconoce que el trabajo es el medio natural del hombre para proveer a la vida propia y de sus hijos, pero el «derecho al trabajo» no es tal donde unos tienen que mendigar la posibilidad de realizar trabajo alguno y donde las condiciones de trabajo no se corresponden con la libertad y dignidad humanas (CAS, 28-30). Y, por otra parte, donde el trabajo es entendido y valorado solamente en función del lucro, deja de ser un medio de humanización y un móvil de socialización, para convertirse en causa de luchas sociales. «Las comunidades de trabajo constituidas en clases antagónicas es la forma de guerra más atroz. El trabajo necesita ser rescatado considerando para él por unos y otros, por todos, como algo en que se juega algo más que la suerte y el interés personal (?). La solidaridad, que une a los hombres, convierte el trabajo en una fuerza de liberación bajo todos los conceptos. Cuando el afán de lucio es el eje de la estructura y desenvolvimiento de las fuerzas de trabajo, estamos despojando al trabajo de su mejor timbre de gloria. Por otra parte en esta situación es inevitable una lucha de clases y de hombres entre sí» (FC,

I, 38-39).

Una vez más Arizmendiarrieta combatirá los privilegios no sólo por injustos, sino por considerarlos perjudiciales para la sociedad en su conjunto y para su mejor desarrollo. El bloqueo de los puestos clave, sea en las empresas, sea en la administración, etc., asignando tales responsabilidades no en función de una auténtica eficiencia, sino condicionándolo a exigencias extrañas, de familia, de partido o grupo de presión, es una enfermedad muy de

704

¿Lucha de clases?

nuestros días y ha corroído en buena parte las bases de la dinámica del progreso. «No pocas veces hemos criticado agriamente esos formulismos hereditarios y de clase, que anquilosan las empresas y con ellas el sueño de muchos trabajadores. Hemos considerado dudosa la defensa ante la moral de la cesión de trastos a la gente inepta. No sólo se juegan sus intereses particulares»

(FC, II, 42).

Este concepto de clase tan estrechamente ligado al de privilegio puede hacer más comprensible que Arizmendiarrieta en sus últimos años se haya referido a los jóvenes con carrera como a una clase privilegiada, en relación a la generación de sus padres, que ha hecho posibles sus estudios; o a los ancianos y a los niños, como a la clase desaventajada. «¿Debe ser un simple sueño la sociedad sin clases? —leemos en uno de sus últimos escritos. Los hombres, por encima de profesiones, edades y actividades circunstanciales, deben resolverse a vivir la experiencia de una “comunión” humana más rica en reciprocidades crecientes, sin dar lugar a que lo que tal vez lamentamos y desdice de nosotros en concepto de lucha de clases (discriminaciones por lo que se tiene, más que por lo que se es), sea reemplazado por las reflexiones derivadas de simple edad, destacando en un polo sin alegría el ejército anónimo de jubilados, de la tercera edad, abandonados a su suerte, sin ligazón con quienes pudieron aprender no poco de ellos, o al menos disfrutar de su tensión vital; y el otro polo, la primera edad, condenada a introducirse en la vida armada para la competencia y la lucha despiadada; y en el centro, los que pueden mantenerse por tiempo limitado y carentes, o al menos condenados a existir con el simple disfrute material de cosas, en lugar de comunión con personas»

(CLP, I, 294).

2.2. Privilegios culturales
Insistimos en que la lucha de clases no se le ha planteado a Arizmendiarrieta como un objeto a definir y analizar académicamente, sino como un problema concreto a superar, tal como se daba en su derredor. Su concepto de clases ofrece, por lo mismo, aspectos que son debidos sin duda al contexto concreto en que Arizmendiarrieta desarrollaba su labor. Ya se ha indicado anteriormente la identidad de intereses de clase entre el proletariado y el pequeño campesinado agrícola. Acabamos de ver que, en sus últimos años; la vieja lucha de clases le parecía suplantada (a nivel de sociedades cooperativas, sin duda) por la cuestión de las tres edades, o (a nivel internacional) por las diferencias entre países ricos y subdesarrollados; regiones ricas y pobres
(nivel interregional). Y, aun a fuerza de repetirnos, debemos subrayar que
Arizmendiarrieta ha insistido más en las diferencias culturales que en las económicas como último fundamento de la hegemonía de la burguesía sobre el proletariado, sin duda porque su propio programa de acción exigía empezar por una acción educativa para, a partir de aquí, llegar a la emancipación

705

El (des)orden establecido

obrera. En una perspectiva de acción y emancipación obrera Arizmendiarrieta juzgaba las diferencias culturales como el primer y básico elemento definitorio de las diferencias de clase, como el primer obstáculo a superar, aun admitiendo que en otro orden la raíz fundamental fuera económica en última instancia. Nos limitaremos a transcribir una vez más sus propias palabras

(1950):

«Herencia más triste del mundo que nos ha precedido hemos de considerar la falta de oportunidades para educarse y formarse que la de las desigualdades económicas.

Las desigualdades económicas, cuando en su base y desenvolvimiento no coinciden con las desigualdades sociales de educación y cultura, poco representan en el desenvolvimiento de la vida social y además están condenadas a su desaparición automática. Las desigualdades económicas que hoy gozan del amparo de los privilegios y exclusivismo de las oportunidades de cultura y educación son las que condenan a la humanidad a la subsistencia de castas cerradas y clases antagónicas, sin perspectivas de solidaridad y hermandad común.

Por eso que la consigna social más en armonía con las exigencias del momento actual, la consigna social cuya realización ha de formar una evolución incontenible en el campo social es la socialización de la cultura. Sí, queridos mondragoneses, la socialización de la cultura es una medida previa, indispensable para que se mitiguen y terminen por desaparecer esas barreras de clases y esos monopolios de privilegios de nuestra sociedad. Mucho más importante y más interesante que la socialización propiamente dicha de las riquezas que, por otra parte, aunque estén detentadas por unos u otros para los efectos de la vida social no tiene importancia, si es que se ha llevado a la práctica la primera reforma de la socialización de la cultura.

Hace tiempo que un conocido líder obrerista decía que cada escuela que se levantaba era una cárcel que se cerraba. No vamos a ser tan ingenuos que vayamos a pensar que basta abrir una escuela que proporcione cultura, simple instrucción, para que se cierren automáticamente las cárceles como innecesarias, pero sí podemos afirmar que cada centro de formación que se pone a disposición y al alcance del pueblo, de los jóvenes en esa edad crítica de los catorce a los veinte años, es un nuevo baluarte de la causa de la justicia social que se conquista, un baluarte que les abre nuevos campos de influencia en la vida social, baluarte que les ha de permitir velar mejor por sus intereses y derechos, baluarte que ha de contribuir a la transformación de la masa de proletarios en pueblo disciplinado, organizado, consciente y, por tanto, en sociedad sana, libre y dueña de sus destinos.

Y en definitiva quién va a perder nada si se nos garantiza la posibilidad de vida en un ambiente de convivencia, de respeto mutuo, en una sociedad en la que el que trabaja tiene derecho a los medios de subsistencia y el que no puede hacerlo, por estar impedido por algún motivo, tiene también asegurada la asistencia de la comunidad, que se ha organizado con miras precisamente para dar a cada uno lo suyo, sin echar en olvido a nadie que necesite del concurso ajeno. Tal vez lo que pasa es que tenemos poca fe en la posibilidad del desarrollo de este género de civilización y, sin embargo, hoy podemos afirmar que, o se hace un esfuerzo para crear estas condiciones sociales de vida, o, lo demás, va al traste nuestra civilización. O evolucionamos decididamente hasta el grado de crear estas condiciones, o seremos arrollados por una revolución que no vamos a pensar que va a ser detenida por las armas, porque es algo que afecta al espíritu de la justicia, de igualdad y verdad, y es algo incontenible por los muros de tanques» (EP, I, 84-86).

2.3. Colaboración de clases
Si la lucha de clases juega un papel mínimo, o está prácticamente ausente, en el pensamiento de Arizmendiarrieta es porque un concepto básico y fun706

¿Lucha de clases?

damental en él es exactamente el contrario: colaboración. Colaboración de clases, de pueblos, de instituciones, colaboración universal.

Se podrán aducir razones externas, como la situación de guerra o de postguerra, etc., que expliquen la importancia fundamental que este concepto decisivo tiene en el pensamiento de Arizmendiarrieta en invariable constancia desde sus primeros a los últimos escritos. La razón inmanente, más profunda, parece radicar en su visión del hombre y de la humanidad. No se podrá comprender la actitud de Arizmendiarrieta ante la lucha de clases sin atender a su filosofía del hombre. Su antropología filosófica es la que fundamenta sus doctrinas sociales e incluso económicas. En la visión de Arizmendiarrieta la humanidad, unida estrechamente en trabajo solidario, va caminando hacia un Punto Omega de perfección. La grandeza humana reside en el espíritu solidario de unión y trabajo. La lucha de clases tenía que aparecer a los ojos de Arizmendiarrieta como contraria a la naturaleza misma del hombre.

Tampoco puede olvidarse el análisis de la gran crisis que hace Arizmendiarrieta. La pérdida de autoridad, ideales, etc., los diversos aspectos de la misma pueden ser reducidos a la pérdida de unión y orden, a la disgregación social y al caos (SS, II, 159, 275, 285). La humanidad ha ido descomponiéndose, luego destruyéndose mutuamente los diversos grupos, clases o países, hasta poner en peligro la existencia de la civilización y del mismo género humano. Y es la unión, la colaboración, lo que la humanidad necesita para reconstruir un orden nuevo de libertad y justicia. Este es un axioma fundamental en el pensamiento de Arizmendiarrieta, que deberemos desarrollar en su lugar más detenidamente.

Por último, Arizmendiarrieta se ha podido sentir, al menos por unos años, decisivos para su actividad, confirmado por la experiencia en esta postura suya. «Creemos, nos dice en un escrito que suponemos de hacia 1950, que puede presentarse como un testimonio de lo que pueda emprenderse con este espíritu y criterio de colaboración, lo que se ha logrado en un pequeño pueblo industrial de Guipúzcoa, en Mondragón (...). Es indudable que algo se ha hecho y la causa de lo que se ha hecho no la vamos a buscar propiamente en unas personas, sino en el espíritu de colaboración sincera de patronos y obreros, del pueblo y de la iglesia, y podemos decir que cabe a todos la satisfacción y el honor de lo que se ha hecho». A continuación enumera (CAS,
146 ss.) dichas realizaciones.

Arizmendiarrieta, pues, no llamará a la lucha, sino a la colaboración y cooperación de las clases. «Nuestra fuerza no se traduce en lucha, sino en Cooperación» (FC, IV, 79). Así lo expresa, bajo el título de Cooperación, en el siguiente texto de 1949:
«He pronunciado una palabra a la que quisiera dar toda la expresión y relieve que se merece, pero es tal la importancia que tiene en la vida social lo que ella expresa que temo quedarme corto en mi afán de subrayarla. Ella es la que señala la única ruta por la que es posible avanzar; es la que señala el único camino por el que se puede alcanzar la verdadera justicia y paz social. El individuo o la colectividad que no ende-

707

El (des)orden establecido reza sus pasos por esta ruta, cuando menos perderá tiempo, pero de ordinario no encontrará más que perdición y ruina.

Nos necesitamos los unos a los otros, estamos llamados todos a complementarnos mutuamente. El hombre capaz de soportar la soledad es un dios o una bestia, como afirmó un célebre filósofo. Y esto quiere decir que las clases se necesitan y deben colaborar; esto quiere decir que el pueblo y las autoridades no deben vivir divorciados.

Esto quiere decir que las instituciones deben prestarse mutua ayuda, quiere decir que cuando lo que sinceramente se persigue es aquello que se confiesa, o sea el bien común, el bien de todos, no tienen sentido los exclusivismos, las particularidades, los personalismos, aunque pretendan encubrirse con las etiquetas, y las razones más especiosas no ocultan nada más que vanidad, orgullo o afán de dominio. A este propósito no basta que los patronos emprendan y hagan cosas buenas, hace falta que participen en las mismas los obreros, para que entre ellos exista una verdadera comunión; no basta que los obreros sueñen en grandes reformas, hace falta que los patronos o empresarios concurran a las mismas aportando su celo, su técnica, su experiencia. No basta que las autoridades se propongan grandes objetivos, pues para alcanzarlos siempre hará falta algo más que lo que tienen ellos en sus manos, o a su alcance, que será el calor, el entusiasmo, el celo de los súbditos. Donde no se haya realizado esta fusión y se haya llegado a esta colaboración espontánea y generosa no hay propiamente vida social y será difícil que en ese ambiente haya fecundidad social. La misma paz existente será superficial, o cosa ficticia.

Las coronas de gloria que por esos derroteros de divorcio o de indiferencia mutua se ciñan, tanto los unos como los otros, no merecerán más consideración y respeto que las de farsa o comedia con que se ciñen los personajes que actúan en un escenario. Es muy efímero, o tal vez no existe para nadie más que para quien se ha ceñido esa gloria o ese reinado. El verdadero monarca no es el que se ha ceñido la corona, sino aquel a quien le han ceñido los demás.

Tenemos que renunciar al afán de apuntar tantos cada uno por su lado para proceder todos a una y convivir en un ambiente de mutua correspondencia de servicios y sentimientos, para constituir una verdadera comunidad dirigiendo todos nuestros esfuerzos al logro del bien común, ya que en eso consiste precisamente la sociabilidad del hombre. Y conviene que no olvidemos que a sacrificios comunes deben corresponder satisfacciones también comunes» (EP, I, 80-82).

La dificultad que se les presenta a las masas trabajadoras es precisamente, a los ojos de Arizmendiarrieta, que no son admitidas por las clases dirigentes a cooperar dignamente, como personas, en este gran quehacer de la comunidad. Deberán, pues, imponer por la fuerza su colaboración. Y la fuerza del proletariado radica en la unión y el trabajo. Como puede verse, es una concepción diametralmente opuesta a la lucha de clases. El concepto dominante es el de cooperación, no el de lucha. Considerará que el movimiento obrero ha quedado estancado en planteamientos decimonónicos que hoy se deben ya superar.

2.4. Replanteamiento de las aspiraciones obreras
Arizmendiarrieta se opone a que el trabajador se entienda a sí mismo como proletario, como clase; debe considerarse ciudadano y asumir las res708

¿Lucha de clases?

ponsabilidades que le corresponden como a tal. Esto significa un replanteamiento de lo que hasta hoy ha sido la estrategia de la lucha obrera.

«En otros tiempos se ha apelado a la unión de los trabajadores para la defensa de derechos elementales, para evitar una explotación inicua, para acabar con los abusos.

Hoy tenemos que apoyarnos en las fuerzas que nos da la unión para asegurar nuestra promoción al plano y nivel que corresponde a la dignidad del trabajo, para situarnos en la vida social y política como ciudadanos normales, ya que con padrinazgos extraños siempre aparecemos como menores de edad o segundones. Pero este estado de discreta “servidumbre” no puede tener término mientras un factor tan fundamental de desarrollo y actividad como es la inversión sea un fenómeno extraño a nosotros: no podemos contemplarlo como si fuera algo sin mayor repercusión e importancia en nuestras posibilidades actuales y futuras, sino como el elemento clave de acción y de superación. De ahí la necesidad urgente de que nos impliquemos en la misma, y podemos hacerlo mediante un desglose claro de las utilidades de la empresa que en cuanto son imputables a nuestra colaboración, es decir, a nuestro trabajo, han de ser objeto de un desglose en salario de consumo y de inversión. Desde este momento podremos decir que nosotros estamos velando por el futuro de la empresa no menos que los propiamente llamados empresarios, que siempre tienen a mano un recurso para bloquear o condicionar nuestras rentas apelando a la necesidad de financiar el desenvolvimiento de la empresa. Que en lo sucesivo el empresario cuente con nosotros para el trabajo y la financiación» (CLP, III, 137).

Los trabajadores se muestran muy sensibles ante conflictos laborales aislados, como despidos de compañeros, etc. Sin embargo, mientras no sean capaces de arbitrar otras soluciones y providencias para respaldo de sus representantes, que pueden ser castigados al exhibir ante los patronos las reivindicaciones de los trabajadores cuya representación ostentan, van a ser muchos los riesgos de su empeño de superación y necesariamente serán limitados los resultados de este esfuerzo. Si los trabajadores se convierten en inversores, su situación cambiará radicalmente. «Para modificar sustancialmente las condiciones laborales actuales y, sobre todo, la consideración que nos merecemos los trabajadores, no necesitamos esperar mucho tiempo si es que desde ahora decidimos retener en nuestro poder para destinarlo a la inversión el importe de una o dos horas diarias de nuestras rentas de trabajo»
(Ib. 142). La presión de las finanzas se acusa en todos los ámbitos, en todas las instituciones, en los propios gobiernos y regímenes más variados. «No cabe duda que los grupos financieros son los dictadores más hábiles o funestos de una comunidad» (Ib.). Los trabajadores no podrán lanzarles un desafío, pero sí podrán actuar con habilidad y prudencia, que a la postre les podrán resultar más útiles.

«Hoy mismo a no pocos de entre los trabajadores, a los de conciencia social más desarrollada, a los que tienen un sentimiento noble de su dignidad, les afecta no poco la desigual suerte de cuantos colaboran en el seno de la misma empresa a la hora de ponderar los resultados imputados a unos y a otros.

Sabemos que el sector trabajo —los que sólo disfrutan de rentas salariales—, llega a posiciones que distan mucho de lo que por su implicación socio-económica pueden imputar a su discreción las utilidades de la empresa. Las desigualdades de clase y de sectores son irritantes hoy.

Si entre nosotros el desarrollo se sigue llevando a cabo sin profundas reformas estructurales, sabemos desde ahora que al cabo de diez o doce años estas desigualdades

709

El (des)orden establecido actuales van a ser mucho más profundas e insoportables al cabo de dicho período.

¿Vamos a confiar simplemente en que un día con un golpe de fuerza pueda trastocarse esta realidad? Aun dando por supuesto que ello constituyera un último remedio para el caso de que la inconsciencia actual del capitalismo o sus posiciones se fueren reforzando pese al interés de los trabajadores, no estamos excusados de hacer presente nuestra postura como testimonio de que al menos somos conscientes del proceso que conduce la vida económica-social» (Ib. 138-139).

Todos los análisis económicos que se hacen de todos los procesos de desarrollo de cualquiera de las comunidades humanas, dice Arizmendiarrieta, muestran con unanimidad que no deja lugar a dudas, que el factor preponderante es el trabajo. Esta preponderancia del factor trabajo sobre otros factores, como pueden ser la naturaleza o el capital —como trabajo fósil— es tal que se atribuyen al mismo en un 80 u 85% los resultados. «La masa de millones de trabajadores no podemos renunciar a esta conciencia de nuestra dignidad y honor con la correspondiente participación en todo el proceso económico-social y, por consiguiente, nuestra implicación en la mecánica del desarrollo conscientemente, por el hecho de que de momento ello suponga una atenuación de nuestras posibilidades inmediatas de “despensa”, es decir, de inmediatas rentas monetarias aplicables al consumo» (Ib. 135).

«Es preciso que estemos resueltos a ser algo más que consumidores más o menos afortunados: debemos llegar a ser también inversores, ya que como simples consumidores lo que en definitiva hacemos es dar a nuestros propios explotadores con una mano lo que tratamos de restarles con la otra. Tenemos dos manos y debemos aceptar la responsabilidad de dos funciones que necesitan estar acompasadas, la del consumo necesario para reponer nuestras fuerzas y compensar los esfuerzos y la de la inversión, indispensable para mirar por nuestro porvenir y ejercer una solidaridad entre las generaciones. Y para que podamos ejercer esta función de inversores, necesitamos de la unión tanto o más que lo que hayamos podido necesitar para asegurar una subsistencia decorosa» (Ib.). Parte de nuestras disponibilidades pueden ir y tienen que ir al consumo. Pero otra parte va siempre a la inversión, forzosamente, sea por nuestro propio conducto o por conducto de nuestros padrinos de empresa o de gobierno. Los trabajadores deben adquirir conciencia de que de hecho están resultando inversores y deben serlo por sí mismos y conscientemente.

«Hagamos acto de presencia en la plataforma social como mayores de edad y ciudadanos plenamente responsables asumiendo sin intermediaciones innecesarias la responsabilidad de la inversión, que al fin y al cabo se nutre con parte de las rentas que se deben a nuestra colaboración» (Ib. 136).

Naturalmente la actividad inversora no basta para asegurar la plena ciudadanía de los trabajadores: debe ir acompañada de la igualdad de oportunidades de cultura para todos y de la socialización del saber.

En la concepción de Arizmendiarrieta la Caja Laboral Popular está llamada a ser el Banco de la clase trabajadora, no sólo de las Cooperativas y de los cooperativistas, con el propósito expreso de que aquella se vaya adueñando de los bienes de producción con los que desarrolle su trabajo:

710

¿Lucha de clases?
(...) «Criterio importante en la administración de Caja Laboral Popular constituye su atención casi exclusiva a las actividades productivas, lo cual asimismo no deja de tener mucha trascendencia en el proceso de emancipación social y económica del hombre, haciendo asequibles los bienes que se reproducen con preferencia a los que se consumen, por el efecto multiplicador que tiene la posición de aquellos y por la contribución que mediante la socialización de los bienes de producción se realiza en cuanto que de esta forma se elimina de raíz el origen de las clases y el distanciamiento de las mismas, inevitable mientras se mantenga una discriminación estructural a través de la posesión de la distinta naturaleza de bienes.

¡Cuántas cosas podríamos decir sobre lo que representa el trabajo como manantial de promoción constante y progresiva de los pueblos y de los hombres! Los bienes generadores de renta deben ser accesibles al trabajo y al trabajador, que para acabar de liberarse de los riesgos de servidumbre que con todo le amenazan constantemente necesita apoyarse en un proceso de asociación constante y progresivo y Caja Laboral da cauce a este proceso asociativo de entidades para que se evite asimismo el riesgo de la servidumbre colectiva, que no queda salvada por el simple hecho de que los hombres en las organizaciones de base hayan superado la individual en primera instancia.

Caja Laboral Popular es una convocatoria de solidaridad humana dirigida por unos sectores a otros, por unas comunidades a otras, por unos pueblos a otros, por unas generaciones a otras» (CLP, I, 161).

2.5. Sociedad cooperativa, sociedad sin clases
En el trance actual, para unos no hay otra alternativa que la lucha de clases: a eso se les apela a los trabajadores, que en todo caso precisarán de sacrificio y de disciplina. Los cooperativistas, a diferencia de la lucha de clases, proponen la «lucha empresarial», que no será menos esforzada que aquella, ni exigirá menos sacrificio y disciplina (FC, III, 304).

Los trabajadores que quieran realmente superar su dependencia de la clase poseedora de bienes de producción no tienen otra alternativa que constituirse ellos mismos en trabajadores-empresarios, lo que significa que deberán aportar su trabajo y el capital necesario al buen funcionamiento de la empresa. «La empresa requiere capital en el origen, mantendrá tal exigencia en su desenvolvimiento y por ello quienes trataren de ser empresarios precisarán concurrir en el supuesto de la sociedad sin clases con su trabajo y sus recursos económicos. Su implicación será indesdoblablemente social y económica. Por ello deberán ser asimismo acreedores a las compensaciones derivadas de sus servicios que consistirán en aportaciones laborales y económicas» (CLP, III, 227).

Independientemente de que en otros sectores se pueda o no propugnar con éxito la colaboración entre patronos y trabajadores, las cooperativas mismas actúan «con el pensamiento puesto no tanto en colaboración de clases cuanto en la promoción de una sociedad sin clases» (Ib. 225). Es preciso dis711

El (des)orden establecido

tinguir: no sólo se aspira como objetivo final a una sociedad sin clases, sino que se rechaza también como medio la lucha de clases, negándose a «levantar parapetos para la defensa sin transformación más radical, sin participación global y universal de todos en las tareas» (CLP, I, 293). La estrategia cooperativa se basa en la unión y el trabajo de quienes aspiran a protagonizar personalmente su propia emancipación: «Promoción integral, individual y comunitaria, protagonizada por el pueblo, por los propios sujetos afectados, se hace viable a través de la cooperación, en marcha hacia una nueva sociedad sin clases, fraternal, dinámica y justa» (Ib. 296).

Aunque el antagonismo de clases queda formalmente superado en las cooperativas, quien, como Arizmendiarrieta, ha destacado sobremanera la importancia de la conciencia, no podía darse por satisfecho con un cambio de estructuras. La lucha de clases puede sobrevivir en la cooperativa, máxime donde esta se desarrolla inmersa en un mundo cultural y político de cariz capitalista, larvada bajo las exigencias reales de la tecnología y de la burocracia
(FC, IV, 68). No se podrá decir, por tanto, que las diferencias de clase han quedado eliminadas o han sido superadas por el mero hecho de que el trabajador haya accedido a la condición de propietario de los bienes de producción. La sociedad cooperativa tendrá que ser algo más que una determinada forma de organizar el capital y el trabajo: una comunidad real o, como gustará de decir Arizmendiarrieta, una familia.

3. Contra el igualitarismo
«No se puede desconocer la nobleza que puede entrañar el afán por la igualdad en un mundo de erupciones individualistas al margen de toda solidaridad. La consolidación estructural de las posiciones individualistas con la consagración de diferencias que no responden propiamente a méritos personales, implica la aceptación de privilegios que hoy no toleran los espíritus nobles. Será un orden caduco el que se basa en cimiento tan poco consistente.

Sobrevivirá en tanto lo ampare una fuerza coactiva» (FC, I, 129).

El cooperativismo trata de promover para el futuro y trata de realizar ahora mismo en su seno la sociedad sin clases. Pero sociedad sin clases no significa sin diferencias. Al contrario, una sociedad realmente solidaria debe saber aceptar las diferencias, aprovechándolas para el bien común. La habilidosa danza de la cuerda del violín, dice Arizmendiarrieta, serviría poco para recrear nuestros oídos si no compartiera su actividad la caja de resonancia.

La batuta del mejor director de orquesta significaría poco sin la colaboración de todos y de cada uno de los músicos intérpretes. En la práctica resulta difícil, por lo regular, valorar lo que es propiamente de atribución personal, porque siempre nos encontramos enlazados a otros en comunidad. Pero tampo712

Igualitarismo

co puede entenderse la acción comunitaria olvidando la participación personal diferente de cada uno de sus miembros.

No podemos pensar que la solidaridad humana no tenga otra versión auténtica que el igualitarismo. «La solidaridad nuestra debe ser compatible e incluso debe implicar la aceptación de diferencias cuando estas responden a cualidades personales y, por tanto, a la contribución de cada componente de la comunidad al interés de esta. Las exigencias de la solidaridad hay que conjugarlas con las de la justicia» (Ib.).

Solidaridad e igualdad no pueden significar renuncia al enriquecimiento que significan las diferencias, so pena de condenar la sociedad a una vida lánguida y mediocre. Más bien «es preciso que la solidaridad sea un marco suficientemente flexible y amplio para aceptar y poner en juego las diferencias como resortes dinámicos, para que la superación se produzca como fuerza vital» (Ib.).

Aún reconociendo las dificultades que ello comportaba, Arizmendiarrieta deseaba en las cooperativas un orden social progresivo de reducción de diferencias. Quería, sobre todo, que estas fueran aceptadas solamente en función de las necesidades de promoción o estímulo de aquellos valores y cualidades personales que sean requeridas por un orden social dinámico al propio tiempo que humano.

Pero se oponía duramente a los igualitaristas, nuevos brujos, aventureros o manipuladores a sueldo, curanderos con facultades milagreras y genialidades de mago, que protestaban contra toda diferencia, por mínima y justificada que fuera. Parece que las ideas igualitaristas tomaron auge con motivo de las valoraciones de puestos de trabajo, en 1973, aunque se procedía a ello, según nos dice Arizmendiarrieta, «con módulos objetivizados en funciones, que por sí mismos sirven para desvanecer cualquier riesgo de arbitrariedad o personalismo» (FC, IV, 182-183). La dureza de las expresiones de Arizmendiarrieta hace suponer dificultades nada despreciables. Tampoco se puede olvidar que, en un contexto cooperativo, el igualitarismo fácilmente puede pretender aparecer como espíritu de auténtica solidaridad y pureza de doctrina, criticando el realismo como claudicación y abandono de los principios.

Prescindiendo ahora de cuestiones concretas (peligro de fuga de técnicos a empresas donde tuvieran ganancias más fáciles y mayores, etc.) y de las soluciones que en cada caso se hayan podido solventar, veamos la posición de principio que Arizmendiarrieta mantenía a este respecto.

3.1. Igualitarismo contra espíritu cooperativo
Arizmendiarrieta se opone al igualitarismo por considerarlo injusto, antisocial, insolidario, ineficiente.

713

El (des)orden establecido

En primer lugar, el igualitarismo es injusto, dice Arizmendiarrieta, aunque pretenda parecer lo contrario: «pues es necesario atender al ser del hombre que es por esencia diverso, en capacidad y aptitud» (FC, II, 256).

En segundo lugar el igualitarismo no tiene nada de social, como pretende aparentar. Porque «lo social es que la empresa sea competitiva y progresista, y lo es en la medida que acepta y sabe seleccionar a sus hombres con cierto sentido diferenciador que responde al objetivo de estimular y desarrollar su capacidad de imaginación, cebando un tanto la ilusión del poder y hasta cierto punto del confort, que es admisible en la medida que el cuadro opcional es plural y libre para todos» (Ib.).

Es insolidario asimismo. Porque la solidaridad, para quien interpreta correctamente la cooperación, no es una simple convergencia de intereses personales o una simple agregación de los mismos, sino creación de una nueva fuerza fecunda y dinámica, multiplicadora y prometedora. La unión de la que precisa la solidaridad no es un concierto de afanes y proyecciones igualitarias; debe ser una aportación realizada para hallarse al término del esfuerzo, compartiendo noble y generosamente, al límite de las respectivas facultades, con unas posibilidades que siempre transcienden lo que pudieran dar de sí las simples sumas unitarias y conciertos concebidos y realizados con racanería o mezquindad individual. La cooperación entraña una vitalidad y una pujanza tales que difícilmente cabe se generen y se mantengan donde prevalecen aires de igualitarismos, «de ordinario medianos camuflajes de individualismos y egoísmos» más que fruto de la conciencia de cooperación y solidaridad (FC,

IV, 175).

3.2. Igualitarismo y eficacia
La masa, según observa Arizmendiarrieta, antepone por instinto la igualdad a la eficacia, quizás en revancha de las desigualdades aparatosas e injustas de la sociedad en que vivimos. En este sentido el igualitarismo es una tentación permanente de toda masa humana (FC, II, 256).

Sin embargo, tan pernicioso como la falta de solidaridad resulta el igualitarismo, que se resiste a toda discriminación profesional y económica, para un buen régimen cooperativo. Arizmendiarrieta admite que puedan surgir tensiones entre la estructura social, por un lado, que pretende asegurar la participación equitativa de los cooperativistas en los resultados, y la estructura empresarial, por el otro, que responde a la necesidad de la eficiencia y a la obtención de los máximos resultados (CLP, III, 155). Es preciso aceptar las leyes de la eficiencia tal como vienen dadas, mientras no esté en nuestras manos el cambiarlas. «No es asunto de hombres de buena voluntad. Hace falta más» (Ib.).

«No olvidemos, concluye, que todo gozo de libertad y ejercicio de participación en la gestión exige el reconocimiento indispensable de las leyes del

714

Igualitarismo

juego de la eficacia, y en este sentido tenemos que aceptar un régimen discretamente diferenciador a pesar de que provoque insatisfacciones, so pena de incurrir en la inconsciente trampa de un paraíso sin tensiones, que la realidad se encargará de desvelar como artificioso e irreal» (FC, II, 256).

3.3. Desigualdades naturales
El concepto mismo de organizar una sociedad implica, dice Arizmendiarrieta, que se quiere organizar la desigualdad humana. Partiendo de ahí, la organización exigirá que unos se sitúen en la escala jerárquica de mando, de decisión, otros deberán tener otras responsabilidades. «Unos dirigen y otros obedecen» (FC, II, 148-149). Se podrá discutir lo que se quiera sobre quiénes son merecedores de una u otra posición, pero las distinciones mismas deberán ser admitidas.

El movimiento cooperativo, en respuesta a las exigencias de una dinámica humanista, alienta la igualdad opcional de partida. Pero aparte otras diferencias (culturales, etc.), que podrán ir superándose, Arizmendiarrieta aduce contra el igualitarismo las diferencias naturales, e.d., «el fenómeno indescifrado de las diferencias personales, imposible de salvar tan sólo por el concurso de las circunstancias favorables», modo de ser personal que condicionará luego, en buena medida, el propio futuro (Ib. 148).

Sin defender desigualdades desaforadas, fruto de arbitrarios accidentes históricos o códigos morales artificiosamente elevados a título de dogmas en interés de clases privilegiadas, indispuestas a todo cuanto pueda lesionar las normas que aseguran su acumulación de plusvalías, tampoco puede abogarse por una radical filosofía igualitarista, «pues nadie puede engañarse de un igualitarismo inexistente en la propia Naturaleza, que clasifica por sus propias leyes la gama de capacidades sin que nosotros tengamos ningún acceso sobre la composición genética, que determina una u otra diferenciación natural» (FC, III, 21).

El argumento de las diferencias naturales es, en principio, tan general, que no parece tener otro objeto que el de probar el absurdo de un igualitarismo extremo. Sin embargo, ha habido al menos un caso concreto de «diferencias naturales» que no ha dejado de provocar problemas y en el que Arizmendiarrieta ha tenido que intervenir en el sentido contrario del que acabamos de ver: es el caso del trabajo de la mujer.

Arizmendiarrieta ha tenido que intervenir aquí en una «discusión interminable», en la que, según nos dice, el argumento de las diferencias naturales era utilizado tanto por ellos como por ellas a favor propio, precisamente restando importancia a dichas diferencias. Mientras los hombres pretendían que algunos trabajos, por ser privativos de ellos (ciertos puestos de fundición, etc.), fueran acreedores a mayor valoración (PR, II, 82), algunas mujeres

715

El (des)orden establecido

pretendían reservarse, «con estudiado egoísmo», determinados puestos (de bata blanca) por razón de su naturaleza femenina, exigiendo igualmente valoración especial por ser estos trabajos privativos de las mujeres (Ib. 83).

Para que la discusión no se pierda en subjetivismos irremontables Arizmendiarrieta hace valer el principio de que la mayoría de los trabajos son indistintamente ejecutables y, por lo demás, el mero hecho de que unos puestos sean más apropiados según el sexo no puede suponer que automáticamente tales puestos sean acreedores a mayor o menor valoración. «La especialización a veces tiene contenido de valor-trabajo, y en otros es simple disposición natural no diferenciable por esta razón» (Ib. 84). Es de interés, sigue Arizmendiarrieta, una clara definición de puestos de trabajo vedados, para que los ejecutivos operen con libertad y sin las trabas de la tradición sobre el tema. «A partir del reconocimiento de la paridad hombre-mujer, todos los puestos, salvo los específicamente prescritos por razones médicas y que lógicamente deben estar recogidos en la valoración con índice más alto, se desempeñarán indistintamente, sin discriminación alguna. Repetimos, sólo son válidos los argumentos de carácter fisiológico, cuya definición corresponde al servicio médico» (Ib. 83).

3.4. Desigualdades «domesticadas»
Explicando la experiencia cooperativa de Mondragón, escribía Arizmendiarrieta para la revista Acción Empresarial: «La normativa elaborada y aceptada evitaba incurrir en posturas igualitarias como de caer en actitudes colectivistas, huyendo de esta forma de individualismos como de diferenciaciones indiscriminadas o inhibitorias. De esta forma se perfiló la comunidad de trabajo y es así como surge la empresa cooperativa en esta experiencia: es exponente de una solidaridad que sincroniza la promoción individual con la comunitaria, los estímulos individuales con posibilidades colectivas. Esta solidaridad adopta para su encarnación efectiva y objetiva un índice de clasificación personal y social, cuyo margen oscila entre el uno y el tres, aplicado en coeficientes fraccionados que se objetivizan con arreglo al puesto laboral o profesional que cada uno ocupa en la empresa, cuya valoración se efectúa con arreglo a un código elaborado y aprobado con arreglo a las normas adoptadas y órganos competentes. Cada miembro ejerce sus derechos sociales y económicos con la base del respectivo coeficiente profesional» (CLP, III,

236).

La cooperativa se ve forzada a admitir desigualdades, si bien domesticadas, como dirá Arizmendiarrieta (FC, III, 23), o discretas y estimulantes para la convivencia humana (Ib. 21).

3.5. Desigualdades funcionales
Las desigualdades admisibles en la sociedad cooperativa son, por otra parte, las derivadas del trabajo. «Prácticamente el hombre se sirve a sí mismo y

716

Igualitarismo

sirve a los demás mediante el trabajo» (FC, I, 130). Los diversos trabajos y las diferentes aportaciones personales en el trabajo, deberán ser valorados como servicios diferentes. «Las distinciones que nacen de la vida laboral, o que en la misma significan calidades de mayor o menor contribución personal a la obra común, pueden ser consideradas e incluso sancionadas en las estructuras laborales, máxime cuando se sabe que tal reconocimiento ha de ser un factor positivo de dinamismo o superación que ha de beneficiar a todos»
(Ib.). El «título de jerarquía» debe ser el trabajo.

En las valoraciones de las diversas modalidades de trabajo se deberá atender a las circunstancias y aquellas deberán tener un carácter funcional, variable en principio. Arizmendiarrieta lamenta que en nuestra sociedad aún no se valore el trabajo como tal, sino que se sigan haciendo distinciones según el calificativo del trabajo. Pero esta realidad no por lamentable puede ser ignorada (FC, III, 23). Hoy se valora por lo general la capacitación técnica, pero podemos encontrarnos en situaciones en las que el esfuerzo o la fatiga física, o trabajos denigrantes u opresivos, deban ser valorados no menos que la capacidad intelectual (FC, I, 130; FC, III, 23). Antigüedad, responsabilidad, etc., deberán ser valoradas asimismo, en la medida que requieran un estímulo o sean acreedoras a una compensación.

«Si queremos que nuestras comunidades cooperativas de trabajo sean entidades capaces de mantenerse en línea de constante superación y para ellas la aplicación del progreso técnico o los diversos métodos de organización doméstica sea un tanto espontánea y ágil, deberemos estructurarlas de forma que la movilidad y adaptabilidad sean una exigencia de su propia vitalidad»

(FC, I, 130).

En definitiva, todas las razones aducidas en contra del igualitarismo se pueden reducir a una: la eficacia, el realismo. Se tiene la impresión de que al mismo Arizmendiarrieta le costaba no poco sacrificar en cierta medida los principios puros del cooperativismo. Pero supo doblegarse al principio del realismo36.

Debe anotarse que, prescindiendo de principios generales, la cuestión del igualitarismo, o de la igualdad relativa de los socios cooperativos, debe ser considerada como un problema que en la práctica admite soluciones variables. Ya se ha dicho en su lugar que el cooperativismo de Mondragón nació como una búsqueda y un tanteo de fórmulas diversas posibles. No parece que se hayan propuesto en ningún momento la absoluta igualdad remunerativa a todos los niveles, ni el asamblearismo total y la democracia directa para todas las decisiones (negación absoluta de toda jerarquía de mando y remunerativa), al menos en los orígenes. Pero sí existió, aunque en una empresa reducida y sólo por poco tiempo (empresa cooperativa Arrasate, 1957) un ensayo de mayor aproximación al igualitarismo, en el que, aceptándose las diferencias mínimas correspondientes a los anticipos, se acordó la igualdad de retornos para todos. En el otro extremo podría citarse el ejemplo «desigualitario» del voto cualificado, modelo utilizado en los primeros años (criticado, como hemos visto, por «J.M.A.») y luego abandonado a favor de la fórmula actual «un hombre, un voto». La fórmula cooperativa se ha ido modelando y definiendo, a base de ensayos y correcciones, en la experiencia y en la práctica. Los críticos, e incluso los enemigos, han ayudado a su configuración. El proceso de mancomunación ha obligado también a dar uniformidad a los diversos ensayos, dando como resultado la fórmula que actualmente se conoce como el cooperativismo de Mondragón. Estos aspectos, de sumo interés para una historia del cooperativismo mondragonés, aparecen como secundarios para nuestro estudio del pensamiento de Arizmendiarrieta.

36

717

4. Contra la utopía
El pensamiento de Arizmendiarrieta no deja de tener elementos igualitaristas. De su valoración del trabajo en sí mismo, por ejemplo, es fácil inferir conclusiones igualitaristas. El lema «cada uno según sus posibilidades, a cada uno según sus necesidades» es claramente igualitarista. Sin embargo las circunstancias y su sentido del realismo han obligado a Arizmendiarrieta a criticar duramente el igualitarismo. Algo muy similar ocurre con la utopía. No hace falta decir que el igualitarismo ha sido criticado ante todo por ser considerado utópico.

El tema de la utopía es muy frecuente en los escritos de Arizmendiarrieta
(véanse, solamente en CAS, páginas 22, 25, 46, 106, 126, 139, 178), generalmente en sentido positivo. Sin embargo, especialmente en los últimos años,
Arizmendiarrieta ha hecho críticas muy duras de la utopía y de los utópicos.

4.1. Utopía y toma de conciencia
Proyectos que parecían utópicos florecen y se desarrollan, mientras instituciones pujantes en el pasado quedan caducas (FC, II, 60). La verdad es que el hombre que sobrevive es el que no se resigna a quedar aprisionado por la realidad en la que se halla, renunciando al esfuerzo por superarse y a crear algo. «La experiencia histórica nos enseña que son muchas las realizaciones presentes que en su día no eran nada más que buenas ideas y proyectos, que al movilizarse los hombres para su ejecución se convirtieron en realidades»
(FC, IV, 177). La utopía es necesaria para una toma de conciencia de nuestras posibilidades (FC, II, 244; FC, IV, 177; CLP, I, 251-252).

«Los instalados, los que más o menos se sienten satisfechos de la situación, tienden a utilizar un recurso aparentemente honesto y correcto y nada violento para desvirtuar los anhelos transformadores de los inquietos: enjuiciar y calificar de Utopía los proyectos cuyo desenlace pudiera resultarles incómodo. Es un arma de combatientes mejor camuflados de pacíficos y hasta altruistas» (FC, IV, 177). Estas palabras han sido escritas en 1973, e.d., cuando Arizmendiarrieta él mismo venía dirigiendo críticas muy severas a igualitaristas y utópicos. Hemos creído conveniente repetir brevemente estas ideas sobre la necesidad de la utopía, expuestas ya en otro lugar, para que las críticas a la utopía que pasamos inmediatamente a ver sean entendidas en su justo sentido.

4.2. Utopía, ideología
La utopía aparece en Arizmendiarrieta frecuentemente asociada a conceptos tales como palabrería, ideología, retórica, teorías, etc.; los utópicos serán calificados de «brujos» o de «aprendices de brujo» (FC, IV, 52), de «in718

Utopía

cendiarios acreditados de bomberos» (Ib. 103). Al «camino fácil de utopías redentoristas, sin base real», se oponen los hechos, el realismo (Ib. 145). «La peor ilusión que todos podemos padecer es la de embriagarnos con simples palabras y tal peligro no es simplemente hipotético» (Ib. 51). Más peligrosa, si cabe, puede resultar la utopía, por provocar la división en el seno del movimiento cooperativista.

«Todo se puede poner en entredicho. Es más, todo es susceptible de marginación para los nuevos revolucionarios, de ordinario no malos retóricos.

Desde luego lo que se hace es hablar, afirmar y anatematizar para, si es caso, ensayar más adelante o contrastar luego, más tarde, lo que es viable, compatible con otros imperativos humanos y sociales. Naturalmente, puesto a ello, en los platillos de la balanza o del contraste por un lado solamente existen formulaciones simples desde el momento que sus portavoces se limitan sólo a ello quedando por ver lo que habían de saber otorgar más que ideologías»
(Ib. 102).

Arizmendiarrieta no olvida que los mismos cooperativistas empezaron dando prioridad a la especulación y a la esperanza, desoyendo no pocas tentaciones. Pero estos hombres, curtidos en la lucha social y fieles a unos presupuestos idealistas, no están para aventuras. «Los trabajadores cooperativistas hemos luchado y trabajado mucho y no podemos ser indiferentes en naves y campos en los que hemos dejado lo mejor de nuestras existencias para que alguien pudiera utilizarlos para prácticas aventureras e irresponsables» (Ib.

107). «La experiencia cooperativa arrancó de una profunda y amplia toma de conciencia de realidades socio-económico-humanas por sus promotores y protagonistas contrastada y avalada siempre con vivencias y compromisos de todo orden no fáciles ni cómodos de mantener. No debe molestar el que noble y lealmente otros apelen a contrastes de ideas o ideologías, pero sí debe quedar claro para todos que ideales para hombres deben ser tales que sirvan para conducir un proceso de evolución todo lo más acelerado y profundo que se quiera, pero no tal que se identifique o requiera de partida la supresión o hipoteca de valores humanos incuestionables» (Ib. 102).

Arizmendiarrieta teme que tanta «explosión retórica» y «doctrinarismo especulativo» como nos invade comporte más riesgos que ventajas, sobre todo que no aporte nada más que confusión. «Se debe saber dónde se está, con qué presupuestos o desde dónde se arranca y a dónde se quiere ir y con qué medios se quiere caminar, qué métodos se deben utilizar. De lo contrario falta la condición más elemental de racionalidad y la base más indispensable de relación y convivencia» (Ib. 103). Se resiste incluso a creer en el espíritu progresista de muchos utópicos: «no pocas veces parece que cuanto más formulación y alarde se hace de actitudes progresistas o revolucionarias existe más conservadurismo en el fondo» (Ib. 42). Exige hechos: «para desvanecer ambigüedades o engaños en torno a tantas formulaciones aparentemente tan buenas, será bueno que vayan avaladas por hechos las palabras» (Ib.).

Teme que algunos espíritus cargados de ideologías, en el sentido de Arizmendiarrieta, o utopías, lleguen a constituir círculos cerrados, creando un

719

El (des)orden establecido

peligro para el movimiento cooperativo. «Es penoso, escribe Arizmendiarrieta en noviembre de 1971, que se parapeten en anonimatos o abstractos protagonistas tantas buenas ideas o sugerencias que pretenden tener curso entre nosotros. En los ámbitos cooperativos no se precisan subterráneos ni cenáculos cerrados so pena de que no queramos transformar en desorden organizado y oprimente nuestro “orden, que debe ser campo de libertad y la libertad contenido del mismo”, como efectivamente lo ha sido en la medida que nos hemos considerado promotores del mismo. Por lo visto parecen haber accedido a este campo sujetos para los que esta filosofía y ética no es válida» (Ib.).

Utopías e ideologías serán acusadas de provocar la división entre los cooperativistas: «ni hemos estado alienados ni nos resignaremos a alienarnos en aras de ideas que chocan con la existencia. Entre ideas que nos dividen y la existencia que nos conduce a unirnos o coexistir nos quedamos con la segunda, y por ello repudiamos tanta ideología como simple utopía» (Ib. 152). «No vivimos mejor por falta de saber, sino de hacer; lo que más nos hace falta es hacer algo más de lo mucho que sabemos en fórmulas para mejorar la existencia. Estamos dotados de facultades no menos para hacer que para pensar.

Nos distraemos y hasta nos dividimos demasiado con lo que no se precisa tanto ni para pensar ni para hacer, hablando y discutiendo, comparando puras teorías con las realidades» (Ib. 229).

4.3. En defensa de los valores humanos
«Las utopías son inevitables y hasta cierto punto convenientes. Pero no hay que echar en olvido que “una utopía se vuelve reaccionaria si su autor intenta imponer sus sueños sobre personas, en contra del deseo público”. La dosis de utopía que regularmente entrañan todas las fórmulas revolucionarias será tanto más ventajosa o menos nociva en la medida en que la revolución no impida la participación en su origen ni en su desarrollo» (Ib. 142).

No se pueden hipotecar valores humanos ahora con el pretexto de que en un incierto futuro la realización del hombre pudiera ser mejor. «(...) La vida humana, los valores humanos, la iniciativa o libertad, la responsabilidad y el compromiso deben estar en primer plano para nosotros» (Ib. 103). «Admitido que pueda haber minorías o sujetos con carisma, es preciso que los propios carismáticos respeten lo que dicen ofrecer como compensación cuando se trata de valores tales como la libertad y la democracia. Es mal juego, nos advierte la historia, el comenzar por hipotecar estos valores para, después, recuperarlos. Con esta estrategia quienes llevan las de ganar suelen ser los menos escrupulosos, los aventureros y, siempre, los tiranos» (EP, II, 118).

«No es, insiste en otro lugar, ni puede ser buena la revolución o la gestión transformadora que para llevarla a cabo empieza a exigir e imponer que nos

720

Utopía

entreguemos con pies y manos atados, es decir, con hipotecas de libertad y dignidad o participación. Claro que con promesas de recuperarlo, pero sin que nadie pueda desvanecernos las dudas de no hacerlo así» (FC, IV, 73).

Arizmendiarrieta no cree en transformaciones súbitas de la sociedad, porque sabe muy bien que toda transformación de las conciencias, de los hombres, exige una labor paciente y profunda. «Es una constante histórica la incapacidad del hombre para realizarse a sí mismo y dar satisfacción a sus aspiraciones sin tener que contar con el tiempo y con sus semejantes. El tiempo y la solidaridad son factores básicos y no simples circunstancias accidentales para la promoción humana y la transformación social. Hay que sembrar o preparar para poder cosechar; hay que poder contar con otros y, consiguientemente, otorgar algo más que exigir siempre para potenciarnos» (FC, IV,

42-43).

Pero Arizmendiarrieta no sólo desconfía de quienes quisieran imponer por la fuerza sus utopías; teme que muchas utopías ellas mismas provocan la violencia y la agresividad. «Indudablemente, escribe, una de las causas de la violencia es la simplificación de la realidad. ¿Cuánto fanatismo ha organizado la falta de saber objetivizar las ideas o los conocimientos formales? Se puede afirmar que algunas ideologías por su sencillez y radicalismo atraen literalmente a los psicópatas. Justifican tan plenamente la violencia que excluyen cualquier otra forma de acción. Son estos fanáticos los que crean otros contra-fanáticos y estos a su vez los contra-contrafanáticos, es decir, toda una cadena sin fin» (Ib. 123).

4.4. Tiranías camufladas
Arizmendiarrieta desconfía de las soluciones perfectas porque, primero, no hay soluciones simples a problemas complejos; «está equivocado quien crea que en la vida no hay más que un problema: la vida es un tejido de problemas. Por eso pueden considerarse utópicas todas las soluciones simplistas que se quieren ofrecer a problemas complejos» (PR, I, 201); y, segundo, porque las soluciones perfectas significarían, en su opinión, un mundo paralizado, lo que repugna a la visión dinámica de Arizmendiarrieta: «mundo estabilizado no es mundo humano» (FC, IV, 142). Con todo, «lo que parece que abunda por ahora en nuestro mercado son buenas ideologías y magníficas fórmulas de solución más que verdaderas mercancías, que tampoco escasean.

El consumismo opresor y enervante se nutre precisamente de eso» (EP, II,

118).

Esta es una observación curiosa de Arizmendiarrieta: que las soluciones utópicas se corresponden con el espíritu de una sociedad consumista. Se ofrecen y aceptan las soluciones fáciles, bien empaquetadas y presentadas, como se ofrece una mercancía al consumidor. «De esta forma ni se contiene ni se

721

El (des)orden establecido

resiste a aceptar nada con criterios objetivos. Huelga hacernos preguntas y proceder a un examen tanto en la opción por mercancías como por ideologías o comportamientos comprometedores y no precisamente carentes de transtendencia» (Ib.).

«Gritar y repetir es el secreto de la publicidad como también tirar para adelante o donde va la gente es la de otros mercaderes en busca de clientes.

La fuerza no la da precisamente el número sino la estrategia y la técnica bien concebidas, compartidas responsablemente. Es difícil apelar a urgencias simples y achacar a dificultades superables la falta de reflexión, de contraste, de diálogo para que, sin más ni más, unas comunidades o un pueblo sea impulsado a obrar a ciegas o a tientas» (Ib.).

Al pueblo, más que soluciones fáciles, hay que ofrecerle interrogantes, dirá Arizmendiarrieta, para que este pueda analizar y contrastar antes de decidirse por una opción. Bajo el epígrafe Tiranías camufladas escribe Arizmendiarrieta (1975):
«Se ha dicho que la tiranía tiene necesidad de esclavos y la mentalidad de esclavos se configura con variopintos recursos que tienen un común denominador: subrogar a los más el juicio personal y hacerlos propensos a secundar órdenes ajenas sin participación y criterio.

Es preciso que tratemos de aceptar y tratemos al pueblo, no como simple masa más o menos inerte, sino como una exigencia permanente de discernimiento, de ponderación o evaluación de los problemas y de las cuestiones que le afectan.

Si algo precisa el pueblo para que cada vez sea menos masa y más consciente y responsable es que cuente con opciones amplias para proceder al examen de sus cuestiones, así como la ponderación de sus fuerzas. Nada se produce por generación espontánea.

Tampoco un pueblo se acredita como tal sin pasar por el crisol de las pruebas que autentifiquen la voluntad de resistencia o superación, para lo que es preciso que no le falten oportunidades.

Un pueblo no se genera sin historia ni se hace historia con histerismos. El pueblo ha de promover su salud y vigor físico y moral, y lo hará en la medida que se vea obligado a actuar consciente y responsablemente, mediante el conocimiento de lo que le interesa y le cuesta. No es lo mismo servirse del pueblo que servir al pueblo. Y lo que caracteriza al que pretende tener aval de confianza debe ser el “servicio al pueblo” acreditado con hechos y desde luego con procedimientos y métodos idóneos para dar testimonio de ello.

El pueblo es la suma de sujetos y de generaciones que coexisten y por ello, unos y otros, todos, deben tener audiencia y ejercitar el deber de servicio. Un pueblo, por otra parte, también debe disfrutar de vitalidad y para ello dispone del resorte de una fuerza. La fuerza también se genera y se debe tratar de promoverla en bien de un pueblo para que este subsista.

De momento lo que más precisa nuestro pueblo es la fuerza derivada de la unión de sus hombres, del Trabajo de sus moradores. Y todo esto puede mancomunarse en la promoción y el respaldo de la fuerza de la razón, sin que ello signifique renunciar a la razón de la fuerza. Mancomunando la unión, el trabajo y la fuerza de la razón, esta irá tomando cuerpo y consistencia para transformarse en razón de la fuerza. De esta forma lograremos que la verdad y la justicia estén al servicio de la libertad y del bienestar de todos» (EP, II, 118-120).

722

5. Contra la violencia
Arizmendiarrieta parte del concepto cristiano de que la vida humana es sagrada bajo todos los conceptos. Matar, por ejemplo, será reflejo de un espíritu pagano, ignorante de la grandeza del ser humano, creado por Dios y dotado por él de un alma inmortal (SS, I, 124-125).

Reconoce que para muchos es grande la tentación de la violencia (1950):
«aquí, piensan no pocos según Arizmendiarrieta, sin declarar una guerra sin piedad al capital y al capitalismo, no hay nada que esperar... hay que despojarse de los escrúpulos y sin manejar la pistola o utilizar la coacción no hay nada que soñar, nada que esperar... y parece que muchas veces la realidad de la vida da razón a los que dicen y piden que se despoje de escrúpulos para penetrar en ese mundo» (SS, 11,247). Sin embargo Arizmendiarrieta rechazará la vía de la violencia en nombre de los valores humanos, especialmente en nombre de la libertad, que es algo más que una forma u otra de gobierno.

«La salvación no la hemos de encontrar por el camino de la violencia y la fuerza. Que quien a hierro mata a hierro muere, dice el refrán; que por el camino de la violencia no se ha de allanar el abismo, sino ahondarlo más y más.

Lo que a lo sumo pasará será que el bastón variará de posición, de forma que la montera haga de mango y el mango de montera» (Ib. 152). La libertad se crea desde la conciencia, no viene dada desde fuera. Se crea con la unión y el trabajo. Estas ideas fundamentales, constatables ya en escritos de 1943, permanecerán constantes en el pensamiento de Arizmendiarrieta hasta sus últimos escritos, del mismo modo que sus conceptos sobre la dignidad del hombre permanecen sustancialmente invariables. Ellos sustentan la posición de
Arizmendiarrieta respecto a la violencia.

El tema de la violencia cobró fuerza, naturalmente, años más tarde y, en concreto, en relación directa con el futuro de Euskadi. Ya se ha dicho que
Arizmendiarrieta no gustaba de actitudes dramáticas o heroicas; prefería el trabajo y la cultura o la educación. «En fin, morir es también ley de vida, pero no así el matar: de la vida sólo Dios puede disponer» (FC, IV, 253), escribía en noviembre de 1975, aludiendo tal vez a la larga agonía de Franco.

Sin embargo, más que su postura contra el igualitarismo o contra la utopía, su postura ante la violencia es de las que actualmente algunos califican de ambiguas y Arizmendiarrieta considera la única actitud honrada.

Pero esta “ambigüedad” no se refiere en modo alguno al movimiento cooperativo, cuya opción antiviolenta es tajante. Un año antes había definido claramente la posición del movimiento cooperativo frente a toda convocatoria a la violencia:
«Una modestia mal utilizada no nos debe impedir reconocer la fuerza creada como a su vez una complicidad subrepticia de la comodidad tampoco desviarnos de nuestra vocación revolucionaria, transformadora o de cambio, como deseare cada uno calificarla.

A la madurez de los individuos debe seguirse la educación de la sociedad que componen. Nuestras sociedades cooperativas deben poder tratar de avanzar por ca-

723

El (des)orden establecido minos nuevos con decisión y audacia, es decir con la disponibilidad y aplicación de recursos en cuantía y forma precisa para materializar nuevos proyectos y planes.

A las multitudes que se alborotan y líderes que se impacientan podemos y debemos darles una lección de serenidad con la credencial de las obras. Las obras que deben poder acreditar el nuevo posicionamiento de las sociedades cooperativas han de ser básicamente cambios y transformaciones de infraestructura educativa, asistencial, económica y financiera, técnica y científica. No olvidemos que de hecho la Experiencia Cooperativa en parte se apoya y nutre de elementos y recursos infraestructurales correspondientes a una concepción y visión humana y social aristocrático-burguesas, más apetecibles para tener que ser, para afianzar y estabilizar que para dinamizarse y proyectarse hacia nuevas fronteras no convencionales. Con los resultados residuales de Obras Sociales poco podemos hacer en este terreno aun cuando mediante la gestión mancomunada y globalización de objetivos sí podrían mejorarse algunas aplicaciones. Pero el obstáculo surge: cada entidad tiene su feudo, se ciñe a su campo más o menos convencionalmente ponderado.

No debemos contenernos en silencio ante tanta convocatoria y proclama, todo lo bien intencionados que se quisiera imaginar, pero no carentes de riesgo y aventura.

Se necesita fuerza y poder y hay que reconstruirlos, crearlos, ¿Cómo? Sin echar en olvido las lecciones de la historia, de la experiencia.

“El modo como se alcanza el poder condiciona la forma en que se ejerce”. Creemos en la democracia como nos aferramos también a la libertad. Contamos con el tiempo como factor indispensable de acción como no renunciamos a la fuerza, primero la fuerza de la razón y, cuando la misma resultare insuficiente, nos vale también, supuesta aquella, la razón de la fuerza» (CLP, I, 269-270).

No por ello se renuncia al empleo de la fuerza, de la agresividad, menos a la eficacia. Pero Arizmendiarrieta distingue entre agresividad y violencia. No es lo mismo agresividad que violencia, dice Arizmendiarrieta: se puede estar a favor de la agresividad y en contra de la violencia (FC: IV, 122). La violencia es la expresión más simple, más primitiva de la agresividad. Hoy nos encontramos ante la paradójica situación de que cualquiera puede tener fácil acceso a la práctica de la violencia eficazmente («¿qué es más temible, el cóctel molotov o la bomba atómica?»), pero su legitimación es tan compleja bajo todos los aspectos, por no decir simplemente repudiable, desde el punto de vista humano y social, que ninguno se halla en situación de poder legitimar su uso de modo plausible. «La violencia se legitima contra la violencia establecida y trata de convertirse en violencia legalizada. Indudablemente, la facultad de decir qué es legítimo y qué no es legítimo entraña un poder en cuyo contraste todos los demás poderes parecen insignificantes. Desgraciadamente, la razón debe utilizar la agresión para ser entendida. La voz de la única razón no tiene audiencia alguna. La razón debe utilizar la tecnología, debe utilizar métodos agresivos siendo necesaria para ser escuchada. Los violentos por comisión u omisión son muchos» (Ib. 123). La razón no puede renunciar a la fuerza. «No se trata de descartar la acción o la agresión precisa para transformar las estructuras que mantienen divididos a los hombres, como son las clases sociales, y consiguientemente de desentenderse de la lucha de clases sin interesarse por la promoción de nuevas estructuras socio-económico-políticas y coherentes» (Ib.). Pero no hay por qué ser ilusos, ni violentos.

724

Violencia

Arizmendiarrieta cree, en efecto, que la eficacia de la violencia, indiscutible para ciertos efectos limitados, no es tal, en relación a los fines de libertad y promoción de la dignidad humana. «¿Es compatible con los métodos de violencia el aprendizaje de la convivencia en libertad?» (Ib). Arizmendiarrieta cree que no. Cree, además, que las conquistas realizadas sin violencia, por vía democrática, son las que tampoco precisan luego de la violencia para subsistir, mientras que las conquistas realizadas por la fuerza siguen necesitando de la fuerza para mantenerse (CLP, I, 249).

Por todo ello Arizmendiarrieta prefiere la fuerza y agresividad de la unión y del trabajo: «Nortasunik iñork galtzeke jaubetu bearrekoa da alkartasun gizaberatsua, indarkeririk oneritzi ezindakoa ta indarrik aundiena sorrerazi dagikeguna» (Ib. 260).

Que la cooperación es un método más humano y más eficaz incluso a efectos de conseguir la libertad y la promoción de la dignidad humana, Arizmendiarrieta lo ha formulado en múltiples formas y ocasiones. «La superioridad específica del hombre radica, escribía en 1973, no tanto en sus instintos cuanto en el dominio de su inteligencia y de su voluntad, proyectados a la búsqueda de la complementariedad y de la transformación de su medio ambiente. La cooperación es el procedimiento con el que cabe solicitar y obtener respuesta en plena reciprocidad de prestaciones y de servicios entre los hombres. El hecho de ser más proclives hacia otros procedimientos y expedientes de relación y convivencia no significa nada más que volver a la vigencia del primitivismo y de la naturaleza sin civilización entre los hombres» (Ib. 250-251).

Como en la mayor parte de los autores que, procediendo de una inspiración cristiana, han abordado el tema de la violencia, también en Arizmendiarrieta se da la doble ecuación de violencia igual a instintos, razón igual a métodos democráticos de lucha. En Arizmendiarrieta esta división, muy discutible, tiene al menos dos claros fundamentos. El primero es su concepto del hombre, de la libertad. El segundo es su visión concreta de la realidad del pueblo vasco, e.d., su concepto de libertad política de Euskadi o, si se quiere, de «independencia» (más bien interdependencia). «Nuestro pueblo está emplazado en una tierra poco exuberante e inmerso en un marco de civilización y de relaciones insoslayables. De ello podemos concluir que nada se impone con más apremio que el trabajo para hacer más confortable nuestra tierra, como tampoco ningún otro procedimiento resulta mejor que la cooperación para promover y disfrutar de espacios vitales, teniendo en cuenta su densa población. Las comunidades de trabajo o las empresas cooperativas constituyen una expresión auténtica y viva de nuestro pueblo, vigoroso y expansivo en aras de lo que, a su vez, apetecen todos los pueblos de la tierra: ser libres y dignos de confianza de los otros. La cooperación que se protagoniza entraña resonancias de libertad y de justicia no menos que de convergencia y desarrollo en la solidaridad y el trabajo, que están destinadas al intercambio y bienestar expansivo en áreas cada vez más dilatadas» (Ib. 251).

Podríamos decir que toda la concepción social de Arizmendiarrieta responde a la búsqueda de una alternativa a la violencia. «Hoy, decía en 1962,

725

El (des)orden establecido

hay que pensar que el mundo seguirá avanzando y progresando, con la única particularidad de que lo que no se logra por la vía de la convicción se ha de ir imponiendo por el de coacción en este campo social. Si repudiamos la violencia, queremos huir de la misma, abracemos las convicciones y procedamos con convicciones que respondan al nivel del sentimiento de dignidad y justicia que va siendo patrimonio común de nuestro tiempo y de nuestra humanidad» (EP, I, 172).

Para Arizmendiarrieta existe como un fatalismo del progreso, en el que los cambios sociales se imponen de un modo u otro. Si la fuerza de la conciencia y de las convicciones no es capaz de llevar a cabo las debidas transformaciones, si el proceso de cambio se estanca, resulta inevitable la explosión de la violencia y la revolución. De ahí que la alternativa a la violencia necesite subrayar la importancia de la educación, de la mentalidad abierta a toda posible mejora y reforma.

En nuestro mundo no tiene, pues, sentido, limitarse a condenar la violencia: la única alternativa real a la violencia es una acción decidida de transformación que la haga superflua. Resulta difícil legitimar el uso de la violencia, como hemos visto. ¿Se infiere de ello que podemos condenarla sin más? Las siguientes palabras de Arizmendiarrieta, en el contexto de algunos secuestros realizados por ETA, especialmente el del industrial Zabala en 1972, en medio de duros conflictos laborales, le podrían parecer irreverentes e inoportunas a más de uno, pero son perfectamente lógicas dentro de la actitud de quien las pronunciaba: «Los secuestros individuales que actualmente tienen resonancia, no nos deben inducir a echar en olvido los secuestros que colectividades o pueblos enteros han padecido y padecen» (FC, IV, 112). Sin duda no significan justificación ninguna de la violencia, pero se alejan mucho de condenas demasiado fáciles de la misma. «Las fuerzas vivas que, efectivamente, son manantial de renovación y energía, son las constituidas en la órbita del trabajo», continúa inmediatamente Arizmendiarrieta. Más que condenar la violencia, Arizmendiarrieta ha querido proponer una alternativa a la misma.

A Arizmendiarrieta no parecen satisfacerle los rituales obligados de condena de la violencia, máxime cuando son realizados en nombre de un cristianismo que él considera hipócrita y falsificador de la historia (Ib. 181). La violencia ha existido siempre en la historia, dirá. Descubrirá «la otra cara de la violencia», los ingentes gastos militares, que los Estados vienen realizando sin que, al parecer, las conciencias dispuestas, por otro lado, a condenar enérgicamente las violencias de los oprimidos se sientan ofendidas (Ib. 124).

Acusará el «estado de violencia» de un mundo en el que 300 millones de adultos, hombres y mujeres sin oportunidades de trabajo, no pueden satisfacer sus necesidades elementales con el sudor de su frente, que estarían muy dispuestos a soportar. Y podrían añadirse a ellos otros 226 millones en paro en países en desarrollo. En los mismos países desarrollados hay motivos suficientes de violencia, dice Arizmendiarrieta, en la miseria de muchos, en me726

Violencia

dio de la opulencia de otros; en las regiones atrasadas, en el paro, etc. «La paz y el bienestar están próximos, pero para poder promocionarlos hemos de saber cada uno penetrar más hacia el futuro y ampliar en el presente más el área de nuestra atención y sensibilidad. No podemos lanzarnos por una escalada de confort sin riesgo de terminar en una cascada de odiosidad y violencia más o menos encubierta; debemos hacernos cargo de la situación real y de los problemas objetivos que comporta el presente en un mundo en el que los espacios se han contraído y el tiempo corre veloz. Los focos de violencia y de agitación tienden a ser cada vez más rápidamente universales y por ello se precisa el que la conciencia de solidaridad de cuantos quisieran ser constructores de paz y promotores de bienestar se vigoricen» (FC, III, 251).

Arizmendiarrieta distingue, con Péguy , la violencia reaccionaria y la violencia revolucionaria, añadiendo que la violencia revolucionaria constituye un porcentaje mínimo de toda la violencia en el mundo (FC, IV, 181). En ningún lugar de sus escritos se podrá encontrar un solo texto favorable a la violencia (revolucionaria); pero sus críticas se han dirigido invariablemente contra la violencia reaccionaria que se resiste a toda transformación social.

«Para todo hace falta Poder, escribe Arizmendiarrieta, que se hace irresistible en cuanto es palanca apoyada en la Fuerza Moral o la Mayoría; es la idónea para crear la Legitimidad, a su vez acreedora al recurso de la Fuerza sin más aditamentos; si bien no por ello empleable sin más consideraciones o ponderaciones, dado que mandar debe ser servir al hombre y a los hombres»
(Ib.).

Aunque el movimiento cooperativo ha optado claramente por la vía pacífica de transformación de la sociedad, no limitándose a palabras, sino ofreciendo hechos, Arizmendiarrieta no quiere convertir esta opción en un absoluto. «No obramos por idealismos quiméricos, dice. Somos realistas; conscientes de lo que podemos y no podemos (...) Nos concentramos en las cosas que tenemos esperanza de cambiar entre nosotros más que en las cosas que no podemos cambiar en otros» (Ib. 65). Está firmemente convencido de que la vía escogida es válida: «Empeñados en cambiar cosas que podemos cambiar y que de hecho cambiamos, somos conscientes de la fuerza que mediante ello se promueve. Además de la razón que precede a la fuerza tratamos de contar con la fuerza que pudiera preceder también a la razón» (Ib.).

No se renuncia a la fuerza; al contrario, se siente poseedor de la misma, dispuesto a utilizarla para transformar la sociedad de modo no violento. Arizmendiarrieta está convencido de que la cooperación es una fuerza capaz de transformar la sociedad. Pero esta elección, hecha desde la personal conciencia de los cooperativistas, no elimina la cuestión de la violencia legítima de raíz. Esta queda en pie, al menos para quienes en su conciencia crean poder y deber utilizarla. Se considera la cooperación como una vía válida y como una fuerza capaz de realizar la revolución necesaria; pero en ningún momento se presenta a esta como la única opción posible. «Si la conciencia de un mundo sustancialmente injusto a nivel individual y a escala social es algo que podemos compartir con unos, la revolución y la antiviolencia no nos son fenóme727

El (des)orden establecida

nos desconocidos ni fuerzas despreciables. De hecho ante la elección de la violencia o de la no violencia el cristiano se ve abocado necesariamente a una elección que pudiera calificarse de contradictoria; si elige la violencia puede ir contra el amor cristiano; si elige la no violencia puede ir contra la justicia, porque en el fondo está apoyando la injusticia» (Ib.).

«Ante esta elección contradictoria, escribe Arizmendiarrieta en febrero de 1972, cuando más vivamente se venía discutiendo el tema de la guerrilla, optamos por ser honrados. Escogemos el camino por el que avanzar no entrañe precisamente dejar ya de lado o minusvalorar valores sustantivos y fuerzas necesarias, como pueden ser la libertad y la solidaridad, el trabajo y la unión, de los que precisamos no menos que del oxígeno que respiramos.

Para solucionar la antinomia antes expuesta se potencia hoy en medios cristianos y de neta conciencia y sensibilidad social la “no violencia activa”, que puede ser una solución, pero puede que no lo sea, ante la enorme fuerza represiva de la estructura establecida.

El que opta por esta solución al menos debe respetar al que opta por la solución violenta, que también puede ser solución y puede que no lo sea. La honradez nos puede imponer una carga revolucionaria y una carga anti-violenta. ¿No entraña una actitud positiva el compromiso de la promoción transformadora abierta a la máxima convergencia de fuerzas y acreditada por sucesivas y progresivas metas de desarrollo, inseparablemente, económico y social?
La honradez, en escala individual y colectiva, que implica la lealtad y fidelidad cooperativa, es un proceso cuyos valores intrínsecos no cabe desconocer, en aras de idealismos no exentos de riesgos difíciles de objetivizar en tanto no se traduzcan en males irreparables.

Efectivamente, como dijo quien no puede ser calificado de sospechoso en orden a su compromiso revolucionario, “después del triunfo del soviet la electrificación es necesaria”. Por tanto antes y después, la organización, la promoción técnica, las matemáticas y el pan de cada día para seguir viviendo, son indispensables» (FC, IV, 66).

«La toma de conciencia de las realidades, concluye Arizmendiarrieta, no es menos indispensable que las bellas teorías para tensar las fuerzas. No solamente hay una minoría que es consciente, sino hay minorías conscientes en el seno de nuestro pueblo» (Ib. 66). Que equivale a decir: hay alternativas diferentes y queda abierto el campo de elección.

Con motivo de este artículo, el Director General de Prensa advirtió a la
Delegación Provincial de Guipúzcoa que en el mismo «se hace una serie de consideraciones en torno al problema de la violencia, que en cuanto entraña una defensa, no exenta de cierto sentido apologético de la violencia, pudiera considerarse roza lo permisible en lo que al mantenimiento del orden público establece el artículo 2.º de la vigente Ley de Prensa e Imprenta. — En consecuencia, deberá V.I. apercibir al Director de la publicación haciéndole ver la improcedencia de insertar textos que en alguna manera pudieran prestarse a interpretaciones que implicasen apología de la violencia»37.

El día 20 de diciembre de 1973 es muerto en atentado el Presidente de
Gobierno L. Carrero Blanco. Arizmendiarrieta, instado al parecer desde

37

Oficio del Ministerio de Información y Turismo, Delegación Provincial de Guipúzcoa, 4 de mayo de 1972, Nr. 1305 (Archivo Arizmendiarrieta).

728

Violencia

altas esferas por personas allegadas a expresar su condena pública, se limita al siguiente comentario:
«Los acontecimientos humanos, como los que hemos lamentado y repudiado recientemente, el asesinato por acto terrorista del propio Presidente del Gobierno, nos mueven a profundizar y renovar el espíritu de COOPERACION, como sistema de relación y convivencia humana y social y antídoto de la violencia y la coacción.

La expresión de nuestro dolor a cuantos de más cerca han acusado la tragedia como de nuestra repulsa a toda forma de violencia llevan aparejada la toma de conciencia más profunda de las responsabilidades a los efectos de tensar los esfuerzos para la reconstrucción de nuevas formas de sociedad que fueran exponente de dignidad, libertad y justicia, conducentes a un mundo mejor, sin caer nunca en las fáciles tentaciones del desaliento ni simple reacción.

“La cuando rejados bemos mana

fe está en el corazón de la existencia”, se ha dicho, y en ello nos apoyamos también añadimos que “el bien y el mal, de hecho, van poco menos que apay por ello en el camino del progreso y de la evolución; olvidar y perdonar desaber practicar todos”. Constituyen hasta un recurso de desintoxicación huindispensable.

Cierto que el mal es contagioso, pero también es verdad que el bien es difusivo; afortunadamente lo que más apetece a cualquier corazón humano es el amor, la paz, la comprensión y la unión. Todo ello se traduce para nosotros, en el terreno práctico, en el ejercicio de la solidaridad y de la cooperación»38.

Este breve comentario, escrito en difícil equilibrio como una solución salomónica de compromiso, tuvo el efecto natural de no satisfacer a nadie. Mereció, entre otras reacciones39, la réplica de la «Fracción Leninista de ETA», que, tras señalar que «tenemos algunos puntos críticos respecto a la acción
[atentado] y estamos en desacuerdo con la línea política e ideología de ETA
Vª», en una hoja volante profusamente diseminada en Mondragón el día 22 de febrero decía:
«La reacción de la prensa burguesa y del régimen ha sido naturalmente muy distinta: miedo, repulsa total, lloriqueo general, cantando las virtudes del mil veces asesino y torturador Carrero, apoyo a un más duro Orden Público, condena máxima de la violencia de los de abajo, etc., todo muy natural.

Pero lo que no es tan normal es que al coro de hipócritas beatas y portavoces de la política de la burguesía se haya unido el órgano de expresión y propaganda de la burocracia dirigente del cooperativismo: TU.

La degeneración está llegando al extremo. Es imposible entender el “realismo”, diciendo sin embargo que se está por la “liberación del hombre y nuestro pueblo”.

Esta política (sí política) antiobrera ha llegado a su ruina:
— se impide la solidaridad real de la clase obrera (IFAM) que es la única garantía y forma de lucha real contra el capitalismo y el régimen.

38

Renovación, TU, Nr. 158, enero 1974.

El paquete de 152 ejemplares de la revista TU destinado a la empresa Irizar S.C.I. fue devuelto íntegro el 29 de febrero con la siguiente nota: «Envíen a Victor Pradera 10 (San Sebastián) donde serán bien atendidos». Sin firma. Esta era la dirección de la Jefatura Provincial del Movimiento (Archivo
Arizmendiarrieta).

39

729

El (des)orden establecido
— se apoya deseducativa y traidoramente la política de la dictadura concediéndole el “perdón”, el “dolor” y la “repulsa a la violencia” (T.U.) cuando todo el pueblo ha festejado la liquidación del opresor.

— se paraliza y debilita la potencialidad de lucha de los trabajadores cooperativistas con una estructura interna que no es otra cosa que un burocrático bozal, que va gastando a los trabajadores en afanes productivistas y en conciencia de clase mientras se encumbra a una nueva clase media con poderes empresariales.

Nosotros afirmamos que sólo la unidad combativa y organizada de la clase obrera, la solidaridad de la clase obrera en los hechos (Iruña), la lucha contra la represión de la dictadura y la defensa de todo luchador (Burgos-70, Puig-74)... son los hechos significativos que impiden las bestialidades de la dictadura y el capitalismo.

Sostenemos —contrariamente a la posición hipócrita y traidora del T.U. y de nuestros “dirigentes”— que sólo la violencia del explotador es condenable por la clase obrera en la lucha entre clases, y que la única forma que tienen los trabajadores y los luchadores antifranquistas para defenderse de aquella violencia permanente es la lucha directa que no es sino la violencia y autodefensa del oprimido para vencer a su enemigo irreconciliable.

No hay una única moral para explotadores y oprimidos. En la lucha un asesinato es para el obrero sólo el asesinato de un oprimido»40.

Sordo, por una vez, al vocerío que él mismo había provocado en torno suyo , Arizmendiarrieta prosiguió su propia reflexión del tema, considerando la fuerza de los débiles: la revuelta de unos países despreciados del tercer mundo, de los que nada estimábamos sino su petróleo, hace tambalear súbitamente todo el orden económico occidental. David puede todavía derribar al coloso. Una vez más alcanzaría con éxito desavenirse tanto con tirios como con troyanos:
«El Faraón soñó que estaba junto al río Nilo; y he aquí que del río subían siete novillas de hermoso aspecto y metidas en carnes, las cuales se pusieron a pacer en el juncar.

Tras ellas he aquí que subían del río otras siete novillas de mal aspecto y flacas de carnes, las cuales se pararon al lado de las novillas primeras junto a la orilla del río.

Luego las novillas de mal aspecto y de carnes flacas devoraron a las siete novillas de aspecto hermoso y metidas en carnes (Génesis 41).

Se ha vivido un año de prosperidad económica. Las empresas industriales han aumentado sus plantillas de personal, han construido nuevos pabellones, han mejorado notablemente la maquinaria y el herramental, han tenido jugosos superavits. Se ha vendido todo; las cosas han marchado sobre ruedas. Las agitaciones sociales, las reclamaciones salariales no se han desbordado.

Los dirigentes políticos, los gerentes y empresarios, los banqueros y los hombres de negocio se las prometían felices. Sin duda alguna, el ejercicio 1973 ha sido brillante. Pero...

(...) En España ha bastado el magnicidio del Presidente del Gobierno, perpetrado al parecer por una minoría, para que la nación haya sufrido un movimiento convulsi-

Hoja volante de «Fracción Leninista de ETA», no fechada [febrero de 1974] (Archivo Arizmendiarrieta).

40

730

Violencia vo que, afortunadamente, apenas ha tenido repercusiones violentas. El golpe bajo ha sido la causa de la caída de un Gobierno con todas sus ilusiones y proyectos...»41.

¿Qué decir de estos comentarios, que provocaron tales protestas? Para nosotros la reacción de Arizmendiarrieta no constituye ninguna sorpresa, ha sido la reflexión propia de un cristiano maritainiano. El clima social del momento, sin duda también la forma algo enigmática en que Arizmendiarrieta creyó oportuno expresarse, no han debido de facilitar nada su comprensión.

Podemos presuponer que tanto los de un bando como los del otro midieron las palabras de Arizmendiarrieta desde la mirilla particular de sus excitados intereses momentáneos. Los malentendidos resultan comprensibles. Ni unos ni otros parecen haber comprendido su verdadero sentido, tal vez han ignorado incluso la fuente de la que Arizmendiarrieta bebía: una lectura serena les hubiera dejado ver que, en el fondo y hasta en la forma, este se había limitado a reproducir la meditación maritainiana sobre la violencia de los oprimidos y su inevitable ambigüedad (en un mundo dominado por la fuerza), la bella doctrina del «entrecruzamiento de máscaras y de roles»42. La postura de
Arizmendiarrieta es todo lo contrario de una condena fácil de toda violencia; todo lo contrario, también, de concesiones oportunistas a la violencia con motivo de un caso particular satisfactorio.

Para Arizmendiarrieta era una cuestión de principio. Una vez más se negó a dejarse atrapar en la sofística alternativa de la simple condena o aprobación total. El atentado al Presidente del Gobierno era un caso suficientemente grave como para exigir un tratamiento a fondo de la cuestión, no particularizado, sino de base. ¿Qué decir de la violencia? Evidentemente la mera represalia o venganza no podían bastar para legitimar moralmente el atentado (Arizmendiarrieta se hace un planteamiento moral, no simplemente político). La alegría popular no representaba, por supuesto, ningún índice de moralidad. ¿Significaba un paso adelante en la liberación de los oprimidos?
Era, de todos modos, violencia ejercida por oprimidos (vacas flacas) que luchaban por su liberación. Esta era la raíz del problema. Arizmendiarrieta partía de este reconocimiento implícito, pero no se daba por satisfecho con un reconocimiento general y benevolente del derecho de los oprimidos a la rebelión. Era preciso profundizar en el problema. Sus mismas obligaciones morales (y quizá «diplomáticas»), que le habían llevado a expresarse públicamente, le brindaban la ocasión para hacerlo.

No basta con decir que existe una violencia legítima. La violencia, dirá
Maritain, incluso la más justa, es el medio que por sí mismo «entraña el contexto histórico más cargado de dolor y de pecado»43. Digamos: la violencia no es nunca amable, no es hermosa. No por ello un cristiano puede a priori con-

41

42

43

Las vacas gordas, las vacas flacas, TU, Nr. 159, febrero 1974.

MARITAIN, J., Humanisme intégral, Aubier, Paris 1968,233.

Ib.255.

731

El (des)orden establecido

denarla: sería, dice Maritain, un purismo farisaico44. En primer lugar, porque existe una violencia moralmente justa45. Y, además, porque en la historia de la humanidad el mal y el bien aparecen bajo máscaras y roles entrecruzados: vemos del mal surgir el bien y del bien el mal. ¿Quién hubiera podido juzgar qué bienes o males acarrearía el atentado? La acción humana se desarrolla en una «simbiosis de lo verdadero y de lo falso»46. «La historia es impura y nocturna, es la historia del mal mezclado con el bien y más frecuente que el bien, la historia de una humanidad desgraciada en marcha hacia un rescate muy misterioso, y de los progresos hacia el bien que en ella se hacen a través del mal y de los medios malos»47. Es la visión trágica y esperanzada de la historia.

Lo que un cristiano, y simplemente un humanista, nunca puede hacer es festejar la violencia como tal. Sólo puede repudiarla y deplorarla, aún bendiciéndola, como un destino fatal y una «horrible necesidad»48. «La peor angustia para el cristiano es precisamente saber que puede haber justicia en el empleo de medios horribles»49. La violencia puede ser aceptada: pero sólo puede ser humanamente aceptada y bendecida por quien es capaz de condenarla íntimamente al mismo tiempo.

Ni puede limitarse a sólo esto el cristiano verdaderamente comprometido en la historia. «Y es necesario también, prosigue Maritain, y es aquí donde interviene la inversión propiamente cristiana de los valores, que antes de la fuerza y los medios de agresividad o de coacción, que son los únicos conocidos por los hombres de la sangre [opuesto aquí a pneuma], sea reconocido y puesto en obra todo un mundo de otros medios, entre ellos, aquellos que nosotros hemos llamado los medios espirituales de la guerra: los medios de la paciencia y del sufrimiento voluntario, que son por excelencia los medios del amor y de la verdad»50. Sólo estos medios aseguran la victoria definitiva. Ni en el mejor de los casos, nunca basta la violencia sola: existe, está ahí, puede tener su función necesaria o inevitable, pero la tarea propia del cristiano consistirá en esforzarse, dirá Maritain, por la liquidación histórica del orden actual, entregándose plenamente «por una cooperación generosa, a convertir en más humano y más justo el mundo en que está comprometido»51.

Arizmendiarrieta ha optado por los medios de la edificación, aun reconociendo la necesidad que hay de labor destructora, para proceder a la instauración del orden nuevo. La violencia desatada ha sido para él un motivo más para «profundizar y renovar el espíritu de COOPERACION, como sistema de relación y convivencia humana y social, y antídoto de la violencia y

44

45

Ib. 252.

Ib. 250.

46

Ib.232.

47

Ib. 252.

48

Ib. 255.

49

Ib. 251-252.

50

Ib. 255-256.

51

Ib. 256-257.

732

Violencia

la coacción». Sólo el espíritu de cooperación hará surgir el orden que garantice a la humanidad la convivencia en paz, sin violencias ni coacciones.

En una de sus últimas reflexiones52 le vemos a Arizmendiarrieta centrando su atención nuevamente sobre la cuestión de la violencia. El contexto en que esta viene planteada es el de la bipolarización que padece la sociedad.

«En definitiva, vivimos en una fase en la que las fuerzas más resistentes están constituidas por los que se empeñan en pedir lo imposible sin contar para ello con el tiempo y el espacio, y sobre todo con el esfuerzo indispensable por parte de cada uno para el bien común, y por otra los que a su vez también se empeñan en bloquear y aplazar los cambios y las transformaciones inaplazables e ineludibles: ahí están los dos frentes en los que se alinean, de ordinario, no simplemente los jóvenes y los adultos, los progresistas y los conservadores, sino todos los que no acaban de tomar conciencia, tanto de las posibilidades como de las aspiraciones que no pueden ignorarse». Esta situación favorece, en primer lugar, la proliferación de ideologías y convocatorias confusas, las radicalizaciones irreflexivas, más alborotadoras que eficaces.

Arizmendiarrieta lo ve de este modo: «La inflación más funesta que padecemos consiste en otorgar a simples términos, palabras, una virtualidad que solamente puede manifestarse y aplicarse mediante hechos, obras, realidades.

Hasta la falta de reflexión y de capacidad de análisis y ponderación personal, las enmascaramos echando mano de palabras rimbombantes, sonoras. Nos autoalistamos como revolucionarios sin siquiera haber acometido un pequeño esfuerzo mental y, por supuesto, sin haber arriesgado nada, o al menos sin habernos acreditado como personas capaces de actuar y obrar de tal forma que las palabras estuvieran precedidas de credibilidad, a la vista de los ejemplos de altruismo que hemos sabido dar. Tratamos de disimular nuestra debilidad lanzándonos, y siendo uno más, a simples movimientos masivos, cuyo rumbo y batuta se ignora en manos de quién pueden estar». La sociedad bipolarizada genera también la violencia, que Arizmendiarrieta parece comprender asimismo como una forma más de la inflación general «que invade y contamina todo lo humano y social», herencia recibida más o menos inconscientemente de la «era de totalitarismos y autoritarismos de todos los colores y campos de presencia y actividad humana». Ciertamente en la violencia se manifiesta la inflación social general de muy distinta manera que en la simple verborrea revolucionaria, pero ambas parecen responder, a juicio de Arizmendiarrieta, a «esta necesidad de droga de eficiencia a la corta, de notoriedad alcanzada más por actos heroicos que por una dedicación constante al empeño transformador».

Sin embargo la cuestión de la violencia no es una cuestión tan simple.

Arizmendiarrieta no desconoce en aquella aspectos que, aunque dudosos o discutibles, pueden ser expresión de una actitud positiva de conciencia. En
Escrito titulado «En el camino común mirando adelante», redactado a demanda de Alfonso Gorroñogoitia para la Asamblea General de Ulgor en marzo de 1976. Este escrito no se halla recogido en la colección de obras de Arizmendiarrieta, ni consta en su Archivo. Agradecemos al Sr. Gorroñogoitia la gentileza de habérnoslo dado a conocer.

52

733

El (des)orden establecido

efecto, los cambios se imponen. La resistencia de unos a todo cambio y la faltas de efectividad o la fatiga de otros, que no alcanzan a imponer las transformaciones necesarias, pueden inducir a ciertos sectores impacientes a recurrir a la violencia.

«Alguien ha dicho que “quien dice revolución —y quién se tiene por menos que revolucionario— dice, por definición, acontecimiento nuevo. Quien dice revolución habla de lo que no se puede pensar, ni siquiera prever, sirviéndose de conceptos antiguos. La materia prima y el primer éxito del espíritu de la revolución consiste en la capacidad de innovar; en la palabra cedida a la imaginación creativa”.

Pero lo que vulgarmente entendemos por revolución, actualizada en términos reales, es un esfuerzo de transformación, un esfuerzo mantenido no sin sacrificios y con una visión más amplia de los intereses tanto propios como ajenos.

También ocurre que la fatiga, o incapacidad, para mantenerse en un esfuerzo continuado en línea de transformaciones, induzca a otros a actuaciones que por el propio concepto de transformación no pueden dejarse más o menos a discreción propia.

Aquí ya tenemos a mano otro recurso que podemos llamar violencia, en cuanto que por la misma se especula y hasta se desea, no sin nobleza, pero tal vez sin suficiente conocimiento de causa, contribuir a la promoción de las mejores causas. La efectividad de la violencia es una cuestión que históricamente resulta compleja y que no pocas veces se presenta con una proyección unidimensional, capaz de movilizar espíritus nobles, pero no tanto de poderse conjugar con resultados en cuya ponderación los costos se vieran suficientemente compensados.

(...) A cada cosa hay que llamarle por su propio nombre: a la violencia, en cuanto llega a ciertas modalidades y términos, hay que denominarla una “forma de guerra” y, por tanto, difícilmente es compatible con lo que pudiera aceptarse de respeto a la vida humana, de repudio de la pena de muerte y, por consiguiente, de libertad sin represión ni coacción.

Cierto que el progreso es obra de inconformistas, pero no de cualquiera de las modalidades o formas de inconformismo. Cierto que el amor a la patria debe prevalecer sobre el amor a sí mismo, pero sin echar en olvido que la patria bien entendida no es solamente la tierra de los padres, sino también la de los hijos, y que padres e hijos, generación pasada, presente y futura, constituyen el cauce tanto de las energías vitales como de los valores más transcendentes.

Es comprensible que se busque y se entregue uno a la idea por la que quiere vivir y morir, pero sin echar en olvido que la idea es un recurso múltiple. No menos que orienta y alienta una forma de vivir, en cuanto uno es capaz de vivir según las ideas que profesa, también es cierto que el que no vive como piensa acaba pensando como vive».

Arizmendiarrieta no quiere doblegarse a la alternativa de inmovilismo reaccionario o revolución violenta. Frente a estas dos posiciones, entiende la acción cooperativa como una tercera vía, más humana y no menos eficaz.

«Para que sea viable la convivencia y, consiguientemente, una participación de base amplia y suficiente para conducir a buen término los procesos de desarrollo, hay que salir de la dialéctica del “todo o nada”, tratando de ser más realistas, y jugar a las posibilidades, por el hecho de que hemos de corresponder también a aspiraciones crecientes». «Vamos por una revolución, concluye Arizmendiarrieta, que sea paso hacia adelante, hacia el futuro, y hemos de percatarnos de que nuestro modelo de empresa entraña no pocos elemen734

Realismo

tos que han de caracterizar a la empresa del futuro, sea tanto socialista como neoliberal, que se deseara promover».

6. Realismo
En las precedentes polémicas, en las que no faltan ciertos tonos de dureza,
Arizmendiarrieta ha definido su posición frente a otras tendencias o alternativas que se presentaban como más radicales. Utopía, igualitarismo, violencia:
Arizmendiarrieta no se ha planteado estos temas como cuestiones abstractas o teóricas, sino como problemas que afectaban más o menos directamente (y siempre bajo este aspecto) al movimiento cooperativo. Y ha planteado estas cuestiones, no ante un público general, sino concretamente ante los trabajadores cooperativos, enfrentándose a críticos y agitadores que trataban de intervenir en este medio, poniendo en peligro el futuro de la experiencia.

En todas estas polémicas, Arizmendiarrieta trata ante todo de defender su proyecto. Ante la opción de la revolución violenta (que no entraba directamente en competencia con el cooperativismo, ni aparecía como elemento perturbador en el seno del mismo), Arizmendiarrieta, señalando sus graves peligros, destaca sobre todo que la experiencia cooperativa como tal no se halla en contradicción con aquella, pues, aun supuesto el éxito de la misma, esta experiencia significaría un complemento enriquecedor y necesario (la electricidad necesaria antes y después del triunfo del soviet). Por lo demás Arizmendiarrieta personalmente se define a favor de un proceso pacífico y democrático de transformación social, y cree firmemente en su posibilidad: no es otro el objetivo del movimiento cooperativo. Igualitaristas y utópicos, por el contrario, aparecen como incompatibles y perjudiciales para la experiencia cooperativa.

Arizmendiarrieta, reconociendo compartir sus ideales, les opondrá la ley del realismo.

Con motivo del conflicto de Alecoop, en el curso 1972-1973, escribe Arizmendiarrieta: «Los problemas suelen ser mínimos en torno a los ideales, pero no así en torno a los métodos para llevarlos a cabo. De ahí que existan incompatibilidades entre los denominados Maximalistas y Pragmáticos o Realistas» (EP, II, 244). Que entre ideales y pragmatismo exista cierta tensión es inevitable, incluso conveniente, como Arizmendiarrieta declara repetidas veces. Arizmendiarrieta acepta la tensión y quiere mantenerla: se opone tanto a quienes quisieran superarla tajantemente con maximalismos, como a quienes renuncian a la misma conformándose con la situación dada. Aceptar la tensión como ley de vida y de acción significa rechazar maximalismos, que quieren en el fondo eliminar la tensión. Arizmendiarrieta emplea en esta ocasión un comentario nada galante de Mitterrand: «los maximalistas son sectarios por definición y con frecuencia tontos por naturaleza» (Ib.).

735

El (des)orden establecido

«La alternativa comunitaria que se impone, escribía Arizmendiarrieta ya en 1966, no es la de que todos tengamos que ser Quijotes o Sanchos para poder convivir. Todos estamos obligados a ser un poco de lo uno y de lo otro los cooperativistas, ya que nuestras cooperativas necesitan ganar y no sólo especular. A veces no faltan recomendaciones que quisieran transformarnos a los cooperativistas en los Quijotes de toda clase de aventuras para enderezar también toda clase de entuertos» (FC, II, 247).

No por eso se acepta quedar en mero Sancho. «¿En qué quedarían los proyectos si se encomendara su formulación exclusivamente a los Sanchos? Y
¿qué sería de nosotros si renunciáramos a tales proyectos con el afán inmoderado de disfrutar al día y satisfacer las exigencias humanas a base solamente del estómago? ¿Soportaríamos acaso un desfile exclusivo de Sanchos?» (Ib.).

6.1. Realismo sin renunciar a ideales
«El ideal es hacer el bien que se puede y no el que se sueña», escribía
Arizmendiarrieta ya en 1945. Esta actitud permanecerá invariable. «Apoya los pies en el suelo», dirá (PR, II, 158). «Una vez más recordaremos que una de las formas de resolver los grandes problemas y las graves cuestiones es la de abordar y solucionar cada vez una necesidad» (PR, I, 213).

No necesitamos insistir nuevamente en la necesidad de los ideales. «Ser realistas y pragmáticos no quiere decir renunciar a los ideales, que no deben ser confundidos con quimeras y bellos sueños, sino aceptados como objetivos a realizar» (FC, IV, 207). Firmeza y lealtad a los principios constituyen la base sólida de un proceso que se sabe ha de tropezar con resistencias u oposiciones de todo tipo en la medida que tal proceso entraña transformaciones y cambios reales. La Experiencia Cooperativa, nos dice Arizmendiarrieta, es una empresa llevada a cabo, no sin sacrificios y dificultades, por cuantos, por encima de diferencias de opinión, optaron por participar en la misma, arriesgando desde ganancias y no pocas prebendas u oportunidades de promoción individual, hasta sus familias y ahorros. En estas condiciones se precisa de tanto idealismo como realismo. «Se puede subsistir con vacilaciones, pero no se puede actuar vacilante en un aspecto que carece de popularidad acomodaticia y donde se precisa de fuerza para realizarlo» (Ib.).

Los ideales no deben hacernos olvidar dos extremos inexcusables: el factor tiempo y el respeto al hombre tal como de hecho es.

No se puede construir el futuro sobre hombres ideales, inexistentes, sino partiendo de los hombres concretos que intervienen en la acción concreta que se trata de desarrollar. El cooperativismo, en la línea de la lucha por la libertad y la justicia social, «que en un pasado próximo determinaron una ingente liberación de energías humanas y unos avances de promoción humana en todas las vertientes de actividad, aunque no por ello exenta de algunos

736

Realismo

aspectos negativos, si bien relativos», acomete los mismos objetivos por la vía de la potenciación económica del proletariado. Para ello «acepta al hombre tal como es, con sus defectos e incluso alienaciones, integrándolo en un movimiento que por ser económico no deja de ser educativo, movilizando todos los resortes del hombre e implicándole, desde el primer momento de la integración comunitaria de este con lo que ello representa de responsabilidad y participación integral, en la conducción de su actividad bajo la superior inspiración de valores humanos, éticos o morales» (FC,

III, 95).

Arizmendiarrieta considera toda la existencia humana como un proceso de educación constante, de transformación y humanización más profunda; y, del mismo modo, la experiencia cooperativa será definida como «un esfuerzo económico que se traduce en una acción educativa o (...) un esfuerzo educativo que emplea la acción económica como vehículo de transformación» (Ib.) , idea que Arizmendiarrieta repite en multitud de ocasiones con pequeñas variantes de formulación. «En resumidas cuentas, contamos con el hombre y es el hombre el que trata de realizarse, no teniendo otra alternativa que contar con sus semejantes para superar su insuficiencia e impotencia sin comprometer ni su afectividad ni su racionalidad, a cuyas exigencias responde por un lado la gozosa servidumbre de la solidaridad y la razonable conjunción de esfuerzo y previsión de necesidades»
(Ib.).

Tampoco se puede descuidar el factor tiempo, que, en opinión de Arizmendiarrieta, hoy no es suficientemente ponderado, especialmente por la juventud, teniendo esta laguna mayores repercusiones de lo que ordinariamente quisiéramos reconocer. «El Tiempo es un presupuesto universalmente válido y necesario para todo cambio y transformación que se deseara materializar en el hombre y en los pueblos a efectos de su ulterior desenvolvimiento progresivo y creativo» (CLP, I, 264).

No se trata de cambiar «cosas», sino de ir cambiando hombres, que puedan ir cambiando las cosas, e.d., de materializar hoy cambios que a su vez promuevan cambios ulteriores, iniciando un desarrollo continuado.

«No hay cosecha sin siembra, ni nada se produce por generación espontánea», escribe Arizmendiarrieta (FC, IV, 225). «La idea de generación espontánea se desvaneció por simple y elemental cultivo cultural; pero no de todos los campos ni a todos los efectos» (CLP, I, 264). Muchos siguen soñando en la nueva sociedad, perfecta, sin contar con el esfuerzo que ello exige de cada uno de nosotros y el tiempo que para ello se requiere. «¿No estaremos incurriendo o incursos en una profunda alienación?»
(FC, IV, 226). Todos hablamos de socialismo, de pueblo, de comunidad, pero basta observar en nuestro derredor, o en nosotros mismos, para ver cuánto cuesta aceptar limitaciones propias y sacrificios en aras de la convivencia. «Las ilusiones, por bellas que sean, deben dar paso a las realidades»: debemos partir de que las cosas son lo que son, no lo que quisiéramos

737

El (des)orden establecido

que fuesen. Los ideales, como realidad fecunda y esperanzadora, se deberán situar en el futuro, más que en el presente.

«El futuro y el tiempo constituyen el espacio vital con el que hay que contar. La tarea que nos hemos impuesto en nuestro compromiso societario no se satisface por el hecho de estar resueltos a obrar a lo héroe o santo de una vez, sino manteniéndose fieles y tensos en el correr del tiempo y cara a una multitud de atenciones.

El bienestar no se producirá como maná o simple regalo venido de manos ajenas.

Hemos de estar presentes y participar todos para lograrlo; los cambios y las transformaciones individuales y sociales, que debe llevar aparejadas e, incluso, previas, no se darán sin esfuerzo y sacrificio de todos.

Nuestro proyecto de bienestar ha de ser tal que vaya siempre de menos a más, ya que todo cuanto empieza a manifestarse de más a menos acaba por desvanecerse. Por eso nuestro plan precisa, como todo plan, no estar destinado a embaucar a ingenuos, sino a un compromiso de participación; participar en todo lo que fuere participable o deseable; en la colaboración, en el mando, en el gobierno. Por eso hemos de ser protagonistas siempre y en todo demócratas ahora y luego» (FC, IV, 226-227).

«Avanzar cada vez poco, pero sin cesar», dará como norma Arizmendiarrieta a la Caja Laboral Popular (CLP, I, 133). «Paso a paso y sin pausa» deberán actuar las cooperativas (FC, IV, 228).

La consideración del factor tiempo será algo que Arizmendiarrieta exigirá también a quienes quieran juzgar, desde fuera o desde dentro, el movimiento cooperativo. «Una atención que rogamos tengan las referencias a la experiencia cooperativa, tanto de parte de los curiosos como de los propios protagonistas, es que no se olvide que lo que está en marcha y avanza o cambia no debe ser juzgado simplemente por lo que instantánea o episódicamente pudiera registrarse» (Ib.).

La cooperativa si bien modesta realidad por el momento, comporta realidades de horizontes y de complejidad que la transcienden ampliamente, tanto en el área educativa y de adiestramiento como en el económico y financiero, en cuanto a viabilidad de posibilidades al alcance de hombres, comunidades o pueblos con profundo amor a la libertad, a la justicia y al progreso.

«Alguien dijo que no existe el amor sino pruebas de amor. Otro pudo añadir que tampoco existe el socialismo, sino pruebas de socialismo. Tampoco ha podido callar quien ha necesitado decir que no existe la democracia sino pruebas de democracia; algo así como votaciones y participaciones en votación de todos. Desde luego, siempre nos ha venido a la pluma la expresión cooperación más fluidamente que cooperativismo, ya que este sin aquel, sin algo así como pruebas de cooperación, no es tampoco nada. Estamos en curso de una experiencia comunitaria y las pruebas de comunidad han de poder encarnarse de múltiples formas.

Vamos adelante con la crítica y autocrítica más que con «otro-crítica»; seguimos desmenuzando los elevados y altos propósitos y planes para que todos con mucho o poco podamos compartirlos» (FC, IV, 282).

6.2. Sentido común
«En no pocos momentos y circunstancias en las masas el sentido común es el menos común; queda relegado en términos que el observador sereno

738

Realismo

no puede menos de sorprenderse profundamente», observa Arizmendiarrieta (FC, IV, 87). «Para progresar hay que poder afrontar los problemas y satisfacer las aspiraciones con sentido común. Es decir, progresar significa que seamos sensatos y no demos vacaciones al sentido común en ningún momento» (Ib.).

Además del factor tiempo y del respeto al hombre, un intento de transformación social como el cooperativo debe tomar en consideración todas las leyes que rigen el mundo de la producción, desde el hecho inapelable de la división del trabajo a los mecanismos de oferta y demanda, por muy molestos que resulten al humanista doctrinario. La forma cooperativista no exime a nadie de poseer las herramientas y la capacidad que necesitaría en otro molde de organización.

Los cooperativistas tienen que producir para un mercado: quedan sometidos, por ello, a todas las exigencias de este. Esto significa esencialmente: lograr una calidad, unos costos y unos artículos que satisfagan a los clientes. Y para eso no nos basta que movamos los músculos, hagamos muchas horas, prestemos la máxima atención; necesitamos, dice Arizmendiarrieta, que nuestras máquinas y herramientas de trabajo sean adecuadas, nuestro ritmo de trabajo se acomode a las exigencias de productividad que impera en el mercado, que lleguemos a tener una organización comercial, que dispongamos de reservas de capital adecuadas para proveer nuestros almacenes a tiempo, para ofrecer mercancías a los clientes con la debida amplitud, que tengamos capacidad de espera, etc. En una palabra, no olvidemos que somos, aparte de una familia de compañeros y hermanos en el trabajo, una empresa, con el riesgo y con los medios que tiene que poner en juego una empresa. Tenemos que partir de esta realidad. La realidad nuestra de que a la par que una cooperativa somos una empresa o, mejor dicho, una empresa cooperativa. Hemos de estar a la altura de los mejores, bajo todos los conceptos, sin perder un ápice de nuestro espíritu de hermandad y solidaridad

(CLP, I, 7).

«Tratamos de mejorar una tierra, una comarca, un país concreto, el más directamente relacionado con las condiciones de vida que nos apetece desarrollar. Es a partir de aquí y de lo concreto y real y cabalgando sobre posibilidades y realidades seguir adelante. Lo económico y lo social como el presente y el futuro son inseparables para nosotros» (FC, IV, 152). Se debe cuidar mucho de caer en idealizaciones de una forma de organización o de un movimiento, grupo, por muy entrañable que nos sea: lo que se le debe pedir a una organización que quiera ser apoyo para el hombre no es que sea perfecta e ideal, sino que sea funcional y correcta para marchar, que ofrezca posibilidades y campo de acción a los hombres. «Hay que vivir de realidades. Ni nos hemos encontrado en un paraíso ni hemos podido transformar nada en términos tales que nos pudieran dar la sensación de hallarnos en el paraíso» (Ib.

150). Son los hombres el fundamento de las instituciones, no al revés. «Lo interesante y la clave no son las cooperativas, sino los cooperativistas, como tampoco es la democracia, sino los demócratas. No tanto ideas cuanto viven739

El (des)orden establecido

cias» (Ib. 152). Sería falsificar las realidades humanas pretender que las cooperativas manifestaran caracteres ideales. «Nada más normal que existan tensiones y conflictos en las mismas, unanimidades y discrepancias. Precisamente su misma constitución democrática, como su composición humana indiscriminada, presuponen la necesidad de tener que regular en su marcha las discrepancias por el voto de la mayoría y tener que plegarse en su proceso a diferencias inevitables si no se quiere sacrificar coactivamente la libertad y menguar el alcance de la justicia y de la equidad. Nuestras cooperativas acreditan su madurez como también su solidez por su efectividad endógena, autogestionada, de regular libremente sus problemas o diferencias» (Ib.). «La democracia existe allí donde se aplican sus normas y no precisamente donde no surgen problemas o se marginan para adormecerse con falsos sueños de ideal» (Ib. 151).

«El nuevo orden social que contemplamos los cooperativistas no es realizable más que poco a poco. La comunidad que nos agrada a nosotros de momento les resulta incómoda a otros (...) Tenemos que acatar la realidad presente, si bien con todas nuestras fuerzas, nosotros seguiremos empeñados en modificarla y para eso reservamos y destinaremos todas nuestras fuerzas y recursos» (CLP, I, 126).

6.3. Obras, más que palabras
«Menos triunfalismo y más realismo; menos palabrería y más hechos; menos profetas y más hombres de palabra; menos ilusos y más prácticos. Las buenas ideas son las que se saben traducir en obras y las buenas palabras las que cada uno sabe avalarlas con hechos» (FC, III, 327).

Se podría decir que, en principio, una verdad vale independientemente de quien la proponga. Sin embargo Arizmendiarrieta no parece compartir del todo esta opinión, a juzgar por la energía con que exige que las obras avalen las palabras, o que, quienes propalan doctrinas, muestren con su forma de vida y acciones el valor de las mismas. Tal vez porque «en el diluvio de palabras» actual el expediente de tener que acreditar las palabras con los hechos es el único método para evitar locas aventuras. «El primer factor que ha de acreditar la palabra es la propia vivencia, como toda buena teoría lo es por la realidad. Mientras una buena idea no haya pasado por la prueba del propio testimonio vital cabe que tanto uno mismo como los demás la pongan en tela de juicio» (FC, IV, 50). Lo decisivo, más que las ideas, son los hombres que las encarnan. «Hay siempre quienes especulan cosechar sin sembrar» (FC,

III, 327).

La exigencia de realismo tiene frecuentemente un cariz polémico en Arizmendiarrieta. Las cooperativas, dirá, son realizaciones limitadas. Nuestra inquietud y proyección social no se ciñen a la empresa en la que nos integramos

740

Realismo

y con cuyo desenvolvimiento tratamos de cooperar al bien del pueblo. Nuestra acción está por encima de una simple reforma de las estructuras empresariales y debe afianzarse en la reforma y promoción de las estructuras sociales requeridas por el progreso. El cooperativismo no aporta ya la solución, muestra sólo que la solución es posible por este medio. «El cooperativismo en el que estamos comprometidos es algo más serio y consistente que el que algunos voceros o aventureros hayan podido imaginarse y será bueno que tengamos que seguir afirmándolo así quienes hemos contribuido o estemos contribuyendo con lo mejor de nuestra respectiva capacidad. Con esto no queremos proclamar que es perfecto, pero sí dar a entender que los perfeccionismos consistentes en puras formulaciones no nos deben acomplejar. Y para que pudieran persuadirnos de otras cosas nos agradaría que quienes tuvieren seguridad en sus fórmulas perfectas las pusieran en obra donde realmente realizadas, sin condicionamientos ajenos, pudieran brillar mejor a la vista de los observadores» (Ib. 328).

«No minivaloremos, continúa, el clima de mitos en que estamos inmersos, mitos que hoy sustituyen a otros que en el pasado tuvieron también su momento de desencadenamiento de sicosis colectivas, que hicieron difícil discriminar lo que pudieran entrañar de objetivo y puramente fantasioso arrastrando a tantos tras ellos. Adoptemos todos un método tal de pensar y vivir que las soluciones se acrediten por sí mismas. Es preciso ser antes que hacer y conocer antes de actuar, como diría alguien» (Ib. 329).

Aunque la exigencia de hechos que avalen las doctrinas no sea tal vez generalizable en cualquier caso personal, el campo cooperativo, al que se refiere Arizmendiarrieta en primera instancia, ofrece, sin duda, más campo de aplicación y validez. La exigencia primordialmente moral de que quien predique practique primero no parece del todo ilegítima en este caso, al tratarse de una comunidad relativamente reducida y de características especiales. «El régimen cooperativo ofrece condiciones suficientes para discriminarlas (las verdades a medias, que no hay que tomar por simples medias verdades, sino por verdades mercantilizadas para embaucar ingenuos) o discriminar a sus portavoces; aquí podemos y debemos conocernos por las ideas y por las obras; estamos suficientemente cercanos como para ello, con tal de que no renunciemos al empleo de este doble módulo de ponderación y enjuiciamiento. Las vivencias y la experiencia de cada uno es la credencial que debemos utilizar para las calificaciones que precisáramos. Los hombres a medias, es decir, de los que pudieran hacer bellas formulaciones, pero que no fueran capaces de rubricarlas con hechos, como las verdades a medias, no nos sirven.

Hay que saber dar tiempo al tiempo para conocer quién es incendiario y quién bombero» (FC, IV, 17).

Arizmendiarrieta acusa que no pocos amantes de la máxima justicia e igualitarismo, opuestos a la valoración y diferenciación de los puestos de trabajo, cambian súbitamente de opinión, en cuanto la situación les favorece,
«hasta el punto que en los primeros brotes de penuria de profesionales (y no nos referimos en estos momentos a los más calificados) empiezan a negociar

741

El (des)orden establecido

su propia valía o su rareza a niveles remunerativos que alteran los campos previstos para tales funciones. Y así se da la eterna paradoja del hombre que demanda el máximo rigor en el sometimiento a unos módulos a quienes han estado y están en condiciones de hacer valer su capacidad profesional e incluso criticarles profundamente como clasistas y enemigos del pueblo y héte aquí, que a la menor ocasión repite el juego» (FC, III, 159). «El hombre se repite eternamente», comenta Arizmendiarrieta (Ib. 160).

«Las “buenas ideas” en hombres incapaces de llevarlas a la práctica pueden ser una morfina peligrosa; los hombres se autentifican por sus obras; los proyectos de empresa por las realizaciones; dejemos hablar a los hechos sin pausa de nuestro esfuerzo» (FC, II, 156).

La necesidad de contrastar dichos y hechos viene polémicamente subrayada en el artículo Dialéctica de los hechos, de noviembre de 1970:
«Parecemos estar en determinados momentos a expensas de una diarrea de fórmulas magistrales, capaces de desorientarnos, a una con una proliferación de guerrillas y guerrilleros, casi siempre apelando al empleo de las citadas fórmulas magistrales en exclusiva gestión.

Admitido que la naturaleza del hombre es el artificio, como también admitiendo que necesitamos transformar cuanto se halla en derredor nuestro o cuanto hubiéramos alcanzado por nacimiento o simple herencia, será obvio el que contrastemos la entidad de las buenas ideas como la de las ideologías de turno, con los hechos, con las realidades a las que conduce su aceptación o aplicación, si no queremos incurrir en nuevas alienaciones que pudieran sernos funestas o al menos inapetecibles a la hora de su realización.

Afirmamos que las ideas y las directrices pueden ser buenas, por lo menos muchas de ellas, pero los hechos, la experiencia, no debe ser desdeñada ni minusvalorada, máxime cuando la misma responde a valores que pudieran justificar en todo caso a las primeras. La experiencia es una legitimidad en cuanto la misma corresponde a los imperativos de la conciencia humana madura y compartida, en la medida que aceptáramos, en virtud de la misma, un proceso de progreso de libertad, de justicia en expansión y, por ello, resueltos a aplicarlo en niveles y vertientes sucesivos y apetecidos.

La objetivización de unas aspiraciones, como la desmitificación de otras fórmulas, es tarea a la que no debemos dejar de prestar atención y para ello deberemos requerir más transparencia social, como también más implicación y responsabilidad personal.

Ello será preciso para no dejarse apacentar por gorriones, que no suelen reparar tanto en quién siembra como en qué pueden aprovechar.

Una crisis de gestión democrática no es algo que, a lo sumo, podría registrarse como fallo de un método en un momento dado, cuanto como crisis de valores humanos y sociales, que pudiera pretenderse servir después de todo. Es esta una afirmación grave y graves pueden ser las consecuencias de ello para unos hombres y unas comunidades en marcha, con fidelidad contrastada por hechos, hacia nuevas fronteras de emancipación, de dignidad, de solidaridad en ponderado equilibrio de promoción y desarrollo personal y comunitario, de transformaciones de estructuras, sin perder sensibilidad hacia las infra o superestructuras.

En esta posición no cabe la falta de firmeza, so pena de actuar con soterrada conciencia de culpabilidad, o de complejos infantiles, o con una concepción y visión de justicia social o de libertad no carente de proyecciones inconfesables o incontrolables.

742

Realismo
Si lo realizado es modesto, o aun queda corto, ello será razón para perfeccionarlo, mejorarlo, pero procedamos a ello con la nobleza y transparencia de lo que hemos resuelto llevar a cabo en común, con las servidumbres que ello deberá implicar en todo momento, si no queremos transformarnos en fuerzas que se autodestruyen, si no queremos encaminarnos a objetivos que no se comparten y, por ello, a una dictadura más, cuando se sabe de todas que ninguna sabe ser más que lo que es.

La experiencia de hoy nos acredita los aciertos de ayer, y debemos poder aspirar, a su vez, a que las actitudes actuales fueran mantenibles más adelante, so pena de autodestruir la fuerza creada por una sensibilidad, por una dignidad, por un esfuerzo mantenido para el logro de un progreso continuo de la libertad y de la promoción de la justicia social, que indudablemente requerirá actualizar la conciencia en nuevos niveles y vertientes. Pero hagámoslo consolidando la gestión democrática cuyas reglas hemos adoptado con pleno conocimiento y plena implicación, indesdoblablemente social y económica, que es lo mismo que decir con iniciativa y responsabilidad, que asimismo deben ser parejos.

De momento los hechos, la experiencia, en la que pudiera haber no pocos defectos, nos sigue otorgando, mientras seamos capaces de mantenernos con fidelidad y lealtad a lo que nos indujo, opciones a todos, tanto para remediar los defectos como para seguir superando, acelerada y expansivamente, los objetivos que podemos ir fijando en posiciones compartibles. Y subrayamos lo de posiciones compartidas, o consenso compartido, pues nuestra divisa “Trabajo y Unión” es, en definitiva, resonancia válida de otros ecos interpretados al día y sensatamente: eficientemente hasta el presente. Deben tener ello presente, tanto los agoreros como los presuntos líderes»

(FC, III, 312-314).

6.4. Transformar las realidades
De nada sirve superar las dificultades de la realidad con un proyecto que nos sitúa fuera de ella. Es preciso partir de las realidades y moverse dentro de ellas para transformarlas. «Se apela y se vive de realidades y en ellas más que en ideologías fósiles se inspira la experiencia cuyos partícipes no dejarán en ningún momento de afirmar sus afinidades bajo el epígrafe de trabajo y unión, tratando asimismo de hacer honor a sus exigencias mediante un desarrollo continuo humanizado, es decir, compartido en sus vertientes de servidumbre y de compensación de tasas de inversión y de resultados» (CLP, III,

225).

Tal vez el nombre de cooperativismo no se deje identificar con éxitos fulgurantes en el pasado; acaso tampoco nos ofrezca los encantos de lo inédito o desconocido, susceptible de ser revestido por nuestra imaginación con lo que a cada uno pudiera resultar más apetecible. Para los espectadores ávidos de espectáculos y cansados de realidades monótonas sin duda carecerá de interés. Pero ello no puede bastar para que nos desentendamos de él y no analicemos serenamente su naturaleza íntima como medio de promoción que nuestro pueblo necesita: la lotería o las quinielas, que sí tienen muchos adeptos, no serán los medios de promoción necesarios (FC, III, 61-62).

Frente a los críticos del cooperativismo, Arizmendiarrieta se muestra dispuesto a reconocer que algunas de las deficiencias, más que elementos espú743

El (des)orden establecido

reos, son «concesiones inevitables, indispensables, a la condición objetiva, real, actual del hombre» (Ib. 84).

«Aquí nadie puede permitirse contar sino con el hombre actual para proveerle al hombre de mañana de estructuras nuevas, capaces de mantener sin abdicaciones, si no dispone de una fórmula para transformarle al hombre actual al conjuro de su palabra o de su “hágase” en el hombre nuevo, que tampoco podemos decir que puede ostentar nadie tales facultades. Por el hecho de otorgarle al hombre una confianza a pesar de sus imperfecciones, ya estamos no solamente haciéndole hombre nuevo, sino también obrando como hombres nuevos. Es el caso de efectivamente arrancar con hombres nuevos, si bien tolerando de momento, por no poder hacer otra cosa, estructuras viejas. En todo caso, entre tolerar estructuras o destruir al hombre para compensar nuestra aventura de crear otro orden, evidentemente será mejor que lo que vamos a tener que afirmar o dejar a salvo el día de mañana no lo comprometamos, no lo hipotequemos. ¿Cabe la aventura de dejar atrás las conquistas penosas y difíciles del pasado, desde la conquista y afirmación de la libertad, de la justicia, de la promoción, en aras de un hipotético mañana en el que para que sea “un mañana mejor” habrá que poner por cimiento de lo que tratemos de levantar tales valores y apetencias?» (FC, III, 84).

Cierto que nos hace falta la guía y la inspiración del ideal, vuelve a repetir
Arizmendiarrieta, «pero no hay que confundirlo con una agitación de ideas».

Si un ideal no es realizable incide fácilmente en un romanticismo incapaz de hacer avanzar nada. «¿Cabe acaso renunciar a opciones reales en aras de especulaciones puras sin correr el riesgo de alienaciones funestas? Entre las ilusiones puestas en el futuro y los recuerdos del pasado nos encontramos con un presente en el que tenemos los deberes. Y ¿se puede decir que el deber que nos incumbe es redimir nosotros cada uno a todos, llegar a todas partes, sin medios prácticos para ello?» (Ib. 85).

Arizmendiarrieta describe la misión transformadora social del cooperativismo como la lucha de David contra Goliat. No contamos, dice, con la armadura de Goliat, con el poder económico, pero no por ello nos encogemos, con tal de poder hacer siempre buen uso de nuestra honda. «El día que nos vieran idolatrando a Goliat o renunciando al brazo de David tal vez muchos pudieran tener mucho que replicarnos. Pero mientras no se den en los cooperativistas ni la servidumbre del uno ni la pasividad o indiferencia, sino la actividad y diligencia de David, a esperar toca» (Ib.).

Nos permitimos recordar una vez más, sin extendernos, que, para Arizmendiarrieta, «una de nuestras características (vascas) ha sido el sentido práctico, el de saber actuar en el ámbito de las posibilidades sin indiferencia ni renuncia a los ideales» (CLP, I, 235). La revolución cooperativa se basará en las realidades sociales y en las características del hombre vasco.

7. Revolución cooperativa
Veamos, finalmente, en trazos generales, la solución que frente al desorden económico vigente propone Arizmendiarrieta, contra los conservadores,

744

Revolución cooperativa

por un lado, pero también contra igualitaristas, utópicos o violentos, por el otro. De entrada, una actitud conservadora, cerrada, le parece de todo punto inaceptable. «Nosotros propugnamos, declara Arizmendiarrieta, una racionalización y una disciplina que nos separan tanto de la resignación como de la rebelión incontrolada» (CAS, 225). Veamos pues, la evolución o revolución realista y disciplinada que propone como alternativa.

7.1. Revolución necesaria
Arizmendiarrieta gusta generalmente de datos concretos, cifras, estadísticas. Extrañamente sólo al referirse al tercer mundo ha argumentado Arizmendiarrieta de ese modo, aduciendo cifras de parados, hambrientos, etc. , para fundamentar la necesidad de la revolución ¿Por qué? Aunque en ninguna parte se diga explícitamente, me inclino a pensar que la razón es que la necesidad de la revolución, en Mondragón, venía impuesta a los ojos de Arizmendiarrieta más por la dignidad del hombre ultrajado que por la miseria material; los fines de la revolución que Arizmendiarrieta se proponía, igualmente, iban mucho más allá de la simple superación de la miseria económica.

No es que esta no le alarmara. Mondragón, villa industrial que hacia 1950 contaba, según datos que nos descubre Arizmendiarrieta, con unos dos mil quinientos obreros industriales, distribuídos en más de cuarenta fábricas y talleres (CAS, 146), había conocido años difíciles. Arizmendiarrieta habla con frecuencia de las familias destrozadas por el hambre, de salarios ínfimos, capitalistas enriquecidos con la sangre del pobre, niños sin qué vestirse, etc. El problema de la vivienda ha revestido especial gravedad: en 1941 una encuesta nos revela casi un centenar de viviendas habitadas por dos o tres familias al menos en cada una, sin espacio ni independencia suficiente para la mínima decencia requerida; el problema se agrava en los cinco años siguientes en los que Mondragón conoce una fuerte inmigración, se casan además «270 audaces y heroicas parejas», mientras se construye una escasa cincuentena de viviendas, contadas las particulares y las contruidas por las empresas. «¿Dónde se han metido?, pregunta Arizmendiarrieta. Otra vez también tendrán mala fama las suegras ... » (PR, I, 180). «Es una realidad, añade, la desesperación de muchos jóvenes que van dejando de ser jóvenes y no pueden casarse por carecer de vivienda» (Ib.). Todavía en 1951 no ha podido iniciarse ningún proyecto de construcción de viviendas. «Va pasando el tiempo y se va agotando la paciencia de la gente, que no es para menos viviendo en las condiciones en que viven muchas familias» (Ib. 217).

Con las pésimas condiciones de vivienda surgen los problemas de la tuberculosis: casi un centenar de tuberculosos en diciembre de 1945, «mal alimentados, poco atendidos, apenas aislados» (PR, I, 173), 71 casos declarados controlados en abril de 1946 (SS, II, 288); problema sin solución
«mientras no desaparezcan esos tugurios indecentes y las familias carezcan

745

El (des)orden establecido

del espacio suficiente e independiente en el que quepa un poco de intimidad y un mínimun de satisfacción y condiciones higiénicas» (Ib. 299).

Ante la gravedad del problema Arizmendiarrieta escribe una vez más, como X.X., «un tuberculoso», una carta de lector, en «Aleluya», de la que transcribimos unos párrafos, reveladores de la sensibilidad de su autor:
«Nos es necesaria la resignación, pues sin ella nos hundiríamos en la desesperación. Pero, aunque no me falte para soportar la enfermedad, créanme que resulta poco menos que imposible callar o no indignarse a la vista de la apatía o indiferencia de la gente, que, por otra parte, hace alarde de solidaridad y compañerismo y hasta de caridad. Eso irrita mi sensibilidad hasta el punto de que más de una vez me siento tentado de salir de mi casa y plantarme a la puerta de nuestras fábricas, o de nuestras tabernas, y de nuestros cines y gritar a todo pulmón “farsa, cuento...”, porque a los unos no les falta para toda clase de lujos y aumentan incesantemente sus capitales y los otros también derrochan en vino y diversión. No es que no tengan derecho a disfrutar, o a divertirse, o todo lo ganen injustamente, pero ni lo uno ni lo otro está bien mientras haya necesidades tan imperiosas y otros hermanos nuestros vivan la tragedia que están viviendo, sin que nadie se acuerde de ellos.

Nadie que no haya experimentado sabe lo que es ser víctima de una enfermedad, sin más perspectiva que la miseria y la muerte y sumido en un ambiente de recelo y temor que ni los mismos familiares pueden a veces disimular. Verdaderamente que nuestra existencia no es otra cosa que una agonía lenta y cruel, con toda clase de tormentos físicos y morales. Cuando considero nuestra situación no puedo contener mi dolor y aunque no lo oiga nadie más que los tabiques de mi habitación tengo que exclamarlo: ¿Nadie se va a acordar de nosotros, nadie va a pensar en nuestras pobres familias? ¿Se va a consentir que nadie hable de solidaridad o espíritu social, o todavía se va a tener el atrevimiento de pronunciar el sagrado nombre de caridad? Solidaridad y compañerismo, invocaría yo a la puerta de nuestros establecimientos públicos, de nuestras tabernas y de nuestros cines. Justicia, justicia social, reclamaría a la puerta de nuestras hermosas fábricas, donde hemos trabajado. Caridad, caridad, gemiría en nuestras calles y plazas. El que unos dejen incumplidos sus deberes no justifica la apatía o inacción de los otros» (PR, I, 173-174).

Arizmendiarrieta no era ciertamente insensible a la miseria material. Y, sin embargo, llama la atención que, por los mismos años, Arizmendiarrieta insiste y repite que el hombre no solo vive de pan; destaca las insatisfacciones morales y espirituales del trabajador en la fábrica, incluso más que su insatisfacción económica (CAS, 92 ss.); subraya los valores espirituales, la necesidad de altos ideales; y, precisamente por estos difíciles años, exigirá, no menos que un salario justo, la participación de los trabajadores en la empresa como agentes inteligentes y responsables, no sólo como meros cumplidores de órdenes emanadas de sus superiores. Tratará de promover el espíritu de colaboración a todos los niveles y remarcará que «en resumidas cuentas tenemos que tomar en consideración la norma de labrar la felicidad o el bienestar a pequeñas dosis. A dosis que están al alcance de cada época, de cada día, de cada uno. Es razonable el procedimiento», observando que esta es la única fórmula mágica, no quimérica, de lograr el bienestar individual y social que se apetece (PR, I, 202). El rechazo por partes iguales tanto del conservadurismo como de la revolución violenta aparece ya en 1942 (Ib. 95), así como la necesidad de una transformación continuada: «para ser hombres prácticos y

746

Revolución cooperativa

consecuentes tenemos que poner la mano en la reforma que se puede hacer cada día« (Ib. 106).

Unos veinte años más tarde —1965— serán estos los términos en los que se reconocerá la necesidad de la revolución: «No podemos ocultar nuestra insatisfacción por el orden existente cuando este estado de cosas es incompatible con las exigencias de la justicia, de la libertad, de la verdad. Entendemos que se nos impone a todos los humanos para hacer honor a nuestra dignidad, nobleza u honor, contribuir a la medida de nuestras fuerzas para superar tal estado de cosas. Si por revolución se entiende la colaboración y el apoyo a un empeño común para hacer una comunidad mejor, tenemos que proclamar que somos revolucionarios» (FC, II, 102). Miremos, dirá, a los que hoy y cerca de nosotros padecen insuficiencias de casa, comida, escolaridad para sus hijos, a los que viven con el sueldo mínimo legal. «Levantemos nuestra voz ante las injusticias que se cometen junto a nosotros aunque ello nos acarree una serie de dificultades en nuestras relaciones con los que nos rodean, se nos rían los que no lo comprendan, retrase nuestras posibilidades de promoción en el trabajo, seamos incomprendidos por los mismos por los que hemos levantado la voz. Levantemos la voz ante las injusticias, no ante una galería que puede aplaudirnos, con lo que conseguimos que los resentidos lo estén más, sino ante los que nos pueden poner gesto poco amigable porque lo que escuchan no es de su agrado. Todo ello no por resentimientos sino con la noble y limpia intención de mejorar las estructuras» (FC, IV, 40).

«Pero, añadirá en seguida, en toda acción transformadora hay que pesar lo que se sacrifica y lo que se asegura. En definitiva, la justificación de una revolución dependerá de su auténtica y real contribución a la promoción humana e instauración de un orden mejor. Revolución y promoción no son dos alternativas, sino que la promoción humana y social, su posibilidad o efectividad, tiene que considerarse como criterio y medida de lo que en calidad de revolución se quiera admitir» (FC, II, 102).

Seamos revolucionarios, dice Arizmendiarrieta, pero sin inflación. Porque a menudo se presenta la revolución como algo asequible con un golpe de mano, con un asalto, con una acción violenta; esta revolución no equivale por sí misma a ascenso de sus beneficiarios en los planos profesional, social, económico, como se suele desear y como interesa para poder justificar y compensar otros impactos dudosos de la violencia. Es más, añade: cabe siempre la suplantación de la meta promoción humana por la meta revolución, pasando lo que era simplemente un medio a constituirse en fin en sí mismo, esterilizando así las más elementales aspiraciones humanas (Ib. 103).

«Expresemos con fuerza y calor nuestras dudas acerca de la viabilidad de la revolución por parte de los que hablan mucho de ella pero se preocupan demasiado por los detalles para que su propio bienestar y nivel no sufran menoscabo, quieren tener más, ganar más, gastar más y mandar más. Sepamos, por último, que la vida sencilla en cuanto a manifestaciones externas, comprometidas, entregada a los demás porque realmente nos sentimos alegres y

747

El (des)orden establecido

pequeños, puede ser más eficaz en orden a una auténtica revolución que los panfletos airados, las manifestaciones y las guerrillas» (FC, IV, 41).

La revolución que Arizmendiarrieta quiere y considera necesaria es, en el fondo, la revolución interior, la revolución de las conciencias, en un sentido no lejano al de la conversión religiosa: seamos capaces, escribe, de conformarnos con menos viajes, ropas y aparatosidad, a lo que la mayoría no están dispuestos; sepamos y pongamos en práctica que el dinero no lo es todo para el hombre: servicio, conciencia, religión pueden sernos más íntimamente satisfactorios, etc. (Ib. 40). Es en la fuerza de las conciencias donde Arizmendiarrieta ve la energía verdaderamente transformadora. «No podemos identificar la violencia de la conciencia con la violencia de los métodos o de los medios» (FC, III, 316). Y Arizmendiarrieta no oculta su temor de que esta conciencia falte, no sólo a las masas, sino a los mismos que aparecen como dirigentes revolucionarios. Arizmendiarrieta critica el abuso que hacen del término revolución muchos que lo más que desean, dice, es conservar o lograr una imagen de prestigio, pues buscan tanto la riqueza como el poder, de cualquier modalidad que fuere, para decorarse más que para servir a otros. No tiene nada de particular que todos ellos, a la hora de la verdad, apelen a la participación y a la democracia sin ánimo de contar en la partida, en el curso o al final con resultados o exigencias democráticas de verdad. Es la «miseria de la condición humana». «Cuando se habla tanto del pueblo no olvidemos que pudiera ser, no menos por el hecho de quererlo servir que por el de quererle secuestrar, que se intente identificar su causa con lo que cada uno apetece o lleva a cuestas» (FC, II, 222).

«Cada uno a lo suyo. Esto se sobreentiende en nuestro régimen doméstico cuando se dice: Uno por todos y todos por uno. En cuanto queramos matizarlo cara al exterior y concretamente cara al mundo del trabajo optamos por la reforma y la promoción, que es meta racional, mientras revolución signifique violencia y fuerza ciega, que derrocha sacrificios, impone inmolaciones, no sabe crear valores, se corrompe, se frustra y devora implacablemente a sus gentes. No nos basta con cambiar escenario» (Ib. 105).

7.2. Revolución día a día
«Las revoluciones a golpe instantáneas tienen admiradores y adeptos a pesar de que en el mejor de los casos para que tales éxitos, aun en el supuesto de que tuvieran éxito sin mayores costos, deben tener que empezar por donde otros pudieran estar andando. No hace falta más que ser sensato para saber que lo que se proyecta sin perspectiva, sin objetivo más concreto, sin exigencias de contribuciones efectivas, que normalmente precisarán de quienes sepan obrar con capacidad de compromiso y perseverancia» (FC, IV, 73).

Arizmendiarrieta se opone a un concepto simplista de la revolución que identifica a esta con un golpe instantáneo, dirigido a destituir los poderes domi748

Revolución cooperativa

nantes, olvidando que para la construcción de la nueva sociedad harán falta hombres capacitados que asuman la responsabilidad de construir el orden nuevo, e.d., todo un pueblo concienciado y dispuesto a participar activa y generosamente. Todo ello requiere un proceso largo de educación, requiere tiempo. Los que sueñan en la revolución instantánea olvidan el factor tiempo; del reino del tiempo vuelan en sus sueños al espacio intemporal de las ideas o de los deseos. Arizmendiarrieta les responde con Lenin:
«La revolución, da lo mismo en primera que en segunda instancia, el cambio que se pretendiera hacer efectivo con hombres o para hombres, también vale la alternativa: por mucho que se airee y muchos adeptos que presumiere tener no pasará de escena episódica y artificial, importa poco si cómica o dramática: esa revolución no nos sirve. La vida es un tejido de relaciones entre pasado y presente y el futuro no se construye a partir del vacío: la experiencia, tanto ajena como propia, es enriquecedora, recurso positivo, y no tiene que impedir la evolución si realmente deseamos acelerarla.

A no pocas urgencias y apremios que pudieran entrañar perplejidad en lo que estamos realizando con participación objetiva del pueblo para bien del pueblo, aprovechando el presente al límite, en libertad pero con fidelidad a unos principios y firmeza en las posturas, podemos responder como respondiera en su día Lenin a otros impacientes y unidimensionales: “Después del triunfo del soviet es necesaria la electrificación”. El adiestramiento y la madurez, el equipamiento material y humano son objetivos con sustantividad propia y necesidad permanente.

Sería improcedente que cuantos hemos dado y seguimos dando lo más entrañable, vital, para la promoción humana y social en curso, nos sintiéramos acomplejados ante tanto grito y no poca explosión infantil que en nuestra periferia se diera cita. Los “inmigrantes del tiempo” son relativas las lecciones que nos pueden dar» (CLP, I,

264-265)53.

A veces, confiesa Arizmendiarrieta, es como para que nos sintiéramos acomplejados de ser tildados y tenidos desde de conservadores y reaccionarios hasta de cómplices de los peores enemigos del país, no pocos de los que nos hemos comprometido seriamente con todo lo que hemos tenido, tenemos y hasta podemos tener, en el desarrollo y transformación social y económica del mismo. ¿Por qué? «Acaso por nada más que por poca afición a teatralidad y alarde episódico o por tener muy poca fe en la eficacia de todo ello para cambiar» (FC, IV, 73).

Aun en el supuesto de una revolución instantánea la experiencia cooperativa, experiencia de promoción humana, no dejaría de ser positiva: ella podría aportar los hombres necesarios con espíritu de trabajo y servicio, iniciativa y solidaridad.

Pero, en general, Arizmendiarrieta siente más temor que otra cosa ante la idea de tales revoluciones instantáneas. A los que sienten prisas por hacer la misma, Arizmendiarrieta les replica con una frase de Rosa Luxenburg, «es53
«“Emigrantes del tiempo” es, nos explica Arróyabe, una de las frases que más le oí repetir al analizar actitudes individuales y situaciones colectivas de una cierta impaciencia “irracional”», ARROYABE, S. Mtz. de, Utopías y revolución cultural. Aproximación al pensamiento de D. José María, TU, Nr.

190, nov.-dic. 1976, 46.

749

El (des)orden establecido

crita sin tibieza revolucionaria y con gran sentido práctico»: «No hay nada más perjudicial para la revolución que las ilusiones, ni nada que le sea más útil que la verdad clara y desnuda» (FC, III, 315). La revolución que quiere
Arizmendiarrieta es la realizada día a día: «Una revolución diaria consistente en transformaciones efectivas consolidadas en estructuras nuevas»
(Ib. 63). Esta revolución no es ninguna utopía, ninguna evasión: «es susceptible de un proceso en cadena que puede llegar más allá de lo que pudiéramos imaginarnos» (Ib.).

Nadie puede abogar, dirá a sus cooperativistas, por la insensibilidad o indiferencia frente a la amplia y compleja problemática humana y social de nuestro tiempo y de nuestra periferia; pero sí debemos exigirnos una actuación coherente con nuestros presupuestos mentales y sociales, con nuestros métodos de acción convenidos, con los objetivos comunes adoptados, con las reglas de convivencia y relación; comprometidos y conscientes todos, hemos de caminar unidos, máxime cuando una de esas verdades incuestionables es que la unión hace la fuerza, y que cuestiones complejas y problemas hondos no pueden ser resueltos con fórmulas simples y sin tiempo. Los trabajadores y los cooperativistas hemos de poder acreditarnos como hombres maduros. Y como tales precisamos de más examen, análisis y desmenuzamiento de los problemas, so pena de dejarnos también drogar con ilusiones o, peor aún, ser víctimas de fraudes y maquinaciones extrañas. Por eso hemos de aspirar a poseer y conocer la verdad clara y desnuda, más que embrujarnos con lo que pudiera resultar de momento más grato y fácil

(FC, III, 315-316).

Las diferencias de Arizmendiarrieta con los que, con su terminología, podríamos llamar «revolucionarios instantáneos», no radican en los fines, sino en los medios y en el método. Los fines son humanos, dirá Arizmendiarrieta, pero es preciso humanizar también los medios, para que la revolución sea realizada con la máxima participación posible de los afectados, condición necesaria para que estos resulten ensalzados y ennoblecidos por la misma. Tal es la revolución gradual que se propone el movimiento cooperativo. Porque las transformaciones verdaderamente profundas, durables, no son las conseguidas de un golpe, sino las realizadas golpe a golpe, en cooperación y en libertad. «Egia da aldaketa sakonak ta mamintsuak bear dituguna ekonomi arloetan lanbideak arindu ta narotzeko. Aldaketa saioa bera be gizaberatu bearra daukagu gizakirik geienok parte arturik gure iraultza jasogarria izan dedin. Baña batez be orain eta gero gizakiok gizatsu izateko bear dogun azkatasunik iñoz eta iñork galtzeke, zuzentasun gizagarritan eta alkartasunean izan gadizan. Burruka ta bakoitzkeriagaz gizadirik ez da oberazten. Ez dogu esan nai Kooperatiba-Saio onetan diarduenak erdiz-erdizko aldaketa ta erabarritzeakin konporme (ez?) dagozanik; erabarritzerik guenengoenak lortzeko asmoz mallaka-mallaka ekin dautzoela baño, “idealak” aazteke, obeto edo egokiago jadesteko asmoz sailka diarduela» (CLP, I, 248). Esta revolución no es un mero proyecto, está ya en marcha: «Indudablemente nuestro cooperativismo va arrojando

750

Revolución

cooperativa

resultados valiosos para un pueblo insobornable y secularmente amante de su libertad» (FC, III, 63).

7.3. Revolución de la mayoría
«Amigos del pueblo o, mejor dicho, pueblo en marcha, precisamos para no repudiar nuestro origen y desdecirnos de nuestra conciencia comunitaria, hacer nuestra “revolución” o llevar a cabo nuestro compromiso de luchar por la transformación en proceso de progresiva participación desde la base hacia arriba. Persuadidos, escribe, de que el árbol se renueva, no tanto desde la copa o la corteza, cuanto desde las raíces, apelamos a la iniciativa y responsabilidad de los más; no nos es suficiente que los concientizados, que se diría, afirmen estar al servicio de aquello al tiempo que prescinden de sus intereses y posiciones más o menos sistemáticamente. En la entraña del estado de conciencia prevalente en la comunidad existe mayor potencial de iniciativa y responsabilidad aprovechable que la que tendemos a reconocer. Tenerlos en cuenta es evitar a tiempo contrarevoluciones que de lo contrario pudieran ser inevitables, como de hecho ocurre en nuestros tiempos. Es esa una verdad clara y desnuda que evidencia la experiencia histórica» (Ib. 317).

La experiencia de revoluciones modernas, realizadas en nombre de la libertad y del pueblo, pero que han acabado por crear sociedades con nuevas clases dirigentes y con el pueblo sometido al antiguo estado de minoría de edad, ha tenido un fuerte impacto en el pensamiento de Arizmendiarrieta.

En ningún lugar ha querido citar dichas experiencias revolucionarias por sus nombres, sin duda por no hacer el juego a un «anticomunismo» demasiado fácil y no siempre muy honesto. Pero las alusiones son numerosas. Arizmendiarrieta no quiere simples cambios de escenario. Quiere la revolución de la mayoría para la mayoría. No se concibe la revolución siendo su sujeto una vanguardia minoritaria, la masa su instrumento, la lucha el método; para
Arizmendiarrieta los líderes no tendrán otra función que la de concienciar la masa, único sujeto autorizado de la revolución, realizándose la misma a través de la educación y la cooperación.

Nos hallamos en un punto crucial del pensamiento de Arizmendiarrieta, en el que convergen temas tan diversos como el de la necesidad de líderes que arrastren al pueblo, la educación, la libertad, libertad y estructuras, conciencia y revolución, poder económico y trabajo, etc., temas que hemos tratado ya anteriormente y rogamos al lector tenga una vez más en cuenta. La importancia que da Arizmendiarrieta a los líderes y a su preparación nos causa a veces la impresión de un eco de las tesis leninistas sobre la vanguardia revolucionaria y puede parecer chocante con la exigencia de una revolución de la mayorías. Las masas, nos dice Arizmendiarrieta, no avanzarán sin un lider que vaya por delante. Y, sin embargo, es muy importante comprender bien el papel del lider o guía en la concepción de Arizmendiarrieta. La masa, es

751

El (des)orden establecido

verdad, debe seguir al guía, pero nunca ciegamente. El papel del guía, en la concepción de Arizmendiarrieta, se traduce en el de educador, moldeador de hombres; es un motor; se necesitan hombres con ambición de mando, pero entendiendo mando como servicio y generosidad: en definitiva, no los líderes, sino las instituciones que tendrán vida propia en la medida en que sean reflejo y expresión de la conciencia general, que las sustenta, podrán asegurar el éxito de la revolución. La hegemonía de la burguesía sólo podrá ser suplantada cuando el pueblo trabajador posea su propia cultura, la civilización del trabajo. Es una concepción coherente de un extremo al otro, en la que los diversos aspectos se iluminan y complementan mutuamente.

Nos hemos permitido retornar brevemente a temas ya tratados, porque sólo así podrá resultar comprensible el concepto de revolución de la mayoría y el papel que en la misma le asigna Arizmendiarrieta al movimiento cooperativo. «Es por ello, escribe, por lo que entre nosotros [en las cooperativas] han de poder convivir y complementarse los conservadores y los progresistas, los reformistas y los revolucionarios, el reposo y la inquietud, la paz y la lucha, la aglutinación de fuerzas dispares. En este contexto mental y socio-económico hemos de saber interpretar y aplicar el principio cooperativo de la neutralidad, cuya denominación más exacta sería de “independencia” o “autonomía” al actualizarlo nosotros» (FC, III, 317). Las cooperativas deben ser, pues, revolucionarias, no al modo de una célula de revolucionarios, sino siendo simultáneamente reflejo de la sociedad y escuelas de convivencia y transformación.

«La doctrina vamos elaborando con la experiencia, la especulación tratamos de contrastarla con los hechos, creamos la fuerza con la unión, apoyamos la conciencia con la comunicación diáfana y transparente, no nos gusta caminar por subterráneos en cuanto no carezcamos de valor para afrontar los problemas con firme decisión para superar obstáculos. Calcular las fuerzas, compartir los objetivos y avanzar presupone que somos conocedores de las limitaciones forzosas derivadas de todo ello, y por eso es comprensible que estemos del lado de la evolución permanente, hasta si se quiere acelerada y forzada, pero hasta el límite de hacerla compatible con la convivencia y la servidumbre derivada de valores humanos, por ello éticos, universales y permanentes, cuyo desmontaje o renuncia desnaturaliza y desvirtualiza totalmente la posición humana y social adoptada» (Ib. 316). De ahí la importancia de la democracia en la concepción de Arizmendiarrieta: su revolución quiere ser democrática, no menos que en sus fines, en los medios y en el procedimiento de tranformación. Y de ahí que la fuerza de la violencia se vea suplantada por la fuerza de la conciencia y del trabajo.

«Los cooperativistas tenemos en contra y al mismo tiempo a favor el que nuestra experiencia y esfuerzo de transformación y desarrollo se lleva a efecto sin discriminaciones de credos, de posiciones sociales y económicas, en pleno régimen participativo, libre y democrático, con realizaciones a la vista que encarnan tales presupuestos día por día y se proyectan en el tiempo.

La marcha de personas y comunidades fieles a tales presupuestos no puede ser tan veloz ni tan eficiente como la que pudiera programarse para sujetos y unidades mino-

752

Revolución cooperativa ritanas, homogéneas o selectas y sometidas a disciplinas castrenses. Pero un pueblo que quedara reducido a encuadramientos castrenses o comunidades que presumieran de serio no serían humanas ni comunidades; serían eso, nada más que eso, regimientos y cuarteles. Es como si en nuestro país de súbito nos hubiera entrado tal vocación o no nos pudiéramos librar de tal destino a juzgar por algunas actitudes o posiciones.

Claro que no son mayoritarias ni prevalentes afortunadamente y desde luego al precio de tener que sacrificar ciertos valores reducidos a contados sujetos, para cuya calificación ulterior sería bueno apelar a siquiatras» (FC, IV, 74).

7.4. Revolución realista
«La verdad del movimiento cooperativo en nuestra región es desarrollo progresivo y acelerado, promesa y esperanza de un futuro mejor. Las verdades del mensaje cooperativista sólo descartan los mitos de la revolución y no tanto esta, en cuanto que parten de que la transformación más radical y amplia de los presupuestos estructurales y humanos de nuestra sociedad es indispensable. Con todo, recalcaremos que, dado que las ideas no son nada sin los hombres, ya que son estos los que dan vida a aquellas, si nuestras ideas llegaran a separarnos de nuestros hermanos volveremos a buscar la reconciliación con ellos. Esta es la actitud que impone la divisa adoptada: Trabajo y
Unión» (CLP, I, 220).

Arizmendiarrieta no comprende algunas posturas radicales que, al parecer, han causado no pocas dificultades (1970) en el movimiento cooperativo.

«Las luchas no nos han encogido por lo que pudieran tener de exigencia, de esfuerzo mantenido, aunque sí de destrucción irreparable, de paz inasequible» (FC, III, 232). «Ciertas impaciencias que desearíamos podernos explicar o disculpar implican tales matices que realmente no logramos. De no sospechar profundas y ocultas conciencias de culpabilidad no sabemos explicarnos la insensata impaciencia de algunos y, menos aún, sin recurrir a raros complejos de inferioridad, que habían de requerir las providencias de psiquiatras, la obsesión de otros por simples signos externos, más formales que reales y efectivos, para su identificación como hombres de pro. Tal vez se dé de todo, desde embrujos mágicos a complejos de culpabilidad, de inferioridad, y sólo nos falte Hombría, es decir, Madurez» (Ib. 333).

Los trabajadores, para no conformarse con puras formulaciones o realizaciones fragdamentarias e incoherentes, de verdadera emancipación social y económica, de efectivas y profundas transformaciones de las estructuras y organizaciones de trabajo, seguidas de otras en todo el ámbito de la economía, necesitan ser dueños de su trabajo, poder valerse por sí mismos (CLP, I,
222-223). «¡Trabajadores, uníos!, tiene valor si añadimos, “trabajadores, acceded al poder más vigoroso que es hoy vuestro trabajo y vuestra propia gestión, indispensable para que no incurráis en la alienación que seguirá despontenciándoos; vosotros tenéis que poder gobernaros y administraros; la

753

El (des)orden establecido

cooperación, llamada siempre a eliminar por la acción directa y la responsabilidad neta las intermediaciones extrañas, os convoca a todos a eso!”» (FC,

III, 99-100).

7.5. Revolución moral y económica
J.M. Mendizabal ha encontrado entre los papeles de Arizmendiarrieta unos apuntes sobre tradición y revolución, de los que dice no estar seguro que sean de él. Nos ha ofrecido un pequeño apartado de los mismos, que, en mi opinión, refleja claramente el pensamiento y hasta el estilo de Arizmendiarrieta hacia 1945. Expresan más ampliamente la idea de que la revolución más radical que haya conocido la historia es la de Jesús de Nazaret, idea que nos es conocida por otros escritos de Arizmendiarrieta (SS, I, 109). Queda bien claro el sentido ascético de la cruz, como medio de paz interior, libertad y servicio al prójimo. Transcribimos el texto íntegro porque, de ser original, como nos atrevemos a suponer, sería la primera reflexión de Arizmendiarrieta sobre la revolución:
«No obstante, esta tradición siempre tiene que ser una revolución, porque su estructura misma es negar los valores y las formas a los que la pasión humana tan poderosamente está adherida. A los que aman el dinero, el placer, la fama y el poder, esta tradición les dice: “Sed pobres, bajad a los fondos de la sociedad, tomad el último lugar entre los hombres, compartid la miseria de los desposeídos, amad a los demás y servidlos en vez de haceros servir por ellos. No los resistáis cuando os persiguen; rogad, empero, por los que os maldicen. No busquéis el placer, sino que, desentendiéndoos de las cosas que agradan a los sentidos y a la mente, buscad a Dios en el hambre, la sed y la oscuridad, a través de los cuales pareciera locura. Tomad sobre vosotros la carga de la cruz de Cristo, que es la humildad, la pobreza, la obediencia y la renunciación de Cristo y encontraréis paz para vuestras almas”.

Esta es la revolución más completa que jamás haya sido predicada. De hecho es la única verdadera revolución, porque todas las demás piden el exterminio de algún otro, pero esta exige la muerte de ese hombre que, en la estimación práctica de la vida, has llegado a creer que eres tú.

Una revolución se entiende que es un cambio que dé vuelta completa a todo. Pero la ideología de una revolución política nunca cambiará nada más que las apariencias.

Arderá la violencia y el poder pasará de un partido a otro, pero cuando se desvanece el humo y los cuerpos de los muertos estén todos enterrados, la situación será la misma que antes; habrá una minoría de fuertes en el poder, explotando a los demás para su provecho. Reinará la misma codicia, la misma crueldad, la misma lujuria, la misma ambición, la misma hipocresía y avaricia que antes.

Porque las revoluciones de los hombres no cambian nada. La única influencia que puede eliminar la injusticia y la iniquidad de los hombres es la fuerza que alienta la tradición cristiana, renovando nuestra participación en la vida que es la Luz de los hombres» (CLP, I, 10-11).

Nos parece importante el texto (que refleja todavía la actitud negativa hacia los partidos y hacia la política, tan marcada en el pensamiento de Ariz754

Revolución cooperativa

mendiarrieta de aquella época), no sólo por ser un documento, al parecer, muy temprano, sino porque expresa muy bien la dimensión moral, básicamente cristiana, de su concepto de revolución. Con los años cambiará el lenguaje, pero las ideas básicas se mantendrán firmes.

Los que, insatisfechos con el orden vigente, escribía Arizmendiarrieta en
1968, aspiran a construir un orden nuevo, han de poder coincidir al menos en la necesidad de reflexionar sobre la naturaleza y la jerarquía de los valores que desean inspiren y constituyan dicho orden. Se acepta cada día con mayor unanimidad, dice Arizmendiarrieta, que la base del bienestar y de la convivencia humana, más que recursos económicos, la constituyen bienes inmateriales. Cara al porvenir importan más que la disponibilidad de recursos materiales las opciones de libertad, justicia, promoción y desarrollo. La dignidad cuenta más que la despensa. «(...) El cooperativismo, afirmando su exigencia básica e irrenunciable, que consiste en adoptar para la conducción y administración de la actividad humana los valores superiores, su inspiración y consiguiente servidumbre, tiene plena actualidad en el ámbito de la organización socio-económica» (FC, III, 81).

«Naturalmente, sigue Arizmendiarrieta, milita por la Evolución de máximo rango, que es lo mismo que decir —según el léxico de otros— por la Revolución radical, pero que no puede por ello dispensar de mantener a salvo lo que es irrenunciable e inhipotecable, la libertad y la dignidad de los hombres de hoy y de mañana, de un color u otro, de una condición social o económica distinta» (Ib.).

No basta que se acepte en principio una escala de valores en la que la libertad, la dignidad, la justicia y la solidaridad estén por encima de los bienes materiales. Es preciso que cada uno aprenda a sacrificar sus intereses individuales en aras de la solidaridad, esté más dispuesto a dar que a recibir. «La previsión y la solidaridad presuponen que nada se hace o se crea por generación espontánea y el hombre debe tener la persuasión de que no le aprovecha menos lo que sabe dar que lo que recibe; se progresa en la medida que esta reciprocidad se encarna en hechos. Es la revolución que se precisa; no es exactamente la que pudiéramos soñar. No identifiquemos progreso con tener simples ventajas, con el esfuerzo mínimo y episódico, compatible con la tendencia por parte de cada uno a colaboraciones mínimas. Vivimos de realidades y no de fábulas o de mitos» (FC, IV, 43). Lo contrario sería confundir crecimiento (cuantitativo) con desarrollo (cualitativo). No hay, pues, revolución mejor que hacer, ni anterior, que la que todos tenemos a nuestro alcance y a nuestra cuenta «bajo nuestras boinas» (FC, II, 109).

«La revolución, en la medida que sabe acreditarse como proceso de transformación compartido por todos o los más, como mantenido para bien ponderado y calificado por los más, de hecho es el buen camino de un mundo en el que nada es acabado y perfecto y sin embargo todo mejorable. El mundo estabilizado no es mundo humano, dado que el hombre es sujeto con ansias de superación y por ello con no menos vocación y destino de activo que de especulativo. No hay lugar en el mundo mudable para soluciones perfectas y definitivas, lo cual constituye el mejor aliciente para que

755

El (des)orden establecido el hombre ejerza su superioridad sobre el mundo que le rodea, precisamente accionando sobre él y acreditando al mismo tiempo sus valores superiores en la medida que se mantiene activo, no solamente en cultivar y fecundar la naturaleza, sino en acomodarla también en su conjunto a las necesidades y conveniencias del hombre o de los hombres.

Por ello lleva consigo la necesidad de: a) participar en la tarea del trabajo, b) participar en la tarea de promover la organización y aplicación idónea de lo generado o disponible para provecho común.

A esto llamamos participación los cooperativistas, que hemos comenzado por instituir la aplicación de las opciones de trabajo; nos hemos afirmado trabajadores y solidarios para actuar sobre la naturaleza transformándola y generando en el seno de la misma nuevas utilidades mediante el esfuerzo compartido, pero no deberá contenerse o quedar bloqueada nuestra acción en ello en tanto que dichos frutos y utilidades no tuvieran vías óptimas de distribución y aplicación» (FC, IV, 142-143).

«Estamos totalmente de acuerdo, escribe Arizmendiarrieta en un artículo titulado Nuestra revolución, de 1966, con la formulación revolucionaria del clarividente pensador cristiano Mounier. La renovación económica será moral o no será. La revolución moral será económica o no será» (FC, II, 246).

«La liberación y el progreso que busca el cooperativista tienen que tener consignación en códigos humanos, pero sobre todo ha de tener vigencia y respaldo en valores económicos. Si descuidamos la constitución de patrimonios comunitarios o personales, ambos debidamente acompasados, será difícil que tarde o temprano podamos rehuir ciertas servidumbres no menos molestas por ser sutiles. Por mucho que nos repugne no podremos evitar la existencia de ciudadanos de diversa categoría o clases sociales opuestas.

Tampoco nos basta con cualquier revolución. La revolución que pudiera tener una mañana confortable para transformarse en vida de más o menos servidumbre, como acontece con todas las que arrancan de la violencia y no saben salir de la misma. Nuestra revolución en tanto será satisfactoria en cuanto fuere moral, entendiendo por tal el acatamiento de los valores superiores. Nuestras leyes de juego están previamente establecidas para cuando se proclama la revolución y para cuando se trata de afianzarla.

Forzosamente ha de ser económica puesto que no nos consideramos seres inmateriales o extratemporales. Queremos la revolución para los hombres de cuerpo y alma, para la vida que nos toca vivir con sus limitaciones y grandezas, para la calle y la casa, para hoy y mañana. Por eso coincidimos también con el aludido pensador cuando dice: “Una revolución se hace con las necesidades de las cosas tanto como con la generosidad de los hombres. Es decir, que todas las coyunturas son buenas con tal que tratemos de que todas las circunstancias sirvan para avanzar. Todos los hombres son interesantes con tal que todos traten de aportar lo que a su saber y entender lealmente estimen que es contribución positiva. Aprovechemos la libertad y también la misma opresión si se diera, para ir adelante, por el camino de solidaridad que hemos escogido, siempre como hombres. El hombre que se da o rehusa darse, pero que no se presta jamás, como ha dicho otro comentarista”» (Ib. 246-247).

Arizmendiarrieta llegará a hablar de la identidad de revolución social y revolución económica, para que cualquiera de ellas sea satisfactoria. La ineficiencia en cualquiera de las dos vertientes, dirá en relación a las cooperativas, bastará para descalificarlas y hacerlas aparecer como un subproducto so756

Revolución cooperativa

cial (FC, III, 267). La revolución cooperativa parte de los hombres de hoy, tal como son hoy, para transformarlos; parte de las realidades económicas que encuentra dadas, para transformarlas, a partir de la empresa y para llegar a todo el orden económico. «Precisamos de la revolución basada en el trabajo y no en los mitos; hemos de conseguir la unión, apoyada en la verdad y nunca en la mentira, la hipocresía y el error. A las corrientes de “la sociedad de consumo que consume” que pudiera drogarnos con un simple bienestar material y en cuyo tablero el hombre se cotiza como cosa y no como persona, responde el movimiento cooperativo, entre nosotros, convocándonos y ayudándonos a participar y actuar como personas. Y como tales, poniendo en juego nuestra iniciativa y responsabilidad, nuestra capacidad creativa a partir de la primera célula u organismo creativo y laboral, es decir, la empresa. Así podremos desencadenar una nueva actitud transformadora de la economía y generar un nuevo orden socio-económico, coherente con la dignidad del hombre y las exigencias de las comunidades humanas» (CLP, I, 223).

757

Otalora, Centro de Formación Cooperativa y Directiva.

